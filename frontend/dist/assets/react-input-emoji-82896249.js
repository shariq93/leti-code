import{r as _,R as j,e as Yt}from"./vendor-c3223a9e.js";import{P as ce}from"./@fortawesome/react-fontawesome-66b2a191.js";function Jt(e,t){t===void 0&&(t={});var n=t.insertAt;if(!(!e||typeof document>"u")){var r=document.head||document.getElementsByTagName("head")[0],o=document.createElement("style");o.type="text/css",n==="top"&&r.firstChild?r.insertBefore(o,r.firstChild):r.appendChild(o),o.styleSheet?o.styleSheet.cssText=e:o.appendChild(document.createTextNode(e))}}var Zt=`.react-input-emoji--container {
  color: #4b4b4b;
  text-rendering: optimizeLegibility;
  background-color: #fff;
  border: 1px solid #fff;
  border-radius: 21px;
  margin: 5px 10px;
  box-sizing: border-box;
  flex: 1 1 auto;
  font-size: 15px;
  font-family: sans-serif;
  font-weight: 400;
  line-height: 20px;
  min-height: 20px;
  min-width: 0;
  outline: none;
  width: inherit;
  will-change: width;
  vertical-align: baseline;
  border: 1px solid #eaeaea;
  margin-right: 0;
}

.react-input-emoji--wrapper {
  display: flex;
  overflow: hidden;
  flex: 1;
  position: relative;
  padding-right: 0;
  vertical-align: baseline;
  outline: none;
  margin: 0;
  padding: 0;
  border: 0;
}

.react-input-emoji--input {
  font-weight: 400;
  max-height: 100px;
  min-height: 20px;
  outline: none;
  overflow-x: hidden;
  overflow-y: auto;
  position: relative;
  white-space: pre-wrap;
  word-wrap: break-word;
  z-index: 1;
  width: 100%;
  user-select: text;
  padding: 9px 12px 11px;
  text-align: left;
}

.react-input-emoji--input img {
  vertical-align: middle;
  width: 18px !important;
  height: 18px !important;
  display: inline !important;
  margin-left: 1px;
  margin-right: 1px;
}

.react-input-emoji--overlay {
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: 9;
}

.react-input-emoji--placeholder {
  color: #a0a0a0;
  pointer-events: none;
  position: absolute;
  user-select: none;
  z-index: 2;
  left: 16px;
  top: 0;
  bottom: 0;
  display: flex;
  align-items: center;
  width: calc(100% - 22px);
}

.react-input-emoji--button {
  position: relative;
  display: block;
  text-align: center;
  padding: 0 10px;
  overflow: hidden;
  transition: color 0.1s ease-out;
  margin: 0;
  box-shadow: none;
  background: none;
  border: none;
  outline: none;
  cursor: pointer;
  flex-shrink: 0;
}

.react-input-emoji--button svg {
  fill: #858585;
}

.react-input-emoji--button__show svg {
  fill: #128b7e;
}

.react-emoji {
  display: flex;
  align-items: center;
  position: relative;
  width: 100%;
}

.react-emoji-picker--container {
  position: absolute;
  top: 0;
  width: 100%;
}

.react-emoji-picker--wrapper {
  position: absolute;
  bottom: 0;
  right: 0;
  height: 435px;
  width: 352px;
  overflow: hidden;
  z-index: 10;
}

.react-emoji-picker {
  position: absolute;
  top: 0;
  left: 0;
  animation: slidein 0.1s ease-in-out;
}

.react-emoji-picker__show {
  top: 0;
}

.react-input-emoji--mention--container {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  z-index: 10;
}

.react-input-emoji--mention--list {
  background-color: #fafafa;
  border: 1px solid #eaeaea;
  border-radius: 4px;
  margin: 0;
  padding: 0;
  list-style: none;
  display: flex;
  gap: 5px;
  flex-direction: column;
  position: absolute;
  bottom: 0;
  width: 100%;
  left: 0;
}

.react-input-emoji--mention--item {
  display: flex;
  align-items: center;
  gap: 10px;
  padding: 5px 10px;
  background-color: transparent;
  width: 100%;
  margin: 0;
  border: 0;
}

.react-input-emoji--mention--item__selected {
  background-color: #eeeeee;
}

.react-input-emoji--mention--item--img {
  width: 34px;
  height: 34px;
  border-radius: 50%;
}

.react-input-emoji--mention--item--name {
  font-size: 16px;
  color: #333;
}

.react-input-emoji--mention--item--name__selected {
  color: green;
}

.react-input-emoji--mention--text {
  color: #039be5;
}

.react-input-emoji--mention--loading {
  background-color: #fafafa;
  border: 1px solid #eaeaea;
  border-radius: 4px;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 10px 0;
  position: absolute;
  bottom: 0;
  left: 0;
  width: 100%;
}

.react-input-emoji--mention--loading--spinner,
.react-input-emoji--mention--loading--spinner::after {
  border-radius: 50%;
  width: 10em;
  height: 10em;
}

.react-input-emoji--mention--loading--spinner {
  margin: 1px auto;
  font-size: 2px;
  position: relative;
  text-indent: -9999em;
  border-top: 1.1em solid rgba(0, 0, 0, 0.1);
  border-right: 1.1em solid rgba(0, 0, 0, 0.1);
  border-bottom: 1.1em solid rgba(0, 0, 0, 0.1);
  border-left: 1.1em solid rgba(0, 0, 0, 0.4);
  transform: translateZ(0);
  animation: load8 1.1s infinite linear;
}

@keyframes load8 {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}

@keyframes slidein {
  from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
}
`;Jt(Zt);function He(e,t){var n=Object.keys(e);if(Object.getOwnPropertySymbols){var r=Object.getOwnPropertySymbols(e);t&&(r=r.filter(function(o){return Object.getOwnPropertyDescriptor(e,o).enumerable})),n.push.apply(n,r)}return n}function Ie(e){for(var t=1;t<arguments.length;t++){var n=arguments[t]!=null?arguments[t]:{};t%2?He(Object(n),!0).forEach(function(r){Qt(e,r,n[r])}):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(n)):He(Object(n)).forEach(function(r){Object.defineProperty(e,r,Object.getOwnPropertyDescriptor(n,r))})}return e}function le(){/*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */le=function(){return e};var e={},t=Object.prototype,n=t.hasOwnProperty,r=typeof Symbol=="function"?Symbol:{},o=r.iterator||"@@iterator",i=r.asyncIterator||"@@asyncIterator",s=r.toStringTag||"@@toStringTag";function l(m,h,y){return Object.defineProperty(m,h,{value:y,enumerable:!0,configurable:!0,writable:!0}),m[h]}try{l({},"")}catch{l=function(h,y,S){return h[y]=S}}function c(m,h,y,S){var k=h&&h.prototype instanceof g?h:g,T=Object.create(k.prototype),R=new N(S||[]);return T._invoke=function(B,V,P){var D="suspendedStart";return function(F,Q){if(D==="executing")throw new Error("Generator is already running");if(D==="completed"){if(F==="throw")throw Q;return H()}for(P.method=F,P.arg=Q;;){var de=P.delegate;if(de){var ne=C(de,P);if(ne){if(ne===a)continue;return ne}}if(P.method==="next")P.sent=P._sent=P.arg;else if(P.method==="throw"){if(D==="suspendedStart")throw D="completed",P.arg;P.dispatchException(P.arg)}else P.method==="return"&&P.abrupt("return",P.arg);D="executing";var ee=u(B,V,P);if(ee.type==="normal"){if(D=P.done?"completed":"suspendedYield",ee.arg===a)continue;return{value:ee.arg,done:P.done}}ee.type==="throw"&&(D="completed",P.method="throw",P.arg=ee.arg)}}}(m,y,R),T}function u(m,h,y){try{return{type:"normal",arg:m.call(h,y)}}catch(S){return{type:"throw",arg:S}}}e.wrap=c;var a={};function g(){}function f(){}function v(){}var p={};l(p,o,function(){return this});var b=Object.getPrototypeOf,w=b&&b(b(se([])));w&&w!==t&&n.call(w,o)&&(p=w);var $=v.prototype=g.prototype=Object.create(p);function M(m){["next","throw","return"].forEach(function(h){l(m,h,function(y){return this._invoke(h,y)})})}function E(m,h){function y(k,T,R,B){var V=u(m[k],m,T);if(V.type!=="throw"){var P=V.arg,D=P.value;return D&&typeof D=="object"&&n.call(D,"__await")?h.resolve(D.__await).then(function(F){y("next",F,R,B)},function(F){y("throw",F,R,B)}):h.resolve(D).then(function(F){P.value=F,R(P)},function(F){return y("throw",F,R,B)})}B(V.arg)}var S;this._invoke=function(k,T){function R(){return new h(function(B,V){y(k,T,B,V)})}return S=S?S.then(R,R):R()}}function C(m,h){var y=m.iterator[h.method];if(y===void 0){if(h.delegate=null,h.method==="throw"){if(m.iterator.return&&(h.method="return",h.arg=void 0,C(m,h),h.method==="throw"))return a;h.method="throw",h.arg=new TypeError("The iterator does not provide a 'throw' method")}return a}var S=u(y,m.iterator,h.arg);if(S.type==="throw")return h.method="throw",h.arg=S.arg,h.delegate=null,a;var k=S.arg;return k?k.done?(h[m.resultName]=k.value,h.next=m.nextLoc,h.method!=="return"&&(h.method="next",h.arg=void 0),h.delegate=null,a):k:(h.method="throw",h.arg=new TypeError("iterator result is not an object"),h.delegate=null,a)}function z(m){var h={tryLoc:m[0]};1 in m&&(h.catchLoc=m[1]),2 in m&&(h.finallyLoc=m[2],h.afterLoc=m[3]),this.tryEntries.push(h)}function U(m){var h=m.completion||{};h.type="normal",delete h.arg,m.completion=h}function N(m){this.tryEntries=[{tryLoc:"root"}],m.forEach(z,this),this.reset(!0)}function se(m){if(m){var h=m[o];if(h)return h.call(m);if(typeof m.next=="function")return m;if(!isNaN(m.length)){var y=-1,S=function k(){for(;++y<m.length;)if(n.call(m,y))return k.value=m[y],k.done=!1,k;return k.value=void 0,k.done=!0,k};return S.next=S}}return{next:H}}function H(){return{value:void 0,done:!0}}return f.prototype=v,l($,"constructor",v),l(v,"constructor",f),f.displayName=l(v,s,"GeneratorFunction"),e.isGeneratorFunction=function(m){var h=typeof m=="function"&&m.constructor;return!!h&&(h===f||(h.displayName||h.name)==="GeneratorFunction")},e.mark=function(m){return Object.setPrototypeOf?Object.setPrototypeOf(m,v):(m.__proto__=v,l(m,s,"GeneratorFunction")),m.prototype=Object.create($),m},e.awrap=function(m){return{__await:m}},M(E.prototype),l(E.prototype,i,function(){return this}),e.AsyncIterator=E,e.async=function(m,h,y,S,k){k===void 0&&(k=Promise);var T=new E(c(m,h,y,S),k);return e.isGeneratorFunction(h)?T:T.next().then(function(R){return R.done?R.value:T.next()})},M($),l($,s,"Generator"),l($,o,function(){return this}),l($,"toString",function(){return"[object Generator]"}),e.keys=function(m){var h=[];for(var y in m)h.push(y);return h.reverse(),function S(){for(;h.length;){var k=h.pop();if(k in m)return S.value=k,S.done=!1,S}return S.done=!0,S}},e.values=se,N.prototype={constructor:N,reset:function(m){if(this.prev=0,this.next=0,this.sent=this._sent=void 0,this.done=!1,this.delegate=null,this.method="next",this.arg=void 0,this.tryEntries.forEach(U),!m)for(var h in this)h.charAt(0)==="t"&&n.call(this,h)&&!isNaN(+h.slice(1))&&(this[h]=void 0)},stop:function(){this.done=!0;var m=this.tryEntries[0].completion;if(m.type==="throw")throw m.arg;return this.rval},dispatchException:function(m){if(this.done)throw m;var h=this;function y(V,P){return T.type="throw",T.arg=m,h.next=V,P&&(h.method="next",h.arg=void 0),!!P}for(var S=this.tryEntries.length-1;S>=0;--S){var k=this.tryEntries[S],T=k.completion;if(k.tryLoc==="root")return y("end");if(k.tryLoc<=this.prev){var R=n.call(k,"catchLoc"),B=n.call(k,"finallyLoc");if(R&&B){if(this.prev<k.catchLoc)return y(k.catchLoc,!0);if(this.prev<k.finallyLoc)return y(k.finallyLoc)}else if(R){if(this.prev<k.catchLoc)return y(k.catchLoc,!0)}else{if(!B)throw new Error("try statement without catch or finally");if(this.prev<k.finallyLoc)return y(k.finallyLoc)}}}},abrupt:function(m,h){for(var y=this.tryEntries.length-1;y>=0;--y){var S=this.tryEntries[y];if(S.tryLoc<=this.prev&&n.call(S,"finallyLoc")&&this.prev<S.finallyLoc){var k=S;break}}k&&(m==="break"||m==="continue")&&k.tryLoc<=h&&h<=k.finallyLoc&&(k=null);var T=k?k.completion:{};return T.type=m,T.arg=h,k?(this.method="next",this.next=k.finallyLoc,a):this.complete(T)},complete:function(m,h){if(m.type==="throw")throw m.arg;return m.type==="break"||m.type==="continue"?this.next=m.arg:m.type==="return"?(this.rval=this.arg=m.arg,this.method="return",this.next="end"):m.type==="normal"&&h&&(this.next=h),a},finish:function(m){for(var h=this.tryEntries.length-1;h>=0;--h){var y=this.tryEntries[h];if(y.finallyLoc===m)return this.complete(y.completion,y.afterLoc),U(y),a}},catch:function(m){for(var h=this.tryEntries.length-1;h>=0;--h){var y=this.tryEntries[h];if(y.tryLoc===m){var S=y.completion;if(S.type==="throw"){var k=S.arg;U(y)}return k}}throw new Error("illegal catch attempt")},delegateYield:function(m,h,y){return this.delegate={iterator:se(m),resultName:h,nextLoc:y},this.method==="next"&&(this.arg=void 0),a}},e}function Be(e,t,n,r,o,i,s){try{var l=e[i](s),c=l.value}catch(u){n(u);return}l.done?t(c):Promise.resolve(c).then(r,o)}function Fe(e){return function(){var t=this,n=arguments;return new Promise(function(r,o){var i=e.apply(t,n);function s(c){Be(i,r,o,s,l,"next",c)}function l(c){Be(i,r,o,s,l,"throw",c)}s(void 0)})}}function Qt(e,t,n){return t in e?Object.defineProperty(e,t,{value:n,enumerable:!0,configurable:!0,writable:!0}):e[t]=n,e}function en(e,t){if(e==null)return{};var n={},r=Object.keys(e),o,i;for(i=0;i<r.length;i++)o=r[i],!(t.indexOf(o)>=0)&&(n[o]=e[o]);return n}function tn(e,t){if(e==null)return{};var n=en(e,t),r,o;if(Object.getOwnPropertySymbols){var i=Object.getOwnPropertySymbols(e);for(o=0;o<i.length;o++)r=i[o],!(t.indexOf(r)>=0)&&Object.prototype.propertyIsEnumerable.call(e,r)&&(n[r]=e[r])}return n}function J(e,t){return rn(e)||an(e,t)||mt(e,t)||cn()}function pt(e){return nn(e)||on(e)||mt(e)||sn()}function nn(e){if(Array.isArray(e))return je(e)}function rn(e){if(Array.isArray(e))return e}function on(e){if(typeof Symbol<"u"&&e[Symbol.iterator]!=null||e["@@iterator"]!=null)return Array.from(e)}function an(e,t){var n=e==null?null:typeof Symbol<"u"&&e[Symbol.iterator]||e["@@iterator"];if(n!=null){var r=[],o=!0,i=!1,s,l;try{for(n=n.call(e);!(o=(s=n.next()).done)&&(r.push(s.value),!(t&&r.length===t));o=!0);}catch(c){i=!0,l=c}finally{try{!o&&n.return!=null&&n.return()}finally{if(i)throw l}}return r}}function mt(e,t){if(e){if(typeof e=="string")return je(e,t);var n=Object.prototype.toString.call(e).slice(8,-1);if(n==="Object"&&e.constructor&&(n=e.constructor.name),n==="Map"||n==="Set")return Array.from(e);if(n==="Arguments"||/^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n))return je(e,t)}}function je(e,t){(t==null||t>e.length)&&(t=e.length);for(var n=0,r=new Array(t);n<t;n++)r[n]=e[n];return r}function sn(){throw new TypeError(`Invalid attempt to spread non-iterable instance.
In order to be iterable, non-array objects must have a [Symbol.iterator]() method.`)}function cn(){throw new TypeError(`Invalid attempt to destructure non-iterable instance.
In order to be iterable, non-array objects must have a [Symbol.iterator]() method.`)}var ln="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";function vt(e){var t=un(e);return t&&(t=pt(new Set(t)),t.forEach(function(n){e=gt(e,n,bt(n))})),e}function gt(e,t,n){return e.replace(new RegExp(t,"g"),n)}function un(e){return e.match(/(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff])[\ufe0e\ufe0f]?(?:[\u0300-\u036f\ufe20-\ufe23\u20d0-\u20f0]|\ud83c[\udffb-\udfff])?(?:\u200d(?:[^\ud800-\udfff]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff])[\ufe0e\ufe0f]?(?:[\u0300-\u036f\ufe20-\ufe23\u20d0-\u20f0]|\ud83c[\udffb-\udfff])?)*/g)}function dn(e){var t,n=document.querySelector("em-emoji-picker");if(!n)return Ue(e.native);var r=n==null||(t=n.shadowRoot)===null||t===void 0?void 0:t.querySelector('[title="'.concat(e.name,'"] > span > span'));if(!r)return Ue(e.native);var o=gt(r.style.cssText,'"',"'");return bt(o,e.native)}function bt(e,t){return'<img style="'.concat(e,'; display: inline-block" data-emoji="').concat(t,'" src="').concat(ln,'" />')}function Ue(e){return'<span class="width: 18px; height: 18px; display: inline-block; margin: 0 1px;">'.concat(e,"</span>")}function fn(e){var t=document.createElement("div");t.innerHTML=e;var n=Array.prototype.slice.call(t.querySelectorAll("img"));return n.forEach(function(r){t.innerHTML=t.innerHTML.replace(r.outerHTML,r.dataset.emoji)}),t.innerHTML}function hn(e){var t,n;if(window.getSelection){if(t=window.getSelection(),t===null)return;if(t.getRangeAt&&t.rangeCount){n=t.getRangeAt(0),n.deleteContents();var r=document.createElement("div");r.innerHTML=e;for(var o=document.createDocumentFragment(),i,s;i=r.firstChild;)s=o.appendChild(i);n.insertNode(o),s&&(n=n.cloneRange(),n.setStartAfter(s),n.collapse(!0),t.removeAllRanges(),t.addRange(n))}}}function Ne(e){var t=e.text,n=e.html,r=t.length,o=(n.match(/<img/g)||[]).length;return r+o}function _t(){var e=_.useRef([]),t=_.useRef(""),n=_.useCallback(function(o){e.current.push(o)},[]),r=_.useCallback(function(o){var i=e.current.reduce(function(s,l){return l(s)},o);return i=pn(i),t.current=i,i},[]);return{addSanitizeFn:n,sanitize:r,sanitizedTextRef:t}}function pn(e){var t=document.createElement("div");t.innerHTML=e;var n=t.innerText||"";return n=n.replace(/\n/gi,""),n}function mn(e){var t=e.ref,n=e.textInputRef,r=e.setValue,o=e.emitChange,i=_t(),s=i.sanitize,l=i.sanitizedTextRef;_.useImperativeHandle(t,function(){return{get value(){return l.current},set value(c){r(c)},focus:function(){n.current!==null&&n.current.focus()},blur:function(){n.current!==null&&s(n.current.html),o()}}})}function vn(e,t,n){var r=_.useRef(null),o=_.useRef(n),i=_.useCallback(function(){if(e.current!==null){var l=r.current,c=e.current.size;(!l||l.width!==c.width||l.height!==c.height)&&typeof t=="function"&&t(c),r.current=c}},[t,e]),s=_.useCallback(function(l){typeof o.current=="function"&&o.current(l),typeof t=="function"&&i()},[i,t]);return _.useEffect(function(){e.current&&i()},[i,e]),s}var gn=["placeholder","style","tabIndex","className","onChange"],bn=function(t,n){var r=t.placeholder,o=t.style,i=t.tabIndex,s=t.className,l=t.onChange,c=tn(t,gn);_.useImperativeHandle(n,function(){return{appendContent:function(b){a.current&&a.current.focus(),hn(b),a.current&&a.current.focus(),a.current&&u.current&&a.current.innerHTML.trim()===""?u.current.style.visibility="visible":u.current&&(u.current.style.visibility="hidden"),a.current&&typeof l=="function"&&l(a.current.innerHTML)},set html(p){a.current&&(a.current.innerHTML=p),u.current&&(p.trim()===""?u.current.style.visibility="visible":u.current.style.visibility="hidden"),typeof l=="function"&&a.current&&l(a.current.innerHTML)},get html(){return a.current?a.current.innerHTML:""},get text(){return a.current?a.current.innerText:""},get size(){return a.current?{width:a.current.offsetWidth,height:a.current.offsetHeight}:{width:0,height:0}},focus:function(){a.current&&a.current.focus()}}});var u=_.useRef(null),a=_.useRef(null);function g(p){p.key==="Enter"?c.onEnter(p):p.key==="ArrowUp"?c.onArrowUp(p):p.key==="ArrowDown"?c.onArrowDown(p):p.key.length===1&&u.current&&(u.current.style.visibility="hidden"),c.onKeyDown(p)}function f(){c.onFocus()}function v(p){c.onKeyUp(p);var b=a.current;if(u.current){var w;(b==null||(w=b.innerText)===null||w===void 0?void 0:w.trim())===""?u.current.style.visibility="visible":u.current.style.visibility="hidden"}typeof l=="function"&&a.current&&l(a.current.innerHTML)}return j.createElement("div",{className:"react-input-emoji--container",style:o},j.createElement("div",{className:"react-input-emoji--wrapper",onClick:f},j.createElement("div",{ref:u,className:"react-input-emoji--placeholder"},r),j.createElement("div",{ref:a,onKeyDown:g,onKeyUp:v,tabIndex:i,contentEditable:!0,className:"react-input-emoji--input".concat(s?" ".concat(s):""),onBlur:c.onBlur,onCopy:c.onCopy,onPaste:c.onPaste,"data-testid":"react-input-emoji--input"})))},_n=_.forwardRef(bn);function Ve(e){var t=e.showPicker,n=e.toggleShowPicker,r=e.buttonElement,o=_.useRef(null),i=_.useState(!1),s=J(i,2),l=s[0],c=s[1];return _.useEffect(function(){var u,a;((u=r==null||(a=r.childNodes)===null||a===void 0?void 0:a.length)!==null&&u!==void 0?u:0)>2&&(o.current.appendChild(r==null?void 0:r.childNodes[0]),c(!0))},[r==null?void 0:r.childNodes]),j.createElement("button",{ref:o,type:"button",className:"react-input-emoji--button".concat(t?" react-input-emoji--button__show":""),onClick:n},!l&&j.createElement("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 24 24",width:"24",height:"24",className:"react-input-emoji--button--icon"},j.createElement("path",{d:"M12 0C5.373 0 0 5.373 0 12s5.373 12 12 12 12-5.373 12-12S18.627 0 12 0m0 22C6.486 22 2 17.514 2 12S6.486 2 12 2s10 4.486 10 10-4.486 10-10 10"}),j.createElement("path",{d:"M8 7a2 2 0 1 0-.001 3.999A2 2 0 0 0 8 7M16 7a2 2 0 1 0-.001 3.999A2 2 0 0 0 16 7M15.232 15c-.693 1.195-1.87 2-3.349 2-1.477 0-2.655-.805-3.347-2H15m3-2H6a6 6 0 1 0 12 0"})))}function yt(e){return e&&e.__esModule?e.default:e}function W(e,t,n){return t in e?Object.defineProperty(e,t,{value:n,enumerable:!0,configurable:!0,writable:!0}):e[t]=n,e}var ke,x,wt,ue,kt,We,ve={},xt=[],yn=/acit|ex(?:s|g|n|p|$)|rph|grid|ows|mnc|ntw|ine[ch]|zoo|^ord|itera/i;function Y(e,t){for(var n in t)e[n]=t[n];return e}function $t(e){var t=e.parentNode;t&&t.removeChild(e)}function Le(e,t,n){var r,o,i,s={};for(i in t)i=="key"?r=t[i]:i=="ref"?o=t[i]:s[i]=t[i];if(arguments.length>2&&(s.children=arguments.length>3?ke.call(arguments,2):n),typeof e=="function"&&e.defaultProps!=null)for(i in e.defaultProps)s[i]===void 0&&(s[i]=e.defaultProps[i]);return pe(e,s,r,o,null)}function pe(e,t,n,r,o){var i={type:e,props:t,key:n,ref:r,__k:null,__:null,__b:0,__e:null,__d:void 0,__c:null,__h:null,constructor:void 0,__v:o??++wt};return o==null&&x.vnode!=null&&x.vnode(i),i}function G(){return{current:null}}function oe(e){return e.children}function K(e,t){this.props=e,this.context=t}function ae(e,t){if(t==null)return e.__?ae(e.__,e.__.__k.indexOf(e)+1):null;for(var n;t<e.__k.length;t++)if((n=e.__k[t])!=null&&n.__e!=null)return n.__e;return typeof e.type=="function"?ae(e):null}function Ct(e){var t,n;if((e=e.__)!=null&&e.__c!=null){for(e.__e=e.__c.base=null,t=0;t<e.__k.length;t++)if((n=e.__k[t])!=null&&n.__e!=null){e.__e=e.__c.base=n.__e;break}return Ct(e)}}function qe(e){(!e.__d&&(e.__d=!0)&&ue.push(e)&&!ge.__r++||We!==x.debounceRendering)&&((We=x.debounceRendering)||kt)(ge)}function ge(){for(var e;ge.__r=ue.length;)e=ue.sort(function(t,n){return t.__v.__b-n.__v.__b}),ue=[],e.some(function(t){var n,r,o,i,s,l;t.__d&&(s=(i=(n=t).__v).__e,(l=n.__P)&&(r=[],(o=Y({},i)).__v=i.__v+1,De(l,i,o,n.__n,l.ownerSVGElement!==void 0,i.__h!=null?[s]:null,r,s??ae(i),i.__h),Lt(r,i),i.__e!=s&&Ct(i)))})}function St(e,t,n,r,o,i,s,l,c,u){var a,g,f,v,p,b,w,$=r&&r.__k||xt,M=$.length;for(n.__k=[],a=0;a<t.length;a++)if((v=n.__k[a]=(v=t[a])==null||typeof v=="boolean"?null:typeof v=="string"||typeof v=="number"||typeof v=="bigint"?pe(null,v,null,null,v):Array.isArray(v)?pe(oe,{children:v},null,null,null):v.__b>0?pe(v.type,v.props,v.key,null,v.__v):v)!=null){if(v.__=n,v.__b=n.__b+1,(f=$[a])===null||f&&v.key==f.key&&v.type===f.type)$[a]=void 0;else for(g=0;g<M;g++){if((f=$[g])&&v.key==f.key&&v.type===f.type){$[g]=void 0;break}f=null}De(e,v,f=f||ve,o,i,s,l,c,u),p=v.__e,(g=v.ref)&&f.ref!=g&&(w||(w=[]),f.ref&&w.push(f.ref,null,v),w.push(g,v.__c||p,v)),p!=null?(b==null&&(b=p),typeof v.type=="function"&&v.__k===f.__k?v.__d=c=Et(v,c,e):c=jt(e,v,f,$,p,c),typeof n.type=="function"&&(n.__d=c)):c&&f.__e==c&&c.parentNode!=e&&(c=ae(f))}for(n.__e=b,a=M;a--;)$[a]!=null&&(typeof n.type=="function"&&$[a].__e!=null&&$[a].__e==n.__d&&(n.__d=ae(r,a+1)),zt($[a],$[a]));if(w)for(a=0;a<w.length;a++)Mt(w[a],w[++a],w[++a])}function Et(e,t,n){for(var r,o=e.__k,i=0;o&&i<o.length;i++)(r=o[i])&&(r.__=e,t=typeof r.type=="function"?Et(r,t,n):jt(n,r,r,o,r.__e,t));return t}function be(e,t){return t=t||[],e==null||typeof e=="boolean"||(Array.isArray(e)?e.some(function(n){be(n,t)}):t.push(e)),t}function jt(e,t,n,r,o,i){var s,l,c;if(t.__d!==void 0)s=t.__d,t.__d=void 0;else if(n==null||o!=i||o.parentNode==null)e:if(i==null||i.parentNode!==e)e.appendChild(o),s=null;else{for(l=i,c=0;(l=l.nextSibling)&&c<r.length;c+=2)if(l==o)break e;e.insertBefore(o,i),s=i}return s!==void 0?s:o.nextSibling}function wn(e,t,n,r,o){var i;for(i in n)i==="children"||i==="key"||i in t||_e(e,i,null,n[i],r);for(i in t)o&&typeof t[i]!="function"||i==="children"||i==="key"||i==="value"||i==="checked"||n[i]===t[i]||_e(e,i,t[i],n[i],r)}function Ke(e,t,n){t[0]==="-"?e.setProperty(t,n):e[t]=n==null?"":typeof n!="number"||yn.test(t)?n:n+"px"}function _e(e,t,n,r,o){var i;e:if(t==="style")if(typeof n=="string")e.style.cssText=n;else{if(typeof r=="string"&&(e.style.cssText=r=""),r)for(t in r)n&&t in n||Ke(e.style,t,"");if(n)for(t in n)r&&n[t]===r[t]||Ke(e.style,t,n[t])}else if(t[0]==="o"&&t[1]==="n")i=t!==(t=t.replace(/Capture$/,"")),t=t.toLowerCase()in e?t.toLowerCase().slice(2):t.slice(2),e.l||(e.l={}),e.l[t+i]=n,n?r||e.addEventListener(t,i?Xe:Ge,i):e.removeEventListener(t,i?Xe:Ge,i);else if(t!=="dangerouslySetInnerHTML"){if(o)t=t.replace(/xlink[H:h]/,"h").replace(/sName$/,"s");else if(t!=="href"&&t!=="list"&&t!=="form"&&t!=="tabIndex"&&t!=="download"&&t in e)try{e[t]=n??"";break e}catch{}typeof n=="function"||(n!=null&&(n!==!1||t[0]==="a"&&t[1]==="r")?e.setAttribute(t,n):e.removeAttribute(t))}}function Ge(e){this.l[e.type+!1](x.event?x.event(e):e)}function Xe(e){this.l[e.type+!0](x.event?x.event(e):e)}function De(e,t,n,r,o,i,s,l,c){var u,a,g,f,v,p,b,w,$,M,E,C=t.type;if(t.constructor!==void 0)return null;n.__h!=null&&(c=n.__h,l=t.__e=n.__e,t.__h=null,i=[l]),(u=x.__b)&&u(t);try{e:if(typeof C=="function"){if(w=t.props,$=(u=C.contextType)&&r[u.__c],M=u?$?$.props.value:u.__:r,n.__c?b=(a=t.__c=n.__c).__=a.__E:("prototype"in C&&C.prototype.render?t.__c=a=new C(w,M):(t.__c=a=new K(w,M),a.constructor=C,a.render=xn),$&&$.sub(a),a.props=w,a.state||(a.state={}),a.context=M,a.__n=r,g=a.__d=!0,a.__h=[]),a.__s==null&&(a.__s=a.state),C.getDerivedStateFromProps!=null&&(a.__s==a.state&&(a.__s=Y({},a.__s)),Y(a.__s,C.getDerivedStateFromProps(w,a.__s))),f=a.props,v=a.state,g)C.getDerivedStateFromProps==null&&a.componentWillMount!=null&&a.componentWillMount(),a.componentDidMount!=null&&a.__h.push(a.componentDidMount);else{if(C.getDerivedStateFromProps==null&&w!==f&&a.componentWillReceiveProps!=null&&a.componentWillReceiveProps(w,M),!a.__e&&a.shouldComponentUpdate!=null&&a.shouldComponentUpdate(w,a.__s,M)===!1||t.__v===n.__v){a.props=w,a.state=a.__s,t.__v!==n.__v&&(a.__d=!1),a.__v=t,t.__e=n.__e,t.__k=n.__k,t.__k.forEach(function(z){z&&(z.__=t)}),a.__h.length&&s.push(a);break e}a.componentWillUpdate!=null&&a.componentWillUpdate(w,a.__s,M),a.componentDidUpdate!=null&&a.__h.push(function(){a.componentDidUpdate(f,v,p)})}a.context=M,a.props=w,a.state=a.__s,(u=x.__r)&&u(t),a.__d=!1,a.__v=t,a.__P=e,u=a.render(a.props,a.state,a.context),a.state=a.__s,a.getChildContext!=null&&(r=Y(Y({},r),a.getChildContext())),g||a.getSnapshotBeforeUpdate==null||(p=a.getSnapshotBeforeUpdate(f,v)),E=u!=null&&u.type===oe&&u.key==null?u.props.children:u,St(e,Array.isArray(E)?E:[E],t,n,r,o,i,s,l,c),a.base=t.__e,t.__h=null,a.__h.length&&s.push(a),b&&(a.__E=a.__=null),a.__e=!1}else i==null&&t.__v===n.__v?(t.__k=n.__k,t.__e=n.__e):t.__e=kn(n.__e,t,n,r,o,i,s,c);(u=x.diffed)&&u(t)}catch(z){t.__v=null,(c||i!=null)&&(t.__e=l,t.__h=!!c,i[i.indexOf(l)]=null),x.__e(z,t,n)}}function Lt(e,t){x.__c&&x.__c(t,e),e.some(function(n){try{e=n.__h,n.__h=[],e.some(function(r){r.call(n)})}catch(r){x.__e(r,n.__v)}})}function kn(e,t,n,r,o,i,s,l){var c,u,a,g=n.props,f=t.props,v=t.type,p=0;if(v==="svg"&&(o=!0),i!=null){for(;p<i.length;p++)if((c=i[p])&&"setAttribute"in c==!!v&&(v?c.localName===v:c.nodeType===3)){e=c,i[p]=null;break}}if(e==null){if(v===null)return document.createTextNode(f);e=o?document.createElementNS("http://www.w3.org/2000/svg",v):document.createElement(v,f.is&&f),i=null,l=!1}if(v===null)g===f||l&&e.data===f||(e.data=f);else{if(i=i&&ke.call(e.childNodes),u=(g=n.props||ve).dangerouslySetInnerHTML,a=f.dangerouslySetInnerHTML,!l){if(i!=null)for(g={},p=0;p<e.attributes.length;p++)g[e.attributes[p].name]=e.attributes[p].value;(a||u)&&(a&&(u&&a.__html==u.__html||a.__html===e.innerHTML)||(e.innerHTML=a&&a.__html||""))}if(wn(e,f,g,o,l),a)t.__k=[];else if(p=t.props.children,St(e,Array.isArray(p)?p:[p],t,n,r,o&&v!=="foreignObject",i,s,i?i[0]:n.__k&&ae(n,0),l),i!=null)for(p=i.length;p--;)i[p]!=null&&$t(i[p]);l||("value"in f&&(p=f.value)!==void 0&&(p!==g.value||p!==e.value||v==="progress"&&!p)&&_e(e,"value",p,g.value,!1),"checked"in f&&(p=f.checked)!==void 0&&p!==e.checked&&_e(e,"checked",p,g.checked,!1))}return e}function Mt(e,t,n){try{typeof e=="function"?e(t):e.current=t}catch(r){x.__e(r,n)}}function zt(e,t,n){var r,o;if(x.unmount&&x.unmount(e),(r=e.ref)&&(r.current&&r.current!==e.__e||Mt(r,null,t)),(r=e.__c)!=null){if(r.componentWillUnmount)try{r.componentWillUnmount()}catch(i){x.__e(i,t)}r.base=r.__P=null}if(r=e.__k)for(o=0;o<r.length;o++)r[o]&&zt(r[o],t,typeof e.type!="function");n||e.__e==null||$t(e.__e),e.__e=e.__d=void 0}function xn(e,t,n){return this.constructor(e,n)}function Pt(e,t,n){var r,o,i;x.__&&x.__(e,t),o=(r=typeof n=="function")?null:n&&n.__k||t.__k,i=[],De(t,e=(!r&&n||t).__k=Le(oe,null,[e]),o||ve,ve,t.ownerSVGElement!==void 0,!r&&n?[n]:o?null:t.firstChild?ke.call(t.childNodes):null,i,!r&&n?n:o?o.__e:t.firstChild,r),Lt(i,e)}ke=xt.slice,x={__e:function(e,t){for(var n,r,o;t=t.__;)if((n=t.__c)&&!n.__)try{if((r=n.constructor)&&r.getDerivedStateFromError!=null&&(n.setState(r.getDerivedStateFromError(e)),o=n.__d),n.componentDidCatch!=null&&(n.componentDidCatch(e),o=n.__d),o)return n.__E=n}catch(i){e=i}throw e}},wt=0,K.prototype.setState=function(e,t){var n;n=this.__s!=null&&this.__s!==this.state?this.__s:this.__s=Y({},this.state),typeof e=="function"&&(e=e(Y({},n),this.props)),e&&Y(n,e),e!=null&&this.__v&&(t&&this.__h.push(t),qe(this))},K.prototype.forceUpdate=function(e){this.__v&&(this.__e=!0,e&&this.__h.push(e),qe(this))},K.prototype.render=oe,ue=[],kt=typeof Promise=="function"?Promise.prototype.then.bind(Promise.resolve()):setTimeout,ge.__r=0;var $n=0;function d(e,t,n,r,o){var i,s,l={};for(s in t)s=="ref"?i=t[s]:l[s]=t[s];var c={type:e,props:l,key:n,ref:i,__k:null,__:null,__b:0,__e:null,__d:void 0,__c:null,__h:null,constructor:void 0,__v:--$n,__source:r,__self:o};if(typeof e=="function"&&(i=e.defaultProps))for(s in i)l[s]===void 0&&(l[s]=i[s]);return x.vnode&&x.vnode(c),c}function Cn(e,t){try{window.localStorage[`emoji-mart.${e}`]=JSON.stringify(t)}catch{}}function Sn(e){try{const t=window.localStorage[`emoji-mart.${e}`];if(t)return JSON.parse(t)}catch{}}var Z={set:Cn,get:Sn};const $e=new Map,En=[{v:14,emoji:"🫠"},{v:13.1,emoji:"😶‍🌫️"},{v:13,emoji:"🥸"},{v:12.1,emoji:"🧑‍🦰"},{v:12,emoji:"🥱"},{v:11,emoji:"🥰"},{v:5,emoji:"🤩"},{v:4,emoji:"👱‍♀️"},{v:3,emoji:"🤣"},{v:2,emoji:"👋🏻"},{v:1,emoji:"🙃"}];function jn(){for(const{v:e,emoji:t}of En)if(Rt(t))return e}function Ln(){return!Rt("🇨🇦")}function Rt(e){if($e.has(e))return $e.get(e);const t=Mn(e);return $e.set(e,t),t}const Mn=(()=>{let e=null;try{navigator.userAgent.includes("jsdom")||(e=document.createElement("canvas").getContext("2d",{willReadFrequently:!0}))}catch{}if(!e)return()=>!1;const t=25,n=20,r=Math.floor(t/2);return e.font=r+"px Arial, Sans-Serif",e.textBaseline="top",e.canvas.width=n*2,e.canvas.height=t,o=>{e.clearRect(0,0,n*2,t),e.fillStyle="#FF0000",e.fillText(o,0,22),e.fillStyle="#0000FF",e.fillText(o,n,22);const i=e.getImageData(0,0,n,t).data,s=i.length;let l=0;for(;l<s&&!i[l+3];l+=4);if(l>=s)return!1;const c=n+l/4%n,u=Math.floor(l/4/n),a=e.getImageData(c,u,1,1).data;return!(i[l]!==a[0]||i[l+2]!==a[2]||e.measureText(o).width>=n)}})();var Ye={latestVersion:jn,noCountryFlags:Ln};const Me=["+1","grinning","kissing_heart","heart_eyes","laughing","stuck_out_tongue_winking_eye","sweat_smile","joy","scream","disappointed","unamused","weary","sob","sunglasses","heart"];let O=null;function zn(e){O||(O=Z.get("frequently")||{});const t=e.id||e;t&&(O[t]||(O[t]=0),O[t]+=1,Z.set("last",t),Z.set("frequently",O))}function Pn({maxFrequentRows:e,perLine:t}){if(!e)return[];O||(O=Z.get("frequently"));let n=[];if(!O){O={};for(let i in Me.slice(0,t)){const s=Me[i];O[s]=t-i,n.push(s)}return n}const r=e*t,o=Z.get("last");for(let i in O)n.push(i);if(n.sort((i,s)=>{const l=O[s],c=O[i];return l==c?i.localeCompare(s):l-c}),n.length>r){const i=n.slice(r);n=n.slice(0,r);for(let s of i)s!=o&&delete O[s];o&&n.indexOf(o)==-1&&(delete O[n[n.length-1]],n.splice(-1,1,o)),Z.set("frequently",O)}return n}var Tt={add:zn,get:Pn,DEFAULTS:Me},At={};At=JSON.parse('{"search":"Search","search_no_results_1":"Oh no!","search_no_results_2":"That emoji couldn’t be found","pick":"Pick an emoji…","add_custom":"Add custom emoji","categories":{"activity":"Activity","custom":"Custom","flags":"Flags","foods":"Food & Drink","frequent":"Frequently used","nature":"Animals & Nature","objects":"Objects","people":"Smileys & People","places":"Travel & Places","search":"Search Results","symbols":"Symbols"},"skins":{"1":"Default","2":"Light","3":"Medium-Light","4":"Medium","5":"Medium-Dark","6":"Dark","choose":"Choose default skin tone"}}');var X={autoFocus:{value:!1},dynamicWidth:{value:!1},emojiButtonColors:{value:null},emojiButtonRadius:{value:"100%"},emojiButtonSize:{value:36},emojiSize:{value:24},emojiVersion:{value:14,choices:[1,2,3,4,5,11,12,12.1,13,13.1,14]},exceptEmojis:{value:[]},icons:{value:"auto",choices:["auto","outline","solid"]},locale:{value:"en",choices:["en","ar","be","cs","de","es","fa","fi","fr","hi","it","ja","kr","nl","pl","pt","ru","sa","tr","uk","vi","zh"]},maxFrequentRows:{value:4},navPosition:{value:"top",choices:["top","bottom","none"]},noCountryFlags:{value:!1},noResultsEmoji:{value:null},perLine:{value:9},previewEmoji:{value:null},previewPosition:{value:"bottom",choices:["top","bottom","none"]},searchPosition:{value:"sticky",choices:["sticky","static","none"]},set:{value:"native",choices:["native","apple","facebook","google","twitter"]},skin:{value:1,choices:[1,2,3,4,5,6]},skinTonePosition:{value:"preview",choices:["preview","search","none"]},theme:{value:"auto",choices:["auto","light","dark"]},categories:null,categoryIcons:null,custom:null,data:null,i18n:null,getImageURL:null,getSpritesheetURL:null,onAddCustomEmoji:null,onClickOutside:null,onEmojiSelect:null,stickySearch:{deprecated:!0,value:!0}};let I=null,L=null;const Ce={};async function Je(e){if(Ce[e])return Ce[e];const n=await(await fetch(e)).json();return Ce[e]=n,n}let Se=null,Dt=null,Ot=!1;function xe(e,{caller:t}={}){return Se||(Se=new Promise(n=>{Dt=n})),e?Rn(e):t&&!Ot&&console.warn(`\`${t}\` requires data to be initialized first. Promise will be pending until \`init\` is called.`),Se}async function Rn(e){Ot=!0;let{emojiVersion:t,set:n,locale:r}=e;if(t||(t=X.emojiVersion.value),n||(n=X.set.value),r||(r=X.locale.value),L)L.categories=L.categories.filter(c=>!c.name);else{L=(typeof e.data=="function"?await e.data():e.data)||await Je(`https://cdn.jsdelivr.net/npm/@emoji-mart/data@latest/sets/${t}/${n}.json`),L.emoticons={},L.natives={},L.categories.unshift({id:"frequent",emojis:[]});for(const c in L.aliases){const u=L.aliases[c],a=L.emojis[u];a&&(a.aliases||(a.aliases=[]),a.aliases.push(c))}L.originalCategories=L.categories}if(I=(typeof e.i18n=="function"?await e.i18n():e.i18n)||(r=="en"?yt(At):await Je(`https://cdn.jsdelivr.net/npm/@emoji-mart/data@latest/i18n/${r}.json`)),e.custom)for(let c in e.custom){c=parseInt(c);const u=e.custom[c],a=e.custom[c-1];if(!(!u.emojis||!u.emojis.length)){u.id||(u.id=`custom_${c+1}`),u.name||(u.name=I.categories.custom),a&&!u.icon&&(u.target=a.target||a),L.categories.push(u);for(const g of u.emojis)L.emojis[g.id]=g}}e.categories&&(L.categories=L.originalCategories.filter(c=>e.categories.indexOf(c.id)!=-1).sort((c,u)=>{const a=e.categories.indexOf(c.id),g=e.categories.indexOf(u.id);return a-g}));let o=null,i=null;n=="native"&&(o=Ye.latestVersion(),i=e.noCountryFlags||Ye.noCountryFlags());let s=L.categories.length,l=!1;for(;s--;){const c=L.categories[s];if(c.id=="frequent"){let{maxFrequentRows:g,perLine:f}=e;g=g>=0?g:X.maxFrequentRows.value,f||(f=X.perLine.value),c.emojis=Tt.get({maxFrequentRows:g,perLine:f})}if(!c.emojis||!c.emojis.length){L.categories.splice(s,1);continue}const{categoryIcons:u}=e;if(u){const g=u[c.id];g&&!c.icon&&(c.icon=g)}let a=c.emojis.length;for(;a--;){const g=c.emojis[a],f=g.id?g:L.emojis[g],v=()=>{c.emojis.splice(a,1)};if(!f||e.exceptEmojis&&e.exceptEmojis.includes(f.id)){v();continue}if(o&&f.version>o){v();continue}if(i&&c.id=="flags"&&!Hn.includes(f.id)){v();continue}if(!f.search){if(l=!0,f.search=","+[[f.id,!1],[f.name,!0],[f.keywords,!1],[f.emoticons,!1]].map(([b,w])=>{if(b)return(Array.isArray(b)?b:[b]).map($=>(w?$.split(/[-|_|\s]+/):[$]).map(M=>M.toLowerCase())).flat()}).flat().filter(b=>b&&b.trim()).join(","),f.emoticons)for(const b of f.emoticons)L.emoticons[b]||(L.emoticons[b]=f.id);let p=0;for(const b of f.skins){if(!b)continue;p++;const{native:w}=b;w&&(L.natives[w]=f.id,f.search+=`,${w}`);const $=p==1?"":`:skin-tone-${p}:`;b.shortcodes=`:${f.id}:${$}`}}}}l&&ie.reset(),Dt()}function Ht(e,t,n){e||(e={});const r={};for(let o in t)r[o]=It(o,e,t,n);return r}function It(e,t,n,r){const o=n[e];let i=r&&r.getAttribute(e)||(t[e]!=null&&t[e]!=null?t[e]:null);return o&&(i!=null&&o.value&&typeof o.value!=typeof i&&(typeof o.value=="boolean"?i=i!="false":i=o.value.constructor(i)),o.transform&&i&&(i=o.transform(i)),(i==null||o.choices&&o.choices.indexOf(i)==-1)&&(i=o.value)),i}const Tn=/^(?:\:([^\:]+)\:)(?:\:skin-tone-(\d)\:)?$/;let ze=null;function An(e){return e.id?e:L.emojis[e]||L.emojis[L.aliases[e]]||L.emojis[L.natives[e]]}function Dn(){ze=null}async function On(e,{maxResults:t,caller:n}={}){if(!e||!e.trim().length)return null;t||(t=90),await xe(null,{caller:n||"SearchIndex.search"});const r=e.toLowerCase().replace(/(\w)-/,"$1 ").split(/[\s|,]+/).filter((l,c,u)=>l.trim()&&u.indexOf(l)==c);if(!r.length)return;let o=ze||(ze=Object.values(L.emojis)),i,s;for(const l of r){if(!o.length)break;i=[],s={};for(const c of o){if(!c.search)continue;const u=c.search.indexOf(`,${l}`);u!=-1&&(i.push(c),s[c.id]||(s[c.id]=0),s[c.id]+=c.id==l?0:u+1)}o=i}return i.length<2||(i.sort((l,c)=>{const u=s[l.id],a=s[c.id];return u==a?l.id.localeCompare(c.id):u-a}),i.length>t&&(i=i.slice(0,t))),i}var ie={search:On,get:An,reset:Dn,SHORTCODES_REGEX:Tn};const Hn=["checkered_flag","crossed_flags","pirate_flag","rainbow-flag","transgender_flag","triangular_flag_on_post","waving_black_flag","waving_white_flag"];function In(e,t){return Array.isArray(e)&&Array.isArray(t)&&e.length===t.length&&e.every((n,r)=>n==t[r])}async function Bn(e=1){for(let t in[...Array(e).keys()])await new Promise(requestAnimationFrame)}function Fn(e,{skinIndex:t=0}={}){const n=e.skins[t]||(()=>(t=0,e.skins[t]))(),r={id:e.id,name:e.name,native:n.native,unified:n.unified,keywords:e.keywords,shortcodes:n.shortcodes||e.shortcodes};return e.skins.length>1&&(r.skin=t+1),n.src&&(r.src=n.src),e.aliases&&e.aliases.length&&(r.aliases=e.aliases),e.emoticons&&e.emoticons.length&&(r.emoticons=e.emoticons),r}const Un={activity:{outline:d("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 24 24",children:d("path",{d:"M12 0C5.373 0 0 5.372 0 12c0 6.627 5.373 12 12 12 6.628 0 12-5.373 12-12 0-6.628-5.372-12-12-12m9.949 11H17.05c.224-2.527 1.232-4.773 1.968-6.113A9.966 9.966 0 0 1 21.949 11M13 11V2.051a9.945 9.945 0 0 1 4.432 1.564c-.858 1.491-2.156 4.22-2.392 7.385H13zm-2 0H8.961c-.238-3.165-1.536-5.894-2.393-7.385A9.95 9.95 0 0 1 11 2.051V11zm0 2v8.949a9.937 9.937 0 0 1-4.432-1.564c.857-1.492 2.155-4.221 2.393-7.385H11zm4.04 0c.236 3.164 1.534 5.893 2.392 7.385A9.92 9.92 0 0 1 13 21.949V13h2.04zM4.982 4.887C5.718 6.227 6.726 8.473 6.951 11h-4.9a9.977 9.977 0 0 1 2.931-6.113M2.051 13h4.9c-.226 2.527-1.233 4.771-1.969 6.113A9.972 9.972 0 0 1 2.051 13m16.967 6.113c-.735-1.342-1.744-3.586-1.968-6.113h4.899a9.961 9.961 0 0 1-2.931 6.113"})}),solid:d("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 512 512",children:d("path",{d:"M16.17 337.5c0 44.98 7.565 83.54 13.98 107.9C35.22 464.3 50.46 496 174.9 496c9.566 0 19.59-.4707 29.84-1.271L17.33 307.3C16.53 317.6 16.17 327.7 16.17 337.5zM495.8 174.5c0-44.98-7.565-83.53-13.98-107.9c-4.688-17.54-18.34-31.23-36.04-35.95C435.5 27.91 392.9 16 337 16c-9.564 0-19.59 .4707-29.84 1.271l187.5 187.5C495.5 194.4 495.8 184.3 495.8 174.5zM26.77 248.8l236.3 236.3c142-36.1 203.9-150.4 222.2-221.1L248.9 26.87C106.9 62.96 45.07 177.2 26.77 248.8zM256 335.1c0 9.141-7.474 16-16 16c-4.094 0-8.188-1.564-11.31-4.689L164.7 283.3C161.6 280.2 160 276.1 160 271.1c0-8.529 6.865-16 16-16c4.095 0 8.189 1.562 11.31 4.688l64.01 64C254.4 327.8 256 331.9 256 335.1zM304 287.1c0 9.141-7.474 16-16 16c-4.094 0-8.188-1.564-11.31-4.689L212.7 235.3C209.6 232.2 208 228.1 208 223.1c0-9.141 7.473-16 16-16c4.094 0 8.188 1.562 11.31 4.688l64.01 64.01C302.5 279.8 304 283.9 304 287.1zM256 175.1c0-9.141 7.473-16 16-16c4.094 0 8.188 1.562 11.31 4.688l64.01 64.01c3.125 3.125 4.688 7.219 4.688 11.31c0 9.133-7.468 16-16 16c-4.094 0-8.189-1.562-11.31-4.688l-64.01-64.01C257.6 184.2 256 180.1 256 175.1z"})})},custom:d("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 448 512",children:d("path",{d:"M417.1 368c-5.937 10.27-16.69 16-27.75 16c-5.422 0-10.92-1.375-15.97-4.281L256 311.4V448c0 17.67-14.33 32-31.1 32S192 465.7 192 448V311.4l-118.3 68.29C68.67 382.6 63.17 384 57.75 384c-11.06 0-21.81-5.734-27.75-16c-8.828-15.31-3.594-34.88 11.72-43.72L159.1 256L41.72 187.7C26.41 178.9 21.17 159.3 29.1 144C36.63 132.5 49.26 126.7 61.65 128.2C65.78 128.7 69.88 130.1 73.72 132.3L192 200.6V64c0-17.67 14.33-32 32-32S256 46.33 256 64v136.6l118.3-68.29c3.838-2.213 7.939-3.539 12.07-4.051C398.7 126.7 411.4 132.5 417.1 144c8.828 15.31 3.594 34.88-11.72 43.72L288 256l118.3 68.28C421.6 333.1 426.8 352.7 417.1 368z"})}),flags:{outline:d("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 24 24",children:d("path",{d:"M0 0l6.084 24H8L1.916 0zM21 5h-4l-1-4H4l3 12h3l1 4h13L21 5zM6.563 3h7.875l2 8H8.563l-2-8zm8.832 10l-2.856 1.904L12.063 13h3.332zM19 13l-1.5-6h1.938l2 8H16l3-2z"})}),solid:d("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 512 512",children:d("path",{d:"M64 496C64 504.8 56.75 512 48 512h-32C7.25 512 0 504.8 0 496V32c0-17.75 14.25-32 32-32s32 14.25 32 32V496zM476.3 0c-6.365 0-13.01 1.35-19.34 4.233c-45.69 20.86-79.56 27.94-107.8 27.94c-59.96 0-94.81-31.86-163.9-31.87C160.9 .3055 131.6 4.867 96 15.75v350.5c32-9.984 59.87-14.1 84.85-14.1c73.63 0 124.9 31.78 198.6 31.78c31.91 0 68.02-5.971 111.1-23.09C504.1 355.9 512 344.4 512 332.1V30.73C512 11.1 495.3 0 476.3 0z"})})},foods:{outline:d("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 24 24",children:d("path",{d:"M17 4.978c-1.838 0-2.876.396-3.68.934.513-1.172 1.768-2.934 4.68-2.934a1 1 0 0 0 0-2c-2.921 0-4.629 1.365-5.547 2.512-.064.078-.119.162-.18.244C11.73 1.838 10.798.023 9.207.023 8.579.022 7.85.306 7 .978 5.027 2.54 5.329 3.902 6.492 4.999 3.609 5.222 0 7.352 0 12.969c0 4.582 4.961 11.009 9 11.009 1.975 0 2.371-.486 3-1 .629.514 1.025 1 3 1 4.039 0 9-6.418 9-11 0-5.953-4.055-8-7-8M8.242 2.546c.641-.508.943-.523.965-.523.426.169.975 1.405 1.357 3.055-1.527-.629-2.741-1.352-2.98-1.846.059-.112.241-.356.658-.686M15 21.978c-1.08 0-1.21-.109-1.559-.402l-.176-.146c-.367-.302-.816-.452-1.266-.452s-.898.15-1.266.452l-.176.146c-.347.292-.477.402-1.557.402-2.813 0-7-5.389-7-9.009 0-5.823 4.488-5.991 5-5.991 1.939 0 2.484.471 3.387 1.251l.323.276a1.995 1.995 0 0 0 2.58 0l.323-.276c.902-.78 1.447-1.251 3.387-1.251.512 0 5 .168 5 6 0 3.617-4.187 9-7 9"})}),solid:d("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 512 512",children:d("path",{d:"M481.9 270.1C490.9 279.1 496 291.3 496 304C496 316.7 490.9 328.9 481.9 337.9C472.9 346.9 460.7 352 448 352H64C51.27 352 39.06 346.9 30.06 337.9C21.06 328.9 16 316.7 16 304C16 291.3 21.06 279.1 30.06 270.1C39.06 261.1 51.27 256 64 256H448C460.7 256 472.9 261.1 481.9 270.1zM475.3 388.7C478.3 391.7 480 395.8 480 400V416C480 432.1 473.3 449.3 461.3 461.3C449.3 473.3 432.1 480 416 480H96C79.03 480 62.75 473.3 50.75 461.3C38.74 449.3 32 432.1 32 416V400C32 395.8 33.69 391.7 36.69 388.7C39.69 385.7 43.76 384 48 384H464C468.2 384 472.3 385.7 475.3 388.7zM50.39 220.8C45.93 218.6 42.03 215.5 38.97 211.6C35.91 207.7 33.79 203.2 32.75 198.4C31.71 193.5 31.8 188.5 32.99 183.7C54.98 97.02 146.5 32 256 32C365.5 32 457 97.02 479 183.7C480.2 188.5 480.3 193.5 479.2 198.4C478.2 203.2 476.1 207.7 473 211.6C469.1 215.5 466.1 218.6 461.6 220.8C457.2 222.9 452.3 224 447.3 224H64.67C59.73 224 54.84 222.9 50.39 220.8zM372.7 116.7C369.7 119.7 368 123.8 368 128C368 131.2 368.9 134.3 370.7 136.9C372.5 139.5 374.1 141.6 377.9 142.8C380.8 143.1 384 144.3 387.1 143.7C390.2 143.1 393.1 141.6 395.3 139.3C397.6 137.1 399.1 134.2 399.7 131.1C400.3 128 399.1 124.8 398.8 121.9C397.6 118.1 395.5 116.5 392.9 114.7C390.3 112.9 387.2 111.1 384 111.1C379.8 111.1 375.7 113.7 372.7 116.7V116.7zM244.7 84.69C241.7 87.69 240 91.76 240 96C240 99.16 240.9 102.3 242.7 104.9C244.5 107.5 246.1 109.6 249.9 110.8C252.8 111.1 256 112.3 259.1 111.7C262.2 111.1 265.1 109.6 267.3 107.3C269.6 105.1 271.1 102.2 271.7 99.12C272.3 96.02 271.1 92.8 270.8 89.88C269.6 86.95 267.5 84.45 264.9 82.7C262.3 80.94 259.2 79.1 256 79.1C251.8 79.1 247.7 81.69 244.7 84.69V84.69zM116.7 116.7C113.7 119.7 112 123.8 112 128C112 131.2 112.9 134.3 114.7 136.9C116.5 139.5 118.1 141.6 121.9 142.8C124.8 143.1 128 144.3 131.1 143.7C134.2 143.1 137.1 141.6 139.3 139.3C141.6 137.1 143.1 134.2 143.7 131.1C144.3 128 143.1 124.8 142.8 121.9C141.6 118.1 139.5 116.5 136.9 114.7C134.3 112.9 131.2 111.1 128 111.1C123.8 111.1 119.7 113.7 116.7 116.7L116.7 116.7z"})})},frequent:{outline:d("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 24 24",children:[d("path",{d:"M13 4h-2l-.001 7H9v2h2v2h2v-2h4v-2h-4z"}),d("path",{d:"M12 0C5.373 0 0 5.373 0 12s5.373 12 12 12 12-5.373 12-12S18.627 0 12 0m0 22C6.486 22 2 17.514 2 12S6.486 2 12 2s10 4.486 10 10-4.486 10-10 10"})]}),solid:d("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 512 512",children:d("path",{d:"M256 512C114.6 512 0 397.4 0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512zM232 256C232 264 236 271.5 242.7 275.1L338.7 339.1C349.7 347.3 364.6 344.3 371.1 333.3C379.3 322.3 376.3 307.4 365.3 300L280 243.2V120C280 106.7 269.3 96 255.1 96C242.7 96 231.1 106.7 231.1 120L232 256z"})})},nature:{outline:d("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 24 24",children:[d("path",{d:"M15.5 8a1.5 1.5 0 1 0 .001 3.001A1.5 1.5 0 0 0 15.5 8M8.5 8a1.5 1.5 0 1 0 .001 3.001A1.5 1.5 0 0 0 8.5 8"}),d("path",{d:"M18.933 0h-.027c-.97 0-2.138.787-3.018 1.497-1.274-.374-2.612-.51-3.887-.51-1.285 0-2.616.133-3.874.517C7.245.79 6.069 0 5.093 0h-.027C3.352 0 .07 2.67.002 7.026c-.039 2.479.276 4.238 1.04 5.013.254.258.882.677 1.295.882.191 3.177.922 5.238 2.536 6.38.897.637 2.187.949 3.2 1.102C8.04 20.6 8 20.795 8 21c0 1.773 2.35 3 4 3 1.648 0 4-1.227 4-3 0-.201-.038-.393-.072-.586 2.573-.385 5.435-1.877 5.925-7.587.396-.22.887-.568 1.104-.788.763-.774 1.079-2.534 1.04-5.013C23.929 2.67 20.646 0 18.933 0M3.223 9.135c-.237.281-.837 1.155-.884 1.238-.15-.41-.368-1.349-.337-3.291.051-3.281 2.478-4.972 3.091-5.031.256.015.731.27 1.265.646-1.11 1.171-2.275 2.915-2.352 5.125-.133.546-.398.858-.783 1.313M12 22c-.901 0-1.954-.693-2-1 0-.654.475-1.236 1-1.602V20a1 1 0 1 0 2 0v-.602c.524.365 1 .947 1 1.602-.046.307-1.099 1-2 1m3-3.48v.02a4.752 4.752 0 0 0-1.262-1.02c1.092-.516 2.239-1.334 2.239-2.217 0-1.842-1.781-2.195-3.977-2.195-2.196 0-3.978.354-3.978 2.195 0 .883 1.148 1.701 2.238 2.217A4.8 4.8 0 0 0 9 18.539v-.025c-1-.076-2.182-.281-2.973-.842-1.301-.92-1.838-3.045-1.853-6.478l.023-.041c.496-.826 1.49-1.45 1.804-3.102 0-2.047 1.357-3.631 2.362-4.522C9.37 3.178 10.555 3 11.948 3c1.447 0 2.685.192 3.733.57 1 .9 2.316 2.465 2.316 4.48.313 1.651 1.307 2.275 1.803 3.102.035.058.068.117.102.178-.059 5.967-1.949 7.01-4.902 7.19m6.628-8.202c-.037-.065-.074-.13-.113-.195a7.587 7.587 0 0 0-.739-.987c-.385-.455-.648-.768-.782-1.313-.076-2.209-1.241-3.954-2.353-5.124.531-.376 1.004-.63 1.261-.647.636.071 3.044 1.764 3.096 5.031.027 1.81-.347 3.218-.37 3.235"})]}),solid:d("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 576 512",children:d("path",{d:"M332.7 19.85C334.6 8.395 344.5 0 356.1 0C363.6 0 370.6 3.52 375.1 9.502L392 32H444.1C456.8 32 469.1 37.06 478.1 46.06L496 64H552C565.3 64 576 74.75 576 88V112C576 156.2 540.2 192 496 192H426.7L421.6 222.5L309.6 158.5L332.7 19.85zM448 64C439.2 64 432 71.16 432 80C432 88.84 439.2 96 448 96C456.8 96 464 88.84 464 80C464 71.16 456.8 64 448 64zM416 256.1V480C416 497.7 401.7 512 384 512H352C334.3 512 320 497.7 320 480V364.8C295.1 377.1 268.8 384 240 384C211.2 384 184 377.1 160 364.8V480C160 497.7 145.7 512 128 512H96C78.33 512 64 497.7 64 480V249.8C35.23 238.9 12.64 214.5 4.836 183.3L.9558 167.8C-3.331 150.6 7.094 133.2 24.24 128.1C41.38 124.7 58.76 135.1 63.05 152.2L66.93 167.8C70.49 182 83.29 191.1 97.97 191.1H303.8L416 256.1z"})})},objects:{outline:d("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 24 24",children:[d("path",{d:"M12 0a9 9 0 0 0-5 16.482V21s2.035 3 5 3 5-3 5-3v-4.518A9 9 0 0 0 12 0zm0 2c3.86 0 7 3.141 7 7s-3.14 7-7 7-7-3.141-7-7 3.14-7 7-7zM9 17.477c.94.332 1.946.523 3 .523s2.06-.19 3-.523v.834c-.91.436-1.925.689-3 .689a6.924 6.924 0 0 1-3-.69v-.833zm.236 3.07A8.854 8.854 0 0 0 12 21c.965 0 1.888-.167 2.758-.451C14.155 21.173 13.153 22 12 22c-1.102 0-2.117-.789-2.764-1.453z"}),d("path",{d:"M14.745 12.449h-.004c-.852-.024-1.188-.858-1.577-1.824-.421-1.061-.703-1.561-1.182-1.566h-.009c-.481 0-.783.497-1.235 1.537-.436.982-.801 1.811-1.636 1.791l-.276-.043c-.565-.171-.853-.691-1.284-1.794-.125-.313-.202-.632-.27-.913-.051-.213-.127-.53-.195-.634C7.067 9.004 7.039 9 6.99 9A1 1 0 0 1 7 7h.01c1.662.017 2.015 1.373 2.198 2.134.486-.981 1.304-2.058 2.797-2.075 1.531.018 2.28 1.153 2.731 2.141l.002-.008C14.944 8.424 15.327 7 16.979 7h.032A1 1 0 1 1 17 9h-.011c-.149.076-.256.474-.319.709a6.484 6.484 0 0 1-.311.951c-.429.973-.79 1.789-1.614 1.789"})]}),solid:d("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 384 512",children:d("path",{d:"M112.1 454.3c0 6.297 1.816 12.44 5.284 17.69l17.14 25.69c5.25 7.875 17.17 14.28 26.64 14.28h61.67c9.438 0 21.36-6.401 26.61-14.28l17.08-25.68c2.938-4.438 5.348-12.37 5.348-17.7L272 415.1h-160L112.1 454.3zM191.4 .0132C89.44 .3257 16 82.97 16 175.1c0 44.38 16.44 84.84 43.56 115.8c16.53 18.84 42.34 58.23 52.22 91.45c.0313 .25 .0938 .5166 .125 .7823h160.2c.0313-.2656 .0938-.5166 .125-.7823c9.875-33.22 35.69-72.61 52.22-91.45C351.6 260.8 368 220.4 368 175.1C368 78.61 288.9-.2837 191.4 .0132zM192 96.01c-44.13 0-80 35.89-80 79.1C112 184.8 104.8 192 96 192S80 184.8 80 176c0-61.76 50.25-111.1 112-111.1c8.844 0 16 7.159 16 16S200.8 96.01 192 96.01z"})})},people:{outline:d("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 24 24",children:[d("path",{d:"M12 0C5.373 0 0 5.373 0 12s5.373 12 12 12 12-5.373 12-12S18.627 0 12 0m0 22C6.486 22 2 17.514 2 12S6.486 2 12 2s10 4.486 10 10-4.486 10-10 10"}),d("path",{d:"M8 7a2 2 0 1 0-.001 3.999A2 2 0 0 0 8 7M16 7a2 2 0 1 0-.001 3.999A2 2 0 0 0 16 7M15.232 15c-.693 1.195-1.87 2-3.349 2-1.477 0-2.655-.805-3.347-2H15m3-2H6a6 6 0 1 0 12 0"})]}),solid:d("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 512 512",children:d("path",{d:"M0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256zM256 432C332.1 432 396.2 382 415.2 314.1C419.1 300.4 407.8 288 393.6 288H118.4C104.2 288 92.92 300.4 96.76 314.1C115.8 382 179.9 432 256 432V432zM176.4 160C158.7 160 144.4 174.3 144.4 192C144.4 209.7 158.7 224 176.4 224C194 224 208.4 209.7 208.4 192C208.4 174.3 194 160 176.4 160zM336.4 224C354 224 368.4 209.7 368.4 192C368.4 174.3 354 160 336.4 160C318.7 160 304.4 174.3 304.4 192C304.4 209.7 318.7 224 336.4 224z"})})},places:{outline:d("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 24 24",children:[d("path",{d:"M6.5 12C5.122 12 4 13.121 4 14.5S5.122 17 6.5 17 9 15.879 9 14.5 7.878 12 6.5 12m0 3c-.275 0-.5-.225-.5-.5s.225-.5.5-.5.5.225.5.5-.225.5-.5.5M17.5 12c-1.378 0-2.5 1.121-2.5 2.5s1.122 2.5 2.5 2.5 2.5-1.121 2.5-2.5-1.122-2.5-2.5-2.5m0 3c-.275 0-.5-.225-.5-.5s.225-.5.5-.5.5.225.5.5-.225.5-.5.5"}),d("path",{d:"M22.482 9.494l-1.039-.346L21.4 9h.6c.552 0 1-.439 1-.992 0-.006-.003-.008-.003-.008H23c0-1-.889-2-1.984-2h-.642l-.731-1.717C19.262 3.012 18.091 2 16.764 2H7.236C5.909 2 4.738 3.012 4.357 4.283L3.626 6h-.642C1.889 6 1 7 1 8h.003S1 8.002 1 8.008C1 8.561 1.448 9 2 9h.6l-.043.148-1.039.346a2.001 2.001 0 0 0-1.359 2.097l.751 7.508a1 1 0 0 0 .994.901H3v1c0 1.103.896 2 2 2h2c1.104 0 2-.897 2-2v-1h6v1c0 1.103.896 2 2 2h2c1.104 0 2-.897 2-2v-1h1.096a.999.999 0 0 0 .994-.901l.751-7.508a2.001 2.001 0 0 0-1.359-2.097M6.273 4.857C6.402 4.43 6.788 4 7.236 4h9.527c.448 0 .834.43.963.857L19.313 9H4.688l1.585-4.143zM7 21H5v-1h2v1zm12 0h-2v-1h2v1zm2.189-3H2.811l-.662-6.607L3 11h18l.852.393L21.189 18z"})]}),solid:d("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 512 512",children:d("path",{d:"M39.61 196.8L74.8 96.29C88.27 57.78 124.6 32 165.4 32H346.6C387.4 32 423.7 57.78 437.2 96.29L472.4 196.8C495.6 206.4 512 229.3 512 256V448C512 465.7 497.7 480 480 480H448C430.3 480 416 465.7 416 448V400H96V448C96 465.7 81.67 480 64 480H32C14.33 480 0 465.7 0 448V256C0 229.3 16.36 206.4 39.61 196.8V196.8zM109.1 192H402.9L376.8 117.4C372.3 104.6 360.2 96 346.6 96H165.4C151.8 96 139.7 104.6 135.2 117.4L109.1 192zM96 256C78.33 256 64 270.3 64 288C64 305.7 78.33 320 96 320C113.7 320 128 305.7 128 288C128 270.3 113.7 256 96 256zM416 320C433.7 320 448 305.7 448 288C448 270.3 433.7 256 416 256C398.3 256 384 270.3 384 288C384 305.7 398.3 320 416 320z"})})},symbols:{outline:d("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 24 24",children:d("path",{d:"M0 0h11v2H0zM4 11h3V6h4V4H0v2h4zM15.5 17c1.381 0 2.5-1.116 2.5-2.493s-1.119-2.493-2.5-2.493S13 13.13 13 14.507 14.119 17 15.5 17m0-2.986c.276 0 .5.222.5.493 0 .272-.224.493-.5.493s-.5-.221-.5-.493.224-.493.5-.493M21.5 19.014c-1.381 0-2.5 1.116-2.5 2.493S20.119 24 21.5 24s2.5-1.116 2.5-2.493-1.119-2.493-2.5-2.493m0 2.986a.497.497 0 0 1-.5-.493c0-.271.224-.493.5-.493s.5.222.5.493a.497.497 0 0 1-.5.493M22 13l-9 9 1.513 1.5 8.99-9.009zM17 11c2.209 0 4-1.119 4-2.5V2s.985-.161 1.498.949C23.01 4.055 23 6 23 6s1-1.119 1-3.135C24-.02 21 0 21 0h-2v6.347A5.853 5.853 0 0 0 17 6c-2.209 0-4 1.119-4 2.5s1.791 2.5 4 2.5M10.297 20.482l-1.475-1.585a47.54 47.54 0 0 1-1.442 1.129c-.307-.288-.989-1.016-2.045-2.183.902-.836 1.479-1.466 1.729-1.892s.376-.871.376-1.336c0-.592-.273-1.178-.818-1.759-.546-.581-1.329-.871-2.349-.871-1.008 0-1.79.293-2.344.879-.556.587-.832 1.181-.832 1.784 0 .813.419 1.748 1.256 2.805-.847.614-1.444 1.208-1.794 1.784a3.465 3.465 0 0 0-.523 1.833c0 .857.308 1.56.924 2.107.616.549 1.423.823 2.42.823 1.173 0 2.444-.379 3.813-1.137L8.235 24h2.819l-2.09-2.383 1.333-1.135zm-6.736-6.389a1.02 1.02 0 0 1 .73-.286c.31 0 .559.085.747.254a.849.849 0 0 1 .283.659c0 .518-.419 1.112-1.257 1.784-.536-.651-.805-1.231-.805-1.742a.901.901 0 0 1 .302-.669M3.74 22c-.427 0-.778-.116-1.057-.349-.279-.232-.418-.487-.418-.766 0-.594.509-1.288 1.527-2.083.968 1.134 1.717 1.946 2.248 2.438-.921.507-1.686.76-2.3.76"})}),solid:d("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 512 512",children:d("path",{d:"M500.3 7.251C507.7 13.33 512 22.41 512 31.1V175.1C512 202.5 483.3 223.1 447.1 223.1C412.7 223.1 383.1 202.5 383.1 175.1C383.1 149.5 412.7 127.1 447.1 127.1V71.03L351.1 90.23V207.1C351.1 234.5 323.3 255.1 287.1 255.1C252.7 255.1 223.1 234.5 223.1 207.1C223.1 181.5 252.7 159.1 287.1 159.1V63.1C287.1 48.74 298.8 35.61 313.7 32.62L473.7 .6198C483.1-1.261 492.9 1.173 500.3 7.251H500.3zM74.66 303.1L86.5 286.2C92.43 277.3 102.4 271.1 113.1 271.1H174.9C185.6 271.1 195.6 277.3 201.5 286.2L213.3 303.1H239.1C266.5 303.1 287.1 325.5 287.1 351.1V463.1C287.1 490.5 266.5 511.1 239.1 511.1H47.1C21.49 511.1-.0019 490.5-.0019 463.1V351.1C-.0019 325.5 21.49 303.1 47.1 303.1H74.66zM143.1 359.1C117.5 359.1 95.1 381.5 95.1 407.1C95.1 434.5 117.5 455.1 143.1 455.1C170.5 455.1 191.1 434.5 191.1 407.1C191.1 381.5 170.5 359.1 143.1 359.1zM440.3 367.1H496C502.7 367.1 508.6 372.1 510.1 378.4C513.3 384.6 511.6 391.7 506.5 396L378.5 508C372.9 512.1 364.6 513.3 358.6 508.9C352.6 504.6 350.3 496.6 353.3 489.7L391.7 399.1H336C329.3 399.1 323.4 395.9 321 389.6C318.7 383.4 320.4 376.3 325.5 371.1L453.5 259.1C459.1 255 467.4 254.7 473.4 259.1C479.4 263.4 481.6 271.4 478.7 278.3L440.3 367.1zM116.7 219.1L19.85 119.2C-8.112 90.26-6.614 42.31 24.85 15.34C51.82-8.137 93.26-3.642 118.2 21.83L128.2 32.32L137.7 21.83C162.7-3.642 203.6-8.137 231.6 15.34C262.6 42.31 264.1 90.26 236.1 119.2L139.7 219.1C133.2 225.6 122.7 225.6 116.7 219.1H116.7z"})})}},Nn={loupe:d("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 20 20",children:d("path",{d:"M12.9 14.32a8 8 0 1 1 1.41-1.41l5.35 5.33-1.42 1.42-5.33-5.34zM8 14A6 6 0 1 0 8 2a6 6 0 0 0 0 12z"})}),delete:d("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 20 20",children:d("path",{d:"M10 8.586L2.929 1.515 1.515 2.929 8.586 10l-7.071 7.071 1.414 1.414L10 11.414l7.071 7.071 1.414-1.414L11.414 10l7.071-7.071-1.414-1.414L10 8.586z"})})};var ye={categories:Un,search:Nn};function Pe(e){let{id:t,skin:n,emoji:r}=e;if(e.shortcodes){const l=e.shortcodes.match(ie.SHORTCODES_REGEX);l&&(t=l[1],l[2]&&(n=l[2]))}if(r||(r=ie.get(t||e.native)),!r)return e.fallback;const o=r.skins[n-1]||r.skins[0],i=o.src||(e.set!="native"&&!e.spritesheet?typeof e.getImageURL=="function"?e.getImageURL(e.set,o.unified):`https://cdn.jsdelivr.net/npm/emoji-datasource-${e.set}@14.0.0/img/${e.set}/64/${o.unified}.png`:void 0),s=typeof e.getSpritesheetURL=="function"?e.getSpritesheetURL(e.set):`https://cdn.jsdelivr.net/npm/emoji-datasource-${e.set}@14.0.0/img/${e.set}/sheets-256/64.png`;return d("span",{class:"emoji-mart-emoji","data-emoji-set":e.set,children:i?d("img",{style:{maxWidth:e.size||"1em",maxHeight:e.size||"1em",display:"inline-block"},alt:o.native||o.shortcodes,src:i}):e.set=="native"?d("span",{style:{fontSize:e.size,fontFamily:'"EmojiMart", "Segoe UI Emoji", "Segoe UI Symbol", "Segoe UI", "Apple Color Emoji", "Twemoji Mozilla", "Noto Color Emoji", "Android Emoji"'},children:o.native}):d("span",{style:{display:"block",width:e.size,height:e.size,backgroundImage:`url(${s})`,backgroundSize:`${100*L.sheet.cols}% ${100*L.sheet.rows}%`,backgroundPosition:`${100/(L.sheet.cols-1)*o.x}% ${100/(L.sheet.rows-1)*o.y}%`}})})}const Vn=typeof window<"u"&&window.HTMLElement?window.HTMLElement:Object;class Bt extends Vn{static get observedAttributes(){return Object.keys(this.Props)}update(t={}){for(let n in t)this.attributeChangedCallback(n,null,t[n])}attributeChangedCallback(t,n,r){if(!this.component)return;const o=It(t,{[t]:r},this.constructor.Props,this);this.component.componentWillReceiveProps?this.component.componentWillReceiveProps({[t]:o}):(this.component.props[t]=o,this.component.forceUpdate())}disconnectedCallback(){this.disconnected=!0,this.component&&this.component.unregister&&this.component.unregister()}constructor(t={}){if(super(),this.props=t,t.parent||t.ref){let n=null;const r=t.parent||(n=t.ref&&t.ref.current);n&&(n.innerHTML=""),r&&r.appendChild(this)}}}class Wn extends Bt{setShadow(){this.attachShadow({mode:"open"})}injectStyles(t){if(!t)return;const n=document.createElement("style");n.textContent=t,this.shadowRoot.insertBefore(n,this.shadowRoot.firstChild)}constructor(t,{styles:n}={}){super(t),this.setShadow(),this.injectStyles(n)}}var Ft={fallback:"",id:"",native:"",shortcodes:"",size:{value:"",transform:e=>/\D/.test(e)?e:`${e}px`},set:X.set,skin:X.skin};class Ut extends Bt{async connectedCallback(){const t=Ht(this.props,Ft,this);t.element=this,t.ref=n=>{this.component=n},await xe(),!this.disconnected&&Pt(d(Pe,{...t}),this)}constructor(t){super(t)}}W(Ut,"Props",Ft);typeof customElements<"u"&&!customElements.get("em-emoji")&&customElements.define("em-emoji",Ut);var Ze,Re=[],Qe=x.__b,et=x.__r,tt=x.diffed,nt=x.__c,rt=x.unmount;function qn(){var e;for(Re.sort(function(t,n){return t.__v.__b-n.__v.__b});e=Re.pop();)if(e.__P)try{e.__H.__h.forEach(me),e.__H.__h.forEach(Te),e.__H.__h=[]}catch(t){e.__H.__h=[],x.__e(t,e.__v)}}x.__b=function(e){Qe&&Qe(e)},x.__r=function(e){et&&et(e);var t=e.__c.__H;t&&(t.__h.forEach(me),t.__h.forEach(Te),t.__h=[])},x.diffed=function(e){tt&&tt(e);var t=e.__c;t&&t.__H&&t.__H.__h.length&&(Re.push(t)!==1&&Ze===x.requestAnimationFrame||((Ze=x.requestAnimationFrame)||function(n){var r,o=function(){clearTimeout(i),it&&cancelAnimationFrame(r),setTimeout(n)},i=setTimeout(o,100);it&&(r=requestAnimationFrame(o))})(qn))},x.__c=function(e,t){t.some(function(n){try{n.__h.forEach(me),n.__h=n.__h.filter(function(r){return!r.__||Te(r)})}catch(r){t.some(function(o){o.__h&&(o.__h=[])}),t=[],x.__e(r,n.__v)}}),nt&&nt(e,t)},x.unmount=function(e){rt&&rt(e);var t,n=e.__c;n&&n.__H&&(n.__H.__.forEach(function(r){try{me(r)}catch(o){t=o}}),t&&x.__e(t,n.__v))};var it=typeof requestAnimationFrame=="function";function me(e){var t=e.__c;typeof t=="function"&&(e.__c=void 0,t())}function Te(e){e.__c=e.__()}function Kn(e,t){for(var n in t)e[n]=t[n];return e}function ot(e,t){for(var n in e)if(n!=="__source"&&!(n in t))return!0;for(var r in t)if(r!=="__source"&&e[r]!==t[r])return!0;return!1}function we(e){this.props=e}(we.prototype=new K).isPureReactComponent=!0,we.prototype.shouldComponentUpdate=function(e,t){return ot(this.props,e)||ot(this.state,t)};var at=x.__b;x.__b=function(e){e.type&&e.type.__f&&e.ref&&(e.props.ref=e.ref,e.ref=null),at&&at(e)};var Gn=x.__e;x.__e=function(e,t,n){if(e.then){for(var r,o=t;o=o.__;)if((r=o.__c)&&r.__c)return t.__e==null&&(t.__e=n.__e,t.__k=n.__k),r.__c(e,t)}Gn(e,t,n)};var st=x.unmount;function Ee(){this.__u=0,this.t=null,this.__b=null}function Nt(e){var t=e.__.__c;return t&&t.__e&&t.__e(e)}function fe(){this.u=null,this.o=null}x.unmount=function(e){var t=e.__c;t&&t.__R&&t.__R(),t&&e.__h===!0&&(e.type=null),st&&st(e)},(Ee.prototype=new K).__c=function(e,t){var n=t.__c,r=this;r.t==null&&(r.t=[]),r.t.push(n);var o=Nt(r.__v),i=!1,s=function(){i||(i=!0,n.__R=null,o?o(l):l())};n.__R=s;var l=function(){if(!--r.__u){if(r.state.__e){var u=r.state.__e;r.__v.__k[0]=function g(f,v,p){return f&&(f.__v=null,f.__k=f.__k&&f.__k.map(function(b){return g(b,v,p)}),f.__c&&f.__c.__P===v&&(f.__e&&p.insertBefore(f.__e,f.__d),f.__c.__e=!0,f.__c.__P=p)),f}(u,u.__c.__P,u.__c.__O)}var a;for(r.setState({__e:r.__b=null});a=r.t.pop();)a.forceUpdate()}},c=t.__h===!0;r.__u++||c||r.setState({__e:r.__b=r.__v.__k[0]}),e.then(s,s)},Ee.prototype.componentWillUnmount=function(){this.t=[]},Ee.prototype.render=function(e,t){if(this.__b){if(this.__v.__k){var n=document.createElement("div"),r=this.__v.__k[0].__c;this.__v.__k[0]=function i(s,l,c){return s&&(s.__c&&s.__c.__H&&(s.__c.__H.__.forEach(function(u){typeof u.__c=="function"&&u.__c()}),s.__c.__H=null),(s=Kn({},s)).__c!=null&&(s.__c.__P===c&&(s.__c.__P=l),s.__c=null),s.__k=s.__k&&s.__k.map(function(u){return i(u,l,c)})),s}(this.__b,n,r.__O=r.__P)}this.__b=null}var o=t.__e&&Le(oe,null,e.fallback);return o&&(o.__h=null),[Le(oe,null,t.__e?null:e.children),o]};var ct=function(e,t,n){if(++n[1]===n[0]&&e.o.delete(t),e.props.revealOrder&&(e.props.revealOrder[0]!=="t"||!e.o.size))for(n=e.u;n;){for(;n.length>3;)n.pop()();if(n[1]<n[0])break;e.u=n=n[2]}};(fe.prototype=new K).__e=function(e){var t=this,n=Nt(t.__v),r=t.o.get(e);return r[0]++,function(o){var i=function(){t.props.revealOrder?(r.push(o),ct(t,e,r)):o()};n?n(i):i()}},fe.prototype.render=function(e){this.u=null,this.o=new Map;var t=be(e.children);e.revealOrder&&e.revealOrder[0]==="b"&&t.reverse();for(var n=t.length;n--;)this.o.set(t[n],this.u=[1,0,this.u]);return e.children},fe.prototype.componentDidUpdate=fe.prototype.componentDidMount=function(){var e=this;this.o.forEach(function(t,n){ct(e,n,t)})};var Xn=typeof Symbol<"u"&&Symbol.for&&Symbol.for("react.element")||60103,Yn=/^(?:accent|alignment|arabic|baseline|cap|clip(?!PathU)|color|dominant|fill|flood|font|glyph(?!R)|horiz|marker(?!H|W|U)|overline|paint|stop|strikethrough|stroke|text(?!L)|underline|unicode|units|v|vector|vert|word|writing|x(?!C))[A-Z]/,Jn=typeof document<"u",Zn=function(e){return(typeof Symbol<"u"&&typeof Symbol()=="symbol"?/fil|che|rad/i:/fil|che|ra/i).test(e)};K.prototype.isReactComponent={},["componentWillMount","componentWillReceiveProps","componentWillUpdate"].forEach(function(e){Object.defineProperty(K.prototype,e,{configurable:!0,get:function(){return this["UNSAFE_"+e]},set:function(t){Object.defineProperty(this,e,{configurable:!0,writable:!0,value:t})}})});var lt=x.event;function Qn(){}function er(){return this.cancelBubble}function tr(){return this.defaultPrevented}x.event=function(e){return lt&&(e=lt(e)),e.persist=Qn,e.isPropagationStopped=er,e.isDefaultPrevented=tr,e.nativeEvent=e};var ut={configurable:!0,get:function(){return this.class}},dt=x.vnode;x.vnode=function(e){var t=e.type,n=e.props,r=n;if(typeof t=="string"){var o=t.indexOf("-")===-1;for(var i in r={},n){var s=n[i];Jn&&i==="children"&&t==="noscript"||i==="value"&&"defaultValue"in n&&s==null||(i==="defaultValue"&&"value"in n&&n.value==null?i="value":i==="download"&&s===!0?s="":/ondoubleclick/i.test(i)?i="ondblclick":/^onchange(textarea|input)/i.test(i+t)&&!Zn(n.type)?i="oninput":/^onfocus$/i.test(i)?i="onfocusin":/^onblur$/i.test(i)?i="onfocusout":/^on(Ani|Tra|Tou|BeforeInp)/.test(i)?i=i.toLowerCase():o&&Yn.test(i)?i=i.replace(/[A-Z0-9]/,"-$&").toLowerCase():s===null&&(s=void 0),r[i]=s)}t=="select"&&r.multiple&&Array.isArray(r.value)&&(r.value=be(n.children).forEach(function(l){l.props.selected=r.value.indexOf(l.props.value)!=-1})),t=="select"&&r.defaultValue!=null&&(r.value=be(n.children).forEach(function(l){l.props.selected=r.multiple?r.defaultValue.indexOf(l.props.value)!=-1:r.defaultValue==l.props.value})),e.props=r,n.class!=n.className&&(ut.enumerable="className"in n,n.className!=null&&(r.class=n.className),Object.defineProperty(r,"className",ut))}e.$$typeof=Xn,dt&&dt(e)};var ft=x.__r;x.__r=function(e){ft&&ft(e),e.__c};const nr={light:"outline",dark:"solid"};class rr extends we{renderIcon(t){const{icon:n}=t;if(n){if(n.svg)return d("span",{class:"flex",dangerouslySetInnerHTML:{__html:n.svg}});if(n.src)return d("img",{src:n.src})}const r=ye.categories[t.id]||ye.categories.custom,o=this.props.icons=="auto"?nr[this.props.theme]:this.props.icons;return r[o]||r}render(){let t=null;return d("nav",{id:"nav",class:"padding","data-position":this.props.position,dir:this.props.dir,children:d("div",{class:"flex relative",children:[this.categories.map((n,r)=>{const o=n.name||I.categories[n.id],i=!this.props.unfocused&&n.id==this.state.categoryId;return i&&(t=r),d("button",{"aria-label":o,"aria-selected":i||void 0,title:o,type:"button",class:"flex flex-grow flex-center",onMouseDown:s=>s.preventDefault(),onClick:()=>{this.props.onClick({category:n,i:r})},children:this.renderIcon(n)})}),d("div",{class:"bar",style:{width:`${100/this.categories.length}%`,opacity:t==null?0:1,transform:this.props.dir==="rtl"?`scaleX(-1) translateX(${t*100}%)`:`translateX(${t*100}%)`}})]})})}constructor(){super(),this.categories=L.categories.filter(t=>!t.target),this.state={categoryId:this.categories[0].id}}}class ir extends we{shouldComponentUpdate(t){for(let n in t)if(n!="children"&&t[n]!=this.props[n])return!0;return!1}render(){return this.props.children}}const he={rowsPerRender:10};class or extends K{getInitialState(t=this.props){return{skin:Z.get("skin")||t.skin,theme:this.initTheme(t.theme)}}componentWillMount(){this.dir=I.rtl?"rtl":"ltr",this.refs={menu:G(),navigation:G(),scroll:G(),search:G(),searchInput:G(),skinToneButton:G(),skinToneRadio:G()},this.initGrid(),this.props.stickySearch==!1&&this.props.searchPosition=="sticky"&&(console.warn("[EmojiMart] Deprecation warning: `stickySearch` has been renamed `searchPosition`."),this.props.searchPosition="static")}componentDidMount(){if(this.register(),this.shadowRoot=this.base.parentNode,this.props.autoFocus){const{searchInput:t}=this.refs;t.current&&t.current.focus()}}componentWillReceiveProps(t){this.nextState||(this.nextState={});for(const n in t)this.nextState[n]=t[n];clearTimeout(this.nextStateTimer),this.nextStateTimer=setTimeout(()=>{let n=!1;for(const o in this.nextState)this.props[o]=this.nextState[o],(o==="custom"||o==="categories")&&(n=!0);delete this.nextState;const r=this.getInitialState();if(n)return this.reset(r);this.setState(r)})}componentWillUnmount(){this.unregister()}async reset(t={}){await xe(this.props),this.initGrid(),this.unobserve(),this.setState(t,()=>{this.observeCategories(),this.observeRows()})}register(){document.addEventListener("click",this.handleClickOutside),this.observe()}unregister(){document.removeEventListener("click",this.handleClickOutside),this.unobserve()}observe(){this.observeCategories(),this.observeRows()}unobserve({except:t=[]}={}){Array.isArray(t)||(t=[t]);for(const n of this.observers)t.includes(n)||n.disconnect();this.observers=[].concat(t)}initGrid(){const{categories:t}=L;this.refs.categories=new Map;const n=L.categories.map(o=>o.id).join(",");this.navKey&&this.navKey!=n&&this.refs.scroll.current&&(this.refs.scroll.current.scrollTop=0),this.navKey=n,this.grid=[],this.grid.setsize=0;const r=(o,i)=>{const s=[];s.__categoryId=i.id,s.__index=o.length,this.grid.push(s);const l=this.grid.length-1,c=l%he.rowsPerRender?{}:G();return c.index=l,c.posinset=this.grid.setsize+1,o.push(c),s};for(let o of t){const i=[];let s=r(i,o);for(let l of o.emojis)s.length==this.getPerLine()&&(s=r(i,o)),this.grid.setsize+=1,s.push(l);this.refs.categories.set(o.id,{root:G(),rows:i})}}initTheme(t){if(t!="auto")return t;if(!this.darkMedia){if(this.darkMedia=matchMedia("(prefers-color-scheme: dark)"),this.darkMedia.media.match(/^not/))return"light";this.darkMedia.addListener(()=>{this.props.theme=="auto"&&this.setState({theme:this.darkMedia.matches?"dark":"light"})})}return this.darkMedia.matches?"dark":"light"}initDynamicPerLine(t=this.props){if(!t.dynamicWidth)return;const{element:n,emojiButtonSize:r}=t,o=()=>{const{width:s}=n.getBoundingClientRect();return Math.floor(s/r)},i=new ResizeObserver(()=>{this.unobserve({except:i}),this.setState({perLine:o()},()=>{this.initGrid(),this.forceUpdate(()=>{this.observeCategories(),this.observeRows()})})});return i.observe(n),this.observers.push(i),o()}getPerLine(){return this.state.perLine||this.props.perLine}getEmojiByPos([t,n]){const r=this.state.searchResults||this.grid,o=r[t]&&r[t][n];if(o)return ie.get(o)}observeCategories(){const t=this.refs.navigation.current;if(!t)return;const n=new Map,r=s=>{s!=t.state.categoryId&&t.setState({categoryId:s})},o={root:this.refs.scroll.current,threshold:[0,1]},i=new IntersectionObserver(s=>{for(const c of s){const u=c.target.dataset.id;n.set(u,c.intersectionRatio)}const l=[...n];for(const[c,u]of l)if(u){r(c);break}},o);for(const{root:s}of this.refs.categories.values())i.observe(s.current);this.observers.push(i)}observeRows(){const t={...this.state.visibleRows},n=new IntersectionObserver(r=>{for(const o of r){const i=parseInt(o.target.dataset.index);o.isIntersecting?t[i]=!0:delete t[i]}this.setState({visibleRows:t})},{root:this.refs.scroll.current,rootMargin:`${this.props.emojiButtonSize*(he.rowsPerRender+5)}px 0px ${this.props.emojiButtonSize*he.rowsPerRender}px`});for(const{rows:r}of this.refs.categories.values())for(const o of r)o.current&&n.observe(o.current);this.observers.push(n)}preventDefault(t){t.preventDefault()}unfocusSearch(){const t=this.refs.searchInput.current;t&&t.blur()}navigate({e:t,input:n,left:r,right:o,up:i,down:s}){const l=this.state.searchResults||this.grid;if(!l.length)return;let[c,u]=this.state.pos;const a=(()=>{if(c==0&&u==0&&!t.repeat&&(r||i))return null;if(c==-1)return!t.repeat&&(o||s)&&n.selectionStart==n.value.length?[0,0]:null;if(r||o){let g=l[c];const f=r?-1:1;if(u+=f,!g[u]){if(c+=f,g=l[c],!g)return c=r?0:l.length-1,u=r?0:l[c].length-1,[c,u];u=r?g.length-1:0}return[c,u]}if(i||s){c+=i?-1:1;const g=l[c];return g?(g[u]||(u=g.length-1),[c,u]):(c=i?0:l.length-1,u=i?0:l[c].length-1,[c,u])}})();if(a)t.preventDefault();else{this.state.pos[0]>-1&&this.setState({pos:[-1,-1]});return}this.setState({pos:a,keyboard:!0},()=>{this.scrollTo({row:a[0]})})}scrollTo({categoryId:t,row:n}){const r=this.state.searchResults||this.grid;if(!r.length)return;const o=this.refs.scroll.current,i=o.getBoundingClientRect();let s=0;if(n>=0&&(t=r[n].__categoryId),t&&(s=(this.refs[t]||this.refs.categories.get(t).root).current.getBoundingClientRect().top-(i.top-o.scrollTop)+1),n>=0)if(!n)s=0;else{const l=r[n].__index,c=s+l*this.props.emojiButtonSize,u=c+this.props.emojiButtonSize+this.props.emojiButtonSize*.88;if(c<o.scrollTop)s=c;else if(u>o.scrollTop+i.height)s=u-i.height;else return}this.ignoreMouse(),o.scrollTop=s}ignoreMouse(){this.mouseIsIgnored=!0,clearTimeout(this.ignoreMouseTimer),this.ignoreMouseTimer=setTimeout(()=>{delete this.mouseIsIgnored},100)}handleEmojiOver(t){this.mouseIsIgnored||this.state.showSkins||this.setState({pos:t||[-1,-1],keyboard:!1})}handleEmojiClick({e:t,emoji:n,pos:r}){if(this.props.onEmojiSelect&&(!n&&r&&(n=this.getEmojiByPos(r)),n)){const o=Fn(n,{skinIndex:this.state.skin-1});this.props.maxFrequentRows&&Tt.add(o,this.props),this.props.onEmojiSelect(o,t)}}closeSkins(){this.state.showSkins&&(this.setState({showSkins:null,tempSkin:null}),this.base.removeEventListener("click",this.handleBaseClick),this.base.removeEventListener("keydown",this.handleBaseKeydown))}handleSkinMouseOver(t){this.setState({tempSkin:t})}handleSkinClick(t){this.ignoreMouse(),this.closeSkins(),this.setState({skin:t,tempSkin:null}),Z.set("skin",t)}renderNav(){return d(rr,{ref:this.refs.navigation,icons:this.props.icons,theme:this.state.theme,dir:this.dir,unfocused:!!this.state.searchResults,position:this.props.navPosition,onClick:this.handleCategoryClick},this.navKey)}renderPreview(){const t=this.getEmojiByPos(this.state.pos),n=this.state.searchResults&&!this.state.searchResults.length;return d("div",{id:"preview",class:"flex flex-middle",dir:this.dir,"data-position":this.props.previewPosition,children:[d("div",{class:"flex flex-middle flex-grow",children:[d("div",{class:"flex flex-auto flex-middle flex-center",style:{height:this.props.emojiButtonSize,fontSize:this.props.emojiButtonSize},children:d(Pe,{emoji:t,id:n?this.props.noResultsEmoji||"cry":this.props.previewEmoji||(this.props.previewPosition=="top"?"point_down":"point_up"),set:this.props.set,size:this.props.emojiButtonSize,skin:this.state.tempSkin||this.state.skin,spritesheet:!0,getSpritesheetURL:this.props.getSpritesheetURL})}),d("div",{class:`margin-${this.dir[0]}`,children:t||n?d("div",{class:`padding-${this.dir[2]} align-${this.dir[0]}`,children:[d("div",{class:"preview-title ellipsis",children:t?t.name:I.search_no_results_1}),d("div",{class:"preview-subtitle ellipsis color-c",children:t?t.skins[0].shortcodes:I.search_no_results_2})]}):d("div",{class:"preview-placeholder color-c",children:I.pick})})]}),!t&&this.props.skinTonePosition=="preview"&&this.renderSkinToneButton()]})}renderEmojiButton(t,{pos:n,posinset:r,grid:o}){const i=this.props.emojiButtonSize,s=this.state.tempSkin||this.state.skin,c=(t.skins[s-1]||t.skins[0]).native,u=In(this.state.pos,n),a=n.concat(t.id).join("");return d(ir,{selected:u,skin:s,size:i,children:d("button",{"aria-label":c,"aria-selected":u||void 0,"aria-posinset":r,"aria-setsize":o.setsize,"data-keyboard":this.state.keyboard,title:this.props.previewPosition=="none"?t.name:void 0,type:"button",class:"flex flex-center flex-middle",tabindex:"-1",onClick:g=>this.handleEmojiClick({e:g,emoji:t}),onMouseEnter:()=>this.handleEmojiOver(n),onMouseLeave:()=>this.handleEmojiOver(),style:{width:this.props.emojiButtonSize,height:this.props.emojiButtonSize,fontSize:this.props.emojiSize,lineHeight:0},children:[d("div",{"aria-hidden":"true",class:"background",style:{borderRadius:this.props.emojiButtonRadius,backgroundColor:this.props.emojiButtonColors?this.props.emojiButtonColors[(r-1)%this.props.emojiButtonColors.length]:void 0}}),d(Pe,{emoji:t,set:this.props.set,size:this.props.emojiSize,skin:s,spritesheet:!0,getSpritesheetURL:this.props.getSpritesheetURL})]})},a)}renderSearch(){const t=this.props.previewPosition=="none"||this.props.skinTonePosition=="search";return d("div",{children:[d("div",{class:"spacer"}),d("div",{class:"flex flex-middle",children:[d("div",{class:"search relative flex-grow",children:[d("input",{type:"search",ref:this.refs.searchInput,placeholder:I.search,onClick:this.handleSearchClick,onInput:this.handleSearchInput,onKeyDown:this.handleSearchKeyDown,autoComplete:"off"}),d("span",{class:"icon loupe flex",children:ye.search.loupe}),this.state.searchResults&&d("button",{title:"Clear","aria-label":"Clear",type:"button",class:"icon delete flex",onClick:this.clearSearch,onMouseDown:this.preventDefault,children:ye.search.delete})]}),t&&this.renderSkinToneButton()]})]})}renderSearchResults(){const{searchResults:t}=this.state;return t?d("div",{class:"category",ref:this.refs.search,children:[d("div",{class:`sticky padding-small align-${this.dir[0]}`,children:I.categories.search}),d("div",{children:t.length?t.map((n,r)=>d("div",{class:"flex",children:n.map((o,i)=>this.renderEmojiButton(o,{pos:[r,i],posinset:r*this.props.perLine+i+1,grid:t}))})):d("div",{class:`padding-small align-${this.dir[0]}`,children:this.props.onAddCustomEmoji&&d("a",{onClick:this.props.onAddCustomEmoji,children:I.add_custom})})})]}):null}renderCategories(){const{categories:t}=L,n=!!this.state.searchResults,r=this.getPerLine();return d("div",{style:{visibility:n?"hidden":void 0,display:n?"none":void 0,height:"100%"},children:t.map(o=>{const{root:i,rows:s}=this.refs.categories.get(o.id);return d("div",{"data-id":o.target?o.target.id:o.id,class:"category",ref:i,children:[d("div",{class:`sticky padding-small align-${this.dir[0]}`,children:o.name||I.categories[o.id]}),d("div",{class:"relative",style:{height:s.length*this.props.emojiButtonSize},children:s.map((l,c)=>{const u=l.index-l.index%he.rowsPerRender,a=this.state.visibleRows[u],g="current"in l?l:void 0;if(!a&&!g)return null;const f=c*r,v=f+r,p=o.emojis.slice(f,v);return p.length<r&&p.push(...new Array(r-p.length)),d("div",{"data-index":l.index,ref:g,class:"flex row",style:{top:c*this.props.emojiButtonSize},children:a&&p.map((b,w)=>{if(!b)return d("div",{style:{width:this.props.emojiButtonSize,height:this.props.emojiButtonSize}});const $=ie.get(b);return this.renderEmojiButton($,{pos:[l.index,w],posinset:l.posinset+w,grid:this.grid})})},l.index)})})]})})})}renderSkinToneButton(){return this.props.skinTonePosition=="none"?null:d("div",{class:"flex flex-auto flex-center flex-middle",style:{position:"relative",width:this.props.emojiButtonSize,height:this.props.emojiButtonSize},children:d("button",{type:"button",ref:this.refs.skinToneButton,class:"skin-tone-button flex flex-auto flex-center flex-middle","aria-selected":this.state.showSkins?"":void 0,"aria-label":I.skins.choose,title:I.skins.choose,onClick:this.openSkins,style:{width:this.props.emojiSize,height:this.props.emojiSize},children:d("span",{class:`skin-tone skin-tone-${this.state.skin}`})})})}renderLiveRegion(){const t=this.getEmojiByPos(this.state.pos),n=t?t.name:"";return d("div",{"aria-live":"polite",class:"sr-only",children:n})}renderSkins(){const n=this.refs.skinToneButton.current.getBoundingClientRect(),r=this.base.getBoundingClientRect(),o={};return this.dir=="ltr"?o.right=r.right-n.right-3:o.left=n.left-r.left-3,this.props.previewPosition=="bottom"&&this.props.skinTonePosition=="preview"?o.bottom=r.bottom-n.top+6:(o.top=n.bottom-r.top+3,o.bottom="auto"),d("div",{ref:this.refs.menu,role:"radiogroup",dir:this.dir,"aria-label":I.skins.choose,class:"menu hidden","data-position":o.top?"top":"bottom",style:o,children:[...Array(6).keys()].map(i=>{const s=i+1,l=this.state.skin==s;return d("div",{children:[d("input",{type:"radio",name:"skin-tone",value:s,"aria-label":I.skins[s],ref:l?this.refs.skinToneRadio:null,defaultChecked:l,onChange:()=>this.handleSkinMouseOver(s),onKeyDown:c=>{(c.code=="Enter"||c.code=="Space"||c.code=="Tab")&&(c.preventDefault(),this.handleSkinClick(s))}}),d("button",{"aria-hidden":"true",tabindex:"-1",onClick:()=>this.handleSkinClick(s),onMouseEnter:()=>this.handleSkinMouseOver(s),onMouseLeave:()=>this.handleSkinMouseOver(),class:"option flex flex-grow flex-middle",children:[d("span",{class:`skin-tone skin-tone-${s}`}),d("span",{class:"margin-small-lr",children:I.skins[s]})]})]})})})}render(){const t=this.props.perLine*this.props.emojiButtonSize;return d("section",{id:"root",class:"flex flex-column",dir:this.dir,style:{width:this.props.dynamicWidth?"100%":`calc(${t}px + (var(--padding) + var(--sidebar-width)))`},"data-emoji-set":this.props.set,"data-theme":this.state.theme,"data-menu":this.state.showSkins?"":void 0,children:[this.props.previewPosition=="top"&&this.renderPreview(),this.props.navPosition=="top"&&this.renderNav(),this.props.searchPosition=="sticky"&&d("div",{class:"padding-lr",children:this.renderSearch()}),d("div",{ref:this.refs.scroll,class:"scroll flex-grow padding-lr",children:d("div",{style:{width:this.props.dynamicWidth?"100%":t,height:"100%"},children:[this.props.searchPosition=="static"&&this.renderSearch(),this.renderSearchResults(),this.renderCategories()]})}),this.props.navPosition=="bottom"&&this.renderNav(),this.props.previewPosition=="bottom"&&this.renderPreview(),this.state.showSkins&&this.renderSkins(),this.renderLiveRegion()]})}constructor(t){super(),W(this,"handleClickOutside",n=>{const{element:r}=this.props;n.target!=r&&(this.state.showSkins&&this.closeSkins(),this.props.onClickOutside&&this.props.onClickOutside(n))}),W(this,"handleBaseClick",n=>{this.state.showSkins&&(n.target.closest(".menu")||(n.preventDefault(),n.stopImmediatePropagation(),this.closeSkins()))}),W(this,"handleBaseKeydown",n=>{this.state.showSkins&&n.key=="Escape"&&(n.preventDefault(),n.stopImmediatePropagation(),this.closeSkins())}),W(this,"handleSearchClick",()=>{this.getEmojiByPos(this.state.pos)&&this.setState({pos:[-1,-1]})}),W(this,"handleSearchInput",async()=>{const n=this.refs.searchInput.current;if(!n)return;const{value:r}=n,o=await ie.search(r),i=()=>{this.refs.scroll.current&&(this.refs.scroll.current.scrollTop=0)};if(!o)return this.setState({searchResults:o,pos:[-1,-1]},i);const s=n.selectionStart==n.value.length?[0,0]:[-1,-1],l=[];l.setsize=o.length;let c=null;for(let u of o)(!l.length||c.length==this.getPerLine())&&(c=[],c.__categoryId="search",c.__index=l.length,l.push(c)),c.push(u);this.ignoreMouse(),this.setState({searchResults:l,pos:s},i)}),W(this,"handleSearchKeyDown",n=>{const r=n.currentTarget;switch(n.stopImmediatePropagation(),n.key){case"ArrowLeft":this.navigate({e:n,input:r,left:!0});break;case"ArrowRight":this.navigate({e:n,input:r,right:!0});break;case"ArrowUp":this.navigate({e:n,input:r,up:!0});break;case"ArrowDown":this.navigate({e:n,input:r,down:!0});break;case"Enter":n.preventDefault(),this.handleEmojiClick({e:n,pos:this.state.pos});break;case"Escape":n.preventDefault(),this.state.searchResults?this.clearSearch():this.unfocusSearch();break}}),W(this,"clearSearch",()=>{const n=this.refs.searchInput.current;n&&(n.value="",n.focus(),this.handleSearchInput())}),W(this,"handleCategoryClick",({category:n,i:r})=>{this.scrollTo(r==0?{row:-1}:{categoryId:n.id})}),W(this,"openSkins",n=>{const{currentTarget:r}=n,o=r.getBoundingClientRect();this.setState({showSkins:o},async()=>{await Bn(2);const i=this.refs.menu.current;i&&(i.classList.remove("hidden"),this.refs.skinToneRadio.current.focus(),this.base.addEventListener("click",this.handleBaseClick,!0),this.base.addEventListener("keydown",this.handleBaseKeydown,!0))})}),this.observers=[],this.state={pos:[-1,-1],perLine:this.initDynamicPerLine(t),visibleRows:{0:!0},...this.getInitialState(t)}}}class Oe extends Wn{async connectedCallback(){const t=Ht(this.props,X,this);t.element=this,t.ref=n=>{this.component=n},await xe(t),!this.disconnected&&Pt(d(or,{...t}),this.shadowRoot)}constructor(t){super(t,{styles:yt(Vt)})}}W(Oe,"Props",X);typeof customElements<"u"&&!customElements.get("em-emoji-picker")&&customElements.define("em-emoji-picker",Oe);var Vt={};Vt=`:host {
  width: min-content;
  height: 435px;
  min-height: 230px;
  border-radius: var(--border-radius);
  box-shadow: var(--shadow);
  --border-radius: 10px;
  --category-icon-size: 18px;
  --font-family: -apple-system, BlinkMacSystemFont, "Helvetica Neue", sans-serif;
  --font-size: 15px;
  --preview-placeholder-size: 21px;
  --preview-title-size: 1.1em;
  --preview-subtitle-size: .9em;
  --shadow-color: 0deg 0% 0%;
  --shadow: .3px .5px 2.7px hsl(var(--shadow-color) / .14), .4px .8px 1px -3.2px hsl(var(--shadow-color) / .14), 1px 2px 2.5px -4.5px hsl(var(--shadow-color) / .14);
  display: flex;
}

[data-theme="light"] {
  --em-rgb-color: var(--rgb-color, 34, 36, 39);
  --em-rgb-accent: var(--rgb-accent, 34, 102, 237);
  --em-rgb-background: var(--rgb-background, 255, 255, 255);
  --em-rgb-input: var(--rgb-input, 255, 255, 255);
  --em-color-border: var(--color-border, rgba(0, 0, 0, .05));
  --em-color-border-over: var(--color-border-over, rgba(0, 0, 0, .1));
}

[data-theme="dark"] {
  --em-rgb-color: var(--rgb-color, 222, 222, 221);
  --em-rgb-accent: var(--rgb-accent, 58, 130, 247);
  --em-rgb-background: var(--rgb-background, 21, 22, 23);
  --em-rgb-input: var(--rgb-input, 0, 0, 0);
  --em-color-border: var(--color-border, rgba(255, 255, 255, .1));
  --em-color-border-over: var(--color-border-over, rgba(255, 255, 255, .2));
}

#root {
  --color-a: rgb(var(--em-rgb-color));
  --color-b: rgba(var(--em-rgb-color), .65);
  --color-c: rgba(var(--em-rgb-color), .45);
  --padding: 12px;
  --padding-small: calc(var(--padding) / 2);
  --sidebar-width: 16px;
  --duration: 225ms;
  --duration-fast: 125ms;
  --duration-instant: 50ms;
  --easing: cubic-bezier(.4, 0, .2, 1);
  width: 100%;
  text-align: left;
  border-radius: var(--border-radius);
  background-color: rgb(var(--em-rgb-background));
  position: relative;
}

@media (prefers-reduced-motion) {
  #root {
    --duration: 0;
    --duration-fast: 0;
    --duration-instant: 0;
  }
}

#root[data-menu] button {
  cursor: auto;
}

#root[data-menu] .menu button {
  cursor: pointer;
}

:host, #root, input, button {
  color: rgb(var(--em-rgb-color));
  font-family: var(--font-family);
  font-size: var(--font-size);
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  line-height: normal;
}

*, :before, :after {
  box-sizing: border-box;
  min-width: 0;
  margin: 0;
  padding: 0;
}

.relative {
  position: relative;
}

.flex {
  display: flex;
}

.flex-auto {
  flex: none;
}

.flex-center {
  justify-content: center;
}

.flex-column {
  flex-direction: column;
}

.flex-grow {
  flex: auto;
}

.flex-middle {
  align-items: center;
}

.flex-wrap {
  flex-wrap: wrap;
}

.padding {
  padding: var(--padding);
}

.padding-t {
  padding-top: var(--padding);
}

.padding-lr {
  padding-left: var(--padding);
  padding-right: var(--padding);
}

.padding-r {
  padding-right: var(--padding);
}

.padding-small {
  padding: var(--padding-small);
}

.padding-small-b {
  padding-bottom: var(--padding-small);
}

.padding-small-lr {
  padding-left: var(--padding-small);
  padding-right: var(--padding-small);
}

.margin {
  margin: var(--padding);
}

.margin-r {
  margin-right: var(--padding);
}

.margin-l {
  margin-left: var(--padding);
}

.margin-small-l {
  margin-left: var(--padding-small);
}

.margin-small-lr {
  margin-left: var(--padding-small);
  margin-right: var(--padding-small);
}

.align-l {
  text-align: left;
}

.align-r {
  text-align: right;
}

.color-a {
  color: var(--color-a);
}

.color-b {
  color: var(--color-b);
}

.color-c {
  color: var(--color-c);
}

.ellipsis {
  white-space: nowrap;
  max-width: 100%;
  width: auto;
  text-overflow: ellipsis;
  overflow: hidden;
}

.sr-only {
  width: 1px;
  height: 1px;
  position: absolute;
  top: auto;
  left: -10000px;
  overflow: hidden;
}

a {
  cursor: pointer;
  color: rgb(var(--em-rgb-accent));
}

a:hover {
  text-decoration: underline;
}

.spacer {
  height: 10px;
}

[dir="rtl"] .scroll {
  padding-left: 0;
  padding-right: var(--padding);
}

.scroll {
  padding-right: 0;
  overflow-x: hidden;
  overflow-y: auto;
}

.scroll::-webkit-scrollbar {
  width: var(--sidebar-width);
  height: var(--sidebar-width);
}

.scroll::-webkit-scrollbar-track {
  border: 0;
}

.scroll::-webkit-scrollbar-button {
  width: 0;
  height: 0;
  display: none;
}

.scroll::-webkit-scrollbar-corner {
  background-color: rgba(0, 0, 0, 0);
}

.scroll::-webkit-scrollbar-thumb {
  min-height: 20%;
  min-height: 65px;
  border: 4px solid rgb(var(--em-rgb-background));
  border-radius: 8px;
}

.scroll::-webkit-scrollbar-thumb:hover {
  background-color: var(--em-color-border-over) !important;
}

.scroll:hover::-webkit-scrollbar-thumb {
  background-color: var(--em-color-border);
}

.sticky {
  z-index: 1;
  background-color: rgba(var(--em-rgb-background), .9);
  -webkit-backdrop-filter: blur(4px);
  backdrop-filter: blur(4px);
  font-weight: 500;
  position: sticky;
  top: -1px;
}

[dir="rtl"] .search input[type="search"] {
  padding: 10px 2.2em 10px 2em;
}

[dir="rtl"] .search .loupe {
  left: auto;
  right: .7em;
}

[dir="rtl"] .search .delete {
  left: .7em;
  right: auto;
}

.search {
  z-index: 2;
  position: relative;
}

.search input, .search button {
  font-size: calc(var(--font-size)  - 1px);
}

.search input[type="search"] {
  width: 100%;
  background-color: var(--em-color-border);
  transition-duration: var(--duration);
  transition-property: background-color, box-shadow;
  transition-timing-function: var(--easing);
  border: 0;
  border-radius: 10px;
  outline: 0;
  padding: 10px 2em 10px 2.2em;
  display: block;
}

.search input[type="search"]::-ms-input-placeholder {
  color: inherit;
  opacity: .6;
}

.search input[type="search"]::placeholder {
  color: inherit;
  opacity: .6;
}

.search input[type="search"], .search input[type="search"]::-webkit-search-decoration, .search input[type="search"]::-webkit-search-cancel-button, .search input[type="search"]::-webkit-search-results-button, .search input[type="search"]::-webkit-search-results-decoration {
  -webkit-appearance: none;
  -ms-appearance: none;
  appearance: none;
}

.search input[type="search"]:focus {
  background-color: rgb(var(--em-rgb-input));
  box-shadow: inset 0 0 0 1px rgb(var(--em-rgb-accent)), 0 1px 3px rgba(65, 69, 73, .2);
}

.search .icon {
  z-index: 1;
  color: rgba(var(--em-rgb-color), .7);
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
}

.search .loupe {
  pointer-events: none;
  left: .7em;
}

.search .delete {
  right: .7em;
}

svg {
  fill: currentColor;
  width: 1em;
  height: 1em;
}

button {
  -webkit-appearance: none;
  -ms-appearance: none;
  appearance: none;
  cursor: pointer;
  color: currentColor;
  background-color: rgba(0, 0, 0, 0);
  border: 0;
}

#nav {
  z-index: 2;
  padding-top: 12px;
  padding-bottom: 12px;
  padding-right: var(--sidebar-width);
  position: relative;
}

#nav button {
  color: var(--color-b);
  transition: color var(--duration) var(--easing);
}

#nav button:hover {
  color: var(--color-a);
}

#nav svg, #nav img {
  width: var(--category-icon-size);
  height: var(--category-icon-size);
}

#nav[dir="rtl"] .bar {
  left: auto;
  right: 0;
}

#nav .bar {
  width: 100%;
  height: 3px;
  background-color: rgb(var(--em-rgb-accent));
  transition: transform var(--duration) var(--easing);
  border-radius: 3px 3px 0 0;
  position: absolute;
  bottom: -12px;
  left: 0;
}

#nav button[aria-selected] {
  color: rgb(var(--em-rgb-accent));
}

#preview {
  z-index: 2;
  padding: calc(var(--padding)  + 4px) var(--padding);
  padding-right: var(--sidebar-width);
  position: relative;
}

#preview .preview-placeholder {
  font-size: var(--preview-placeholder-size);
}

#preview .preview-title {
  font-size: var(--preview-title-size);
}

#preview .preview-subtitle {
  font-size: var(--preview-subtitle-size);
}

#nav:before, #preview:before {
  content: "";
  height: 2px;
  position: absolute;
  left: 0;
  right: 0;
}

#nav[data-position="top"]:before, #preview[data-position="top"]:before {
  background: linear-gradient(to bottom, var(--em-color-border), transparent);
  top: 100%;
}

#nav[data-position="bottom"]:before, #preview[data-position="bottom"]:before {
  background: linear-gradient(to top, var(--em-color-border), transparent);
  bottom: 100%;
}

.category:last-child {
  min-height: calc(100% + 1px);
}

.category button {
  font-family: -apple-system, BlinkMacSystemFont, Helvetica Neue, sans-serif;
  position: relative;
}

.category button > * {
  position: relative;
}

.category button .background {
  opacity: 0;
  background-color: var(--em-color-border);
  transition: opacity var(--duration-fast) var(--easing) var(--duration-instant);
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
}

.category button:hover .background {
  transition-duration: var(--duration-instant);
  transition-delay: 0s;
}

.category button[aria-selected] .background {
  opacity: 1;
}

.category button[data-keyboard] .background {
  transition: none;
}

.row {
  width: 100%;
  position: absolute;
  top: 0;
  left: 0;
}

.skin-tone-button {
  border: 1px solid rgba(0, 0, 0, 0);
  border-radius: 100%;
}

.skin-tone-button:hover {
  border-color: var(--em-color-border);
}

.skin-tone-button:active .skin-tone {
  transform: scale(.85) !important;
}

.skin-tone-button .skin-tone {
  transition: transform var(--duration) var(--easing);
}

.skin-tone-button[aria-selected] {
  background-color: var(--em-color-border);
  border-top-color: rgba(0, 0, 0, .05);
  border-bottom-color: rgba(0, 0, 0, 0);
  border-left-width: 0;
  border-right-width: 0;
}

.skin-tone-button[aria-selected] .skin-tone {
  transform: scale(.9);
}

.menu {
  z-index: 2;
  white-space: nowrap;
  border: 1px solid var(--em-color-border);
  background-color: rgba(var(--em-rgb-background), .9);
  -webkit-backdrop-filter: blur(4px);
  backdrop-filter: blur(4px);
  transition-property: opacity, transform;
  transition-duration: var(--duration);
  transition-timing-function: var(--easing);
  border-radius: 10px;
  padding: 4px;
  position: absolute;
  box-shadow: 1px 1px 5px rgba(0, 0, 0, .05);
}

.menu.hidden {
  opacity: 0;
}

.menu[data-position="bottom"] {
  transform-origin: 100% 100%;
}

.menu[data-position="bottom"].hidden {
  transform: scale(.9)rotate(-3deg)translateY(5%);
}

.menu[data-position="top"] {
  transform-origin: 100% 0;
}

.menu[data-position="top"].hidden {
  transform: scale(.9)rotate(3deg)translateY(-5%);
}

.menu input[type="radio"] {
  clip: rect(0 0 0 0);
  width: 1px;
  height: 1px;
  border: 0;
  margin: 0;
  padding: 0;
  position: absolute;
  overflow: hidden;
}

.menu input[type="radio"]:checked + .option {
  box-shadow: 0 0 0 2px rgb(var(--em-rgb-accent));
}

.option {
  width: 100%;
  border-radius: 6px;
  padding: 4px 6px;
}

.option:hover {
  color: #fff;
  background-color: rgb(var(--em-rgb-accent));
}

.skin-tone {
  width: 16px;
  height: 16px;
  border-radius: 100%;
  display: inline-block;
  position: relative;
  overflow: hidden;
}

.skin-tone:after {
  content: "";
  mix-blend-mode: overlay;
  background: linear-gradient(rgba(255, 255, 255, .2), rgba(0, 0, 0, 0));
  border: 1px solid rgba(0, 0, 0, .8);
  border-radius: 100%;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  box-shadow: inset 0 -2px 3px #000, inset 0 1px 2px #fff;
}

.skin-tone-1 {
  background-color: #ffc93a;
}

.skin-tone-2 {
  background-color: #ffdab7;
}

.skin-tone-3 {
  background-color: #e7b98f;
}

.skin-tone-4 {
  background-color: #c88c61;
}

.skin-tone-5 {
  background-color: #a46134;
}

.skin-tone-6 {
  background-color: #5d4437;
}

[data-index] {
  justify-content: space-between;
}

[data-emoji-set="twitter"] .skin-tone:after {
  box-shadow: none;
  border-color: rgba(0, 0, 0, .5);
}

[data-emoji-set="twitter"] .skin-tone-1 {
  background-color: #fade72;
}

[data-emoji-set="twitter"] .skin-tone-2 {
  background-color: #f3dfd0;
}

[data-emoji-set="twitter"] .skin-tone-3 {
  background-color: #eed3a8;
}

[data-emoji-set="twitter"] .skin-tone-4 {
  background-color: #cfad8d;
}

[data-emoji-set="twitter"] .skin-tone-5 {
  background-color: #a8805d;
}

[data-emoji-set="twitter"] .skin-tone-6 {
  background-color: #765542;
}

[data-emoji-set="google"] .skin-tone:after {
  box-shadow: inset 0 0 2px 2px rgba(0, 0, 0, .4);
}

[data-emoji-set="google"] .skin-tone-1 {
  background-color: #f5c748;
}

[data-emoji-set="google"] .skin-tone-2 {
  background-color: #f1d5aa;
}

[data-emoji-set="google"] .skin-tone-3 {
  background-color: #d4b48d;
}

[data-emoji-set="google"] .skin-tone-4 {
  background-color: #aa876b;
}

[data-emoji-set="google"] .skin-tone-5 {
  background-color: #916544;
}

[data-emoji-set="google"] .skin-tone-6 {
  background-color: #61493f;
}

[data-emoji-set="facebook"] .skin-tone:after {
  border-color: rgba(0, 0, 0, .4);
  box-shadow: inset 0 -2px 3px #000, inset 0 1px 4px #fff;
}

[data-emoji-set="facebook"] .skin-tone-1 {
  background-color: #f5c748;
}

[data-emoji-set="facebook"] .skin-tone-2 {
  background-color: #f1d5aa;
}

[data-emoji-set="facebook"] .skin-tone-3 {
  background-color: #d4b48d;
}

[data-emoji-set="facebook"] .skin-tone-4 {
  background-color: #aa876b;
}

[data-emoji-set="facebook"] .skin-tone-5 {
  background-color: #916544;
}

[data-emoji-set="facebook"] .skin-tone-6 {
  background-color: #61493f;
}

`;function ar(e){const t=_.useRef(null),n=_.useRef(null);return n.current&&n.current.update(e),_.useEffect(()=>(n.current=new Oe({...e,ref:t}),()=>{n.current=null}),[]),j.createElement("div",{ref:t})}function Wt(e){var t=e.theme,n=e.onSelectEmoji,r=e.disableRecent,o=e.customEmojis,i=_.useMemo(function(){var s=[];return r||s.push("frequent"),s=[].concat(pt(s),["people","nature","foods","activity","places","objects","symbols","flags"]),s},[r]);return j.createElement(ar,{data:void 0,theme:t,previewPosition:"none",onEmojiSelect:n,custom:o,categories:i,set:"apple"})}Wt.propTypes={theme:ce.oneOf(["light","dark","auto"]),onSelectEmoji:ce.func,disableRecent:ce.bool,customEmojis:ce.array};var sr=_.memo(Wt);function ht(e){var t=e.showPicker,n=e.theme,r=e.handleSelectEmoji,o=e.disableRecent,i=e.customEmojis;return j.createElement("div",{className:"react-emoji-picker--container"},t&&j.createElement("div",{className:"react-emoji-picker--wrapper",onClick:function(l){return l.stopPropagation()}},j.createElement("div",{className:"react-emoji-picker"},j.createElement(sr,{theme:n,onSelectEmoji:r,disableRecent:o,customEmojis:i}))))}var cr=function(t){var n=t.theme,r=t.keepOpened,o=t.disableRecent,i=t.customEmojis,s=t.addSanitizeFn,l=t.addPolluteFn,c=t.appendContent,u=t.buttonElement,a=_.useState(!1),g=J(a,2),f=g[0],v=g[1],p=_.useState(),b=J(p,2),w=b[0],$=b[1];_.useEffect(function(){s(fn)},[s]),_.useEffect(function(){l(vt)},[l]),_.useEffect(function(){function C(z){var U=z.target;U.classList.contains("react-input-emoji--button")||U.classList.contains("react-input-emoji--button--icon")||v(!1)}return document.addEventListener("click",C),function(){document.removeEventListener("click",C)}},[]);function M(C){C.stopPropagation(),C.preventDefault(),v(function(z){return!z})}function E(C){c(dn(C)),r||v(function(z){return!z})}return _.useEffect(function(){u!=null&&u.style&&(u.style.position="relative",$(u))},[u]),w?Yt.createPortal(j.createElement(j.Fragment,null,j.createElement(ht,{showPicker:f,theme:n,handleSelectEmoji:E,disableRecent:o,customEmojis:i}),j.createElement(Ve,{showPicker:f,toggleShowPicker:M,buttonElement:w})),w):j.createElement(j.Fragment,null,j.createElement(ht,{showPicker:f,theme:n,handleSelectEmoji:E,disableRecent:o,customEmojis:i}),j.createElement(Ve,{showPicker:f,toggleShowPicker:M}))};function lr(){var e=qt();if(!e)return null;var t=e.text.substring(e.begin,e.end);return t||null}function ur(){var e=qt();e&&e.element.deleteData(e.begin,e.end-e.begin)}function qt(){var e=Ae();if(!e)return null;var t=e.element,n=e.caretOffset,r=t.textContent,o=r.lastIndexOf("@");return o===-1||o>=n||o!==0&&r[o-1]!==" "?null:{begin:o,end:n,text:r,element:t}}function Ae(){var e=dr();if(e===null)return null;var t=0;if(typeof window.getSelection<"u"){var n=window.getSelection().getRangeAt(0),r=n.cloneRange();r.selectNodeContents(e),r.setEnd(n.endContainer,n.endOffset),t=r.toString().length}else if(typeof document.selection<"u"&&document.selection.type!="Control"){var o=document.selection.createRange(),i=document.body.createTextRange();i.moveToElementText(e),i.setEndPoint("EndToEnd",o),t=i.text.length}return{element:e,caretOffset:t}}function dr(){var e=document.getSelection().anchorNode;return(e==null?void 0:e.nodeType)==3?e:null}function fr(e){var t=_.useState(!1),n=J(t,2),r=n[0],o=n[1],i=_.useState([]),s=J(i,2),l=s[0],c=s[1],u=_.useState(null),a=J(u,2),g=a[0],f=a[1],v=_.useCallback(function(){ur(),c([])},[]),p=_.useCallback(Fe(le().mark(function $(){var M,E;return le().wrap(function(z){for(;;)switch(z.prev=z.next){case 0:if(M=lr(),f(M),M!==null){z.next=6;break}c([]),z.next=12;break;case 6:return o(!0),z.next=9,e(M);case 9:E=z.sent,o(!1),c(E);case 12:case"end":return z.stop()}},$)})),[e]),b=_.useCallback(function(){var $=Fe(le().mark(function M(E){var C,z;return le().wrap(function(N){for(;;)switch(N.prev=N.next){case 0:if(typeof e=="function"){N.next=2;break}return N.abrupt("return");case 2:E.key==="Backspace"&&(C=Ae())!==null&&C!==void 0&&C.element.parentElement.hasAttribute("data-mention-id")?(z=Ae(),z.element.parentElement.remove()):["ArrowUp","ArrowDown","Esc","Escape"].includes(E.key)||p();case 3:case"end":return N.stop()}},M)}));return function(M){return $.apply(this,arguments)}}(),[p,e]),w=_.useCallback(function(){p()},[p]);return{mentionSearchText:g,mentionUsers:l,onKeyUp:b,onFocus:w,onSelectUser:v,loading:r}}var hr=function(t,n){var r=t.users,o=t.mentionSearchText,i=t.onSelect,s=t.addEventListener,l=_.useState(0),c=J(l,2),u=c[0],a=c[1];_.useImperativeHandle(n,function(){return{prevUser:function(){a(function(b){return b===0?0:b-1})},nextUser:function(){a(function(b){return b===r.length-1?r.length-1:b+1})}}}),_.useEffect(function(){a(0)},[r]);function g(p,b){return'<span class="react-input-emoji--mention--item--name__selected" data-testid="metion-selected-word">'.concat(p,"</span>").concat(b)}var f=_.useMemo(function(){var p=o?o.substring(1).toLocaleLowerCase():"";return r.map(function(b){var w=b.name;if(o&&o.length>1)if(b.name.toLowerCase().startsWith(p))w=g(b.name.substring(0,p.length),b.name.substring(p.length));else{var $=b.name.split(" ");w=$.map(function(M){return M.toLocaleLowerCase().startsWith(p)?g(M.substring(0,p.length),M.substring(p.length)):M}).join(" ")}return Ie(Ie({},b),{},{nameHtml:w})})},[o,r]);function v(p){return function(b){b.stopPropagation(),b.preventDefault(),i(p)}}return _.useEffect(function(){var p=s("enter",function(b){b.stopPropagation(),b.preventDefault(),i(f[u])});return function(){p()}},[s,i,u,f]),j.createElement("ul",{className:"react-input-emoji--mention--list","data-testid":"mention-user-list"},f.map(function(p,b){return j.createElement("li",{key:p.id},j.createElement("button",{type:"button",onClick:v(p),className:"react-input-emoji--mention--item".concat(u===b?" react-input-emoji--mention--item__selected":""),onMouseOver:function(){return a(b)}},j.createElement("img",{className:"react-input-emoji--mention--item--img",src:p.image}),j.createElement("div",{className:"react-input-emoji--mention--item--name",dangerouslySetInnerHTML:{__html:p.nameHtml}})))}))},Kt=_.forwardRef(hr);Kt.propTypes={users:ce.array.isRequired};var pr=function(t){var n=t.searchMention,r=t.addEventListener,o=t.appendContent,i=t.addSanitizeFn,s=_.useRef(null),l=_.useState(!1),c=J(l,2),u=c[0],a=c[1],g=fr(n),f=g.mentionSearchText,v=g.mentionUsers,p=g.loading,b=g.onKeyUp,w=g.onFocus,$=g.onSelectUser;_.useEffect(function(){i(function(E){var C=document.createElement("div");C.innerHTML=E;var z=Array.prototype.slice.call(C.querySelectorAll(".react-input-emoji--mention--text"));return z.forEach(function(U){C.innerHTML=C.innerHTML.replace(U.outerHTML,"@[".concat(U.dataset.mentionName,"](userId:").concat(U.dataset.mentionId,")"))}),C.innerHTML})},[i]),_.useEffect(function(){a(v.length>0)},[v]),_.useEffect(function(){function E(){a(!1)}return document.addEventListener("click",E),function(){document.removeEventListener("click",E)}},[]),_.useEffect(function(){var E=r("keyUp",b);return function(){E()}},[r,b]),_.useEffect(function(){function E(z){switch(z.key){case"Esc":case"Escape":a(!1);break}}var C=r("keyDown",E);return function(){C()}},[r]),_.useEffect(function(){var E=r("focus",w);return function(){E()}},[r,w]),_.useEffect(function(){if(u){var E=r("arrowUp",function(z){z.stopPropagation(),z.preventDefault(),s.current.prevUser()}),C=r("arrowDown",function(z){z.stopPropagation(),z.preventDefault(),s.current.nextUser()});return function(){E(),C()}}},[r,u]);function M(E){$(),o('<span class="react-input-emoji--mention--text" data-mention-id="'.concat(E.id,'" data-mention-name="').concat(E.name,'">@').concat(E.name,"</span> "))}return j.createElement(j.Fragment,null,p?j.createElement("div",{className:"react-input-emoji--mention--container"},j.createElement("div",{className:"react-input-emoji--mention--loading"},j.createElement("div",{className:"react-input-emoji--mention--loading--spinner"},"Loading..."))):u&&j.createElement("div",{className:"react-input-emoji--mention--container",onClick:function(C){return C.stopPropagation()}},j.createElement(Kt,{ref:s,mentionSearchText:f,users:v,onSelect:M,addEventListener:r})))};function te(){var e=[];return{subscribe:function(n){return e.push(n),function(){e=e.filter(function(r){return r!==n})}},publish:function(n){e.forEach(function(r){return r(n)})},get currentListerners(){return e}}}function mr(){var e=_.useMemo(function(){return{keyDown:te(),keyUp:te(),arrowUp:te(),arrowDown:te(),enter:te(),focus:te(),blur:te()}},[]),t=_.useCallback(function(n,r){return e[n].subscribe(r)},[e]);return{addEventListener:t,listeners:e}}function vr(){var e=_.useRef([]),t=_.useCallback(function(r){e.current.push(r)},[]),n=_.useCallback(function(r){var o=e.current.reduce(function(i,s){return s(i)},r);return o},[]);return{addPolluteFn:t,pollute:n}}function gr(e,t){var n=e.onChange,r=e.onEnter,o=e.onResize,i=e.onClick,s=e.onFocus,l=e.onBlur,c=e.onKeyDown,u=e.theme,a=e.cleanOnEnter,g=e.placeholder,f=e.maxLength,v=e.keepOpened,p=e.inputClass,b=e.disableRecent,w=e.tabIndex,$=e.value,M=e.customEmojis,E=e.searchMention,C=e.buttonElement,z=e.borderRadius,U=e.borderColor,N=e.fontSize,se=e.fontFamily,H=_.useRef(null),m=mr(),h=m.addEventListener,y=m.listeners,S=_t(),k=S.addSanitizeFn,T=S.sanitize,R=S.sanitizedTextRef,B=vr(),V=B.addPolluteFn,P=B.pollute,D=_.useCallback(function(){var A=arguments.length>0&&arguments[0]!==void 0?arguments[0]:"";H.current!==null&&(H.current.html=vt(A),R.current=A)},[R]),F=_.useCallback(function(A){D(A)},[D]),Q=vn(H,o,n);mn({ref:t,setValue:F,textInputRef:H,emitChange:Q}),_.useEffect(function(){R.current!==$&&F($)},[R,F,$]),_.useEffect(function(){D()},[D]),_.useEffect(function(){function A(re){if(typeof f<"u"&&re.key!=="Backspace"&&H.current!==null&&Ne(H.current)>=f&&re.preventDefault(),re.key==="Enter"&&H.current){re.preventDefault();var Xt=T(H.current.html);return Q(R.current),typeof r=="function"&&y.enter.currentListerners.length===0&&r(Xt),a&&y.enter.currentListerners.length===0&&D(""),typeof c=="function"&&c(re.nativeEvent),!1}return typeof c=="function"&&c(re.nativeEvent),!0}var q=h("keyDown",A);return function(){q()}},[h,a,Q,y.enter.currentListerners.length,f,r,c,T,R,D]),_.useEffect(function(){function A(){typeof i=="function"&&i(),typeof s=="function"&&s()}var q=h("focus",A);return function(){q()}},[h,i,s]),_.useEffect(function(){function A(){typeof l=="function"&&l()}var q=h("blur",A);return function(){q()}},[h,l]);function de(A){T(A),Q(R.current)}function ne(A){typeof f<"u"&&H.current!==null&&Ne(H.current)>=f||H.current!==null&&H.current.appendContent(A)}function ee(A){A.clipboardData.setData("text",R.current),A.preventDefault()}function Gt(A){A.preventDefault();var q;A.clipboardData&&(q=A.clipboardData.getData("text/plain"),q=P(q),document.execCommand("insertHTML",!1,q))}return j.createElement("div",{className:"react-emoji"},j.createElement(pr,{searchMention:E,addEventListener:h,appendContent:ne,addSanitizeFn:k}),j.createElement(_n,{ref:H,onCopy:ee,onPaste:Gt,onBlur:y.blur.publish,onFocus:y.focus.publish,onArrowUp:y.arrowUp.publish,onArrowDown:y.arrowDown.publish,onKeyUp:y.keyUp.publish,onKeyDown:y.keyDown.publish,onEnter:y.enter.publish,placeholder:g,style:{borderRadius:z,borderColor:U,fontSize:N,fontFamily:se},tabIndex:w,className:p,onChange:de}),j.createElement(cr,{theme:u,keepOpened:v,disableRecent:b,customEmojis:M,addSanitizeFn:k,addPolluteFn:V,appendContent:ne,buttonElement:C}))}var br=_.forwardRef(gr);br.defaultProps={theme:"auto",height:30,placeholder:"Type a message",borderRadius:21,borderColor:"#EAEAEA",fontSize:15,fontFamily:"sans-serif",tabIndex:0,customEmojis:[]};export{br as I};
