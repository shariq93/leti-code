import React, { useEffect, useState } from "react";

import MkdSDK from "../utils/MkdSDK";
import { GlobalContext, showToast } from "../globalContext";
import { useNavigate, useParams } from "react-router-dom";
import { AuthContext, tokenExpireError } from "../authContext";

import moment from "moment";

let sdk = new MkdSDK();

const ViewEmails = () => {
  const { dispatch } = React.useContext(AuthContext);
  const { dispatch: globalDispatch } = React.useContext(GlobalContext);
  const navigate = useNavigate();
  const [id, setId] = useState(0);
  const [fName, setFName] = useState("");
  const [lName, setLName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [preferredDate, setPreferredDate] = useState("");
  const [tuition, setTuition] = useState("");
  const [admission, setadmission] = useState("");
  const [requirement, setRequirement] = useState("");
  const [programs, setPrograms] = useState("");
  const [other, setOther] = useState("");
  const [message, setMessage] = useState("");

  const [contentValue, setContentValue] = React.useState("");

  const params = useParams();

  useEffect(function () {
    globalDispatch({
      type: "SETPATH",
      payload: {
        path: "contact_email",
      },
    });

    (async function () {
      try {
        sdk.setTable("contact_form");
        const result = await sdk.callRestAPI({ id: Number(params?.id) }, "GET");
        if (!result.error) {
          // setValue("page", result.model.page);
          setId(result.model.id);
          setFName(result.model.first_name);
          setLName(result.model.last_name);
          setEmail(result.model.email);
          setPhone(result.model.phone);
          setPreferredDate(result.model.preferred_date);
          setTuition(result.model.tuition);
          setadmission(result.model.admission);
          setRequirement(result.model.requirement);
          setPrograms(result.model.programs);
          setOther(result.model.other);
          setMessage(result.model.message);
        }
      } catch (error) {
        console.log("error", error);
        tokenExpireError(dispatch, error.message);
      }
    })();
  }, []);

  return (
    <div className=" shadow-md rounded   mx-auto p-5">
      <h4 className="text-xl font-bold">
        View {fName} {lName}'s Email
      </h4>
      <ul className="grid grid-cols-2 gap-5 mt-4">
        <li>Full Name</li>
        <li className="font-bold">
          {fName} {lName}
        </li>
        <li>Email</li>
        <li className="font-bold">{email}</li>
        <li>Phone Number</li>
        <li className="font-bold">{phone}</li>
        <li>Preferred Date</li>
        <li className="font-bold">{moment(preferredDate).format(
                                      "YYYY MM DD"
                                    )}</li>
        
        <li>Prefer Type of Communication</li>
        <li className="font-bold">
          <div className="flex gap-2 items-center p-1 rounded-lg bg-slate-200 w-fit">
            <span>
            {tuition ? "Tuition" : ""}
            </span>
            <span>
            {admission ? "Admission" : ""}
            </span>
            <span>
            {programs ? "Programs" : ""}
            </span>
            <span>
            {requirement ? "Requirement" : ""}
            </span>
            <span>
            {other ? "Other" : ""}
            </span>
          </div>
        </li>
        <li>Message</li>
        <li className="font-semibold">{message}</li>
       
      </ul>
    </div>
  );
};

export default ViewEmails;
