import React, { useState, useEffect } from "react";
import * as yup from "yup";
import MkdSDK from "../utils/MkdSDK";
import { useParams, useNavigate } from "react-router-dom";
import { GlobalContext, showToast } from "../globalContext";
import { tokenExpireError } from "../authContext";
import ReactQuill from "react-quill";
import { isImage } from "../utils/utils";
import "react-quill/dist/quill.snow.css";

const schema = yup.object({
  program_name: yup.string().required(),
  description: yup.string().required(),
  status: yup.string().required(),
  program_category: yup.string().required(),
  program_outcome: yup.string().required(),
  program_summary: yup.string().required(),
  program_cover: yup.string().required(),
});

const EditAdminProgram = () => {
  const { dispatch } = React.useContext(GlobalContext);
  const { dispatch: globalDispatch } = React.useContext(GlobalContext);
  const [fileObj, setFileObj] = React.useState({});

  const navigate = useNavigate();
  const { programId } = useParams();

  const [formData, setFormData] = useState({
    program_name: "",
    description: "",
    status: "",
    program_category: "",
    program_outcome: "",
    program_summary: "",
    program_cover:"",
    levels: [{ label: "", semesters: [""] }],
  });

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value,
    }));
  };

  const handleEditorChange = (field, value) => {
    setFormData((prevFormData) => ({
      ...prevFormData,
      [field]: value,
    }));
  };

  const handleLevelsChange = (levelIndex, value) => {
    setFormData((prevFormData) => {
      const updatedLevels = [...prevFormData.levels];
      updatedLevels[levelIndex].label = value;
      return {
        ...prevFormData,
        levels: updatedLevels,
      };
    });
  };


  const quillModules = {
    toolbar: [
      [{ header: [1, 2, 3, 4, 5, 6, false] }],
      ["bold", "italic", "underline", "strike"],
      [{ list: "ordered" }, { list: "bullet" }],
      ["link"],
      ["image"],
      [{ align: [] }], 
      ["html"],
    ],
  };
  
  const quillFormats = [
    "header",
    "bold",
    "italic",
    "underline",
    "strike",
    "list",
    "bullet",
    "link",
    "align", 
    "image",
    "html",
  ];

  const handleSemesterChange = (levelIndex, semesterIndex, value) => {
    setFormData((prevFormData) => {
      const updatedLevels = [...prevFormData.levels];
      updatedLevels[levelIndex].semesters[semesterIndex] = value;
      return {
        ...prevFormData,
        levels: updatedLevels,
      };
    });
  };

  const handleAddLevel = () => {
    setFormData((prevFormData) => ({
      ...prevFormData,
      levels: [...prevFormData.levels, { label: "", semesters: [""] }],
    }));
  };

  const handleRemoveLevel = (levelIndex) => {
    setFormData((prevFormData) => {
      const updatedLevels = [...prevFormData.levels];
      updatedLevels.splice(levelIndex, 1);
      return {
        ...prevFormData,
        levels: updatedLevels,
      };
    });
  };

  const handleAddSemester = (levelIndex) => {
    setFormData((prevFormData) => {
      const updatedLevels = [...prevFormData.levels];
      updatedLevels[levelIndex].semesters.push("");
      return {
        ...prevFormData,
        levels: updatedLevels,
      };
    });
  };

  const handleRemoveSemester = (levelIndex, semesterIndex) => {
    setFormData((prevFormData) => {
      const updatedLevels = [...prevFormData.levels];
      updatedLevels[levelIndex].semesters.splice(semesterIndex, 1);
      return {
        ...prevFormData,
        levels: updatedLevels,
      };
    });
  };


  const previewImage = (field, target) => {
    let tempFileObj = fileObj;
    tempFileObj[field] = {
      file: target.files[0],
      tempURL: URL.createObjectURL(target.files[0]),
    };
    setFileObj({ ...tempFileObj });
  };


  const onSubmit = async (event) => {
    event.preventDefault();
    let sdk = new MkdSDK();

    try {

      const uploadedFileUrls = {}; // Create an object to store uploaded file URLs

      for (let item in fileObj) {
        let formData = new FormData();
        formData.append("file", fileObj[item].file);
        let uploadResult = await sdk.uploadImage(formData);
        uploadedFileUrls[item] = uploadResult.url; // Store the uploaded URL in the object
      }

      const programData = {
        program_name: formData.program_name,
        status: formData.status,
        description: formData.description,
        program_category: formData.program_category,
        program_outcome: formData.program_outcome,
        program_summary: formData.program_summary,
        program_cover: uploadedFileUrls["program_cover"],
      };

      const programResult = await sdk.callRawAPI(
        `/v4/api/records/cms_programs/${programId}`,
        programData,
        "PUT"
      );

      if (!programResult.error) {
        // Update levels and semesters
        await Promise.all(
          formData.levels.map(async (level, levelIndex) => {
            if (level.id) {
              // Update existing level
              await sdk.callRawAPI(`/v4/api/records/cms_program_level/${level.id}`, {
                label: level.label,
              }, "PUT");
            } else {
              // Add new level
              const levelResult = await sdk.callRawAPI("/v4/api/records/cms_program_level", {
                program_id: programId,
                label: level.label,
              }, "POST");

              if (!levelResult.error) {
                const levelId = levelResult.data;
                // Add semesters
                await Promise.all(
                  level.semesters.map(async (semester) => {
                    await sdk.callRawAPI("/v4/api/records/cms_program_semester", {
                      level_id: levelId,
                      label: 'semester',
                      description:semester,
                    }, "POST");
                  })
                );
              }
            }
          })
        );

        showToast(globalDispatch, "Updated");
        navigate("/admin/programs");
      } else {
        // Handle error
      }
    } catch (error) {
      console.log("Error", error);
      tokenExpireError(dispatch, error.message);
    }
  };

  useEffect(() => {
    const fetchProgramData = async () => {
      let sdk = new MkdSDK();
      try {
        const programResult = await sdk.callRawAPI(
          `/v4/api/records/cms_programs/${programId}`,
          {},
          "GET"
        );

        if (!programResult.error) {
          const levelsResult = await sdk.callRawAPI(
            `/v4/api/records/cms_program_level?filter=program_id,eq,${programId}`,
            {},
            "GET"
          );

          if (!levelsResult.error) {
            const levelsWithSemesters = await Promise.all(
              levelsResult.list.map(async (level) => {
                const semestersResult = await sdk.callRawAPI(
                  `/v4/api/records/cms_program_semester?filter=level_id,eq,${level.id}`,
                  {},
                  "GET"
                );

                if (!semestersResult.error) {
                  const semesters = semestersResult.list.map(
                    (semester) => semester.description
                  );
                  return { ...level, semesters };
                }
                return level;
              })
            );

            setFormData({
              program_name: programResult.model.program_name,
              description: programResult.model.description,
              status: programResult.model.status,
              program_category: programResult.model.program_category,
              program_outcome: programResult.model.program_outcome,
              program_summary: programResult.model.program_summary,
              program_cover: programResult.model.program_cover,
              levels: levelsWithSemesters,
            });
          }
        }
      } catch (error) {
        console.log("Error", error);
      }
    };

    fetchProgramData();
  }, [programId]);

  React.useEffect(() => {
    globalDispatch({
      type: "SETPATH",
      payload: {
        path: "program",
      },
    });
  }, []);

  return (
    <div className="shadow-md rounded mx-auto p-5">
      <h4 className="text-2xl font-medium">Edit Program</h4>
      <form className="w-full max-w-lg" onSubmit={onSubmit}>
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="program_name">
            Program Name
          </label>
          <input
            type="text"
            placeholder="Program Name"
            name="program_name"
            value={formData.program_name}
            onChange={handleChange}
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
          />
        </div>

        <div className="mb-4 mt-2">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="program_cover"
          >
            Cover
          </label>
          <input
            type="file"
            placeholder="photo"
            value={formData.program_cover}
            onChange={
              (e) => previewImage("program_cover", e.target)
            }
          
            className={`"shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline`}
          />
          {fileObj["program_cover"] ? (
            isImage(fileObj["program_cover"]) ? (
              <img
                className={"preview-image"}
                src={fileObj["program_cover"]["tempURL"]}
              ></img>
            ) : (
              <>
                 <img
                className={"preview-image"}
                src={formData.program_cover}
              ></img>
              </>
            )
          ) : (
            <>
              <img
                className={"preview-image"}
                src={formData.program_cover}
              ></img>
            </>
          )}

      
        </div>

        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="program_category">
            Program Category
          </label>
          <select
            name="program_category"
            value={formData.program_category}
            onChange={handleChange}
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
          >
          <option value="">Select Category</option>
            <option value="current programs">Current Programs</option>
            <option value="german">German</option>
            <option value="french">French</option>
            <option value="other">Other</option>
            <option value="upcoming">Upcoming</option>
          </select>
        </div>
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="program_outcome">
            Program Outcome
          </label>
          <ReactQuill
           modules={quillModules}
           formats={quillFormats}
            value={formData.program_outcome}
            onChange={(value) => handleEditorChange("program_outcome", value)}
            className="quill-editor"
          />
        </div>
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="program_summary">
            Program Summary
          </label>
          <ReactQuill
           modules={quillModules}
           formats={quillFormats}
            value={formData.program_summary}
            onChange={(value) => handleEditorChange("program_summary", value)}
            className="quill-editor"
          />
        </div>
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="description">
            Description
          </label>
          <ReactQuill
           modules={quillModules}
           formats={quillFormats}
            value={formData.description}
            onChange={(value) => handleEditorChange("description", value)}
            className="quill-editor"
          />
        </div>
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="status">
            Status
          </label>
          <select
            name="status"
            value={formData.status}
            onChange={handleChange}
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
          >
            <option value="">Select Status</option>
            <option value="active">Active</option>
            <option value="draft">Draft</option>
          </select>
        </div>
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="levels">
            Levels
          </label>
          {formData.levels.map((level, levelIndex) => (
            <div key={levelIndex} className="mb-4">
              <div className="flex">
                <input
                  type="text"
                  placeholder="Enter level"
                  name="levels"
                  value={level.label}
                  onChange={(e) =>
                    handleLevelsChange(levelIndex, e.target.value)
                  }
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                />
                {levelIndex > 0 && (
                  <button
                    type="button"
                    className="ml-6 px-2 py-1 whitespace-nowrap text-red-600 hover:text-red-800 focus:outline-none"
                    onClick={() => handleRemoveLevel(levelIndex)}
                  >
                    Remove Level
                  </button>
                )}
              </div>
              {level.semesters.map((semester, semesterIndex) => (
                <div key={semesterIndex} className="flex mt-2">
                  {/* <input
                    type="text"
                    placeholder="Enter semester"
                    name="semesters"
                    value={semester}
                    onChange={(e) => handleSemesterChange(levelIndex, semesterIndex, e.target.value)}
                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  /> */}
                  <ReactQuill
                   modules={quillModules}
                   formats={quillFormats}
                    value={semester}
                    onChange={(value) =>
                      handleSemesterChange(levelIndex, semesterIndex, value)
                    }
                    className="mb-12"
                  />
                  {semesterIndex > 0 && (
                    <button
                      type="button"
                      className="ml-2 px-2 py-1 text-red-600 hover:text-red-800 focus:outline-none"
                      onClick={() =>
                        handleRemoveSemester(levelIndex, semesterIndex)
                      }
                    >
                      Remove
                    </button>
                  )}
                </div>
              ))}
              <button
                type="button"
                className="ml-6 mt-2 px-2 py-1 bg-blue-500 hover:bg-blue-700 text-white font-bold rounded focus:outline-none focus:shadow-outline"
                onClick={() => handleAddSemester(levelIndex)}
              >
                Add Semester
              </button>
            </div>
          ))}
          <button
            type="button"
            className="mb-4 px-2 py-1 bg-blue-500 hover:bg-blue-700 text-white font-bold rounded focus:outline-none focus:shadow-outline"
            onClick={handleAddLevel}
          >
            Add Level
          </button>
        </div>
        <button
          type="submit"
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
        >
          Submit
        </button>
      </form>
    </div>
  );
};

export default EditAdminProgram;
