import React from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import MkdSDK from "../utils/MkdSDK";
import { useNavigate, useParams } from "react-router-dom";
import { isImage } from "../utils/utils";
import { GlobalContext, showToast } from "../globalContext";
import { tokenExpireError } from "../authContext";
import ReactQuill from "react-quill";
import DynamicContentType from "../components/DynamicContentType";

let sdk = new MkdSDK();


const EditOurPartnersSlider = () => {
  const schema = yup
  .object({
    tagline: yup.string().required(),
    title: yup.string().required(),
    logo: yup.string().required(),
    descriotion1: yup.string(),
    descriotion2: yup.string(),
    descriotion3: yup.string(),
  })
  .required();


const { dispatch } = React.useContext(GlobalContext);
const { dispatch: globalDispatch } = React.useContext(GlobalContext);


  const [showPhoto, setShowPhoto] = React.useState("");
  const [id, setId] = React.useState(0);


const params = useParams();


const [fileObj, setFileObj] = React.useState({
  photo: null,
  logo: null,
});
const navigate = useNavigate();
const {
  register,
  handleSubmit,
  control,
  setError,
  setValue,
  formState: { errors },
} = useForm({
  resolver: yupResolver(schema),
});

const quillModules = {
  toolbar: [
    [{ header: [1, 2, 3, 4, 5, 6, false] }],
    ["bold", "italic", "underline", "strike"],
    [{ list: "ordered" }, { list: "bullet" }],
    ["link"],
    ["image"],
    [{ align: [] }], 
  ],
};

const quillFormats = [
  "header",
  "bold",
  "italic",
  "underline",
  "strike",
  "list",
  "bullet",
  "link",
  "align", 
  "image",
];

React.useEffect(() => {
  globalDispatch({
    type: "SETPATH",
    payload: {
      path: "partner",
    },
  });
  (async function () {
    try {
      const result = await sdk.callRawAPI(
        `/v4/api/records/cms_campus_detail/${params?.id}`,
        {},
        "GET"
      );
      if (!result.error) {
        setValue("title", result.model.title);
        setValue("tagline", result.model.tagline);
        setValue("logo", result.model.logo);
        setValue("description1", result.model.description1);
        setValue("description2", result.model.description2);
        setValue("description3", result.model.description3);
        setShowPhoto(result.model.logo)
        setId(result.model.id);
      }
    } catch (error) {
      console.log("error", error);
      tokenExpireError(dispatch, error.message);
    }
  })();
}, []);


const previewImage = (field, target) => {
  let tempFileObj = { ...fileObj };
  tempFileObj[field] = {
    file: target.files[0],
    tempURL: URL.createObjectURL(target.files[0]),
  };
  setFileObj(tempFileObj);
};


const onSubmit = async (data) => {
  let sdk = new MkdSDK();

  try {
   

    // Upload user name label photo
    if (fileObj.logo) {
      let formData = new FormData();
      formData.append("file", fileObj.logo.file);
      let uploadResult = await sdk.uploadImage(formData);
      data.logo = uploadResult.url;
    }
    const result = await sdk.callRawAPI(
      `/v4/api/records/cms_campus_detail/${id}`,
      {
        id,
        title: data.title,
        tagline: data.tagline,
        description1: data.description1,
        description2: data.description2,
        description3: data.description3,
        logo: data.logo,
      },
      "PUT"
    );
    if (!result.error) {
      showToast(globalDispatch, "Edited");
      navigate("/admin/partner");
    } else {
      if (result.validation) {
        const keys = Object.keys(result.validation);
        for (let i = 0; i < keys.length; i++) {
          const field = keys[i];
          setError(field, {
            type: "manual",
            message: result.validation[field],
          });
        }
      }
    }
  } catch (error) {
    console.log("Error", error);
    setError("seq", {
      type: "manual",
      message: error.message,
    });
    tokenExpireError(dispatch, error.message);
  }
};



  return (
    <div className=" shadow-md rounded  mx-auto p-5">
    <h4 className="text-2xl font-medium">Edit Partner SLide </h4>
    <form className=" w-full max-w-lg" onSubmit={handleSubmit(onSubmit)}>
      <div className="mb-4">
        <label
          className="block text-gray-700 text-sm font-bold mb-2"
          htmlFor="tagline"
        >
          Tagline
        </label>
        <input
          placeholder="Type Tagline"
          {...register("tagline")}
          className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
        />
      </div>

      <div className="mb-4">
        <label
          className="block text-gray-700 text-sm font-bold mb-2"
          htmlFor="title"
        >
          Title
        </label>
        <input
          placeholder="Type Title"
          {...register("title")}
          className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
        />
      </div>

      <div className="mb-4">
        <label
          className="block text-gray-700 text-sm font-bold mb-2"
          htmlFor="description1"
        >
          Description 1
        </label>

        <Controller
          name="description1"
          control={control}
          defaultValue=""
          render={({ field }) => (
            <ReactQuill
              value={field.value}
              onChange={field.onChange}
              onBlur={field.onBlur}
              theme="snow"
              modules={quillModules}
              formats={quillFormats}
            />
          )}
        />
      </div>

      <div className="mb-4">
        <label
          className="block text-gray-700 text-sm font-bold mb-2"
          htmlFor="description2"
        >
          Description 2
        </label>

        <Controller
          name="description2"
          control={control}
          defaultValue=""
          render={({ field }) => (
            <ReactQuill
              value={field.value}
              onChange={field.onChange}
              onBlur={field.onBlur}
              theme="snow"
              modules={quillModules}
              formats={quillFormats}
            />
          )}
        />
      </div>

      <div className="mb-4">
        <label
          className="block text-gray-700 text-sm font-bold mb-2"
          htmlFor="description3"
        >
          Description 3
        </label>

        <Controller
          name="description3"
          control={control}
          defaultValue=""
          render={({ field }) => (
            <ReactQuill
              value={field.value}
              onChange={field.onChange}
              onBlur={field.onBlur}
              theme="snow"
              modules={quillModules}
              formats={quillFormats}
            />
          )}
        />
      </div>

      <div className="mb-4">
        <label
          className="block text-gray-700 text-sm font-bold mb-2"
          htmlFor="logo"
        >
          Logo
        </label>
        <input
          type="file"

          {...register("logo", {
            onChange: (e) => previewImage("logo", e.target),
          })}
          className={`"shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${
            errors.logo?.message ? "border-red-500" : ""
          }`}
        />
        {fileObj["logo"] ? (
          isImage(fileObj["logo"]) ? (
            <img
              className={"preview-image"}
              src={fileObj["logo"]["tempURL"]}
            ></img>
          ) : (
            <>
            <img
              className={"preview-image"}
              src={showPhoto}
            ></img>
            </>
          )
        ) : (
          <>
           <img
              className={"preview-image"}
              src={showPhoto}
            ></img></>
        )}
      </div>

    

      <button
        type="submit"
        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
      >
        Submit
      </button>
    </form>
  </div>
  );
}

export default EditOurPartnersSlider