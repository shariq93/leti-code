import React from "react";
import { AuthContext } from "../authContext";
import MkdSDK from "../utils/MkdSDK";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { GlobalContext } from "../globalContext";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { getNonNullValue } from "../utils/utils";
import PaginationBar from "../components/PaginationBar";
import AddButton from "../components/AddButton";
import ModalDelete from "Components/Modal/ModalDelete";
import { useCallback, useState } from "react";
import TreeSDK from "Utils/TreeSDK";
import ProgramPageContentBox from "Components/ProgramPageContentBox";
import NoImg from "../assets/no-img.jpeg"


let sdk = new MkdSDK();
let tdk = new TreeSDK();

const columns = [
  {
    header: "Action",
    accessor: "",
  },
  {
    header: "Program Name",
    accessor: "program_name",
  },
  {
    header: "Category",
    accessor: "program_category",
  },
  {
    header: "Levels",
    accessor: "levels",
  },
  {
    header: "Status",
    accessor: "status",
  },
  {
    header: "Cover Image",
    accessor: "program_cover",
  },
];

const fetchProgramLevels = async () => {
  try {
    const result = await sdk.callRawAPI(
      "/v4/api/records/cms_program_level",
      {},
      "GET"
    );
    return result.list;
  } catch (error) {
    console.log("Error", error);
    throw error;
  }
};

const AdminProgramsList = () => {
  const { dispatch } = React.useContext(AuthContext);
  const { dispatch: globalDispatch } = React.useContext(GlobalContext);

  const [query, setQuery] = React.useState("");
  const [data, setCurrentTableData] = React.useState([]);
  const [pageSize, setPageSize] = React.useState(3);
  const [pageCount, setPageCount] = React.useState(0);
  const [dataTotal, setDataTotal] = React.useState(0);
  const [currentPage, setPage] = React.useState(0);
  const [canPreviousPage, setCanPreviousPage] = React.useState(false);
  const [canNextPage, setCanNextPage] = React.useState(false);
  const navigate = useNavigate();

  const schema = yup.object({
    program_name: yup.string(),
    program_category: yup.string(),
    status: yup.string(),
  });

  const [itemToDelete, setItemToDelete] = React.useState(null);
  const [ShowConfirmationModal, setConfirmationModal] = useState(false);
  const [id, setID] = useState("");
  const onCloseConfirmationModal = useCallback(() => {
    setConfirmationModal(false);
  }, [ShowConfirmationModal]);

  function handleDeleteClick(itemId) {
    setItemToDelete(itemId);
    setConfirmationModal(true);
  }

  function handleConfirmDelete() {
    // console.log(itemToDelete);
    deleteItem(itemToDelete);
    setConfirmationModal(false);
  }

  function handleCancelDelete() {
    console.log(itemToDelete);
    setItemToDelete(null);
    setConfirmationModal(false);
  }

  const {
    register,
    handleSubmit,
    setError,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  function updatePageSize(limit) {
    (async function () {
      setPageSize(limit);
      await getData(0, limit);
    })();
  }

  function previousPage() {
    (async function () {
      await getData(currentPage - 1 > 0 ? currentPage - 1 : 0, pageSize);
    })();
  }

  function nextPage() {
    (async function () {
      await getData(
        currentPage + 1 <= pageCount ? currentPage + 1 : 0,
        pageSize
      );
    })();
  }

  async function getData(pageNum, limitNum, data) {
    try {
      const result = await tdk.getPaginate("cms_programs", {
        filter: data,
        payload: { ...data },
        page: pageNum || 1,
        limit: limitNum,
        size: limitNum,
        order: "create_at",
      });

      let { list, total, limit, num_pages, page } = result;

      const levels = await fetchProgramLevels();

      list = list.map((program) => {
        const programLevels = levels.filter(
          (level) => level.program_id === program.id
        );
        return {
          ...program,
          levels: programLevels,
        };
      });

      setCurrentTableData(list);
      setPageSize(limit);
      setPageCount(num_pages);
      setPage(page);
      setDataTotal(total);
      setCanPreviousPage(page > 1);
      setCanNextPage(page + 1 <= num_pages);
    } catch (error) {
      console.log("ERROR", error);
      tokenExpireError(dispatch, error.message);
    }
  }

  const onSubmit = (data) => {
    let program_name = getNonNullValue(data.program_name);
    let program_category = getNonNullValue(data.program_category);
    let status = getNonNullValue(data.status);

    let filter = [];
    if (data.program_name) {
      filter.push(`program_name,cs,${program_name}`);
    }
    if (data.program_category) {
      filter.push(`program_category,cs,${program_category}`);
    }
    if (data.status) {
      filter.push(`status,cs,${status}`);
    }
    {
      getData(1, 200, filter);
    }
  };
  const deleteItem = async (id) => {
    try {
      const result = await sdk.callRawAPI(
        "/v4/api/records/cms_programs/" + id,
        {},
        "DELETE"
      );
      setCurrentTableData((list) =>
        list.filter((x) => Number(x.id) !== Number(id))
      );
    } catch (err) {
      throw new Error(err);
    }
  };

  const resetForm = async () => {
    reset();
    await getData(0, 10);
  };

  React.useEffect(() => {
    globalDispatch({
      type: "SETPATH",
      payload: {
        path: "program",
      },
    });

    (async function () {
      await getData(0, 10);
    })();
  }, []);

  const { dispatch: gDispatch } = React.useContext(GlobalContext);

  return (
    <>
      {ShowConfirmationModal ? (
        <ModalDelete
          title="Are you sure"
          id={id}
          deleteItem={deleteItem}
          onConfirmDelete={handleConfirmDelete}
          onCloseConfirmationModal={handleCancelDelete}
        />
      ) : null}

      <div className="flex flex-col gap-10">
       

        <div>
          <form
            className="p-2 bg-white shadow rounded mb-5 "
            onSubmit={handleSubmit(onSubmit)}
          >
       
            <div className="flex justify-between">
            <h4 className="text-2xl font-medium">Programs Search</h4>
          <button
            onClick={() => {
              gDispatch({
                type: "EDIT_PAGE",
                payload: { pageName: "page_programs" },
              });
              navigate("/admin/page");
            }}
            className=" inline ml-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
          >
            Page Hide/Show
          </button>
        </div>
            <div className="filter-form-holder mt-10 flex flex-wrap">
              <div className="mb-4 w-full md:w-1/3 pr-2 pl-2">
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  Program Name
                </label>
                <input
                  type="text"
                  placeholder="program name"
                  {...register("program_name")}
                  className="shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                />
                <p className="text-red-500 text-xs italic">
                  {errors.program_name?.message}
                </p>
              </div>
              <div className="mb-4 w-full md:w-1/3 pr-2 pl-2">
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  Identifier
                </label>
                <select
                  name="program_category"
                  {...register("program_category")}
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                >
                  <option value="">Select Category</option>
                  <option value="current programs">Current Programs</option>
                  <option value="german">German</option>
                  <option value="french">French</option>
                  <option value="other">Other</option>
                  <option value="upcoming">Upcoming</option>
                </select>

                <p className="text-red-500 text-xs italic">
                  {errors.program_category?.message}
                </p>
              </div>
              <div className="mb-4 w-full md:w-1/3 pr-2 pl-2">
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  Status
                </label>
                <select
                  name="status"
                  {...register("status")}
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                >
                  <option value="">Select Status</option>
                  <option value="active">Active</option>
                  <option value="draft">Draft</option>
                </select>

                <p className="text-red-500 text-xs italic">
                  {errors.status?.message}
                </p>
              </div>
            </div>

            <button
              type="submit"
              className=" inline ml-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            >
              Search
            </button>
            <button
              onClick={() => {
                resetForm();
              }}
              type="button"
              className=" inline ml-2 bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            >
              Clear
            </button>
          </form>

          <div className="overflow-x-auto  p-2 pt-0 bg-white shadow rounded">
            <div className="mb-3 text-center justify-between w-full flex  ">
              <h4 className="text-2xl font-medium">Programs </h4>
              <AddButton link={"/admin/add-programs"} />
            </div>
            <div className="shadow overflow-x-auto border-b border-gray-200 ">
              <table className="min-w-full divide-y divide-gray-200">
                <thead className="bg-gray-50">
                  <tr>
                    {columns.map((column, i) => (
                      <th
                        key={i}
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        {column.header}
                        <span>
                          {column.isSorted
                            ? column.isSortedDesc
                              ? " ▼"
                              : " ▲"
                            : ""}
                        </span>
                      </th>
                    ))}
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {data.map((row, i) => {
                    return (
                      <tr key={i}>
                        {columns.map((cell, index) => {
                          if (cell.accessor == "content_value") {
                            return (
                              <td>
                                <p className="text-sm">{row[cell.accessor]}</p>
                              </td>
                            );
                          }
                          if (cell.accessor === "program_cover") {
                            return (
                              <td key={index} className="px-6 py-4 whitespace-nowrap">
                                {row[cell.accessor] ? (
                                  <img width={'200px'} height={'200px'} src={row[cell.accessor]} />
                                ) : (
                                  <img src={NoImg} />
                                )}
                              </td>
                            );
                          }
                          if (cell.accessor == "") {
                            return (
                              <td
                                key={index}
                                className="px-6 py-4 whitespace-nowrap"
                              >
                                <button
                                  className="text-xs"
                                  onClick={() => {
                                    navigate("/admin/edit-programs/" + row.id, {
                                      state: row,
                                    });
                                  }}
                                >
                                  {" "}
                                  Edit
                                </button>

                                <button
                                  className="text-xs ml-2"
                                  onClick={() => {
                                    navigate("/admin/view-programs/" + row.id, {
                                      state: row,
                                    });
                                  }}
                                >
                                  {" "}
                                  View
                                </button>

                                <button
                                  className="text-xs px-1 text-red-500"
                                  // onClick={() => deleteItem(row.id)}
                                  onClick={() => handleDeleteClick(row.id)}
                                >
                                  {" "}
                                  Delete
                                </button>
                              </td>
                            );
                          }
                          if (cell.mapping) {
                            return (
                              <td
                                key={index}
                                className="px-6 py-4 whitespace-nowrap"
                              >
                                {cell.mapping[row[cell.accessor]]}
                              </td>
                            );
                          }
                          if (cell.accessor === "levels") {
                            return (
                              <td
                                key={index}
                                className="px-6 py-4 whitespace-nowrap"
                              >
                                {row.levels.map((level) => (
                                  <div key={level.id}>{level.label}</div>
                                ))}
                              </td>
                            );
                          }
                          return (
                            <td
                              key={index}
                              className="px-6 py-4 whitespace-nowrap"
                            >
                              {row[cell.accessor]}
                            </td>
                          );
                        })}
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
          <PaginationBar
            currentPage={currentPage}
            pageCount={pageCount}
            pageSize={pageSize}
            canPreviousPage={canPreviousPage}
            canNextPage={canNextPage}
            updatePageSize={updatePageSize}
            previousPage={previousPage}
            nextPage={nextPage}
          />
        </div>

        <ProgramPageContentBox />
      </div>
    </>
  );
};

export default AdminProgramsList;
