import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import MkdSDK from "../utils/MkdSDK";
import { GlobalContext, showToast } from "../globalContext";
import { useNavigate, useParams } from "react-router-dom";
import { AuthContext, tokenExpireError } from "../authContext";
import DynamicContentType from "../components/DynamicContentType";

let sdk = new MkdSDK();


const EditAdminAdmissionPage = () => {
    const schema = yup
    .object({
    //   page: yup.string().required(),
      key: yup.string().required(),
      type: yup.string().required(),
      // seq:yup.number(),
      value: yup.string(),
    })
    .required();

  const selectType = [
    { key: "text", value: "Text" },
    { key: "image", value: "Image" },
    { key: "number", value: "Number" },
    { key: "kvp", value: "Key-Value Pair" },
    { key: "image-list", value: "Image List" },
    { key: "captioned-image-list", value: "Captioned Image List" },
    { key: "team-list", value: "Team List" },
  ];
  
  const { dispatch } = React.useContext(AuthContext);
  const { dispatch: globalDispatch } = React.useContext(GlobalContext);
  const navigate = useNavigate();
  const [id, setId] = useState(0);
  const [contentType, setContentType] = React.useState(selectType[0]?.key);
  const [contentValue, setContentValue] = React.useState('');


  const {
    register,
    handleSubmit,
    setError,
    setValue,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const params = useParams();

  useEffect(function () {
    globalDispatch({
      type: "SETPATH",
      payload: {
        path: "admission",
      },
    });

    (async function () {
      try {
        sdk.setTable("cms_admissions");
        const result = await sdk.callRestAPI({ id: Number(params?.id) }, "GET");
        if (!result.error) {
        //   setValue("page", result.model.page);
          setValue("key", result.model.content_key);
          // setValue("seq", result.model.seq);
          setValue("type", result.model.content_type);
          setValue("value", result.model.content_value);
          setContentType(result.model.content_type);
          setContentValue(result.model.content_value);
          setId(result.model.id);
        }
      } catch (error) {
        console.log("error", error);
        tokenExpireError(dispatch, error.message);
      }
    })();
  }, []);

  const onSubmit = async (data) => {
    let sdk2 = new MkdSDK();
  

    try {
      sdk2.setTable("cms_admissions");

      const result = await sdk2.callRestAPI(
        {
          id,
        //   page: data.page,
          // seq: data.seq,
          content_key: data.key,
          content_type: data.type,
          content_value: contentValue,
        },
        "PUT"
      );
      if (!result.error) {
        showToast(globalDispatch, "Added");
        navigate("/admin/admission");
      } else {
        if (result.validation) {
          const keys = Object.keys(result.validation);
          for (let i = 0; i < keys.length; i++) {
            const field = keys[i];
            setError(field, {
              type: "manual",
              message: result.validation[field],
            });
          }
        }
      }
    } catch (error) {
      console.log("Error", error);
      setError("content_type", {
        type: "manual",
        message: error.message,
      });
      tokenExpireError(dispatch, error.message);
    }
  };

  return (
    <div className=" shadow-md rounded   mx-auto p-5">
      <h4 className="text-2xl font-medium">Edit Admission</h4>
      <form className=" w-full max-w-lg" onSubmit={handleSubmit(onSubmit)}>
      {/* <div className="mb-4 d-none">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="seq"
          >
            sequence
          </label>
          <input
            type="text"
            placeholder="seq"pmain
            {...register("seq")}
            className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
          />
        </div> */}
        {/* <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="page"
          >
            Page
          </label>
          <select  {...register("page")}
            className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}>
            <option>select page</option>
            <option value='index' >Home</option>
            <option value='about' >About</option>
            <option value='academic-calendar'> Academic_calender</option>
            <option value='accreditation'>Accreditation </option>
            <option value='admissiion'>Admission </option>
            <option value='application'>Application </option>
            <option value='campus'>Campus </option>
            <option value='contact'>Contact </option>
            <option value='dubai'>Dubai </option>
            <option value='faculty'>Faculty </option>
            <option value='faq'>Faq </option>
            <option value='news-and-events'>News_events</option>
            <option value='our-partners'>Our_partners </option>
            <option value='programs'>Programs</option>
            <option value='student-life'>Student_life </option>
            <option value='student-path'>Student_path </option>
            <option value='study'>Study </option>
            <option value='tution-fee'>Tution_fee </option>
            <option value='visa-and-insurance'>Visa_insurance </option>
            <option value='why'>Why </option>
          </select>
        </div> */}
        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="key"
          >
            Content Identifier
          </label>
          <input
            type="text"
            placeholder="Content Identifier"
            {...register("key")}
            className={`shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${errors.key?.message ? "border-red-500" : ""}`}
          />
          <p className="text-red-500 text-xs italic">{errors.key?.message}</p>
        </div>
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-2">
            Content Type
          </label>
          <select
            name="type"
            id="type"
            className="shadow border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
            {...register("type", {onChange:  e => setContentType(e.target.value) })}
          >
            {selectType.map((option) => (
              <option name={option.name} value={option.key} key={option.key}>
                {option.value}
              </option>
            ))}
          </select>
        </div>
        <DynamicContentType contentType={contentType} contentValue={contentValue} setContentValue={setContentValue} />
        
        <button
          type="submit"
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
        >
          Submit
        </button>
      </form>
    </div>
  );
}

export default EditAdminAdmissionPage