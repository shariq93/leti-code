import FacultyContentBox from 'Components/FacultyContentBox'
import FacultyTeamBox from 'Components/FacultyTeamBox'
import React from 'react'

const FacultyContentPage = () => {
  return (
  <>
  <div className="flex flex-col gap-10">
<FacultyContentBox/>
<FacultyTeamBox/>
  </div>
  </>
  )
}

export default FacultyContentPage