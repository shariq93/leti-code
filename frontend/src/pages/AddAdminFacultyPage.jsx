import React from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import MkdSDK from "../utils/MkdSDK";
import { useNavigate } from "react-router-dom";
import { GlobalContext, showToast } from "../globalContext";
import { tokenExpireError } from "../authContext";
import { isImage } from "../utils/utils";
import ReactQuill from "react-quill";
import DynamicContentType from "../components/DynamicContentType";

const AddAdminFacultyPage = () => {
  const schema = yup
    .object({
      first_name: yup.string().required(),
      specialty: yup.string().required(),
      summary: yup.string(),
      description: yup.string().required(),
      twitter: yup.string(),
      linkedin: yup.string(),
      facebook: yup.string(),
      instagram: yup.string(),
      photo: yup.string(),
      designation: yup.string().required(),
      order: yup.string(),
    })


  const { dispatch } = React.useContext(GlobalContext);
  const { dispatch: globalDispatch } = React.useContext(GlobalContext);
  const [fileObj, setFileObj] = React.useState({});
  const [showOrder, setShowOrder] = React.useState("");

  const navigate = useNavigate();
  const {
    register,
    handleSubmit,
    control,
    setError,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const previewImage = (field, target) => {
    let tempFileObj = fileObj;
    tempFileObj[field] = {
      file: target.files[0],
      tempURL: URL.createObjectURL(target.files[0]),
    };
    setFileObj({ ...tempFileObj });
  };

  const onSubmit = async (data) => {
    let sdk = new MkdSDK();

    try {
      for (let item in fileObj) {
        let formData = new FormData();
        formData.append("file", fileObj[item].file);
        let uploadResult = await sdk.uploadImage(formData);
        data[item] = uploadResult.url;
      }

      if (showOrder !== "faculty") {
        data.order = "-";
      } else {
        data.order = data.order || "-"; // Use selected value or "-" if order is not selected
      }

      const result = await sdk.callRawAPI(
        "/v4/api/records/cms_team",
        {
          first_name: data.first_name,
          last_name: data.first_name,
          specialty: data.specialty,
          summary: data.summary,
          description: data.description,
          designation: data.designation,
          order_: data.order,
          twitter: data.twitter,
          linkedin: data.linkedin,
          facebook: data.facebook,
          instagram: data.instagram,
          photo: data.photo,
        },
        "POST"
      );
      if (!result.error) {
        showToast(globalDispatch, "Added");
        navigate("/admin/faculty_content");
      } else {
        if (result.validation) {
          const keys = Object.keys(result.validation);
          for (let i = 0; i < keys.length; i++) {
            const field = keys[i];
            setError(field, {
              type: "manual",
              message: result.validation[field],
            });
          }
        }
      }
    } catch (error) {
      console.log("Error", error);
      setError("seq", {
        type: "manual",
        message: error.message,
      });
      tokenExpireError(dispatch, error.message);
    }
  };

  React.useEffect(() => {
    globalDispatch({
      type: "SETPATH",
      payload: {
        path: "team",
      },
    });
  }, []);

  return (
    <div className=" shadow-md rounded  mx-auto p-5">
      <h4 className="text-2xl font-medium">Add Faculty </h4>
      <form className=" w-full max-w-lg" onSubmit={handleSubmit(onSubmit)}>
        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="first_name"
          >
            Name
          </label>
          <input
            type="text"
            placeholder="Enter your name"
            {...register("first_name")}
            className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
          />
        </div>

        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="first_name"
          >
            Designation
          </label>
          <select
            {...register("designation")}
            onChange={(event) => {
              const selectedValue = event.target.value;
              setShowOrder(selectedValue);
            }}
            className={`shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline`}
          >
            <option value="">Select</option>
            <option value="chairman">Chairman</option>
            <option value="leadership">Leadership</option>
            <option value="faculty">Other Faculty</option>
          </select>
        </div>

        {showOrder == "faculty" && (
          <div className="mb-4">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="first_name"
            >
              Order
            </label>
            <select
              {...register("order")}
              className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
            >
              <option value="">Select</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
              <option value="7">7</option>
              <option value="8">8</option>
              <option value="9">9</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
              <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
              <option value="16">16</option>
              <option value="17">17</option>
              <option value="18">18</option>
              <option value="19">19</option>
            </select>
          </div>
        )}

        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="specialty"
          >
            specialty
          </label>
          <input
            type="text"
            placeholder="specialty"
            {...register("specialty")}
            className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
          />
        </div>

        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="summary"
          >
            Summary
          </label>
          <input
            type="text"
            placeholder="summary"
            {...register("summary")}
            className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
          />
        </div>
        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="description"
          >
            description
          </label>
          <Controller
            name="description"
            control={control}
            defaultValue=""
            render={({ field }) => (
              <ReactQuill
                value={field.value}
                onChange={field.onChange}
                onBlur={field.onBlur}
                theme="snow"
              />
            )}
          />
        </div>
        <div className="mb-4 mt-2">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="photo"
          >
            Profile
          </label>
          <input
            type="file"
            placeholder="photo"
            {...register("photo", {
              onChange: (e) => previewImage("photo", e.target),
            })}
            className={`"shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${
              errors.photo?.message ? "border-red-500" : ""
            }`}
          />
          {fileObj["photo"] ? (
            isImage(fileObj["photo"]) ? (
              <img
                className={"preview-image"}
                src={fileObj["photo"]["tempURL"]}
              ></img>
            ) : (
              <></>
            )
          ) : (
            <></>
          )}

          <p className="text-red-500 text-xs italic">{errors.photo?.message}</p>
        </div>

        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="twitter"
          >
            twitter
          </label>
          <input
            type="text"
            placeholder="twitter"
            {...register("twitter")}
            className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
          />
        </div>
        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="linkedin"
          >
            linkedin
          </label>
          <input
            type="text"
            placeholder="linkedin"
            {...register("linkedin")}
            className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
          />
        </div>
        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="facebook"
          >
            Facebook
          </label>
          <input
            type="text"
            placeholder="facebook"
            {...register("facebook")}
            className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
          />
        </div>

        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="instagram"
          >
            Instagram
          </label>
          <input
            type="text"
            placeholder="instagram"
            {...register("instagram")}
            className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
          />
        </div>

        <button
          type="submit"
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
        >
          Submit
        </button>
      </form>
    </div>
  );
};

export default AddAdminFacultyPage;
