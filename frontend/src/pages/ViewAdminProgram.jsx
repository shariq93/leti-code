import React, { useState, useEffect } from "react";
import MkdSDK from "../utils/MkdSDK";
import { useParams, useNavigate } from "react-router-dom";
import { GlobalContext } from "../globalContext";

const AdminProgramDetail = () => {
  const { dispatch } = React.useContext(GlobalContext);
  const navigate = useNavigate();
  const { programId } = useParams();

  const [programData, setProgramData] = useState(null);

  useEffect(() => {
    const fetchProgramData = async () => {
      let sdk = new MkdSDK();
      try {
        const programResult = await sdk.callRawAPI(
          `/v4/api/records/cms_programs/${programId}`,
          {},
          "GET"
        );

        if (!programResult.error) {
          const levelsResult = await sdk.callRawAPI(
            `/v4/api/records/cms_program_level?filter=program_id,eq,${programId}`,
            {},
            "GET"
          );

          
          if (!levelsResult.error) {
            const programLevels = levelsResult.list.map((level) => ({
              ...level,
              semesters: [],
            }));

            for (const level of programLevels) {
              const semestersResult = await sdk.callRawAPI(
                `/v4/api/records/cms_program_semester?filter=level_id,eq,${level.id}`,
                {},
                "GET"
              );

              if (!semestersResult.error) {
                level.semesters = semestersResult.list.map(
                  (semester) => semester.description
                );
              }
            }

            setProgramData({
              ...programResult.model,
              levels: programLevels,
            });
          }else{
            console.log("Error", levelsResult.error);
          }
        }
      } catch (error) {
        console.log("Error", error);
      }
    };

    fetchProgramData();
  }, [programId]);

  const handleEditClick = () => {
    navigate(`/admin/edit-programs/${programId}`);
  };

  const handleDeleteClick = async () => {
    let sdk = new MkdSDK();
    try {
      const result = await sdk.callRawAPI(
        `/v4/api/records/cms_programs/${programId}`,
        {},
        "DELETE"
      );
      if (!result.error) {
        navigate("/admin/programs");
      }
    } catch (error) {
      console.log("Error", error);
    }
  };

  if (!programData) {
    return <div>Loading...</div>;
  }

  return (
    <div className=" shadow-md rounded  mx-auto p-5">
      <h4 className="text-2xl font-medium">Program Detail</h4>
      <div className="my-4">
        <strong>Program Name:</strong> {programData.program_name}
      </div>
      <div className="my-4">
        <strong>Program Category:</strong> {programData.program_category}
      </div>
      <div className="my-4">
        <strong>Program Outcome:</strong>
        <div dangerouslySetInnerHTML={{ __html: programData.program_outcome }} />
      </div>
      <div className="my-4">
        <strong>Program Summary:</strong>
        <div dangerouslySetInnerHTML={{ __html: programData.program_summary }} />
      </div>
      <div className="my-4">
        <strong>Description:</strong>
        <div dangerouslySetInnerHTML={{ __html: programData.description }} />
      </div>
      <div className="my-4">
        <strong>Status:</strong> {programData.status}
      </div>

      {programData.levels && programData.levels.length > 0 && (
        <div className="my-4">
          <strong>Levels:</strong>
          {programData.levels.map((level, levelIndex) => (
            <div key={levelIndex} className="mb-4">
              <div className="flex items-center">
                <strong>Level {levelIndex + 1}:</strong> {level.label}
              </div>
              {level.semesters && level.semesters.length > 0 && (
                <div className="ml-4">
                  <strong>Semesters:</strong>
                  {level.semesters.map((semester, semesterIndex) => (
                    <div key={semesterIndex} className="flex mt-2">
                      <strong>Semester {semesterIndex + 1}:</strong> {semester}
                    </div>
                  ))}
                </div>
              )}
            </div>
          ))}
        </div>
      )}

      <div className="flex justify-between mt-4">
        <button
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
          onClick={handleEditClick}
        >
          Edit
        </button>
        <button
          className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
          onClick={handleDeleteClick}
        >
          Delete
        </button>
      </div>
    </div>
  );
};

export default AdminProgramDetail;
