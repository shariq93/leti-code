
  import React, { useRef, useState } from "react";
  import { useForm } from "react-hook-form";
  import { yupResolver } from "@hookform/resolvers/yup";
  import * as yup from "yup";
  import MkdSDK from "Utils/MkdSDK";
  import { useNavigate } from "react-router-dom";
  import { tokenExpireError } from "Src/authContext";
  import { GlobalContext, showToast } from "Src/globalContext";
  import ReactQuill from "react-quill";
  import "react-quill/dist/quill.snow.css";
  import { isImage, empty, isVideo, isPdf } from "Utils/utils";
  import Editor from "Components/Blog/Editor";
  import MultiSelect from "Components/MultiSelect";
  import { InteractiveButton } from "Components/InteractiveButton";
  // import EditorJS from '@editorjs/editorjs'
  
  const sdk = new MkdSDK();
  const defaultImage = "https://via.placeholder.com/150?text=%20";
  const AddAdminBlogTagPage = () => {
    //-----------------------------------
    const [previewUrl, setPreviewUrl] = React.useState(defaultImage);
    const [contentValue, setContentValue] = React.useState("");
    const [content, setContent] = useState("");
    const [name, setName] = useState("");
    const [submitLoading, setSubmitLoading] = useState(false);
    // const [ parentId, setParentId ] = useState( 0 );
    const [tags, setTags] = useState([]);
    const [errors, setError] = useState({
      name: { message: "" },
      body: { message: "" },
      tags: { message: "" },
      category: { message: "" },
    });
  
    const handleImageChange = async e => {
      const formData = new FormData();
      formData.append("file", e.target.files[0]);
      try {
        const result = await sdk.uploadImage(formData);
        setPreviewUrl(result.url);
        setContentValue(result.url);
        console.log("image url", result);
      } catch (err) {
        console.error(err);
      }
    };
  
    function handleNameChange(value) {
      setName(value);
      if (value !== "") {
        if (errors.name.message !== "") {
          setError(prev => ({ ...prev, name: { message: "" } }));
        }
      } else {
        if (errors.name.message === "") {
          setError(prev => ({ ...prev, name: { message: "Name is required" } }));
        }
      }
      // console.log( value )
    }
  
    const selectCategory = [
      { key: "category-1", value: "Category 1" },
      { key: "category-2", value: "Category 2" },
      { key: "category-3", value: "Category 3" },
    ];
    //-----------------------------------
    const { dispatch: globalDispatch } = React.useContext(GlobalContext);
    const navigate = useNavigate();
  
    const onSubmit = async () => {
      if (name === "") {
        return setError(prev => ({
          ...prev,
          name: { message: "Name is required" },
        }));
      }
  
      try {
        setSubmitLoading(true);
        const result = await sdk.createBlogTag({
          name,
        });
        // categories: categoryIds
        if (!result.error) {
          setSubmitLoading(false);
          showToast(globalDispatch, "Added");
          navigate("/admin/blogs-tag");
        }
      } catch (error) {
        console.log("Error", error);
        setSubmitLoading(false);
        // setError( "create_at", {
        //   type: "manual",
        //   message: error.message,
        // } );
        tokenExpireError(globalDispatch, error.message);
      }
    };
  
    React.useEffect(() => {
      globalDispatch({
        type: "SETPATH",
        payload: {
          path: "tags",
        },
      });
    }, []);
  
    return (
      <div className=" shadow-md rounded  mx-auto p-5">
        <h4 className="text-2xl font-medium">Add Blog Tag</h4>
        <div className=" w-full max-w-lg">
          <div className="mb-4  ">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="name"
            >
              Name
            </label>
            <input
              placeholder="Name"
              value={name}
              id="name"
              onChange={e => handleNameChange(e.target.value)}
              className={`shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${
                errors.name?.message ? "border-red-500" : ""
              }`}
            />
  
            <p className="text-red-500 text-field-error italic">
              {errors.name?.message}
            </p>
          </div>
  
          <div className="mb-4  ">
            {/* <MultiSelect
              label={ `Category` }
              options={ [ { name: 'Tech', id: 1 }, { name: 'Travel', id: 2 } ] }
              display={ `name` }
              selected={ category }
              setSelected={ handleCategoryChange }
            /> */}
            <p className="text-red-500 text-field-error italic">
              {errors.category?.message}
            </p>
          </div>
        </div>
        <InteractiveButton
          onClick={() => onSubmit()}
          disabled={submitLoading}
          loading={submitLoading}
          type="submit"
          className="bg-blue-500 hover:bg-blue-700 mt-5 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
        >
          {submitLoading ? "Submiting" : "Submit"}
        </InteractiveButton>
      </div>
    );
  };
  
  export default AddAdminBlogTagPage;
  
