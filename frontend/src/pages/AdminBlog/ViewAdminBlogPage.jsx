
  import React from "react";
  import { useForm } from "react-hook-form";
  import { yupResolver } from "@hookform/resolvers/yup";
  import * as yup from "yup";
  import MkdSDK from "Utils/MkdSDK";
  import { useNavigate, useParams } from "react-router-dom";
  import { tokenExpireError } from "Src/authContext";
  import { GlobalContext, showToast } from "Src/globalContext";
  import { isImage, empty, isVideo } from "Utils/utils";
  
  let sdk = new MkdSDK();
  
  const ViewAdminBlogPage = () => {
  
    const formatter = new Intl.DateTimeFormat('en-US', {
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      hour12: true
    });
    const { dispatch: globalDispatch } = React.useContext( GlobalContext );
  
    const { dispatch } = React.useContext( GlobalContext );
    const [ viewModel, setViewModel ] = React.useState( null );
  
  
  
    const params = useParams();
  
    React.useEffect( function () {
      ( async function () {
        try {
          sdk.setTable( "blogs" );
          const result = await sdk.getSingleBlog( Number( params?.id ) );
          if ( !result.error ) {
  
            setViewModel( result.data );
  
          }
        } catch ( error ) {
          console.log( "error", error );
          tokenExpireError( dispatch, error.message );
        }
      } )();
    }, [] );
    return (
      <div className=" shadow-md rounded  mx-auto p-5">
        <h4 className="text-2xl font-medium">View Blog</h4>
  
  
  
        <div className="mb-4 mt-4 border-b-2">
          <div className="flex mb-4">
            <div className="flex-1">Id</div>
            <div className="flex-1">{ viewModel && viewModel?.id }</div>
          </div>
        </div>
  
  
  
        <div className="mb-4 mt-4 border-b-2">
          <div className="flex mb-4">
            <div className="flex-1">Create At</div>
            <div className="flex-1">{ viewModel &&  new Date(viewModel?.create_at).toLocaleDateString() + ' ' +  new Date(viewModel?.create_at).toLocaleTimeString() }</div>
          </div>
        </div>
  
  
  
        <div className="mb-4 mt-4 border-b-2">
          <div className="flex mb-4">
            <div className="flex-1">Update At</div>
            <div className="flex-1">{ viewModel &&   new Date(viewModel?.update_at).toLocaleDateString() + ' ' +  new Date(viewModel?.update_at).toLocaleTimeString() }</div>
          </div>
        </div>
  
  
  
  
        <div className="mb-4 mt-4 border-b-2">
          <div className="flex mb-4">
            <div className="flex-1">Title</div>
            <div className="flex-1">{ viewModel && viewModel?.title }</div>
          </div>
        </div>
  
        <div className="mb-4 mt-4 border-b-2">
          <div className="flex mb-4">
            <div className="flex-1">Content</div>
            <div className="flex-1">{ viewModel && viewModel?.content }</div>
          </div>
        </div>
  
  
  
        <div className="mb-4 mt-4 border-b-2">
          <div className="flex mb-4">
            <div className="flex-1">Category</div>
            <div className="flex-1">{ viewModel && viewModel?.categories && viewModel?.categories.map( ( cat ) => cat[ 'name' ] ).filter( Boolean ).join( ', ' ) }</div>
          </div>
        </div>
  
  
  
        <div className="mb-4 mt-4 border-b-2">
          <div className="flex mb-4">
            <div className="flex-1">Tags</div>
            <div className="flex-1">{ viewModel && viewModel?.tags && viewModel?.tags.map( ( tag ) => "#"+tag[ 'name' ] ).filter( Boolean ).join( ', ' ) }</div>
          </div>
        </div>
  
  
  
        <div className="mb-4 mt-4 border-b-2">
          <div className="flex mb-4">
            <div className="flex-1">Description</div>
            <div className="flex-1">{ viewModel && viewModel?.description }</div>
          </div>
        </div>
  
         <div className="mb-4 mt-4">
          <div className="flex mb-4">
            <div className="flex-1">Thumbnail</div>
            <div className="flex-1">
            <img className="flex-1 w-[50%] rounded" src={viewModel && viewModel?.thumbnail} alt={viewModel && viewModel?.title} />
            </div>
           
          </div>
        </div>
  
  
  
        {/* <div className="mb-4 mt-4">
          <div className="flex mb-4">
            <div className="flex-1">Content</div>
            <div className="flex-1">{ viewModel && viewModel?.content }</div>
          </div>
        </div> */}
  
  
  
  
  
      </div>
    );
  };
  
  export default ViewAdminBlogPage;
  
