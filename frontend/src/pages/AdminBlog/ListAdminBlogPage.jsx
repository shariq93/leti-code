
  import React from "react";
  import { AuthContext } from "Src/authContext";
  import MkdSDK from "Utils/MkdSDK";
  import { useForm } from "react-hook-form";
  import { useNavigate } from "react-router-dom";
  import { GlobalContext } from "Src/globalContext";
  import { yupResolver } from "@hookform/resolvers/yup";
  import * as yup from "yup";
  import { getNonNullValue } from "Utils/utils";
  import PaginationBar from "Components/PaginationBar";
  import AddButton from "Components/AddButton";
  import ExportButton from "Components/ExportButton";
  import { tokenExpireError } from "Src/authContext";
  import { ModalPrompt } from "Components/Modal";
  
  let sdk = new MkdSDK();
  // TABLE COLUMN ARRAY //
  const columns = [
    {
      header: "ID",
      accessor: "id",
      // isSorted: false,
      // isSortedDesc: false,
      // mappingExist: false,
      // mappings: {}
    },
    {
      header: "Date Posted",
      accessor: "create_at",
      // isSorted: false,
      // isSortedDesc: false,
      // mappingExist: false,
      // mappings: {}
    },
  
    {
      header: "Thumbnail",
      accessor: "thumbnail",
      // isSorted: false,
      // isSortedDesc: false,
      // mappingExist: false,
      // mappings: {}
    },
  
    {
      header: "Title",
      accessor: "title",
      isSorted: false,
      isSortedDesc: false,
      mappingExist: false,
      mappings: {},
    },
    {
      header: "Categories",
      accessor: "categories",
      // isSorted: false,
      // isSortedDesc: false,
      // mappingExist: false,
      // mappings: {}
    },
    // {
    //   header: 'Categories',
    //   accessor: 'categories',
    //   // isSorted: false,
    //   // isSortedDesc: false,
    //   // mappingExist: false,
    //   // mappings: {}
    // },
  
    {
      header: "Description",
      accessor: "description",
      // isSorted: false,
      // isSortedDesc: false,
      // mappingExist: false,
      // mappings: {}
    },
  
    {
      header: "Action",
      accessor: "",
    },
  ];
  
  const ListAdminBlogPage = () => {
    const { dispatch } = React.useContext(AuthContext);
    const { dispatch: globalDispatch } = React.useContext(GlobalContext);
    const navigate = useNavigate();
  
    const [showDeleteModal, setShowDeleteModal] = React.useState(false);
    const [deleteLoading, setDeleteLoading] = React.useState(false);
    const [deleteId, setDeleteId] = React.useState(null);
    const [fetcingTable, setFatchingTable] = React.useState(false);
    const [currentTableData, setCurrentTableData] = React.useState([]);
    const [categories, setCategories] = React.useState([]);
  
    const [query, setQuery] = React.useState("");
    const [blogs, setBlogs] = React.useState([]);
    const [pageSize, setPageSize] = React.useState(10);
    const [pageCount, setPageCount] = React.useState(0);
    const [dataTotal, setDataTotal] = React.useState(0);
    const [currentPage, setPage] = React.useState(0);
    const [canPreviousPage, setCanPreviousPage] = React.useState(false);
    const [canNextPage, setCanNextPage] = React.useState(false);
  
    const schema = yup.object({
      // dateTime: yup.string(),
      // title: yup.string(),
      categories: yup.array(yup.number()),
    });
  
    const {
      register,
      handleSubmit,
      setError,
      reset,
      formState: { errors },
    } = useForm({
      resolver: yupResolver(schema),
    });
  
    // function onSort ( columnIndex ) {
    //   console.log( columns[ columnIndex ] );
    //   if ( columns[ columnIndex ].isSorted ) {
    //     columns[ columnIndex ].isSortedDesc = !columns[ columnIndex ].isSortedDesc;
    //   } else {
    //     columns.map( ( i ) => ( i.isSorted = false ) );
    //     columns.map( ( i ) => ( i.isSortedDesc = false ) );
    //     columns[ columnIndex ].isSorted = true;
    //   }
  
    //   ( async function () {
    //     await getData( 0, pageSize );
    //   } )();
    // }
  
    function updatePageSize(limit) {
      (async function () {
        setPageSize(limit);
        await getData(0, limit);
      })();
    }
  
    function previousPage() {
      (async function () {
        await getData(currentPage - 1 > 0 ? currentPage - 1 : 0, pageSize);
      })();
    }
  
    function nextPage() {
      (async function () {
        await getData(
          currentPage + 1 <= pageCount ? currentPage + 1 : 0,
          pageSize
        );
      })();
    }
  
    async function getData(pageNum, pageSize, tags = [], categories = [], role) {
      try {
        setFatchingTable(true);
        // sdk.setTable( "blog" );
        // let sortField = columns.filter( ( col ) => col.isSorted );
        const result = await sdk.getFilteredBlogs(
          tags,
          categories,
          role,
          pageSize,
          pageNum
        );
        if (!result.error) {
          // setCurrentTableData(result?.data);
          setFatchingTable(false);
          const { data, total, limit, num_pages, page } = result;
  
          setCurrentTableData(data);
          setPageSize(limit);
          setPageCount(num_pages);
          setPage(page);
          setDataTotal(total);
          setCanPreviousPage(page > 1);
          setCanNextPage(page + 1 <= num_pages);
        }
      } catch (error) {
        console.log("ERROR", error);
        tokenExpireError(dispatch, error.message);
        setFatchingTable(false);
      }
    }
    async function getCategories() {
      try {
        const result = await sdk.getallBlogCategories();
        if (!result.error) {
          setCategories(result?.data);
        }
      } catch (error) {
        console.log("ERROR", error);
        tokenExpireError(dispatch, error.message);
      }
    }
  
    // FUNCTION FOR DELETE TABLE //
    const onDeleteClick = id => {
      setDeleteId(id);
      setShowDeleteModal(true);
      // try {
      //   sdk.setTable( "articles" );
      //   const result = await sdk.callRestAPI( { id }, "DELETE" );
      //   setCurrentTableData( list => list.filter( x => Number( x.id ) !== Number( id ) ) );
      // } catch ( err ) {
      //   throw new Error( err );
      // }
    };
    const deleteItem = async () => {
      // setDeleteId( id )
      // setShowDeleteModal( true )
      try {
        setDeleteLoading(true);
        const result = await sdk.deleteBlog(deleteId);
        if (!result.error) {
          getData();
          setDeleteLoading(false);
          setShowDeleteModal(false);
        }
        // setCurrentTableData( list => list.filter( x => Number( x.id ) !== Number( id ) ) );
      } catch (err) {
        setDeleteLoading(false);
        setShowDeleteModal(false);
        tokenExpireError(dispatch, err.message);
      }
    };
  
    const exportTable = async id => {
      try {
        sdk.setTable("notes");
        const result = await sdk.exportCSV();
      } catch (err) {
        throw new Error(err);
      }
    };
  
    // const resetForm = async () => {
    //   reset();
    //   await getData( 0, pageSize );
    // }
  
    // FUNCTION FOR FITERING BLOG //
    const onSubmit = _data => {
      let dateTime = getNonNullValue(_data.dateTime);
      let title = getNonNullValue(_data.title);
      let tags = getNonNullValue(_data.tags);
      console.log("input-info", _data);
      // return;
      // let filter = {
      //   id,
      //   create_at: create_at,
      //   update_at: update_at,
      //   thumbnail: thumbnail,
      //   title: title,
      //   category: category,
      //   tags: tags,
      //   description: description,
      //   content: content,
      // };
      getData(0, pageSize, [], _data.categories);
    };
  
    React.useEffect(() => {
      globalDispatch({
        type: "SETPATH",
        payload: {
          path: "blogs",
        },
      });
  
      (async function () {
        await getData(0, pageSize);
        await getCategories();
      })();
    }, []);
  
    console.log("currentTableData", currentTableData);
  
    return (
      <>
        
  
        <div className="overflow-x-auto  p-5 bg-white shadow rounded">
          <div className="mb-3 text-center justify-between w-full flex  ">
            <h4 className="text-2xl font-medium">Blogs</h4>
            <div className="flex">
              <AddButton link={"/admin/add-blog"} />
              <ExportButton onClick={exportTable} className="mx-1" />
            </div>
          </div>
          <div className="shadow overflow-x-auto border-b border-gray-200 ">
            <table className="min-w-full divide-y divide-gray-200">
              <thead className="bg-gray-50">
                <tr>
                  {columns.map((column, i) => (
                    <th
                      key={i}
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      // onClick={ () => onSort( i ) }
                    >
                      {column.header}
                      <span>
                        {column.isSorted
                          ? column.isSortedDesc
                            ? " ▼"
                            : " ▲"
                          : ""}
                      </span>
                    </th>
                  ))}
                </tr>
              </thead>
              <tbody className="bg-white divide-y divide-gray-200">
                {fetcingTable && !currentTableData.length > 0 ? (
                  <tr className="text-xl font-semibold">
                    <td className="py-12 px-12">Loading...</td>
                  </tr>
                ) : (
                  <>
                    {currentTableData.map((row, i) => {
                      return (
                        <tr key={i}>
                          {columns.map((cell, index) => {
                            if (cell.accessor.indexOf("image") > -1) {
                              return (
                                <td
                                  key={index}
                                  className="text-[1.5rem] px-6 py-4 whitespace-nowrap"
                                >
                                  <img
                                    src={row[cell.accessor]}
                                    className="h-[100px] w-[150px]"
                                  />
                                </td>
                              );
                            }
                            if (
                              cell.accessor.indexOf("pdf") > -1 ||
                              cell.accessor.indexOf("doc") > -1 ||
                              cell.accessor.indexOf("file") > -1 ||
                              cell.accessor.indexOf("video") > -1
                            ) {
                              return (
                                <td
                                  key={index}
                                  className="text-[1.5rem] px-6 py-4 whitespace-nowrap"
                                >
                                  <a
                                    className="text-blue-500"
                                    target="_blank"
                                    href={row[cell.accessor]}
                                  >
                                    {" "}
                                    View
                                  </a>
                                </td>
                              );
                            }
                            if (cell.accessor == "") {
                              return (
                                <td
                                  key={index}
                                  className="text-[1.5rem] px-6 py-4 whitespace-nowrap space-x-1"
                                >
                                  <button
                                    className="text-xs px-1 text-blue-500"
                                    onClick={() => {
                                      navigate("/admin/edit-blog/" + row.id, {
                                        state: row,
                                      });
                                    }}
                                  >
                                    {" "}
                                    Edit
                                  </button>
                                  <button
                                    className="text-xs px-1 text-blue-500"
                                    onClick={() => {
                                      navigate("/admin/view-blog/" + row.id, {
                                        state: row,
                                      });
                                    }}
                                  >
                                    {" "}
                                    View
                                  </button>
                                  <button
                                    className="text-xs px-1 text-red-500"
                                    onClick={() => onDeleteClick(row.id)}
                                  >
                                    {" "}
                                    Delete
                                  </button>
                                </td>
                              );
                            }
                            if (cell.mappingExist) {
                              return (
                                <td
                                  key={index}
                                  className="text-[1.5rem] px-6 py-4 whitespace-nowrap"
                                >
                                  {cell.mappings[row[cell.accessor]]}
                                </td>
                              );
                            }
                            if (cell.accessor === "categories") {
                              return (
                                <td
                                  key={index}
                                  className="text-[1.5rem] px-6 py-4 whitespace-nowrap"
                                >
                                  {row[cell.accessor].length
                                    ? row[cell.accessor]
                                        .map(cat => cat["name"])
                                        .filter(Boolean)
                                        .join(", ")
                                    : "No Category"}
                                </td>
                              );
                            }
                            if (cell.accessor === "tags") {
                              return (
                                <td
                                  key={index}
                                  className="text-[1.5rem] px-6 py-4 whitespace-nowrap"
                                >
                                  {row[cell.accessor].length
                                    ? row[cell.accessor]
                                        .map(tag => "#" + tag["name"])
                                        .filter(Boolean)
                                        .join(", ")
                                    : "No Tag"}
                                </td>
                              );
                            }
                            return (
                              <td
                                key={index}
                                className="text-[1.5rem] px-6 py-4 whitespace-nowrap"
                                title={row[cell.accessor]}
                              >
                                {row[cell.accessor].length > 19
                                  ? `${row[cell.accessor].slice(0, 20) + "..."}`
                                  : row[cell.accessor]}
                              </td>
                            );
                          })}
                        </tr>
                      );
                    })}
                  </>
                )}
              </tbody>
            </table>
          </div>
        </div>
        <div className={`text-xs font-medium`}></div>
        {showDeleteModal ? (
          <ModalPrompt
            closeModalFunction={() => setShowDeleteModal(false)}
            actionHandler={deleteItem}
            title={`Delete Blog`}
            message={`Are you sure you want to delete this blog with ID ${deleteId}? this is irreversible!`}
            messageClasses={`text-xs font-medium`}
            titleClasses={`text-lg font-semibold`}
            acceptText={`${deleteLoading ? "DELETING" : "DELETE"}`}
            rejectText={`CANCEL`}
            loading={deleteLoading}
          />
        ) : null}
        <PaginationBar
          currentPage={currentPage}
          pageCount={pageCount}
          pageSize={pageSize}
          canPreviousPage={canPreviousPage}
          canNextPage={canNextPage}
          updatePageSize={updatePageSize}
          previousPage={previousPage}
          nextPage={nextPage}
        />
      </>
    );
  };
  
  export default ListAdminBlogPage;
  // client_secret
  // :
  // "vs_1MvjnKBgOlWo0lDUqRqTW6bM_secret_test_YWNjdF8xTGw1dWtCZ09sV28wbERVLF9OaDgzN0NJTTJ2ejR5aVZZd2Zoa2tiTTRhVnJqUW450100OHa59Kw1"
  
