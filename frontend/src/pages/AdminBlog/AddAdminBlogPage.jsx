
  import React, { useRef, useState } from "react";
  import { useForm } from "react-hook-form";
  import { yupResolver } from "@hookform/resolvers/yup";
  import * as yup from "yup";
  import MkdSDK from "Utils/MkdSDK";
  import { useNavigate } from "react-router-dom";
  import { tokenExpireError } from "Src/authContext";
  import { GlobalContext, showToast } from "Src/globalContext";
  import ReactQuill from "react-quill";
  import "react-quill/dist/quill.snow.css";
  import { isImage, empty, isVideo, isPdf } from "Utils/utils";
  import Editor from "Components/Blog/Editor";
  import MultiSelect from "Components/MultiSelect";
  import { InteractiveButton } from "Components/InteractiveButton";
  import AddTags from "Components/AddTags";
  // import EditorJS from '@editorjs/editorjs'
  
  const sdk = new MkdSDK();
  const defaultImage = "https://via.placeholder.com/150?text=%20";
  const AddAdminBlogPage = () => {
    const thumbnailRef = useRef(null);
    const { dispatch: globalDispatch } = React.useContext(GlobalContext);
    const { dispatch } = React.useContext(GlobalContext);
    const navigate = useNavigate();
    const [tagData, setTagData] = useState([]);
    const [categoryData, setCategoryData] = useState([]);
  
    const [isUploadThumb, setIsUploadThumb] = React.useState(true);
    const [submitLoading, setSubmitLoading] = useState(false);
    const [previewUrl, setPreviewUrl] = React.useState(defaultImage);
    const [thumbnail, setThumbnail] = React.useState("");
    const [content, setContent] = useState("");
    const [title, setTitle] = useState("");
    const [categories, setCategories] = useState([]);
    const [tag, setTag] = useState("");
    const [description, setDescription] = useState("");
    const [errors, setError] = useState({
      title: { message: "" },
      content: { message: "" },
      tag: { message: "" },
      categories: { message: "" },
      thumbnail: { message: "" },
      description: { message: "" },
    });
  
    // FUCNTION FOR UPLOAD THUMBNAIL //
    const handleImageChange = async e => {
      const formData = new FormData();
      formData.append("file", e.target.files[0]);
      try {
        const result = await sdk.uploadImage(formData);
        setPreviewUrl(result.url);
        setThumbnail(result.url);
        setIsUploadThumb(false);
        setError(prev => ({ ...prev, thumbnail: { message: "" } }));
        // console.log( 'image url', result )
      } catch (err) {
        console.error(err);
      }
    };
  
    // FUNCTION FOR CONTROL THUMBNAIL //
    const handleChangeThumbnail = () => {
      thumbnailRef.current.value = "";
      thumbnailRef.current.title = "";
      setIsUploadThumb(true);
    };
    const handleRemoveThumbnail = () => {
      setPreviewUrl(defaultImage);
      setThumbnail("");
      thumbnailRef.current.value = "";
      thumbnailRef.current.title = "";
      setIsUploadThumb(true);
    };
  
    // FUNCTION FOR SET CONTENT //
    function handleContentChange(value) {
      setContent(value);
      if (value !== "") {
        if (errors.content.message !== "") {
          setError(prev => ({ ...prev, content: { message: "" } }));
        }
      }
      // console.log( value )
    }
    // FUNCTION FOR SET TITLE //
    function handleTitleChange(value) {
      setTitle(value);
      if (value !== "") {
        if (errors.title.message !== "") {
          setError(prev => ({ ...prev, title: { message: "" } }));
        }
      } else {
        if (errors.title.message === "") {
          setError(prev => ({
            ...prev,
            title: { message: "Title is required" },
          }));
        }
      }
      // console.log( value )
    }
  
    // FUNCTION FOR ADD TAGS //
    function handleAddTag(e) {
      if (e.key !== "Enter") return;
      const value = e.target.value;
      // if(tagData.includes(value)){
      //   setError( ( prev ) => ( { ...prev, tags: { message: 'Tag already exists' } } ) )
      //   return
      // }
      if (value.trim().split(" ").length > 1) {
        setError(prev => ({ ...prev, tag: { message: "Accept only one word" } }));
        return;
      }
      setTag(value.trim().toLowerCase());
      e.target.value = "";
      setError(prev => ({ ...prev, tag: { message: "" } }));
    }
  
    function handleCategoryChange(category) {
      setCategories([...categories, category]);
      if (category !== "") {
        setError(prev => ({ ...prev, categories: { message: "" } }));
      }
    }
  
    // const previewImage = ( field, target ) => {
    //   let tempFileObj = fileObj;
    //   tempFileObj[ field ] = {
    //     file: target.files[ 0 ],
    //     tempURL: URL.createObjectURL( target.files[ 0 ] ),
    //   };
    //   setFileObj( { ...tempFileObj } );
    // };
  
    // console.log('category', categoryData)
  
    // FUNCTION FOR ADD NEW BLOG
    const onSubmit = async () => {
      console.log({ title, content, tag, thumbnail, categories, description });
      // console.log( content )
      if (
        title === "" ||
        content === "" ||
        thumbnail === "" ||
        description === "" ||
        tag === "" ||
        !categories.length
      ) {
        if (title === "") {
          setError(prev => ({
            ...prev,
            title: { message: "Title is required" },
          }));
        }
        if (content === "") {
          setError(prev => ({
            ...prev,
            content: { message: "Content is required" },
          }));
        }
        if (thumbnail === "") {
          setError(prev => ({
            ...prev,
            thumbnail: { message: "Thumbnail is required" },
          }));
        }
        if (description === "") {
          setError(prev => ({
            ...prev,
            description: { message: "Description is required" },
          }));
        }
        if (tag === "") {
          setError(prev => ({ ...prev, tag: { message: "Tag is required" } }));
        }
        if (!categories.length) {
          setError(prev => ({
            ...prev,
            categories: { message: "Categories is required" },
          }));
        }
        return;
      }
  
      try {
        setSubmitLoading(true);
  
        // console.log({title,description, content,thumbnail, tags:tagData, categories, description})
        const result = await sdk.createBlog({
          title,
          description,
          content,
          thumbnail,
          meta: {
            site_title: "site_title",
            domain: "domain",
          },
          tags: tagData.length ? tagData.map(tags => tags.id) : [],
          categories,
          // tags: tagIds.length ? tagIds : [],
          // categories: categoryIds.length ? categoryIds : []
        });
        // console.log('result--->',result)
        // return
        if (!result.error) {
          setSubmitLoading(false);
          showToast(globalDispatch, "Added blog");
          navigate("/admin/blogs");
        }
      } catch (error) {
        console.log("Error", error);
        setSubmitLoading(false);
        //   type: "manual",
        //   message: error.message,
        // } );
        tokenExpireError(dispatch, error.message);
      }
    };
  
    // const getTags = async () => {
    //   try {
    //     const result = await sdk.getallBlogTags()
    //     if ( !result.error ) {
    //       setTagData( result?.data )
    //     }
    //   } catch ( error ) {
    //     console.log( "Error", error );
    //     // setSubmitLoading( false )
    //     tokenExpireError( dispatch, error.message );
    //   }
    // }
  
    // FUNCTION FOR GETTING CATEGORIES
    const getCategories = async () => {
      try {
        const result = await sdk.getallBlogCategories();
        if (!result.error) {
          setCategoryData(result?.data);
        }
      } catch (error) {
        console.log("Error", error);
        tokenExpireError(dispatch, error.message);
      }
    };
  
    React.useEffect(() => {
      globalDispatch({
        type: "SETPATH",
        payload: {
          path: "blogs",
        },
      });
    }, []);
  
    React.useEffect(() => {
      if (!categoryData.length) {
        getCategories();
      }
    }, []);
  
    // FUNCTION FOR ADD NEW TAGS AND GET EXISTING TAG
    const addTag = async name => {
      try {
        const result = await sdk.createBlogTag({
          name,
        });
        if (!result.error) {
          setTagData([...tagData, { id: result?.tag_id, name }]);
          // console.log('tagData', tagData)
        }
      } catch (error) {
        console.log("Error", error);
        // tokenExpireError( globalDispatch, error.message );
      }
    };
  
    React.useEffect(() => {
      if (tag !== "") {
        addTag(tag);
      }
    }, [tag]);
  
    return (
      <div className=" shadow-md rounded  mx-auto p-5">
        <h4 className="text-2xl font-medium mb-6">Add News</h4>
        {/* <div className=" w-full max-w-lg"> */}
        <div className=" w-full">
          <div className="mb-4  ">
            <label
              htmlFor="thumbnail"
              className="block text-gray-700 text-sm font-bold mb-2 mt-6"
            >
              Thumbnail
            </label>
            <div
              className={`w-full h-[300px] flex justify-center mb-4 rounded  overflow-hidden ${
                empty(thumbnail) ? "border" : "border-transparent"
              }`}
            >
              <img
                className={`${empty(thumbnail) ? "w-full" : "scale-150"}`}
                src={empty(thumbnail) ? previewUrl : thumbnail}
                alt="preview img"
              />
            </div>
            <input
              ref={thumbnailRef}
              type="file"
              onChange={handleImageChange}
              className={`${
                isUploadThumb ? "block" : "invisible h-0"
              } shadow border-gray-900 border  rounded w-full py-1 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline file:mr-4 file:hover:cursor-pointer file:py-1 file:px-4 file:rounded file:border-0 file:text-sm file:font-semibold file:bg-transparent file:text-blue-500 hover:file:bg-blue-100 duration-300`}
            />
            {errors.thumbnail && (
              <p className="text-red-500 text-field-error italic">
                {errors.thumbnail?.message}
              </p>
            )}
            <div className={`space-x-5 ${isUploadThumb ? "block" : "-mt-6"}`}>
              <button
                disabled={!thumbnail}
                onClick={() => handleChangeThumbnail()}
                className={`text-blue-500 font-semibold duration-300 ${
                  thumbnail && "hover:underline"
                }  disabled:opacity-50`}
              >
                Change
              </button>
              <button
                disabled={!thumbnail}
                onClick={() => handleRemoveThumbnail()}
                className={`text-red-500 font-semibold duration-300 ${
                  thumbnail && "hover:underline"
                } disabled:opacity-50`}
              >
                Remove
              </button>
            </div>
          </div>
  
          <div className="mb-4  ">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="title"
            >
              Title
            </label>
            <input
              placeholder="Title"
              value={title}
              onChange={e => handleTitleChange(e.target.value)}
              className={`shadow appearance-none border border-gray-900 rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-blue-500 ${
                errors.title?.message ? "border-red-500" : ""
              }`}
            />
            {errors.title && (
              <p className="text-red-500 text-field-error italic">
                {errors.title?.message}
              </p>
            )}
          </div>
  
          <div className="mb-4">
            <label
              htmlFor="categories"
              className="block text-gray-700 text-sm font-bold mb-2"
            >
              Category
            </label>
            <select
              name="categories"
              id="categories"
              className="border border-gray-900 rounded w-full py-2 px-3 text-gray-700 leading-tight  focus:shadow-outline"
              onChange={e => handleCategoryChange(e.target.value)}
            >
              {[[], ...categoryData]?.map((option, i) => (
                <option
                  className="py-4 inline-block"
                  name={option?.name}
                  value={option?.id}
                  key={i}
                >
                  {option?.name}
                </option>
              ))}
            </select>
            {errors.categories && (
              <p className="text-red-500 text-field-error italic">
                {errors.categories?.message}
              </p>
            )}
          </div>
  
          <div className="mb-4  ">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="tags"
            >
              Tags
            </label>
            <ul className="border border-gray-900 rounded py-1 px-2 flex flex-wrap gap-2">
              {tagData.length > 0 &&
                tagData.map((tag, i) => (
                  <AddTags
                    key={i}
                    tags={tagData}
                    tag={tag.name}
                    setTagData={setTagData}
                  />
                ))}
              <li>
                <input
                  type="text"
                  className="border-0 p-0 outline-0 focus:ring-transparent"
                  placeholder="Allow multiple tags"
                  onKeyDown={handleAddTag}
                />
              </li>
            </ul>
            {errors.tag && (
              <p className="text-red-500 text-field-error italic">
                {errors.tag?.message}
              </p>
            )}
          </div>
          <div>
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="description"
            >
              Description
            </label>
            <textarea
              className={`shadow appearance-none border border-gray-900 rounded w-full py-2 px-3 text-gray-700 leading-tight focus:ring-blue-500 ${
                errors.description?.message ? "border-red-500" : ""
              }`}
              placeholder="Description"
              name=""
              id="description"
              cols="30"
              onChange={e => {
                setDescription(e.target.value);
                if (description !== "") {
                  setError(prev => ({ ...prev, description: { message: "" } }));
                }
              }}
            ></textarea>
            {errors.description && (
              <p className="text-red-500 text-field-error italic mb-3 -mt-1">
                {errors.description?.message}
              </p>
            )}
          </div>
        </div>
        <div className="mb-4  ">
          <label
            className="block text-gray-700 text-sm font-bold -mb-2"
            htmlFor="content"
          >
            Content
          </label>
        </div>
        <Editor content={content} handleChange={handleContentChange} />
        {errors.content && (
          <p className="text-red-500 text-field-error italic">
            {errors.content?.message}
          </p>
        )}
        <InteractiveButton
          onClick={() => onSubmit()}
          disabled={submitLoading}
          loading={submitLoading}
          type="submit"
          className="bg-blue-500 hover:bg-blue-700 mt-5 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline flex items-center gap-x-2"
        >
          {submitLoading ? "Submiting" : "Submit"}
        </InteractiveButton>
      </div>
    );
  };
  
  export default AddAdminBlogPage;
  
