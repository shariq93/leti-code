
  import React, { useEffect, useRef, useState } from "react";
  import { useForm } from "react-hook-form";
  import { yupResolver } from "@hookform/resolvers/yup";
  import * as yup from "yup";
  import MkdSDK from "Utils/MkdSDK";
  import { GlobalContext, showToast } from "Src/globalContext";
  import { useNavigate, useParams } from "react-router-dom";
  import { AuthContext, tokenExpireError } from "Src/authContext";
  import ReactQuill from "react-quill";
  import "react-quill/dist/quill.snow.css";
  import { isImage, empty, isVideo, isPdf } from "Utils/utils";
  import MultiSelect from "Components/MultiSelect";
  import Editor from "Components/Blog/Editor";
  import { InteractiveButton } from "Components/InteractiveButton";
  import AddTags from "Components/AddTags";
  
  let sdk = new MkdSDK();
  const defaultImage = "https://via.placeholder.com/150?text=%20";
  const EditAdminBlogPage = () => {
    const thumbnailRef = useRef(null);
    const { dispatch } = React.useContext(AuthContext);
    // const schema = yup
    //   .object( {
    //     create_at: yup.string(),
    //     update_at: yup.string(),
    //     thumbnail: yup.string(),
    //     title: yup.string(),
    //     category: yup.string(),
    //     tag: yup.string(),
    //     description: yup.string(),
    //     content: yup.string(),
    //   } )
    //   .required();
    const { dispatch: globalDispatch } = React.useContext(GlobalContext);
    const params = useParams();
    const navigate = useNavigate();
    const [blogData, setBlogData] = useState(null);
    const [content, setContent] = useState("");
    const [title, setTitle] = useState("");
    const [submitLoading, setSubmitLoading] = useState(false);
    const [category, setCategory] = useState([]);
    const [tag, setTag] = useState("");
    const [categoryData, setCategoryData] = useState([]);
    const [description, setDescription] = useState("");
    const [tagData, setTagData] = useState([]);
    const [thumbnail, setThumbnail] = useState("");
    const [errors, setError] = useState({
      title: { message: "" },
      body: { message: "" },
      tag: { message: "" },
      category: { message: "" },
      discription: { message: "" },
      thumbnail: { message: "" },
    });
    // const [ fileObj, setFileObj ] = React.useState( {} );
    const [id, setId] = useState(0);
  
    const [isUploadThumb, setIsUploadThumb] = React.useState(false);
    const [previewUrl, setPreviewUrl] = React.useState(defaultImage);
    // FUCNTION FOR UPLOAD THUMBNAIL
    const handleImageChange = async e => {
      const formData = new FormData();
      formData.append("file", e.target.files[0]);
      try {
        const result = await sdk.uploadImage(formData);
        setPreviewUrl(result.url);
        setThumbnail(result.url);
        setIsUploadThumb(false);
        setError(prev => ({ ...prev, thumbnail: { message: "" } }));
        console.log("image url", result);
      } catch (err) {
        console.error(err);
      }
    };
  
    // FUNCTION FOR CONTROL THUMBNAIL
    const handleChangeThumbnail = () => {
      thumbnailRef.current.value = "";
      thumbnailRef.current.title = "";
      setIsUploadThumb(true);
    };
    const handleRemoveThumbnail = () => {
      setPreviewUrl(defaultImage);
      setThumbnail("");
      thumbnailRef.current.value = "";
      thumbnailRef.current.title = "";
      setIsUploadThumb(true);
    };
  
    // FUNCTION FOR ADD TAGS
    function handleAddTag(e) {
      if (e.key !== "Enter") return;
      const value = e.target.value;
      // if(tagData.includes(value)){
      //   setError( ( prev ) => ( { ...prev, tags: { message: 'Tag already exists' } } ) )
      //   return
      // }
      if (value.trim().split(" ").length > 1) {
        setError(prev => ({ ...prev, tag: { message: "Accept only one word" } }));
        return;
      }
      setTag(value.trim().toLowerCase());
      e.target.value = "";
      setError(prev => ({ ...prev, tag: { message: "" } }));
    }
  
    // ADD NEW AND GET EXISTING TAG
    const addTag = async name => {
      try {
        const result = await sdk.createBlogTag({
          name,
        });
        if (!result.error) {
          setTagData([...tagData, { id: result?.tag_id, name }]);
          // console.log('tagData', tagData)
        }
      } catch (error) {
        console.log("Error", error);
      }
    };
    React.useEffect(() => {
      if (tag !== "") {
        addTag(tag);
      }
    }, [tag]);
  
    // FUNCTION FOR CHANGE CONTENT
    function handleContentChange(value) {
      setContent(value);
      if (value !== "") {
        if (errors.body.message !== "") {
          setError(prev => ({ ...prev, body: { message: "" } }));
        }
      }
      // console.log( value )
    }
  
    // FUNCTION FOR CHANGE TITLE
    function handleTitleChange(value) {
      setTitle(value);
      if (value !== "") {
        if (errors.title.message !== "") {
          setError(prev => ({ ...prev, title: { message: "" } }));
        }
      } else {
        if (errors.title.message === "") {
          setError(prev => ({
            ...prev,
            title: { message: "Title is required" },
          }));
        }
      }
      // console.log( value )
    }
    // function handleTagChange ( tags ) {
    //   // console.log( tags )
    //   setTags( tags )
    //   // if ( tags.length ) {
    //   //   if ( errors.tags.message !== '' ) {
    //   //     setError( ( prev ) => ( { ...prev, tags: { message: '' } } ) )
    //   //   }
    //   // } else {
    //   //   if ( errors.tags.message === '' ) {
    //   //     setError( ( prev ) => ( { ...prev, tags: { message: 'Select at least one tag' } } ) )
    //   //   }
    //   // }
    //   // console.log( value )
    // }
  
    // FUNCTION FOR CHANGE CATEGORY
    function handleCategoryChange(categories) {
      setCategory(categories);
      // if ( categories.length ) {
      //   if ( errors.category.message !== '' ) {
      //     setError( ( prev ) => ( { ...prev, category: { message: '' } } ) )
      //   }
      // } else {
      //   if ( errors.category.message === '' ) {
      //     setError( ( prev ) => ( { ...prev, category: { message: 'Select at least one category' } } ) )
      //   }
      // }
      // console.log( value )
    }
  
    // FUNCTION FOR EDIT BLOG
    const onSubmit = async () => {
      // console.log( content )
      // if ( title === '' || content === '' ) {
      //   if ( title === '' ) {
      //     setError( ( prev ) => ( { ...prev, title: { message: 'Title is required' } } ) )
      //   }
      //   if ( content === '' ) {
      //     setError( ( prev ) => ( { ...prev, body: { message: 'Body is required' } } ) )
      //   }
      // if ( !tags.length ) {
      //   setError( ( prev ) => ( { ...prev, tags: { message: 'Select at least one tag' } } ) )
      // }
      // if ( !category.length ) {
      //   setError( ( prev ) => ( { ...prev, category: { message: 'Select at least one category' } } ) )
      // }
      // return
      // }
  
      console.log({
        title,
        description,
        content,
        thumbnail,
        tags: tagData,
        category,
      });
  
      try {
        setSubmitLoading(true);
        // const tagIds = tags.map( ( tag ) => tag.id ).filter( Boolean )
        // const categoryIds = category.map( ( cat ) => cat.id ).filter( Boolean )
  
        const result = await sdk.editBlog(Number(params.id), {
          title,
          description,
          content,
          thumbnail,
          // meta: {
          //   site_title: 'site_title',
          //   domain: 'domain'
          // },
          tags: tagData.length ? tagData.map(tags => tags.id) : [],
          categories: category.length
            ? category.map(category => category.id)
            : [],
          // tags: tagData.length ? tagIds : [],
          // categories: categoryIds.length ? categoryIds : []
        });
        if (!result.error) {
          setSubmitLoading(false);
          showToast(globalDispatch, "Edited Successfully");
          navigate("/admin/blogs");
        }
      } catch (error) {
        console.log("Error", error);
        setSubmitLoading(false);
        //   type: "manual",
        //   message: error.message,
        // } );
        tokenExpireError(dispatch, error.message);
      }
    };
  
    // FUNCTION FOR GET BLOG //
    const getBlog = async () => {
      try {
        const result = await sdk.getSingleBlog(Number(params.id));
  
        if (!result.error) {
          setTitle(result?.data?.title);
          setContent(result?.data?.content);
          setId(result?.data?.id);
          setTagData(result?.data?.tags);
          setCategory(result?.data?.categories);
          setBlogData(result?.data);
          setThumbnail(result?.data.thumbnail);
          setDescription(result?.data.description);
        }
      } catch (error) {
        console.log("Error", error);
        // setSubmitLoading( false )
        tokenExpireError(dispatch, error.message);
      }
    };
    // const getTags = async () => {
    //   try {
    //     const result = await sdk.getallBlogTags()
    //     if ( !result.error ) {
    //       setTagData( result?.data )
    //     }
    //   } catch ( error ) {
    //     console.log( "Error", error );
    //     // setSubmitLoading( false )
    //     tokenExpireError( dispatch, error.message );
    //   }
    // }
  
    // FUNCTION FOR GET CATEGORY //
    const getCategories = async () => {
      try {
        const result = await sdk.getallBlogCategories();
        if (!result.error) {
          setCategoryData(result?.data);
        }
      } catch (error) {
        console.log("Error", error);
        tokenExpireError(dispatch, error.message);
      }
    };
    React.useEffect(() => {
      globalDispatch({
        type: "SETPATH",
        payload: {
          path: "blogs",
        },
      });
    }, []);
  
    React.useEffect(() => {
      if (!categoryData.length) {
        getCategories();
      }
      if (!blogData) {
        getBlog();
      }
    }, []);
  
    return (
      <div className=" shadow-md rounded  mx-auto p-5">
        <h4 className="text-2xl font-medium">Edit Blog</h4>
        {/* <div className=" w-full max-w-lg"> */}
        <div className=" w-full">
          <div className="mb-4  ">
            <label
              htmlFor="thumbnail"
              className="block text-gray-700 text-sm font-bold mb-2 mt-6"
            >
              Thumbnail
            </label>
            <div
              className={`w-full h-[300px] flex justify-center mb-4 rounded  overflow-hidden ${
                empty(thumbnail) ? "border" : "border-transparent"
              }`}
            >
              <img
                className={`${empty(thumbnail) ? "w-full" : "scale-150"}`}
                src={empty(thumbnail) ? previewUrl : thumbnail}
                alt="preview img"
              />
            </div>
            <input
              ref={thumbnailRef}
              type="file"
              onChange={handleImageChange}
              className={`${
                isUploadThumb ? "block" : "invisible h-0"
              } shadow border-gray-900 border  rounded w-full py-1 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline file:mr-4 file:hover:cursor-pointer file:py-1 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-transparent file:text-blue-500 hover:file:bg-blue-100 duration-300`}
            />
            {errors.thumbnail && (
              <p className="text-red-500 text-field-error italic">
                {errors.thumbnail?.message}
              </p>
            )}
            <div className={`space-x-5 ${isUploadThumb ? "block" : "-mt-6"}`}>
              <button
                disabled={!thumbnail}
                onClick={() => handleChangeThumbnail()}
                className={`text-blue-500 font-semibold duration-300 ${
                  thumbnail && "hover:underline"
                }  disabled:opacity-50`}
              >
                Change
              </button>
              <button
                disabled={!thumbnail}
                onClick={() => handleRemoveThumbnail()}
                className={`text-red-500 font-semibold duration-300 ${
                  thumbnail && "hover:underline"
                } disabled:opacity-50`}
              >
                Remove
              </button>
            </div>
          </div>
  
          <div className="mb-4  ">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="title"
            >
              Title
            </label>
            <input
              placeholder="Title"
              value={title}
              onChange={e => handleTitleChange(e.target.value)}
              className={`shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${
                errors.title?.message ? "border-red-500" : ""
              }`}
            />
  
            <p className="text-red-500 text-field-error italic">
              {errors.title?.message}
            </p>
          </div>
  
          <div className="mb-4  ">
            <MultiSelect
              label={`Category`}
              options={[...categoryData]}
              display={`name`}
              selected={category}
              setSelected={handleCategoryChange}
            />
            <p className="text-red-500 text-field-error italic">
              {errors.category?.message}
            </p>
          </div>
  
          <div className="mb-4  ">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="tags"
            >
              Tags
            </label>
            <ul className="border border-gray-900 rounded py-1 px-2 flex flex-wrap gap-2">
              {tagData.length > 0 &&
                tagData.map((tag, i) => (
                  <AddTags
                    key={i}
                    tags={tagData}
                    tag={tag.name}
                    setTagData={setTagData}
                  />
                ))}
              <li>
                <input
                  type="text"
                  className="border-0 p-0 outline-0 focus:ring-transparent"
                  placeholder="Allow multiple tags"
                  onKeyDown={handleAddTag}
                />
              </li>
            </ul>
            {errors.tag && (
              <p className="text-red-500 text-field-error italic">
                {errors.tag?.message}
              </p>
            )}
          </div>
  
          {/* <div className="mb-4  ">
            <MultiSelect
              label={ `Tags` }
              options={ [ ...tagData ] }
              display={ `name` }
              selected={ tags }
              setSelected={ handleTagChange }
            />
            <p className="text-red-500 text-field-error italic">
              { errors.tags?.message }
            </p>
          </div> */}
        </div>
        <div>
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="description"
          >
            Description
          </label>
          <textarea
            className={`shadow appearance-none border border-gray-900 rounded w-full py-2 px-3 text-gray-700 leading-tight focus:ring-blue-500 ${
              errors.description?.message ? "border-red-500" : ""
            }`}
            placeholder="Description"
            name=""
            id="description"
            cols="30"
            value={description}
            onChange={e => {
              setDescription(e.target.value);
              if (description !== "") {
                setError(prev => ({ ...prev, description: { message: "" } }));
              }
            }}
          ></textarea>
          {errors.description && (
            <p className="text-red-500 text-field-error italic mb-3 -mt-1">
              {errors.description?.message}
            </p>
          )}
        </div>
        <div className="mb-4  ">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="content"
          >
            Content
          </label>
        </div>
        <Editor content={content} handleChange={handleContentChange} />
        <p className="text-red-500 text-field-error italic">
          {errors.body?.message}
        </p>
        <InteractiveButton
          onClick={() => onSubmit()}
          disabled={submitLoading}
          loading={submitLoading}
          type="submit"
          className="bg-blue-500 hover:bg-blue-700 mt-5 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline flex items-center gap-x-2"
        >
          {submitLoading ? "Submiting" : "Submit"}
        </InteractiveButton>
      </div>
    );
  };
  
  export default EditAdminBlogPage;  
