
  import React from "react";
  import { AuthContext } from "Src/authContext";
  import MkdSDK from "Utils/MkdSDK";
  import { useForm } from "react-hook-form";
  import { useNavigate } from "react-router-dom";
  import { GlobalContext } from "Src/globalContext";
  import { yupResolver } from "@hookform/resolvers/yup";
  import * as yup from "yup";
  import { getNonNullValue } from "Utils/utils";
  import PaginationBar from "Components/PaginationBar";
  import AddButton from "Components/AddButton";
  import ExportButton from "Components/ExportButton";
  import { tokenExpireError } from "Src/authContext";
  import { ModalPrompt } from "Components/Modal";
  
  let sdk = new MkdSDK();
  
  const columns = [
    {
      header: "ID",
      accessor: "id",
      // isSorted: false,
      // isSortedDesc: false,
      // mappingExist: false,
      // mappings: {}
    },
    {
      header: "Name",
      accessor: "name",
      isSorted: false,
      isSortedDesc: false,
      mappingExist: false,
      mappings: {},
    },
    {
      header: "PARENT ID",
      accessor: "parent_id",
      // isSorted: false,
      // isSortedDesc: false,
      // mappingExist: false,
      // mappings: {}
    },
    {
      header: "Date Created",
      accessor: "create_at",
      // isSorted: false,
      // isSortedDesc: false,
      // mappingExist: false,
      // mappings: {}
    },
    {
      header: "Action",
      accessor: "",
    },
  ];
  
  const ListAdminBlogCategoryPage = () => {
    const { dispatch } = React.useContext(AuthContext);
    const { dispatch: globalDispatch } = React.useContext(GlobalContext);
  
    const [query, setQuery] = React.useState("");
    const [blogs, setBlogs] = React.useState([]);
    const [currentTableData, setCurrentTableData] = React.useState([]);
  
    const [showDeleteModal, setShowDeleteModal] = React.useState(false);
    const [deleteLoading, setDeleteLoading] = React.useState(false);
    const [deleteId, setDeleteId] = React.useState(null);
    // const [ pageSize, setPageSize ] = React.useState( 10 );
    // const [ pageCount, setPageCount ] = React.useState( 0 );
    // const [ dataTotal, setDataTotal ] = React.useState( 0 );
    // const [ currentPage, setPage ] = React.useState( 0 );
    // const [ canPreviousPage, setCanPreviousPage ] = React.useState( false );
    // const [ canNextPage, setCanNextPage ] = React.useState( false );
    const navigate = useNavigate();
  
    // const schema = yup.object( {
    //   dateTime: yup.string(),
    //   title: yup.string(),
    //   tags: yup.string(),
    // } );
    // const {
    //   register,
    //   handleSubmit,
    //   setError,
    //   reset,
    //   formState: { errors },
    // } = useForm( {
    //   resolver: yupResolver( schema ),
    // } );
  
    // function onSort ( columnIndex ) {
    //   console.log( columns[ columnIndex ] );
    //   if ( columns[ columnIndex ].isSorted ) {
    //     columns[ columnIndex ].isSortedDesc = !columns[ columnIndex ].isSortedDesc;
    //   } else {
    //     columns.map( ( i ) => ( i.isSorted = false ) );
    //     columns.map( ( i ) => ( i.isSortedDesc = false ) );
    //     columns[ columnIndex ].isSorted = true;
    //   }
  
    //   ( async function () {
    //     await getData( 0, pageSize );
    //   } )();
    // }
  
    // function updatePageSize ( limit ) {
    //   ( async function () {
    //     setPageSize( limit );
    //     await getData( 0, limit );
    //   } )();
    // }
  
    // function previousPage () {
    //   ( async function () {
    //     await getData( currentPage - 1 > 0 ? currentPage - 1 : 0, pageSize );
    //   } )();
    // }
  
    // function nextPage () {
    //   ( async function () {
    //     await getData(
    //       currentPage + 1 <= pageCount ? currentPage + 1 : 0,
    //       pageSize
    //     );
    //   } )();
    // }
  
    const onDeleteClick = id => {
      setDeleteId(id);
      setShowDeleteModal(true);
      // try {
      //   sdk.setTable( "articles" );
      //   const result = await sdk.callRestAPI( { id }, "DELETE" );
      //   setCurrentTableData( list => list.filter( x => Number( x.id ) !== Number( id ) ) );
      // } catch ( err ) {
      //   throw new Error( err );
      // }
    };
  
    const deleteItem = async () => {
      // setDeleteId( id )
      // setShowDeleteModal( true )
      try {
        setDeleteLoading(true);
        const result = await sdk.deleteBlogCategory(deleteId);
        if (!result.error) {
          getData();
          setDeleteLoading(false);
          setShowDeleteModal(false);
        }
        // setCurrentTableData( list => list.filter( x => Number( x.id ) !== Number( id ) ) );
      } catch (err) {
        setDeleteLoading(false);
        setShowDeleteModal(false);
        tokenExpireError(dispatch, err.message);
        // throw new Error( err );
      }
    };
    async function getData() {
      try {
        // sdk.setTable( "blog" );
        // let sortField = columns.filter( ( col ) => col.isSorted );
        const result = await sdk.getallBlogCategories();
        if (!result.error) {
          setCurrentTableData(result?.data);
        }
        // const { list, total, limit, num_pages, page } = result;
  
        // setCurrentTableData( list );
        // setPageSize( limit );
        // setPageCount( num_pages );
        // setPage( page );
        // setDataTotal( total );
        // setCanPreviousPage( page > 1 );
        // setCanNextPage( page + 1 <= num_pages );
      } catch (error) {
        console.log("ERROR", error);
        tokenExpireError(dispatch, error.message);
      }
    }
  
    // const deleteItem = async ( id ) => {
    //   try {
    //     sdk.setTable( "articles" );
    //     const result = await sdk.callRestAPI( { id }, "DELETE" );
    //     setCurrentTableData( list => list.filter( x => Number( x.id ) !== Number( id ) ) );
    //   } catch ( err ) {
    //     throw new Error( err );
    //   }
  
    // }
  
    const exportTable = async id => {
      try {
        sdk.setTable("notes");
        const result = await sdk.exportCSV();
      } catch (err) {
        throw new Error(err);
      }
    };
  
    // const resetForm = async () => {
    //   reset();
    //   await getData( 0, pageSize );
    // }
  
    const onSubmit = _data => {
      let dateTime = getNonNullValue(_data.dateTime);
      let title = getNonNullValue(_data.title);
      let tags = getNonNullValue(_data.tags);
      console.log("input-info", dateTime, title, tags);
      return;
      // let filter = {
      //   id,
      //   create_at: create_at,
      //   update_at: update_at,
      //   thumbnail: thumbnail,
      //   title: title,
      //   category: category,
      //   tags: tags,
      //   description: description,
      //   content: content,
      // };
      // getData( 1, pageSize, filter );
    };
  
    React.useEffect(() => {
      globalDispatch({
        type: "SETPATH",
        payload: {
          path: "category",
        },
      });
  
      (async function () {
        await getData();
      })();
    }, []);
  
    return (
      <>
        {/* <form
          className="p-5 bg-white shadow rounded mb-10"
          // onSubmit={ handleSubmit( onSubmit ) }
        >
          <h4 className="text-2xl font-medium">Blogs Search</h4>
          <div className="filter-form-holder mt-10 flex flex-wrap">
  
            <div className="mb-4 w-full md:w-4/12 pr-2 pl-2 ">
              <label
                className="block text-gray-700 text-sm font-bold mb-2"
                htmlFor="dateTime"
              >
                Date & Time
              </label>
              <input
                type="datetime-local"
                placeholder="Date & Time"
                { ...register( "dateTime" ) }
                className={ `shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${ errors.dateTime?.message ? "border-red-500" : ""
                  }` }
              />
              <p className="text-red-500 text-field-error italic">
                { errors.dateTime?.message }
              </p>
            </div>
  
  
            <div className="mb-4 w-full md:w-4/12 pr-2 pl-2 ">
              <label
                className="block text-gray-700 text-sm font-bold mb-2"
                htmlFor="title"
              >
                Title
              </label>
              <input
                placeholder="Title"
                { ...register( "title" ) }
                className={ `shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${ errors.title?.message ? "border-red-500" : ""
                  }` }
              />
              <p className="text-red-500 text-field-error italic">
                { errors.title?.message }
              </p>
            </div>
  
  
            <div className="mb-4 w-full md:w-4/12 pr-2 pl-2 ">
              <label
                className="block text-gray-700 text-sm font-bold mb-2"
                htmlFor="tags"
              >
                Tags
              </label>
              <input
                placeholder="Tags"
                { ...register( "tags" ) }
                className={ `shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${ errors.tags?.message ? "border-red-500" : ""
                  }` }
              />
              <p className="text-red-500 text-field-error italic">
                { errors.tags?.message }
              </p>
            </div>
          </div>
          <button
            type="submit"
            className=" inline ml-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
          >
            Search
          </button>
        </form> */}
  
        <div className="overflow-x-auto  p-5 bg-white shadow rounded">
          <div className="mb-3 text-center justify-between w-full flex  ">
            <h4 className="text-2xl font-medium">Categories</h4>
            <div className="flex">
              <AddButton link={"/admin/add-blog-category"} />
              <ExportButton onClick={exportTable} className="mx-1" />
            </div>
          </div>
          <div className="shadow overflow-x-auto border-b border-gray-200 ">
            <table className="min-w-full divide-y divide-gray-200">
              <thead className="bg-gray-50">
                <tr>
                  {columns.map((column, i) => (
                    <th
                      key={i}
                      scope="col"
                      className="px-6 py-3 text-left text-[1rem] font-medium text-gray-500 uppercase tracking-wider"
                      // onClick={ () => onSort( i ) }
                    >
                      {column.header}
                      <span>
                        {column.isSorted
                          ? column.isSortedDesc
                            ? " ▼"
                            : " ▲"
                          : ""}
                      </span>
                    </th>
                  ))}
                </tr>
              </thead>
              <tbody className="bg-white divide-y divide-gray-200">
                {currentTableData.map((row, i) => {
                  return (
                    <tr key={i}>
                      {columns.map((cell, index) => {
                        if (cell.accessor.indexOf("image") > -1) {
                          return (
                            <td
                              key={index}
                              className="text-[1.5rem] px-6 py-4 whitespace-nowrap"
                            >
                              <img
                                src={row[cell.accessor]}
                                className="h-[100px] w-[150px]"
                              />
                            </td>
                          );
                        }
                        if (
                          cell.accessor.indexOf("pdf") > -1 ||
                          cell.accessor.indexOf("doc") > -1 ||
                          cell.accessor.indexOf("file") > -1 ||
                          cell.accessor.indexOf("video") > -1
                        ) {
                          return (
                            <td
                              key={index}
                              className="text-[1.5rem] px-6 py-4 whitespace-nowrap"
                            >
                              <a
                                className="text-blue-500"
                                target="_blank"
                                href={row[cell.accessor]}
                              >
                                {" "}
                                View
                              </a>
                            </td>
                          );
                        }
                        if (cell.accessor == "") {
                          return (
                            <td
                              key={index}
                              className="text-[1.5rem] px-6 py-4 whitespace-nowrap"
                            >
                              <button
                                className="text-xs"
                                onClick={() => {
                                  navigate(
                                    "/admin/edit-blog-category/" + row.id,
                                    {
                                      state: row,
                                    }
                                  );
                                }}
                              >
                                {" "}
                                Edit
                              </button>
                              {/* <button
                                className="text-xs px-1 text-blue-500"
                                onClick={ () => {
                                  navigate( "/admin/view-blog-category/" + row.id, {
                                    state: row,
                                  } );
                                } }
                              >
                                { " " }
                                View
                              </button> */}
                              <button
                                className="text-xs px-1 text-red-500"
                                onClick={() => onDeleteClick(row.id)}
                              >
                                {" "}
                                Delete
                              </button>
                            </td>
                          );
                        }
                        if (cell.mappingExist) {
                          return (
                            <td
                              key={index}
                              className="text-[1.5rem] px-6 py-4 whitespace-nowrap"
                            >
                              {cell.mappings[row[cell.accessor]]}
                            </td>
                          );
                        }
                        return (
                          <td
                            key={index}
                            className="text-[1.5rem] px-6 py-4 whitespace-nowrap"
                          >
                            {row[cell.accessor]}
                          </td>
                        );
                      })}
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
        {showDeleteModal ? (
          <ModalPrompt
            closeModalFunction={() => setShowDeleteModal(false)}
            actionHandler={deleteItem}
            title={`Delete Category`}
            message={`Are you sure you want to delete this category with ID ${deleteId}? this is irreversible!`}
            messageClasses={`text-xs font-medium`}
            titleClasses={`text-lg font-semibold`}
            acceptText={`${deleteLoading ? "DELETING" : "DELETE"}`}
            rejectText={`CANCEL`}
            loading={deleteLoading}
          />
        ) : null}
        {/* <PaginationBar
          currentPage={ currentPage }
          pageCount={ pageCount }
          pageSize={ pageSize }
          canPreviousPage={ canPreviousPage }
          canNextPage={ canNextPage }
          updatePageSize={ updatePageSize }
          previousPage={ previousPage }
          nextPage={ nextPage }
        /> */}
      </>
    );
  };
  
  export default ListAdminBlogCategoryPage;
  
