import React from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import MkdSDK from "../utils/MkdSDK";
import { useNavigate, useParams } from "react-router-dom";
import { isImage } from "../utils/utils";
import { GlobalContext, showToast } from "../globalContext";
import { tokenExpireError } from "../authContext";
import ReactQuill from "react-quill";
import DynamicContentType from "../components/DynamicContentType";

let sdk = new MkdSDK();


const EditAdminTestimonialPage = () => {
  const schema = yup
  .object({
    review: yup.string().required(),
    rate: yup.string().required(),
    user_first_name: yup.string().required(),
    // user_last_name: yup.string().required(),
    photo: yup.string(),
    logo: yup.string(),
  })
  .required();

//   const selectType = [
//     { key: "text", value: "Text" },
//     { key: "image", value: "Image" },
//     { key: "number", value: "Number" },
//     { key: "kvp", value: "Key-Value Pair" },
//     { key: "image-list", value: "Image List" },
//     { key: "captioned-image-list", value: "Captioned Image List" },
//     { key: "team-list", value: "Team List" },
//   ];

const { dispatch } = React.useContext(GlobalContext);
const { dispatch: globalDispatch } = React.useContext(GlobalContext);

//   const [contentType, setContentType] = React.useState(selectType[0]?.key);
  const [showPhoto, setShowPhoto] = React.useState("");
  const [showFlag, setShowFlag] = React.useState("");
  const [id, setId] = React.useState(0);


const params = useParams();


const [fileObj, setFileObj] = React.useState({
  photo: null,
  logo: null,
});
const navigate = useNavigate();
const {
  register,
  handleSubmit,
  control,
  setError,
  setValue,
  formState: { errors },
} = useForm({
  resolver: yupResolver(schema),
});

const quillModules = {
  toolbar: [
    [{ header: [1, 2, 3, 4, 5, 6, false] }],
    ["bold", "italic", "underline", "strike"],
    [{ list: "ordered" }, { list: "bullet" }],
    ["link"],
    ["image"],
    [{ align: [] }], 
  ],
};

const quillFormats = [
  "header",
  "bold",
  "italic",
  "underline",
  "strike",
  "list",
  "bullet",
  "link",
  "align", 
  "image",
];

React.useEffect(() => {
  globalDispatch({
    type: "SETPATH",
    payload: {
      path: "testimonial",
    },
  });
  (async function () {
    try {
      const result = await sdk.callRawAPI(
        `/v4/api/records/cms_testimonials/${params?.id}`,
        {},
        "GET"
      );
      if (!result.error) {
        setValue("review", result.model.review);
        setValue("rate", result.model.rate);
        setValue("user_first_name", result.model.user_first_name);
        setValue("logo", result.model.user_name_label);
        setValue("photo", result.model.photo);
        setShowPhoto(result.model.photo)
        setShowFlag(result.model.user_name_label)
        setId(result.model.id);
      }
    } catch (error) {
      console.log("error", error);
      tokenExpireError(dispatch, error.message);
    }
  })();
}, []);


const previewImage = (field, target) => {
  let tempFileObj = { ...fileObj }; // Make a copy of fileObj
  tempFileObj[field] = {
    file: target.files[0],
    tempURL: URL.createObjectURL(target.files[0]),
  };
  setFileObj(tempFileObj);
};


const onSubmit = async (data) => {
  let sdk = new MkdSDK();

  try {
    if (fileObj.photo) {
      let formData = new FormData();
      formData.append("file", fileObj.photo.file);
      let uploadResult = await sdk.uploadImage(formData);
      data.photo = uploadResult.url;
    }

    // Upload user name label photo
    if (fileObj.logo) {
      let formData = new FormData();
      formData.append("file", fileObj.logo.file);
      let uploadResult = await sdk.uploadImage(formData);
      data.logo = uploadResult.url;
    }
    const result = await sdk.callRawAPI(
      `/v4/api/records/cms_testimonials/${id}`,
      {
        id,
        review: data.review,
        rate: data.rate,
        user_first_name: data.user_first_name,
        user_last_name: data.user_first_name,
        photo: data.photo,
        user_name_label: data.logo,
      },
      "PUT"
    );
    if (!result.error) {
      showToast(globalDispatch, "Edited");
      navigate("/admin/testimonials");
    } else {
      if (result.validation) {
        const keys = Object.keys(result.validation);
        for (let i = 0; i < keys.length; i++) {
          const field = keys[i];
          setError(field, {
            type: "manual",
            message: result.validation[field],
          });
        }
      }
    }
  } catch (error) {
    console.log("Error", error);
    setError("seq", {
      type: "manual",
      message: error.message,
    });
    tokenExpireError(dispatch, error.message);
  }
};



  return (
    <div className=" shadow-md rounded  mx-auto p-5">
    <h4 className="text-2xl font-medium">Edit Testimonial </h4>
    <form className=" w-full max-w-lg" onSubmit={handleSubmit(onSubmit)}>
      <div className="mb-4">
        <label
          className="block text-gray-700 text-sm font-bold mb-2"
          htmlFor="rate"
        >
          Rate
        </label>

        <select
          {...register("rate")}
          className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
        >
          <option value="">Select Rating</option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select>
      
      </div>
      <div className="mb-4">
        <label
          className="block text-gray-700 text-sm font-bold mb-2"
          htmlFor="user_first_name"
        >
          Name
        </label>
        <input
          placeholder="Enter name"
          {...register("user_first_name")}
          className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
        />
      </div>
      {/* <div className="mb-4">
        <label
          className="block text-gray-700 text-sm font-bold mb-2"
          htmlFor="user_last_name"
        >
          Last Name
        </label>
        <input
          placeholder="Last name"
          {...register("user_last_name")}
          className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
        />
      </div> */}

      <div className="mb-4">
        <label
          className="block text-gray-700 text-sm font-bold mb-2"
          htmlFor="review"
        >
          Reviews
        </label>

        <Controller
          name="review"
          control={control}
          defaultValue=""
          render={({ field }) => (
            <ReactQuill
              value={field.value}
              onChange={field.onChange}
              onBlur={field.onBlur}
              theme="snow"
              modules={quillModules}
              formats={quillFormats}
            />
          )}
        />
      </div>

      <div className="mb-4">
        <label
          className="block text-gray-700 text-sm font-bold mb-2"
          htmlFor="photo"
        >
          photo
        </label>
        <div className="mb-4 mt-2">
          <input
            type="file"
            placeholder="photo"
            {...register("photo", {
              onChange: (e) => previewImage("photo", e.target),
            })}
            className={`"shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${
              errors.photo?.message ? "border-red-500" : ""
            }`}
          />
          {fileObj["photo"] ? (
            isImage(fileObj["photo"]) ? (
              <img
                className={"preview-image"}
                src={fileObj["photo"]["tempURL"]}
              ></img>
            ) : (
              <>
              <img src={showPhoto} />
              </>
            )
          ) : (
            <>   <img src={showPhoto} /></>
          )}

          <p className="text-red-500 text-xs italic">
            {errors.photo?.message}
          </p>
        </div>
      </div>

      <div className="mb-4">
        <label
          className="block text-gray-700 text-sm font-bold mb-2"
          htmlFor="logo"
        >
          Flag
        </label>
        <input
            type="file"
            placeholder="photo"
            {...register("logo", {
              onChange: (e) => previewImage("logo", e.target), // Use "logo" for user name label photo
            })}
            className={`"shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${
              errors.photo?.message ? "border-red-500" : ""
            }`}
          />
          {fileObj["logo"] ? (
            isImage(fileObj["logo"]) ? (
              <img
                className={"preview-image"}
                src={fileObj["logo"]["tempURL"]}
              ></img>
            ) : (
              <>  <img width={'300px'} src={showFlag} /></>
            )
          ) : (
            <> <img width={'200px'} src={showFlag} /></>
          )}
       
      </div>

      <button
        type="submit"
        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
      >
        Submit
      </button>
    </form>
  </div>
  );
}

export default EditAdminTestimonialPage