import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import MkdSDK from "../utils/MkdSDK";
import { Link, useLocation } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { InteractiveButton } from "../components/InteractiveButton";
import { AuthContext } from "../authContext";
import { GlobalContext, showToast } from "../globalContext";
import logo from "../assets/logo.svg";

let sdk = new MkdSDK();

const AdminLoginPage = () => {
  const schema = yup
    .object({
      email: yup.string().email().required(),
      password: yup.string().required(),
    })
    .required();

  const { dispatch } = React.useContext(AuthContext);
  const { dispatch: GlobalDispatch } = React.useContext(GlobalContext);

  const [submitLoading, setSubmitLoading] = useState(false);
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const redirect_uri = searchParams.get("redirect_uri");
  const navigate = useNavigate();
  const {
    register,
    handleSubmit,
    setError,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = async (data) => {
    try {
      setSubmitLoading(true);
      const result = await sdk.login(data.email, data.password, "admin");
      if (!result.error) {
        dispatch({
          type: "LOGIN",
          payload: result,
        });
        showToast(GlobalDispatch, "Succesfully Logged In", 4000, "success");
        navigate(redirect_uri ?? "/dashboard");
      } else {
        setSubmitLoading(false);
        if (result.validation) {
          const keys = Object.keys(result.validation);
          for (let i = 0; i < keys.length; i++) {
            const field = keys[i];
            setError(field, {
              type: "manual",
              message: result.validation[field],
            });
          }
        }
      }
    } catch (error) {
      setSubmitLoading(false);
      console.log("Error", error);
      setError("email", {
        type: "manual",
        message: error.response.data.message
          ? error.response.data.message
          : error.message,
      });
    }
  };

  return (
    <div className="h-screen m-auto max-h-screen min-h-screen">
      <div className="min-h-full flex justify-center w-full max-h-full h-full">

      <section className="md:block hidden w-1/2">
          <div className="login-right-box relative">
            <img className="login-page-logo" width="100" src={logo} alt="" />
            <h2>
              <span>Empowering Learning</span> Your Personalized Path with IBTE
            </h2>
          </div>
        </section>

        <section className="md:w-1/2 w-full flex flex-col items-center justify-center bg-white">
          <form
            onSubmit={handleSubmit(onSubmit)}
            className="flex flex-col max-w-md w-full px-6 mt-[9.375rem]"
          >
            <h1 className="md:font-bold font-semibold md:text-5xl text-3xl text-center mb-8">
              Log In
            </h1>
            <div className="mb-4">
              <label
                className="block text-gray-700 text-sm font-bold mb-2"
                htmlFor="email"
              >
                Email
              </label>

              <input
                type="email"
                autoComplete="off"
                placeholder="Email"
                {...register("email")}
                className={`resize-none border-2 p-2 px-4 bg-transparent mb-3 active: outline-none shadow appearance-none rounded w-full py-2  text-gray-700 leading-tight focus:outline-none focus: shadow-outline ${
                  errors && errors.email?.message ? "border-red-500" : ""
                }`}
              />
              <p className="text-red-500 text-xs italic">
                {errors.email?.message}
              </p>
            </div>

            <div className="mb-6">
              <label
                className="block text-gray-700 text-sm font-bold mb-2"
                htmlFor="password"
              >
                Password
              </label>
              <input
                autoComplete="off"
                type="password"
                name="password"
                placeholder="Password"
                {...register("password")}
                className={`border-2 focus: outline-none active: outline-none flex-grow p-2 px-4 shadow appearance-none rounded w-full py-2 text-gray-700 mb-3 leading-tight focus: shadow-outline ${
                  errors && errors.password?.message ? "border-red-500" : ""
                } `}
              />
              <button type="button" className="absolute right-1 top-[20%]">
                <img src="/invisible.png" alt="" className="w-6 mr-2" />
              </button>
              <p className="text-red-500 text-xs italic">
                {errors.password?.message}
              </p>
            </div>

            <Link
              className={`self-end mb-6 text-black font-semibold text-sm bg-clip-text bg-gradient-to-l from-[#33d4b7_9.11 %] to-[#0d9895_69.45 %]`}
              to="/admin/forgot"
            >
              Forgot Password
            </Link>

            <InteractiveButton
              type="submit"
              className={`flex justify-center items-center login-btn`}
              loading={submitLoading}
              disabled={submitLoading}
            >
              <span>Continue</span>
            </InteractiveButton>
          </form>

          <div className="oauth flex flex-col gap-4 max-w-md w-full px-6 text-[#344054] grow">
            <div>
              <h3 className="text-center text-sm text-gray-800 normal-case">
                Don't have an account?{" "}
                <Link
                  className="self-end mb-8 my-text-gradient font-semibold text-sm"
                  to="/admin/sign-up"
                >
                  Sign up
                </Link>{" "}
              </h3>
            </div>
          </div>
          {/* <div className={ `h-auto w-full` }></div> */}
          <p className="text-center text-gray-500 text-xs h-10 my-5">
            &copy; {new Date().getFullYear()} IBTE inc. All rights
            reserved.
          </p>
        </section>
        
      </div>
    </div>
  );
};

export default AdminLoginPage;
