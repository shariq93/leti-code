import React from "react";
import { AuthContext } from "../authContext";
import MkdSDK from "../utils/MkdSDK";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { GlobalContext, showToast } from "../globalContext";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { getNonNullValue } from "../utils/utils";
import PaginationBar from "../components/PaginationBar";
import AddButton from "../components/AddButton";
import ModalDelete from "Components/Modal/ModalDelete";
import { useCallback, useState } from "react";

import TreeSDK from "Utils/TreeSDK";
import moment from "moment";
import TableEmailBox from "Components/TableEmailBox";

let sdk = new MkdSDK();
let tdk = new TreeSDK();

const columns = [
  {
    header: "Action",
    accessor: "",
  },

  {
    header: "Name",
    accessor: "first_name",
  },
  {
    header: "Email",
    accessor: "email",
  },
  {
    header: "Phone",
    accessor: "phone",
  },
  {
    header: "Preferred Date",
    accessor: "preferred_date",
  },
  {
    header: "Tuition",
    accessor: "tuition",
    mappingExist: true,
    mappings: { null: "--", 1: "Selected" },
  },
  {
    header: "Admission",
    accessor: "admission",
    mappingExist: true,
    mappings: { null: "--", 1: "Selected" },
  },
  {
    header: "Requirement",
    accessor: "requirement",
    mappingExist: true,
    mappings: { null: "--", 1: "Selected" },
  },
  {
    header: "Programs",
    accessor: "programs",
    mappingExist: true,
    mappings: { null: "--", 1: "Selected" },
  },
  {
    header: "other",
    accessor: "other",
    mappingExist: true,
    mappings: { null: "--", 1: "Selected" },
  },
  {
    header: "Message",
    accessor: "message",
  },
  {
    header: "Send Email",
    accessor: "sendEmail",
  },
];

const ContactEmails = () => {
  const { dispatch } = React.useContext(AuthContext);
  const { dispatch: globalDispatch } = React.useContext(GlobalContext);


  const [query, setQuery] = React.useState("");
  const [data, setCurrentTableData] = React.useState([]);
  const [pageSize, setPageSize] = React.useState(3);
  const [pageCount, setPageCount] = React.useState(0);
  const [dataTotal, setDataTotal] = React.useState(0);
  const [currentPage, setPage] = React.useState(0);
  const [canPreviousPage, setCanPreviousPage] = React.useState(false);
  const [canNextPage, setCanNextPage] = React.useState(false);

 
  const navigate = useNavigate();

  const schema = yup.object({
    content_value: yup.string(),
  });
  const selectType = [
    { key: "", value: "All" },
    { key: "text", value: "Text" },
    { key: "image", value: "Image" },
    { key: "number", value: "Number" },
  ];
  const [itemToDelete, setItemToDelete] = React.useState(null);
  const [ShowConfirmationModal, setConfirmationModal] = useState(false);
  const [id, setID] = useState("");
  const onCloseConfirmationModal = useCallback(() => {
    setConfirmationModal(false);
  }, [ShowConfirmationModal]);

  function handleDeleteClick(itemId) {
    setItemToDelete(itemId);
    setConfirmationModal(true);
  }

  function handleConfirmDelete() {
    // console.log(itemToDelete);
    deleteItem(itemToDelete);
    setConfirmationModal(false);
  }

  function handleCancelDelete() {
    console.log(itemToDelete);
    setItemToDelete(null);
    setConfirmationModal(false);
  }

  const {
    register,
    handleSubmit,
    setError,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  function updatePageSize(limit) {
    (async function () {
      setPageSize(limit);
      await getData(0, limit);
    })();
  }

  function previousPage() {
    (async function () {
      await getData(currentPage - 1 > 0 ? currentPage - 1 : 0, pageSize);
    })();
  }

  function nextPage() {
    (async function () {
      await getData(
        currentPage + 1 <= pageCount ? currentPage + 1 : 0,
        pageSize
      );
    })();
  }

  async function getData(pageNum, limitNum, data) {
    try {
      // sdk.setTable("cms_admissions");
      // const result = await sdk.callRestAPI(
      //   {
      //     payload: { ...data },
      //     page: pageNum,
      //     limit: limitNum,
      //   },
      //   "PAGINATE"
      // );

      const result = await tdk.getPaginate("contact_form", {
        filter: data,
        payload: { ...data },
        page: pageNum || 1,
        limit: limitNum,
        size: limitNum,
        order: "create_at",
      });

      const { list, total, limit, num_pages, page } = result;

      setCurrentTableData(list);
      setPageSize(limit);
      setPageCount(num_pages);
      setPage(page);
      setDataTotal(total);
      setCanPreviousPage(page > 1);
      setCanNextPage(page + 1 <= num_pages);
    } catch (error) {
      console.log("ERROR", error);
      tokenExpireError(dispatch, error.message);
    }
  }

  const onSubmit = (data) => {
    let content_value = getNonNullValue(data.content_value);
    let filter = [];
    if (data.content_value) {
      filter.push(`content_value,cs,${content_value}`);
    }
    {
      getData(1, 200, filter);
    }
  };



 

  const sendEmail = async (row,toEmail) => {
    let sdk = new MkdSDK();

    try {
      
      const formatDate = moment(row.prefer_date).format(
        "YYYY-MM-DD"
      )

      const result = await sdk.callRawAPI('/v3/api/lambda/contact-us',
        {
          first_name: row.first_name,
          last_name: row.last_name,
          phone: row.phone,
          prefer_date: formatDate,
          message: row.message,
          email: row.email,
          to: toEmail,
          tuition: row.tuition,
          admission: row.admission,
          requirement: row.requirement,
          programs: row.programs,
          other: row.other,
        },
        "POST"
      );
      if (!result.error) {
        showToast(globalDispatch, "Email Sent");
      } else {
        if (result.validation) {
          const keys = Object.keys(result.validation);
          for (let i = 0; i < keys.length; i++) {
            const field = keys[i];
            setError(field, {
              type: "manual",
              message: result.validation[field],
            });
          }
        }
      }
    } catch (error) {
      console.log("Error", error);
      setError("seq", {
        type: "manual",
        message: error.message,
      });
      tokenExpireError(dispatch, error.message);
    }
  };

  const deleteItem = async (id) => {
    try {
      sdk.setTable("contact_form");
      const result = await sdk.callRestAPI({ id }, "DELETE");
      setCurrentTableData((list) =>
        list.filter((x) => Number(x.id) !== Number(id))
      );
    } catch (err) {
      throw new Error(err);
    }
  };

  const resetForm = async () => {
    reset();
    await getData(0, 10);
  };

  React.useEffect(() => {
    globalDispatch({
      type: "SETPATH",
      payload: {
        path: "contact_email",
      },
    });

    (async function () {
      await getData(0, 10);
    })();
  }, []);

  return (
    <>
      {ShowConfirmationModal ? (
        <ModalDelete
          title="Are you sure"
          id={id}
          deleteItem={deleteItem}
          onConfirmDelete={handleConfirmDelete}
          onCloseConfirmationModal={handleCancelDelete}
        />
      ) : null}

      <div className="flex flex-col gap-10">
        <div>
          {/* <form
              className="p-2 bg-white shadow rounded mb-5"
              onSubmit={handleSubmit(onSubmit)}
            >
              <h4 className="text-2xl font-medium">Emails Search</h4>
              <div className="filter-form-holder mt-10 flex flex-wrap">
                <div className="mb-4 w-full md:w-1/3 pr-2 pl-2">
                  <label className="block text-gray-700 text-sm font-bold mb-2">
                    Value
                  </label>
                  <input
                    type="text"
                    placeholder="Value"
                    {...register("content_value")}
                    className="shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                  />
                  <p className="text-red-500 text-xs italic">
                    {errors.content_value?.message}
                  </p>
                </div>
              </div>
  
              <button
                type="submit"
                className=" inline ml-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
              >
                Search
              </button>
              <button
                onClick={() => {
                  resetForm();
                }}
                type="button"
                className=" inline ml-2 bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
              >
                Clear
              </button>
            </form> */}

          <div className="overflow-x-auto  p-2 pt-0 bg-white shadow rounded">
            <div className="mb-3 text-center justify-between w-full flex  ">
              <h4 className="text-2xl font-medium">Contact Emails </h4>
              <AddButton link={"/admin/add-admission"} />
            </div>
            <div className="shadow overflow-x-auto border-b border-gray-200 ">
              <table className="min-w-full divide-y divide-gray-200">
                <thead className="bg-gray-50">
                  <tr>
                    {columns.map((column, i) => (
                      <th
                        key={i}
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        {column.header}
                        <span>
                          {column.isSorted
                            ? column.isSortedDesc
                              ? " ▼"
                              : " ▲"
                            : ""}
                        </span>
                      </th>
                    ))}
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {data.map((row, i) => {
                    return (
                      <tr key={i}>
                        {columns.map((cell, index) => {
                          if (cell.accessor == "content_value") {
                            return (
                              <td>
                                <p className="text-sm"> {row[cell.accessor]}</p>
                              </td>
                            );
                          }
                          if (cell.accessor == "preferred_date") {
                            return (
                              <td>
                                <span className="text-sm">
                                  {moment(row[cell.accessor]).format(
                                    "YYYY-MM-DD"
                                  )}
                                </span>
                              </td>
                            );
                          }
                          if (cell.accessor == "") {
                            return (
                              <td
                                key={index}
                                className="px-6 py-4 whitespace-nowrap"
                              >
                                <button
                                  className="text-xs"
                                  onClick={() => {
                                    navigate("/admin/view_emails/" + row.id, {
                                      state: row,
                                    });
                                  }}
                                >
                                  {" "}
                                  View
                                </button>
                                <button
                                  className="text-xs px-1 text-red-500"
                                  // onClick={() => deleteItem(row.id)}
                                  onClick={() => handleDeleteClick(row.id)}
                                >
                                  {" "}
                                  Delete
                                </button>
                              </td>
                            );
                          }

                          if (cell.accessor == "sendEmail") {
                            return (
                              <td
                                key={index}
                                className="px-6 py-4 whitespace-nowrap"
                              >
                                
                             <TableEmailBox sendEmail={sendEmail} row={row}/>
                              </td>
                            );
                          }

                          if (cell.mappingExist) {
                            return (
                              <td
                                key={index}
                                className="px-6 py-4 whitespace-nowrap"
                              >
                                {cell.mappings[row[cell.accessor]]}
                              </td>
                            );
                          }

                          if (cell.mapping) {
                            return (
                              <td
                                key={index}
                                className="px-6 py-4 whitespace-nowrap"
                              >
                                {cell.mapping[row[cell.accessor]]}
                              </td>
                            );
                          }
                          return (
                            <td
                              key={index}
                              className="px-6 py-4 whitespace-nowrap"
                            >
                              {row[cell.accessor]}
                            </td>
                          );
                        })}
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
          <PaginationBar
            currentPage={currentPage}
            pageCount={pageCount}
            pageSize={pageSize}
            canPreviousPage={canPreviousPage}
            canNextPage={canNextPage}
            updatePageSize={updatePageSize}
            previousPage={previousPage}
            nextPage={nextPage}
          />
        </div>
      </div>
    </>
  );
};

export default ContactEmails;
