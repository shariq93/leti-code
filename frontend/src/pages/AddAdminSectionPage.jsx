import React from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import MkdSDK from "../utils/MkdSDK";
import { useNavigate } from "react-router-dom";
import { GlobalContext, showToast } from "../globalContext";
import { tokenExpireError } from "../authContext";
import DynamicContentType from "../components/DynamicContentType";

const AddAdminSectionPage = () => {
  const schema = yup
  .object({
    // page: yup.string().required(),
    key: yup.string().required(),
    value: yup.string(),
    animate: yup.string(),
  })
  .required();



const { dispatch } = React.useContext(GlobalContext);
const { dispatch: globalDispatch } = React.useContext(GlobalContext);


const navigate = useNavigate();
const {
  register,
  handleSubmit,
  setError,
  formState: { errors },
} = useForm({
  resolver: yupResolver(schema),
});

// const onSubmit = async (data) => {
//   let sdk = new MkdSDK();
//   console.log(data);
//   try {
//     sdk.setTable("cms");

//     const result = await sdk.callRestAPI(
//       {  page: data.page, seq: data.seq, key: data.key, type: data.type, contentValue },
//       "POST"
//     );

//     if (!result.error) {
//       navigate("/admin/cms");
//       showToast(globalDispatch, "Added");
//     } else {
//       if (result.validation) {
//         const keys = Object.keys(result.validation);
//         for (let i = 0; i < keys.length; i++) {
//           const field = keys[i];
//           setError(field, {
//             type: "manual",
//             message: result.validation[field],
//           });
//         }
//       }
//     }
//   } catch (error) {
//     console.log("Error", error);
//     setError("page", {
//       type: "manual",
//       message: error.message,
//     });
//     tokenExpireError(dispatch, error.message);
//   }
// };

const onSubmit = async (data) => {
  let sdk = new MkdSDK();

  try {
    sdk.setTable("render_settings");

    const result = await sdk.callRestAPI(
      {
        // page: data.page,
        // seq: data.seq,
        setting_key: data.key,
        setting_value: data.value,
        animate: data.animate,
      },
      "POST"
    );
    if (!result.error) {
      showToast(globalDispatch, "Added");
      navigate("/admin/section");
    } else {
      if (result.validation) {
        const keys = Object.keys(result.validation);
        for (let i = 0; i < keys.length; i++) {
          const field = keys[i];
          setError(field, {
            type: "manual",
            message: result.validation[field],
          });
        }
      }
    }
  } catch (error) {
    console.log("Error", error);
    setError("seq", {
      type: "manual",
      message: error.message,
    });
    tokenExpireError(dispatch, error.message);
  }
};

React.useEffect(() => {
  globalDispatch({
    type: "SETPATH",
    payload: {
      path: "section",
    },
  });
}, []);

return (
  <div className=" shadow-md rounded  mx-auto p-5">
    <h4 className="text-2xl font-medium">Add section Content</h4>
    <form className=" w-full max-w-lg" onSubmit={handleSubmit(onSubmit)}>
     
     
      <div className="mb-4">
        <label
          className="block text-gray-700 text-sm font-bold mb-2"
          htmlFor="key"
        >
          Content Identifier
        </label>
        <input
          type="text"
          placeholder="Content Identifier"
          {...register("key")}
          className={`shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${
            errors.key?.message ? "border-red-500" : ""
          }`}
        />
        <p className="text-red-500 text-xs italic">{errors.key?.message}</p>
      </div>


      <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="value"
          >
            Status
          </label>
          <select
         {...register("value")}
         className={`shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${errors.value?.message ? "border-red-500" : ""}`}
       
          >
            <option value="1">Active</option>
            <option value="0">Inactive</option>
          </select>
         
          <p className="text-red-500 text-xs italic">{errors.value?.message}</p>
        </div>

        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="value"
          >
            Animation
          </label>
          <select
         {...register("animate")}
         className={`shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${errors.value?.message ? "border-red-500" : ""}`}
       
          >
            <option value="1">Enable</option>
            <option value="0">Disable</option>
          </select>
         
          <p className="text-red-500 text-xs italic">{errors.animate?.message}</p>
        </div>
      

      <button
        type="submit"
        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
      >
        Submit
      </button>
    </form>
  </div>
);
}

export default AddAdminSectionPage