import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import MkdSDK from "../utils/MkdSDK";
import { GlobalContext, showToast } from "../globalContext";
import { useNavigate, useParams } from "react-router-dom";
import { AuthContext, tokenExpireError } from "../authContext";
import DynamicContentType from "../components/DynamicContentType";

let sdk = new MkdSDK();


const EditMarketingPages = () => {

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
    });
  };

  scrollToTop();


   const schema = yup
    .object({
      // page: yup.string().required(),
      key: yup.string(),
      value: yup.string(),

    })
    .required();


  
  const { dispatch } = React.useContext(AuthContext);
  const { dispatch: globalDispatch } = React.useContext(GlobalContext);
  const navigate = useNavigate();
  const [id, setId] = useState(0);
  const [contentValue, setContentValue] = React.useState('');


  const {
    register,
    handleSubmit,
    setError,
    setValue,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const params = useParams();

  useEffect(function () {
    globalDispatch({
      type: "SETPATH",
      payload: {
        path: "page",
      },
    });

    (async function () {
      try {
        sdk.setTable("render_settings");
        const result = await sdk.callRestAPI({ id: Number(params?.id) }, "GET");
        if (!result.error) {
          // setValue("page", result.model.page);
          setValue("key", result.model.setting_key);
          setValue("value", result.model.setting_value);
          setContentType(result.model.setting_type);
          setContentValue(result.model.setting_value);
          setId(result.model.id);
        }
      } catch (error) {
        console.log("error", error);
        tokenExpireError(dispatch, error.message);
      }
    })();
  }, []);

  const onSubmit = async (data) => {
    let sdk2 = new MkdSDK();
  

    try {
      sdk2.setTable("render_settings");

      const result = await sdk2.callRestAPI(
        {
          id: params?.id,
          seq: data.seq,
          setting_key: data.key,
          setting_value: data.value,
          animate: 0,
        },
        "PUT"
      );
      if (!result.error) {
        showToast(globalDispatch, "Edited Page Condition");
        navigate("/admin/page");
      } else {
        if (result.validation) {
          const keys = Object.keys(result.validation);
          for (let i = 0; i < keys.length; i++) {
            const field = keys[i];
            setError(field, {
              type: "manual",
              message: result.validation[field],
            });
          }
        }
      }
    } catch (error) {
      console.log("Error", error);
      setError("seq", {
        type: "manual",
        message: error.message,
      });
      tokenExpireError(dispatch, error.message);
    }
  };

  return (
    <div className=" shadow-md rounded   mx-auto p-5">
      <h4 className="text-2xl font-medium">Edit Page Conditions</h4>
      <form className=" w-full max-w-lg" onSubmit={handleSubmit(onSubmit)}>
    
     
      <div className="mb-4 pointer-events-none opacity-5">
          <label
            className="block text-gray-700 text-sm font-bold mb-2 "
            htmlFor="key"
          >
            Content Identifier
          </label>
          <input
            type="text"
            placeholder="Content Identifier"
            {...register("key")}
            className={`shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${errors.key?.message ? "border-red-500" : ""}`}
          />
          <p className="text-red-500 text-xs italic">{errors.key?.message}</p>
        </div>

        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="value"
          >
            Status
          </label>
          <select
         {...register("value")}
         className={`shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${errors.value?.message ? "border-red-500" : ""}`}
       
          >
            <option value="1">Active</option>
            <option value="0">Draft</option>
          </select>
         
          <p className="text-red-500 text-xs italic">{errors.value?.message}</p>
        </div>

    

       
      
        <button
          type="submit"
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
        >
          Submit
        </button>
      </form>
    </div>
  );
}

export default EditMarketingPages