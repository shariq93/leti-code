import React from "react";
import OurPartnersContent from "Components/OurPartnersContent";
import OurPartnersLogos from "Components/OurPartnersLogos";
import OurPartnersSlides from "Components/OurPartnersSlides";



const AdminPartnerList = () => {
 

  return (
   <>
   <div className="flex flex-col gap-10">
   
   <OurPartnersContent/>
   <OurPartnersSlides/>
   <OurPartnersLogos/>
   </div>
   </>
  );
};

export default AdminPartnerList;
