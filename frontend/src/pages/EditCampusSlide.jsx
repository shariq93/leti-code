import React from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import MkdSDK from "../utils/MkdSDK";
import { useNavigate, useParams } from "react-router-dom";
import { isImage } from "../utils/utils";
import { GlobalContext, showToast } from "../globalContext";
import { tokenExpireError } from "../authContext";
import ReactQuill from "react-quill";
import DynamicContentType from "../components/DynamicContentType";

let sdk = new MkdSDK();

const EditCampusSlide = () => {
  const schema = yup
    .object({
      tagline: yup.string().required(),
      title: yup.string().required(),
      logo: yup.string().required(),
      descriotion: yup.string(),
      order_: yup.string(),
    })
    .required();

  const { dispatch } = React.useContext(GlobalContext);
  const { dispatch: globalDispatch } = React.useContext(GlobalContext);

  const [showPhoto, setShowPhoto] = React.useState("");
  const [id, setId] = React.useState(0);

  const params = useParams();

  const [fileObj, setFileObj] = React.useState({
    photo: null,
    logo: null,
  });
  const navigate = useNavigate();
  const {
    register,
    handleSubmit,
    control,
    setError,
    setValue,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const quillModules = {
    toolbar: [
      [{ header: [1, 2, 3, 4, 5, 6, false] }],
      ["bold", "italic", "underline", "strike"],
      [{ list: "ordered" }, { list: "bullet" }],
      ["link"],
      ["image"],
      [{ align: [] }],
    ],
  };

  const quillFormats = [
    "header",
    "bold",
    "italic",
    "underline",
    "strike",
    "list",
    "bullet",
    "link",
    "align",
    "image",
  ];

  React.useEffect(() => {
    globalDispatch({
      type: "SETPATH",
      payload: {
        path: "campus",
      },
    });
    (async function () {
      try {
        const result = await sdk.callRawAPI(
          `/v4/api/records/cms_campus/${params?.id}`,
          {},
          "GET"
        );
        if (!result.error) {
          setValue("title", result.model.title);
          setValue("tagline", result.model.tagline);
          setValue("logo", result.model.image);
          setValue("description", result.model.description);
          setValue("order_", result.model.order_);
          setShowPhoto(result.model.image);
          setId(result.model.id);
        }
      } catch (error) {
        console.log("error", error);
        tokenExpireError(dispatch, error.message);
      }
    })();
  }, []);

  const previewImage = (field, target) => {
    let tempFileObj = { ...fileObj };
    tempFileObj[field] = {
      file: target.files[0],
      tempURL: URL.createObjectURL(target.files[0]),
    };
    setFileObj(tempFileObj);
  };

  const onSubmit = async (data) => {
    let sdk = new MkdSDK();

    try {
      // Upload user name label photo
      if (fileObj.logo) {
        let formData = new FormData();
        formData.append("file", fileObj.logo.file);
        let uploadResult = await sdk.uploadImage(formData);
        data.logo = uploadResult.url;
      }
      const result = await sdk.callRawAPI(
        `/v4/api/records/cms_campus/${id}`,
        {
          id,
          title: data.title,
          tagline: data.tagline,
          description: data.description,
          image: data.logo,
          order_: data.order_
        },
        "PUT"
      );
      if (!result.error) {
        showToast(globalDispatch, "Edited");
        navigate("/admin/campus");
      } else {
        if (result.validation) {
          const keys = Object.keys(result.validation);
          for (let i = 0; i < keys.length; i++) {
            const field = keys[i];
            setError(field, {
              type: "manual",
              message: result.validation[field],
            });
          }
        }
      }
    } catch (error) {
      console.log("Error", error);
      setError("seq", {
        type: "manual",
        message: error.message,
      });
      tokenExpireError(dispatch, error.message);
    }
  };

  return (
    <div className=" shadow-md rounded  mx-auto p-5">
      <h4 className="text-2xl font-medium">Edit Campus SLide </h4>
      <form className=" w-full max-w-lg" onSubmit={handleSubmit(onSubmit)}>
        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="tagline"
          >
            Tagline
          </label>
          <input
            placeholder="Type Tagline"
            {...register("tagline")}
            className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
          />
        </div>

        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="order_"
          >
            Order
          </label>
          <select
              {...register("order_")}
              className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
            >
              <option value="">Select</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
              <option value="7">7</option>
              <option value="8">8</option>
              <option value="9">9</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
              <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
              <option value="16">16</option>
              <option value="17">17</option>
              <option value="18">18</option>
              <option value="19">19</option>
            </select>
        </div>

        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="title"
          >
            Title
          </label>
          <input
            placeholder="Type Title"
            {...register("title")}
            className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
          />
        </div>

        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="description1"
          >
            Description
          </label>

          <Controller
            name="description"
            control={control}
            defaultValue=""
            render={({ field }) => (
              <ReactQuill
                value={field.value}
                onChange={field.onChange}
                onBlur={field.onBlur}
                theme="snow"
                modules={quillModules}
                formats={quillFormats}
              />
            )}
          />
        </div>


        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="logo"
          >
            Logo
          </label>
          <input
            type="file"
            {...register("logo", {
              onChange: (e) => previewImage("logo", e.target),
            })}
            className={`"shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${
              errors.logo?.message ? "border-red-500" : ""
            }`}
          />
          {fileObj["logo"] ? (
            isImage(fileObj["logo"]) ? (
              <img
                className={"preview-image"}
                src={fileObj["logo"]["tempURL"]}
              ></img>
            ) : (
              <>
                <img className={"preview-image"} src={showPhoto}></img>
              </>
            )
          ) : (
            <>
              <img className={"preview-image"} src={showPhoto}></img>
            </>
          )}
        </div>

        <button
          type="submit"
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
        >
          Submit
        </button>
      </form>
    </div>
  );
};

export default EditCampusSlide;
