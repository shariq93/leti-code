import FAQList from 'Components/FAQList'
import FaqContentBox from 'Components/FaqContentBox'
import React from 'react'

const AdminFaqList = () => {
  return (
  <div className="flex flex-col gap-10">
    <FaqContentBox/>
    <FAQList/>
  </div>
  )
}

export default AdminFaqList