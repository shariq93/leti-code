import React from "react";
import { AuthContext } from "../authContext";
import MkdSDK from "../utils/MkdSDK";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { GlobalContext } from "../globalContext";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { getNonNullValue } from "../utils/utils";
import PaginationBar from "../components/PaginationBar";
import AddButton from "../components/AddButton";
import { useCallback, useState } from "react";
import ReactHtmlParser from "react-html-parser";
import moment from "moment";
import TreeSDK from "Utils/TreeSDK";
import NewContentBox from "Components/NewContentBox";

let sdk = new MkdSDK();
let tdk = new TreeSDK();

const columns = [
  {
    header: "Action",
    accessor: "",
  },
  {
    header: "Title",
    accessor: "title",
  },
  // {
  //   header: "Summary",
  //   accessor: "summary",
  // },
  // {
  //   header: "Content",
  //   accessor: "content",
  // },
  {
    header: "Status",
    accessor: "status",
  },
  {
    header: "Tags",
    accessor: "tags",
  },
  {
    header: "Cover",
    accessor: "cover",
  },
  {
    header: "Publish date",
    accessor: "published_at",
  },

  {
    header: "Author",
    accessor: "author",
  },
];

const AdminNewsPage = () => {
  const { dispatch } = React.useContext(AuthContext);
  const { dispatch: globalDispatch } = React.useContext(GlobalContext);

  const [query, setQuery] = React.useState("");
  const [data, setCurrentTableData] = React.useState([]);
  const [pageSize, setPageSize] = React.useState(3);
  const [pageCount, setPageCount] = React.useState(0);
  const [dataTotal, setDataTotal] = React.useState(0);
  const [currentPage, setPage] = React.useState(0);
  const [canPreviousPage, setCanPreviousPage] = React.useState(false);
  const [canNextPage, setCanNextPage] = React.useState(false);
  const navigate = useNavigate();

  const schema = yup.object({
    title: yup.string(),
    tags: yup.string(),
    author: yup.string(),
  });

  const selectType = [
    { key: "", value: "All" },
    { key: "text", value: "Text" },
    { key: "image", value: "Image" },
    { key: "number", value: "Number" },
  ];
  const [itemToDelete, setItemToDelete] = React.useState(null);
  const [ShowConfirmationModal, setConfirmationModal] = useState(false);
  const [id, setID] = useState("");
  const onCloseConfirmationModal = useCallback(() => {
    setConfirmationModal(false);
  }, [ShowConfirmationModal]);

  const {
    register,
    handleSubmit,
    setError,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  function updatePageSize(limit) {
    (async function () {
      setPageSize(limit);
      await getData(0, limit);
    })();
  }

  function previousPage() {
    (async function () {
      await getData(currentPage - 1 > 0 ? currentPage - 1 : 0, pageSize);
    })();
  }

  function nextPage() {
    (async function () {
      await getData(
        currentPage + 1 <= pageCount ? currentPage + 1 : 0,
        pageSize
      );
    })();
  }

  async function getData(pageNum, limitNum, data) {
    try {
      const result = await tdk.getPaginate("cms_events_news", {
        filter: data,
        payload: { ...data },
        page: pageNum || 1,
        limit: limitNum,
        size: limitNum,
        order: "create_at",
      });

      const { list, total, limit, num_pages, page } = result;

      setCurrentTableData(list);
      setPageSize(limit);
      setPageCount(num_pages);
      setPage(page);
      setDataTotal(total);
      setCanPreviousPage(page > 1);
      setCanNextPage(page + 1 <= num_pages);
    } catch (error) {
      console.log("ERROR", error);
      tokenExpireError(dispatch, error.message);
    }
  }

  const onSubmit = (data) => {
    let title = getNonNullValue(data.title);
    let tags = getNonNullValue(data.tags);
    let author = getNonNullValue(data.author);
    let filter = [];
    if (data.title) {
      filter.push(`title,cs,${title}`);
    }
    if (data.tags) {
      filter.push(`tags,cs,${tags}`);
    }
    if (data.author) {
      filter.push(`author,cs,${author}`);
    }
    {
      getData(1, 200, filter);
    }
  };
  const deleteItem = async (id) => {
    try {
      // const result = await sdk.callRawAPI('/v4/api/records/cms_team',{ id }, "DELETE");
      const result = await sdk.callRawAPI(
        "/v4/api/records/cms_events_news/" + id,
        {},
        "DELETE"
      );
      setCurrentTableData((list) =>
        list.filter((x) => Number(x.id) !== Number(id))
      );
    } catch (err) {
      throw new Error(err);
    }
  };

  const resetForm = async () => {
    reset();
    await getData(0, 10);
  };
  React.useEffect(() => {
    globalDispatch({
      type: "SETPATH",
      payload: {
        path: "news",
      },
    });

    (async function () {
      await getData(0, 10);
    })();
  }, []);

  const { dispatch: gDispatch } = React.useContext(GlobalContext);
  return (
    <>
      <div className="flex flex-col gap-10">
        <NewContentBox />
        <div>
          <form
            className="p-2 bg-white shadow rounded mb-5"
            onSubmit={handleSubmit(onSubmit)}
          >
       
            <div className="flex justify-between">
            <h4 className="text-2xl font-medium">News Search</h4>
              <button
                onClick={() => {
                  gDispatch({
                    type: "EDIT_PAGE",
                    payload: { pageName: "page_news_and_events" },
                  });
                  navigate("/admin/page");
                }}
                className=" inline ml-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
              >
                Page Hide/Show
              </button>
            </div>
            <div className="filter-form-holder mt-10 flex flex-wrap">
              <div className="mb-4 w-full md:w-1/3 pr-2 pl-2">
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  TITLE
                </label>
                <input
                  type="text"
                  placeholder="title"
                  {...register("title")}
                  className="shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                />
                <p className="text-red-500 text-xs italic">
                  {errors.title?.message}
                </p>
              </div>
              <div className="mb-4 w-full md:w-1/3 pr-2 pl-2">
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  TAGS
                </label>
                <input
                  type="text"
                  placeholder="tags"
                  {...register("tags")}
                  className="shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                />
                <p className="text-red-500 text-xs italic">
                  {errors.tags?.message}
                </p>
              </div>
              <div className="mb-4 w-full md:w-1/3 pr-2 pl-2">
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  AUTHOR
                </label>
                <input
                  type="text"
                  placeholder="author"
                  {...register("author")}
                  className="shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                />
                <p className="text-red-500 text-xs italic">
                  {errors.author?.message}
                </p>
              </div>
            </div>

            <button
              type="submit"
              className=" inline ml-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            >
              Search
            </button>
            <button
              onClick={() => {
                resetForm();
              }}
              type="button"
              className=" inline ml-2 bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            >
              Clear
            </button>
          </form>

          <div className="overflow-x-auto  p-2 pt-0 bg-white shadow rounded">
            <div className="mb-3 text-center justify-between w-full flex  ">
              <h4 className="text-2xl font-medium">News </h4>
              <AddButton link={"/admin/add-news"} />
            </div>
            <div className="shadow overflow-x-auto border-b border-gray-200 ">
              <table className="min-w-full divide-y divide-gray-200">
                <thead className="bg-gray-50">
                  <tr>
                    {columns.map((column, i) => (
                      <th
                        key={i}
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        {column.header}
                        <span>
                          {column.isSorted
                            ? column.isSortedDesc
                              ? " ▼"
                              : " ▲"
                            : ""}
                        </span>
                      </th>
                    ))}
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {data.map((row, i) => {
                    return (
                      <tr key={i}>
                        {columns.map((cell, index) => {
                          if (cell.accessor == "description") {
                            return (
                              <td>
                                <p className="text-sm"> {row[cell.accessor]}</p>
                              </td>
                            );
                          }
                          if (cell.accessor == "summary") {
                            return (
                              <td>
                                <span className="text-sm">
                                  {" "}
                                  {ReactHtmlParser(row[cell.accessor])}
                                </span>
                              </td>
                            );
                          }
                          if (cell.accessor == "title") {
                            return (
                              <td>
                                <span className="text-sm mx-8">
                                  {" "}
                                  {ReactHtmlParser(row[cell.accessor])}
                                </span>
                              </td>
                            );
                          }
                          if (cell.accessor == "published_at") {
                            return (
                              <td>
                                <span className="text-sm">
                                  {moment(row[cell.accessor]).format(
                                    "YYYY MM DD"
                                  )}
                                </span>
                              </td>
                            );
                          }
                          if (cell.accessor == "content") {
                            return (
                              <td>
                                <span className="text-sm">
                                  {" "}
                                  {ReactHtmlParser(row[cell.accessor])}
                                </span>
                              </td>
                            );
                          }
                          if (cell.accessor == "cover") {
                            return (
                              <td
                                key={index}
                                className="px-6 py-4 whitespace-nowrap"
                              >
                                {cell.accessor == "cover" ? (
                                  <img
                                    width={"200px"}
                                    height={"200px"}
                                    src={`${row[cell.accessor]}`}
                                  />
                                ) : (
                                  row[cell.accessor]
                                )}
                              </td>
                            );
                          }
                          if (cell.accessor == "") {
                            return (
                              <td
                                key={index}
                                className="px-6 py-4 whitespace-nowrap"
                              >
                                <button
                                  className="text-xs"
                                  onClick={() => {
                                    navigate("/admin/edit-news/" + row.id, {
                                      state: row,
                                    });
                                  }}
                                >
                                  {" "}
                                  Edit
                                </button>
                                <button
                                  className="text-xs px-1 text-red-500"
                                  onClick={() => deleteItem(row.id)}
                                >
                                  Delete
                                </button>
                              </td>
                            );
                          }
                          if (cell.mapping) {
                            return (
                              <td
                                key={index}
                                className="px-6 py-4 whitespace-nowrap"
                              >
                                {cell.mapping[row[cell.accessor]]}
                              </td>
                            );
                          }
                          return (
                            <td
                              key={index}
                              className="px-6 py-4 whitespace-nowrap"
                            >
                              {row[cell.accessor]}
                            </td>
                          );
                        })}
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
          <PaginationBar
            currentPage={currentPage}
            pageCount={pageCount}
            pageSize={pageSize}
            canPreviousPage={canPreviousPage}
            canNextPage={canNextPage}
            updatePageSize={updatePageSize}
            previousPage={previousPage}
            nextPage={nextPage}
          />
        </div>
      </div>
    </>
  );
};

export default AdminNewsPage;
