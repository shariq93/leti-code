import React, { useEffect, useState } from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import MkdSDK from "../utils/MkdSDK";
import { GlobalContext, showToast } from "../globalContext";
import { isImage } from "../utils/utils";
import { useNavigate, useParams } from "react-router-dom";
import { AuthContext, tokenExpireError } from "../authContext";
import ReactQuill from "react-quill";
import DynamicContentType from "../components/DynamicContentType";
import moment from "moment";
let sdk = new MkdSDK();

const EditAdminNewsPage = () => {
  const schema = yup
    .object({
        question: yup.string().required(),
        answer: yup.string().required(),
        category: yup.string().required(),
    })
    .required();

  const { dispatch } = React.useContext(AuthContext);
  const { dispatch: globalDispatch } = React.useContext(GlobalContext);

  const [published_date, setPublished_at] = useState('');
  const navigate = useNavigate();
  const [id, setId] = React.useState(0);
  

  const {
    register,
    handleSubmit,
    setError,
    control,
    setValue,
    getValues,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const params = useParams();

  useEffect(function () {
    globalDispatch({
      type: "SETPATH",
      payload: {
        path: "faq",
      },
    });

    (async function () {
      try {
        const result = await sdk.callRawAPI(
          `/v4/api/records/cms_faq/${params?.id}`,
          {},
          "GET"
        );
        if (!result.error) {
          setValue("question", result.model.question);
          setValue("answer", result.model.answer);
          setValue("category", result.model.category);
          setId(result.model.id);
        }
      } catch (error) {
        console.log("error", error);
        tokenExpireError(dispatch, error.message);
      }
    })();
  }, []);


  const quillModules = {
    toolbar: [
      [{ header: [1, 2, 3, 4, 5, 6, false] }],
      ["bold", "italic", "underline", "strike"],
      [{ list: "ordered" }, { list: "bullet" }],
      ["link"],
      ["image"],
      [{ align: [] }], 
    ],
  };
  
  const quillFormats = [
    "header",
    "bold",
    "italic",
    "underline",
    "strike",
    "list",
    "bullet",
    "link",
    "align", 
    "image",
  ];


  const onSubmit = async (data) => {
    let sdk = new MkdSDK();

    try {
      const result = await sdk.callRawAPI(
        `/v4/api/records/cms_faq/${id}`,
        {
          id,
          question: data.question,
          answer: data.answer,
          category: data.category,
        },
        "PUT"
      );
      if (!result.error) {
        showToast(globalDispatch, "Edited");
        navigate("/admin/faq");
      } else {
        if (result.validation) {
          const keys = Object.keys(result.validation);
          for (let i = 0; i < keys.length; i++) {
            const field = keys[i];
            setError(field, {
              type: "manual",
              message: result.validation[field],
            });
          }
        }
      }
    } catch (error) {
      console.log("Error", error);
      setError("seq", {
        type: "manual",
        message: error.message,
      });
      tokenExpireError(dispatch, error.message);
    }
  };

  return (
    <div className=" shadow-md rounded  mx-auto p-5">
    <h4 className="text-2xl font-medium">Edit FAQ </h4>
    <form className=" w-full max-w-lg" onSubmit={handleSubmit(onSubmit)}>
      <div className="mb-4">
        <label
          className="block text-gray-700 text-sm font-bold mb-2"
          htmlFor="category"
        >
          Category
        </label>
        <select
          className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
          {...register("category")}
        >
          <option value="">Select Category</option>
          <option value="General Info">General Info</option>
          <option value="Students Info">Students Info</option>
          <option value="IBTE Portal">IBTE Portal</option>
          <option value="Programs">Programs</option>
          <option value="Admissions">Admissions</option>
          <option value="Fees">Fees</option>
          <option value="Career and Internship">Career and Internship</option>
          <option value="Transfer">Transfer</option>
          <option value="Online">Online</option>
          <option value="Student Life">Student Life</option>
          <option value="International campuses">
            International campuses
          </option>
          <option value="partnership">Partnership and Cooperation</option>
        </select>
      </div>
      <div className="mb-4">
        <label
          className="block text-gray-700 text-sm font-bold mb-2"
          htmlFor="question"
        >
          Question
        </label>
        <input
          type="text"
          placeholder="question"
          {...register("question")}
          className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
        />
      </div>
      <div className="mb-4">
        <label
          className="block text-gray-700 text-sm font-bold mb-2"
          htmlFor="answer"
        >
          Answer
        </label>
        <Controller
          name="answer"
          control={control}
          defaultValue=""
          render={({ field }) => (
            <ReactQuill
              value={field.value}
              onChange={field.onChange}
              onBlur={field.onBlur}
              theme="snow"
              placeholder="Type your answer here..."
            />
          )}
        />
        {/* <input
          
        
          className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
        /> */}
      </div>

      <button
        type="submit"
        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
      >
        Submit
      </button>
    </form>
  </div>
  );
}

export default EditAdminNewsPage