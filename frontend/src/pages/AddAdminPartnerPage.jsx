import React from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import MkdSDK from "../utils/MkdSDK";
import { useNavigate } from "react-router-dom";
import { GlobalContext, showToast } from "../globalContext";
import { tokenExpireError } from "../authContext";
import { isImage } from "../utils/utils";
import ReactQuill from "react-quill";
import Select from "react-select";
import { useState } from "react";
import axios from "axios";

const AddAdminPartnerPage = () => {
  // const [countryOptions, setCountryOptions] = useState([]);
  const schema = yup
    .object({
      // label: yup.string(),
      country: yup.string().required(),
      // title: yup.string().required(),
      description: yup.string().required(),
      photo: yup.string(),
    })
    .required();

  const { dispatch } = React.useContext(GlobalContext);
  const { dispatch: globalDispatch } = React.useContext(GlobalContext);
  const [fileObj, setFileObj] = React.useState({});

  const navigate = useNavigate();
  const {
    register,
    control,
    handleSubmit,
    setError,
    setValue,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  // const fetchCountryNames = async () => {
  //   try {
  //     const response = await axios.get("https://restcountries.com/v3.1/all");
  //     const countryNames = response.data.map((country) => ({
  //       value: country.name.common,
  //       label: country.name.common,
  //     }));
  //     return countryNames;
  //   } catch (error) {
  //     console.error("Error fetching country names:", error);
  //     return [];
  //   }
  // };

  const previewImage = (field, target) => {
    let tempFileObj = fileObj;
    tempFileObj[field] = {
      file: target.files[0],
      tempURL: URL.createObjectURL(target.files[0]),
    };
    setFileObj({ ...tempFileObj });
  };

  const onSubmit = async (data) => {
    console.log("Form data submitted:", data);
    let sdk = new MkdSDK();

    try {
      for (let item in fileObj) {
        let formData = new FormData();
        formData.append("file", fileObj[item].file);
        let uploadResult = await sdk.uploadImage(formData);
        data[item] = uploadResult.url;
      }
      const result = await sdk.callRawAPI(
        "/v4/api/records/cms_partners",
        {
          label: data.country,
          country: data.country,
          title: data.country,
          description: data.description,
          logo: data.photo,
        },
        "POST"
      );
      if (!result.error) {
        showToast(globalDispatch, "Added");
        navigate("/admin/partner");
      } else {
        if (result.validation) {
          const keys = Object.keys(result.validation);
          for (let i = 0; i < keys.length; i++) {
            const field = keys[i];
            setError(field, {
              type: "manual",
              message: result.validation[field],
            });
          }
        }
      }
    } catch (error) {
      console.log("Error", error);
      setError("seq", {
        type: "manual",
        message: error.message,
      });
      tokenExpireError(dispatch, error.message);
    }
  };

  React.useEffect(() => {
    // const fetchCountries = async () => {
    //   const countries = await fetchCountryNames();
    //   setCountryOptions(countries);
    // };
    // fetchCountries();
    globalDispatch({
      type: "SETPATH",
      payload: {
        path: "partner",
      },
    });
  }, []);

  return (
    <div className=" shadow-md rounded  mx-auto p-5">
      <h4 className="text-2xl font-medium">Add Partners </h4>
      <form className=" w-full max-w-lg" onSubmit={handleSubmit(onSubmit)}>
       
        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="country"
          >
            Country
          </label>
          <input
            placeholder="country"
            {...register("country")}
            className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
          />

          {errors.country && (
            <p className="text-red-500 text-xs italic">
              {errors.country.message}
            </p>
          )}
        </div>
       
        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="description"
          >
            Description
          </label>
          <Controller
            name="description"
            control={control}
            defaultValue=""
            render={({ field }) => (
              <ReactQuill
                value={field.value}
                onChange={field.onChange}
                onBlur={field.onBlur}
                theme="snow"
              />
            )}
          />
         
        </div>

        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="photo"
          >
            Flage
          </label>
          <div className="mb-4 mt-2">
            <input
              type="file"
              placeholder="photo"
              {...register("photo", {
                onChange: (e) => previewImage("photo", e.target),
              })}
              className={`"shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${
                errors.photo?.message ? "border-red-500" : ""
              }`}
            />
            {fileObj["photo"] ? (
              isImage(fileObj["photo"]) ? (
                <img
                  className={"preview-image"}
                  src={fileObj["photo"]["tempURL"]}
                ></img>
              ) : (
                <></>
              )
            ) : (
              <></>
            )}

            <p className="text-red-500 text-xs italic">
              {errors.photo?.message}
            </p>
          </div>
        </div>

        <button
          type="submit"
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
        >
          Submit
        </button>
      </form>
    </div>
  );
};

export default AddAdminPartnerPage;
