import React from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import MkdSDK from "../utils/MkdSDK";
import { useNavigate } from "react-router-dom";
import { GlobalContext, showToast } from "../globalContext";
import { tokenExpireError } from "../authContext";
import { isImage } from "../utils/utils";
import ReactQuill from "react-quill";
import DynamicContentType from "../components/DynamicContentType";

const AddAdminNewsPage = () => {
  const schema = yup
    .object({
      title: yup.string().required(),
      summary: yup.string().required(),
      content: yup.string().required(),
      author: yup.string().required(),
      status: yup.string().required(),
      tags: yup.string().required(),
      cover: yup.string().required(),
      profile: yup.string().required(),
      published_at: yup.string().required(),
    })
    .required();

  const { dispatch } = React.useContext(GlobalContext);
  const { dispatch: globalDispatch } = React.useContext(GlobalContext);
  // const [fileObj, setFileObj] = React.useState({});
  const [fileObj, setFileObj] = React.useState({
    cover: null,
    profile: null,
  });

  const navigate = useNavigate();
  const {
    register,
    handleSubmit,
    control,
    setError,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const quillModules = {
    toolbar: [
      [{ header: [1, 2, 3, 4, 5, 6, false] }],
      ["bold", "italic", "underline", "strike"],
      [{ list: "ordered" }, { list: "bullet" }],
      ["link"],
      ["image"],
      [{ align: [] }], 
    ],
  };
  
  const quillFormats = [
    "header",
    "bold",
    "italic",
    "underline",
    "strike",
    "list",
    "bullet",
    "link",
    "align", 
    "image",
  ];


  const previewImage = (field, target) => {
    let tempFileObj = fileObj;
    tempFileObj[field] = {
      file: target.files[0],
      tempURL: URL.createObjectURL(target.files[0]),
    };
    setFileObj({ ...tempFileObj });
  };

  const onSubmit = async (data) => {
    let sdk = new MkdSDK();

    try {
      if (fileObj.cover) {
        let formData = new FormData();
        formData.append("file", fileObj.cover.file);
        let uploadResult = await sdk.uploadImage(formData);
        data.cover = uploadResult.url;
      }

      // Upload user name label photo
      if (fileObj.profile) {
        let formData = new FormData();
        formData.append("file", fileObj.profile.file);
        let uploadResult = await sdk.uploadImage(formData);
        data.profile = uploadResult.url;
      }
      const result = await sdk.callRawAPI(
        "/v4/api/records/cms_events_news",
        {
          title: data.title,
          summary: data.summary,
          content: data.content,
          author: data.author,
          author_image: data.profile,
          status: data.status,
          tags: data.tags,
          cover: data.cover,
          published_at: data.published_at,
        },
        "POST"
      );
      if (!result.error) {
        showToast(globalDispatch, "Added");
        navigate("/admin/news");
      } else {
        if (result.validation) {
          const keys = Object.keys(result.validation);
          for (let i = 0; i < keys.length; i++) {
            const field = keys[i];
            setError(field, {
              type: "manual",
              message: result.validation[field],
            });
          }
        }
      }
    } catch (error) {
      console.log("Error", error);
      setError("seq", {
        type: "manual",
        message: error.message,
      });
      tokenExpireError(dispatch, error.message);
    }
  };

  React.useEffect(() => {
    globalDispatch({
      type: "SETPATH",
      payload: {
        path: "news",
      },
    });
  }, []);

  return (
    <div className=" shadow-md rounded  mx-auto p-5">
      <h4 className="text-2xl font-medium">Add News </h4>
      <form className=" w-full max-w-lg" onSubmit={handleSubmit(onSubmit)}>
        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="title"
          >
            Title
          </label>
          <input
            type="text"
            placeholder="New title"
            {...register("title")}
            className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
          />
        </div>

        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="summary"
          >
            Summary
          </label>
          <Controller
            name="summary"
            control={control}
            defaultValue=""
            render={({ field }) => (
              <ReactQuill
                value={field.value}
                onChange={field.onChange}
                onBlur={field.onBlur}
                theme="snow"
                modules={quillModules}
                formats={quillFormats}
              />
            )}
          />
        </div>
        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="content"
          >
            Content
          </label>
          <Controller
            name="content"
            control={control}
            defaultValue=""
            render={({ field }) => (
              <ReactQuill
                value={field.value}
                onChange={field.onChange}
                onBlur={field.onBlur}
                theme="snow"
                modules={quillModules}
                formats={quillFormats}
              />
            )}
          />
        </div>
        <div className="mb-4 mt-2">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="cover"
          >
            Cover
          </label>
          <input
            type="file"
            placeholder="cover"
            {...register("cover", {
              onChange: (e) => previewImage("cover", e.target),
            })}
            className={`"shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${
              errors.cover?.message ? "border-red-500" : ""
            }`}
          />
          {fileObj["cover"] ? (
            isImage(fileObj["cover"]) ? (
              <img
                className={"preview-image"}
                src={fileObj["cover"]["tempURL"]}
              ></img>
            ) : (
              <></>
            )
          ) : (
            <></>
          )}

          <p className="text-red-500 text-xs italic">{errors.cover?.message}</p>
        </div>

        <div className="mb-4 mt-2">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="profile"
          >
            Profile
          </label>
          <input
            type="file"
            placeholder="profile"
            {...register("profile", {
              onChange: (e) => previewImage("profile", e.target),
            })}
            className={`"shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${
              errors.cover?.message ? "border-red-500" : ""
            }`}
          />
          {fileObj["profile"] ? (
            isImage(fileObj["profile"]) ? (
              <img
                className={"preview-image"}
                src={fileObj["profile"]["tempURL"]}
              ></img>
            ) : (
              <></>
            )
          ) : (
            <></>
          )}

          <p className="text-red-500 text-xs italic">{errors.cover?.message}</p>
        </div>

        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="author"
          >
            Author
          </label>
          <input
            type="text"
            placeholder="author"
            {...register("author")}
            className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
          />
        </div>
        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="published_at"
          >
            Publish Date
          </label>
          <input
            type="date"
            {...register("published_at")}
            className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
          />
        </div>
        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="tags"
          >
            Tags
          </label>
          <input
            type="text"
            placeholder="tags"
            {...register("tags")}
            className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-1 leading-tight focus:outline-none focus:shadow-outline}`}
          />
          <p className="text-sm mb-3" >Note: Seperate tags by adding coma</p>
        </div>
        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="status"
          >
            Status
          </label>
          <select
            {...register("status")}
            className={`shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline}`}
          >
            <option value=''>Select status</option>
            <option value="active">Active</option>
            <option value="Inactive">Inactive</option>
          </select>
        </div>

        <button
          type="submit"
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
        >
          Submit
        </button>
      </form>
    </div>
  );
};

export default AddAdminNewsPage;
