import React from "react";
import { AuthContext } from "./authContext";
import { Routes, Route } from "react-router-dom";
import SnackBar from "./components/SnackBar";
import PublicHeader from "./components/PublicHeader";
import TopHeader from "./components/TopHeader";

import AdminHeader from "./components/AdminHeader";

import NotFoundPage from "./pages/NotFoundPage";
import AdminLoginPage from "./pages/AdminLoginPage";
import AdminForgotPage from "./pages/AdminForgotPage";
import AdminResetPage from "./pages/AdminResetPage";
import AdminDashboardPage from "./pages/AdminDashboardPage";
import AdminProfilePage from "./pages/AdminProfilePage";
import SessionExpiredModal from "./components/SessionExpiredModal";

import AddAdminBlogCategoryPage from "./pages/AdminBlog/AddAdminBlogCategoryPage";
import AddAdminBlogPage from "./pages/AdminBlog/AddAdminBlogPage";
import AddAdminBlogTagPage from "./pages/AdminBlog/AddAdminBlogTagPage";
import EditAdminBlogPage from "./pages/AdminBlog/EditAdminBlogPage";
import ListAdminBlogCategoryPage from "./pages/AdminBlog/ListAdminBlogCategoryPage";
import ListAdminBlogPage from "./pages/AdminBlog/ListAdminBlogPage";
import ListAdminBlogTagPage from "./pages/AdminBlog/ListAdminBlogTagPage";
import ViewAdminBlogPage from "./pages/AdminBlog/ViewAdminBlogPage";
import AddAdminCmsPage from "./pages/AddAdminCmsPage";
import AddAdminEmailPage from "./pages/AddAdminEmailPage";
import AddAdminPhotoPage from "./pages/AddAdminPhotoPage";
import AdminChatPage from "./pages/AdminChatPage";
import AdminCmsListPage from "./pages/AdminCmsListPage";
import AdminEmailListPage from "./pages/AdminEmailListPage";
import AdminPhotoListPage from "./pages/AdminPhotoListPage";
import EditAdminCmsPage from "./pages/EditAdminCmsPage";
import EditAdminEmailPage from "./pages/EditAdminEmailPage";
import UserMagicLoginPage from "./pages/MagicLogin/UserMagicLoginPage";
import MagicLoginVerifyPage from "./pages/MagicLogin/MagicLoginVerifyPage";
import AddAdminPostsTablePage from "./pages/AddAdminPostsTablePage";
import EditAdminPostsTablePage from "./pages/EditAdminPostsTablePage";
import ViewAdminPostsTablePage from "./pages/ViewAdminPostsTablePage";
import ListAdminPostsTablePage from "./pages/ListAdminPostsTablePage";
import AddAdminSettingTablePage from "./pages/AddAdminSettingTablePage";
import EditAdminSettingTablePage from "./pages/EditAdminSettingTablePage";
import ViewAdminSettingTablePage from "./pages/ViewAdminSettingTablePage";
import ListAdminSettingTablePage from "./pages/ListAdminSettingTablePage";
import AdminUserListPage from "./pages/AdminUserListPage";
import AddAdminUserPage from "./pages/AddAdminUserPage";
import EditAdminUserPage from "./pages/EditAdminUserPage";

import AdminTestimonialListPage from "Pages/AdminTestimonialListPage";
import AddAdminTestimonialPage from "Pages/AddAdminTestimonialPage";
import EditAdminTestimonialPage from "Pages/EditAdminTestimonialPage";
import AdminProgramsList from "Pages/AdminProgramsList";
import AddAdminProgram from "Pages/AddAdminProgram";
import AdminFaqList from "Pages/AdminFaqList";
import AddAdminFaqPage from "Pages/AddAdminFaqPage";
import AdminPartnerList from "Pages/AdminPartnerList";
import AddAdminPartnerPage from "Pages/AddAdminPartnerPage";
import EditAdminProgram from "Pages/EditAdminProgram";
import AdminProgramDetail from "Pages/ViewAdminProgram";
import AdminCmsFeatureslistPage from "Pages/AdminCmsFeatureslistPage";
import AddAdminCmsFeaturePage from "Pages/AddAdminCmsFeaturePage";
import AdminCmsFeesListPage from "Pages/AdminCmsFeesListPage";
import AddAdminCmsFeesPage from "Pages/AddAdminCmsFeesPage";
import EditAdminCmsFeaturePage from "Pages/EditAdminCmsFeaturePage";
import EditAdminCmsFeesPage from "Pages/EditAdminCmsFeesPage";
import AdminFacultyListPage from "Pages/AdminFacultyListPage";
import AddAdminFacultyPage from "Pages/AddAdminFacultyPage";
import AdminAdmissionPage from "Pages/AdminAdmissionPage";
import AddAdminAdmissionPage from "Pages/AddAdminAdmissionPage";
import EditAdminAdmissionPage from "Pages/EditAdminAdmissionPage";
import EditAdminFacultyPage from "Pages/EditAdminFacultyPage";

import AddAdminNewsPage from "Pages/AddAdminNewsPage";
import AdminNewsPage from "Pages/AdminNewsPage";
import EditAdminNewsPage from "Pages/EditAdminNewsPage";
import EditAdminFaqPage from "Pages/EditAdminFaqPage";
import EditAdminPartnerPage from "Pages/EditAdminPartnerPage";

import AddAdminSectionPage from "Pages/AddAdminSectionPage";
import EditAdminSectionPage from "Pages/EditAdminSectionPage";
import AdminSectionListPage from "Pages/AdminSectionListPage";

import AdminHomePage from "Pages/AdminHomePage";
import EditAdminHomePage from "Pages/EditAdminHomePage";
import AddAdminHomePage from "Pages/AddAdminHomePage";
import AdminAboutPage from "Pages/AdminAboutPage";
import EditAboutAdminPage from "Pages/EditAboutAdminPage";
import AddAdminAboutPage from "Pages/AddAdminAboutPage";
import AdminStudyPage from "Pages/AdminStudyPage";
import EditAdminStudyPage from "Pages/EditAdminStudyPage";
import AddAdminStudyPage from "Pages/AddAdminStudyPage";
import AdminWhyPage from "Pages/AdminWhyPage";
import EditWhyPage from "Pages/EditWhyPage";
import AddWhyPage from "Pages/AddWhyPage";
import EditFacultyContentPage from "Pages/EditFacultyContentPage";
import FacultyContentPage from "Pages/FacultyContentPage";
import AddFacultyContentPage from "Pages/AddFacultyContentPage";
import AccreditationPage from "Pages/AccreditationPage";
import EditAccreditationPage from "Pages/EditAccreditationPage";
import AddAccreditationPage from "Pages/AddAccreditationPage";
import DubaiPage from "Pages/DubaiPage";
import AddDubaiPage from "Pages/AddDubaiPage";
import EditDubaiPage from "Pages/EditDubaiPage";
import AddProgramsContent from "Pages/AddProgramsContent";
import EditProgramsContent from "Pages/EditProgramsContent";
import AddFeesContent from "Pages/AddFeesContent";
import EditFeesContent from "Pages/EditFeesContent";
import AddAdmisssionContent from "Pages/AddAdmisssionContent";
import EditAdmissionContent from "Pages/EditAdmissionContent";
import VisaPage from "Pages/VisaPage";
import AddVisaPage from "Pages/AddVisaPage";
import EditVisaPage from "Pages/EditVisaPage";
import StudentPathPage from "Pages/StudentPathPage";
import EditStudentPath from "Pages/EditStudentPath";
import AddStudentPath from "Pages/AddStudentPath";
import AcademicCalendarPage from "Pages/AcademicCalendarPage";
import EditAcademicCalendar from "Pages/EditAcademicCalendar";
import AddAcademicCalendar from "Pages/AddAcademicCalendar";
import CampusPage from "Pages/CampusPage";
import EditCampus from "Pages/EditCampus";
import AddCampus from "Pages/AddCampus";
import StudentLifePage from "Pages/StudentLifePage";
import EditStudentLife from "Pages/EditStudentLife";
import AddStudentLife from "Pages/AddStudentLife";
import SupportPage from "Pages/SupportPage";
import EditSupport from "Pages/EditSupport";
import AddSupport from "Pages/AddSupport";
import GraduationPage from "Pages/GraduationPage";
import EditGraduation from "Pages/EditGraduation";
import AddGraduation from "Pages/AddGraduation";
import AddFaqContent from "Pages/AddFaqContent";
import EditFaqContent from "Pages/EditFaqContent";
import ProgramDetails from "Pages/ProgramDetails";
import AddProgramsDetails from "Pages/AddProgramsDetails";
import EditProgramDetails from "Pages/EditProgramDetails";
import EditNewsContent from "Pages/EditNewsContent";
import AddNewsContent from "Pages/AddNewsContent";
import ContactInfo from "Pages/ContactInfo";
import EditContactInfo from "Pages/EditContactInfo";
import AddContactInfo from "Pages/AddContactInfo";
import ApplicationPage from "Pages/ApplicationPage";
import AddApplication from "Pages/AddApplication";
import EditApplication from "Pages/EditApplication";
import HeaderContent from "Components/HeaderContent";
import EditHeaderContent from "Components/EditHeaderContent";
import AddHeaderContent from "Components/AddHeaderContent";
import FooterContent from "Components/FooterContent";
import AddFooterContent from "Components/AddFooterContent";
import EditFooterContent from "Components/EditFooterContent";
import ContactEmails from "Pages/ContactEmails";
import ViewEmails from "Pages/ViewEmails";
import MarketingPages from "Pages/MarketingPages";
import EditMarketingPages from "Pages/EditMarketingPages";
import AddMarketingPages from "Pages/AddMarketingPages";
import EditPartnerContent from "Pages/EditPartnerContent";
import AddPartnerContent from "Pages/AddPartnerContent";
import EditOurPartnersSlider from "Pages/EditOurPartnersSlider";
import AddOurPartnersSlide from "Pages/AddOurPartnersSlide";
import AddCampusSlide from "Pages/AddCampusSlide";
import EditCampusSlide from "Pages/EditCampusSlide";
import AddAccreditationTab from "Pages/AddAccreditationTab";
import EditAccreditationTab from "Pages/EditAccreditationTab";
import TermsAndCondition from "Pages/TermsAndCondition";
import AddTermsAndCondition from "Pages/AddTermsAndCondition";
import EditTermsAndConditon from "Pages/EditTermsAndConditon";
import PrivacyPolicy from "Pages/PrivacyPolicy";
import AddPrivacy from "Pages/AddPrivacy";
import EditPrivacy from "Pages/EditPrivacy";

function renderHeader(role) {
  switch (role) {
    case "admin":
      return <AdminHeader />;

    default:
      return <PublicHeader />;
  }
}

function renderRoutes(role) {
  switch (role) {
    case "admin":
      return (
        <Routes>
          <Route
            path="/admin/dashboard"
            element={<AdminDashboardPage />}
          ></Route>
          <Route
            exact
            path="/admin/profile"
            element={<AdminProfilePage />}
          ></Route>
          <Route
            path="/admin/add-category"
            element={<AddAdminBlogCategoryPage />}
          ></Route>
          <Route path="/admin/add-blog" element={<AddAdminBlogPage />}></Route>
          <Route
            path="/admin/add-tag"
            element={<AddAdminBlogTagPage />}
          ></Route>
          <Route
            path="/admin/edit-blog/:id"
            element={<EditAdminBlogPage />}
          ></Route>
          <Route
            path="/admin/blog_category"
            element={<ListAdminBlogCategoryPage />}
          ></Route>
          <Route path="/admin/blog" element={<ListAdminBlogPage />}></Route>
          <Route
            path="/admin/blog_tags"
            element={<ListAdminBlogTagPage />}
          ></Route>
          <Route
            path="/admin/view-blog/:id"
            element={<ViewAdminBlogPage />}
          ></Route>
          <Route path="/admin/add-cms" element={<AddAdminCmsPage />}></Route>
          <Route
            path="/admin/add-email"
            element={<AddAdminEmailPage />}
          ></Route>
          <Route
            path="/admin/add-photo"
            element={<AddAdminPhotoPage />}
          ></Route>
          <Route path="/admin/chat" element={<AdminChatPage />}></Route>
          <Route path="/admin/cms" element={<AdminCmsListPage />}></Route>
          <Route path="/admin/email" element={<AdminEmailListPage />}></Route>
          <Route path="/admin/photo" element={<AdminPhotoListPage />}></Route>
          <Route
            path="/admin/edit-cms/:id"
            element={<EditAdminCmsPage />}
          ></Route>
          <Route
            path="/admin/edit-email/:id"
            element={<EditAdminEmailPage />}
          ></Route>
          <Route
            path="/admin/add-posts"
            element={<AddAdminPostsTablePage />}
          ></Route>
          <Route
            path="/admin/edit-posts/:id"
            element={<EditAdminPostsTablePage />}
          ></Route>
          <Route
            path="/admin/view-posts/:id"
            element={<ViewAdminPostsTablePage />}
          ></Route>
          <Route
            path="/admin/posts"
            element={<ListAdminPostsTablePage />}
          ></Route>
          <Route
            path="/admin/add-setting"
            element={<AddAdminSettingTablePage />}
          ></Route>
          <Route
            path="/admin/edit-setting/:id"
            element={<EditAdminSettingTablePage />}
          ></Route>
          <Route
            path="/admin/view-setting/:id"
            element={<ViewAdminSettingTablePage />}
          ></Route>
          <Route
            path="/admin/setting"
            element={<ListAdminSettingTablePage />}
          ></Route>
          <Route path="/admin/users" element={<AdminUserListPage />}></Route>
          <Route path="/admin/add-user" element={<AddAdminUserPage />}></Route>
          <Route
            path="/admin/edit-user/:id"
            element={<EditAdminUserPage />}
          ></Route>
          <Route
            path="/admin/testimonials"
            element={<AdminTestimonialListPage />}
          ></Route>
          <Route
            path="/admin/add-testimonial"
            element={<AddAdminTestimonialPage />}
          ></Route>
          <Route
            path="/admin/edit-testimonial/:id"
            element={<EditAdminTestimonialPage />}
          ></Route>
          <Route path="/admin/programs" element={<AdminProgramsList />}></Route>
          <Route
            path="/admin/add-programs"
            element={<AddAdminProgram />}
          ></Route>
          <Route
            path="/admin/edit-programs/:programId"
            element={<EditAdminProgram />}
          ></Route>
          <Route
            path="/admin/view-programs/:programId"
            element={<AdminProgramDetail />}
          ></Route>
          <Route path="/admin/faq" element={<AdminFaqList />}></Route>
          <Route path="/admin/add-faq" element={<AddAdminFaqPage />}></Route>
          <Route
            path="/admin/edit-faq/:id"
            element={<EditAdminFaqPage />}
          ></Route>
          <Route path="/admin/partner" element={<AdminPartnerList />}></Route>
          <Route
            path="/admin/add-partner"
            element={<AddAdminPartnerPage />}
          ></Route>
          <Route
            path="/admin/edit-partner/:id"
            element={<EditAdminPartnerPage />}
          ></Route>
          <Route
            path="/admin/features"
            element={<AdminCmsFeatureslistPage />}
          ></Route>
          <Route
            path="/admin/add-feature"
            element={<AddAdminCmsFeaturePage />}
          ></Route>
          <Route
            path="/admin/edit-feature/:id"
            element={<EditAdminCmsFeaturePage />}
          ></Route>
          <Route path="/admin/fees" element={<AdminCmsFeesListPage />}></Route>
          <Route
            path="/admin/add-fees"
            element={<AddAdminCmsFeesPage />}
          ></Route>
          <Route
            path="/admin/edit-fees/:id"
            element={<EditAdminCmsFeesPage />}
          ></Route>
          <Route
            path="/admin/section"
            element={<AdminSectionListPage />}
          ></Route>
          <Route
            path="/admin/add-section"
            element={<AddAdminSectionPage />}
          ></Route>
          <Route
            path="/admin/edit-section/:id"
            element={<EditAdminSectionPage />}
          ></Route>
          {/* <Route
            path="/admin/faculty"
            element={<AdminFacultyListPage />}
          ></Route> */}
          <Route
            path="/admin/add-faculty"
            element={<AddAdminFacultyPage />}
          ></Route>
          <Route
            path="/admin/admission"
            element={<AdminAdmissionPage />}
          ></Route>
          <Route
            path="/admin/add-admission"
            element={<AddAdminAdmissionPage />}
          ></Route>
          <Route
            path="/admin/edit-admission/:id"
            element={<EditAdminAdmissionPage />}
          ></Route>
          <Route
            path="/admin/edit-faculty/:id"
            element={<EditAdminFacultyPage />}
          ></Route>
          <Route path="/admin/add-news/" element={<AddAdminNewsPage />}></Route>
          <Route path="/admin/news/" element={<AdminNewsPage />}></Route>
          <Route
            path="/admin/edit-news/:id"
            element={<EditAdminNewsPage />}
          ></Route>
          <Route path="/admin/home" element={<AdminHomePage />}></Route>
          <Route
            path="/admin/edit_home/:id"
            element={<EditAdminHomePage />}
          ></Route>
          <Route path="/admin/add_home" element={<AddAdminHomePage />}></Route>
          <Route path="/admin/about" element={<AdminAboutPage />}></Route>
          <Route
            path="/admin/edit_about/:id"
            element={<EditAboutAdminPage />}
          ></Route>
          <Route
            path="/admin/add_about/"
            element={<AddAdminAboutPage />}
          ></Route>
          <Route path="/admin/study/" element={<AdminStudyPage />}></Route>
          <Route
            path="/admin/edit_study/:id"
            element={<EditAdminStudyPage />}
          ></Route>
          <Route
            path="/admin/add_study/"
            element={<AddAdminStudyPage />}
          ></Route>

          <Route path="/admin/why/" element={<AdminWhyPage />}></Route>
          <Route path="/admin/edit_why/:id" element={<EditWhyPage />}></Route>
          <Route path="/admin/add_why/" element={<AddWhyPage />}></Route>

          <Route path="/admin/faculty_content/" element={<FacultyContentPage />}></Route>
          <Route path="/admin/edit_faculty_content/:id" element={<EditFacultyContentPage />}></Route>
          <Route path="/admin/add_faculty_content/" element={<AddFacultyContentPage />}></Route>

          <Route path="/admin/accreditation/" element={<AccreditationPage />}></Route>
          <Route path="/admin/edit_accreditation/:id" element={<EditAccreditationPage />}></Route>
          <Route path="/admin/add_accreditation/" element={<AddAccreditationPage />}></Route>

          <Route path="/admin/dubai" element={<DubaiPage />}></Route>
          <Route path="/admin/add_dubai" element={<AddDubaiPage />}></Route>
          <Route path="/admin/edit_dubai/:id" element={<EditDubaiPage />}></Route>

          <Route path="/admin/add_programs_content" element={<AddProgramsContent />}></Route>
          <Route path="/admin/edit_programs_content/:id" element={<EditProgramsContent />}></Route>

          <Route path="/admin/add_fees_content" element={<AddFeesContent />}></Route>
          <Route path="/admin/edit_fees_content/:id" element={<EditFeesContent />}></Route>

          <Route path="/admin/add_admission_content" element={<AddAdmisssionContent />}></Route>
          <Route path="/admin/edit_admission_content/:id" element={<EditAdmissionContent />}></Route>

          <Route path="/admin/visa" element={<VisaPage />}></Route>
          <Route path="/admin/edit_visa/:id" element={<EditVisaPage />}></Route>
          <Route path="/admin/add_visa" element={<AddVisaPage />}></Route>

          <Route path="/admin/path" element={<StudentPathPage />}></Route>
          <Route path="/admin/edit_path/:id" element={<EditStudentPath />}></Route>
          <Route path="/admin/add_path" element={<AddStudentPath />}></Route>

          <Route path="/admin/academic" element={<AcademicCalendarPage />}></Route>
          <Route path="/admin/edit_academic/:id" element={<EditAcademicCalendar />}></Route>
          <Route path="/admin/add_academic" element={<AddAcademicCalendar />}></Route>

          <Route path="/admin/campus" element={<CampusPage />}></Route>
          <Route path="/admin/edit_campus/:id" element={<EditCampus />}></Route>
          <Route path="/admin/add_campus" element={<AddCampus />}></Route>

          <Route path="/admin/life" element={<StudentLifePage />}></Route>
          <Route path="/admin/edit_life/:id" element={<EditStudentLife />}></Route>
          <Route path="/admin/add_life" element={<AddStudentLife />}></Route>

          <Route path="/admin/support" element={<SupportPage />}></Route>
          <Route path="/admin/edit_support/:id" element={<EditSupport />}></Route>
          <Route path="/admin/add_support" element={<AddSupport />}></Route>

          <Route path="/admin/graduation" element={<GraduationPage />}></Route>
          <Route path="/admin/edit_graduation/:id" element={<EditGraduation />}></Route>
          <Route path="/admin/add_graduation" element={<AddGraduation />}></Route>

          <Route path="/admin/edit_faq_content/:id" element={<EditFaqContent />}></Route>
          <Route path="/admin/add_faq_content" element={<AddFaqContent />}></Route>

          <Route path="/admin/program-details" element={<ProgramDetails />}></Route>
          <Route path="/admin/edit_program-details/:id" element={<EditProgramDetails />}></Route>
          <Route path="/admin/add_program-details" element={<AddProgramsDetails />}></Route>

          <Route path="/admin/edit_news_content/:id" element={<EditNewsContent />}></Route>
          <Route path="/admin/add_news_content" element={<AddNewsContent />}></Route>

          <Route path="/admin/contact_info" element={<ContactInfo />}></Route>
          <Route path="/admin/edit_contact_info/:id" element={<EditContactInfo />}></Route>
          <Route path="/admin/add_contact_info" element={<AddContactInfo />}></Route>

          <Route path="/admin/application" element={<ApplicationPage />}></Route>
          <Route path="/admin/edit_application/:id" element={<EditApplication />}></Route>
          <Route path="/admin/add_application" element={<AddApplication />}></Route>

          <Route path="/admin/header" element={<HeaderContent />}></Route>
          <Route path="/admin/edit_header/:id" element={<EditHeaderContent />}></Route>
          <Route path="/admin/add_header" element={<AddHeaderContent />}></Route>


          <Route path="/admin/footer" element={<FooterContent />}></Route>
          <Route path="/admin/edit_footer/:id" element={<EditFooterContent />}></Route>
          <Route path="/admin/add_footer" element={<AddFooterContent />}></Route>

          <Route path="/admin/contact_email" element={<ContactEmails />}></Route>
          <Route path="/admin/view_emails/:id" element={<ViewEmails />}></Route>

          <Route path="/admin/page" element={<MarketingPages />}></Route>
          <Route path="/admin/edit_page/:id" element={<EditMarketingPages />}></Route>
          <Route path="/admin/add_page" element={<AddMarketingPages />}></Route>

          <Route path="/admin/edit_partner_content/:id" element={<EditPartnerContent/>}></Route>
          <Route path="/admin/add_partner_content" element={<AddPartnerContent/>}></Route>

          <Route path="/admin/add_partner_slide/" element={<AddOurPartnersSlide />}></Route>
          <Route path="/admin/edit/partner_slide/:id" element={<EditOurPartnersSlider />}></Route>

          <Route path="/admin/add_campus_slide/" element={<AddCampusSlide />}></Route>
          <Route path="/admin/edit_campus_slide/:id" element={<EditCampusSlide />}></Route>

          <Route path="/admin/add_accreditation_tab/" element={<AddAccreditationTab />}></Route>
          <Route path="/admin/edit_accreditation_tab/:id" element={<EditAccreditationTab />}></Route>

          <Route path="/admin/terms/" element={<TermsAndCondition />}></Route>
          <Route path="/admin/add_terms/" element={<AddTermsAndCondition />}></Route>
          <Route path="/admin/edit_terms/:id" element={<EditTermsAndConditon />}></Route>

          <Route path="/admin/privacy/" element={<PrivacyPolicy />}></Route>
          <Route path="/admin/add_privacy/" element={<AddPrivacy />}></Route>
          <Route path="/admin/edit_privacy/:id" element={<EditPrivacy />}></Route>

       

         
          
        </Routes>
      );

    default:
      return (
        <Routes>
          <Route exact path="/admin/login" element={<AdminLoginPage />}></Route>
          <Route
            exact
            path="/admin/forgot"
            element={<AdminForgotPage />}
          ></Route>
          <Route exact path="/admin/reset" element={<AdminResetPage />}></Route>
          <Route path="*" exact element={<NotFoundPage />}></Route>
          <Route path="/magic-login" element={<UserMagicLoginPage />}></Route>
          <Route
            path="/magic-login/verify"
            element={<MagicLoginVerifyPage />}
          ></Route>
        </Routes>
      );
  }
}

function Main() {
  const { state } = React.useContext(AuthContext);

  return (
    <div className="h-full">
      <div className="flex w-full">
        {!state.isAuthenticated ? <PublicHeader /> : renderHeader(state.role)}
        <div className="w-full">
          {state.isAuthenticated ? <TopHeader /> : null}
          <div
            className={`page-wrapper w-full ${
              state.isAuthenticated ? "p-5" : ""
            }`}
          >
            {!state.isAuthenticated
              ? renderRoutes("none")
              : renderRoutes(state.role)}
          </div>
        </div>
      </div>
      <SessionExpiredModal />
      <SnackBar />
    </div>
  );
}

export default Main;
