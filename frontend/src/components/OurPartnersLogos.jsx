import React from "react";
import { AuthContext } from "../authContext";
import MkdSDK from "../utils/MkdSDK";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { GlobalContext } from "../globalContext";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { getNonNullValue } from "../utils/utils";
import PaginationBar from "../components/PaginationBar";
import AddButton from "../components/AddButton";
import ReactHtmlParser from "react-html-parser";
import TreeSDK from "Utils/TreeSDK";

import { useCallback, useState } from "react";
import OurPartnersContent from "Components/OurPartnersContent";


let sdk = new MkdSDK();
let tdk = new TreeSDK();

const columns = [
  {
    header: "Action",
    accessor: "",
  },
  // {
  //   header: "Label",
  //   accessor: "label",
  // },
  {
    header: "Country",
    accessor: "country",
  },
  // {
  //   header: "Title",
  //   accessor: "title",
  // },
  {
    header: "Description",
    accessor: "description",
  },
  {
    header: "Logo",
    accessor: "logo",
  },
];
const OurPartnersLogos = () => {
    const { dispatch } = React.useContext(AuthContext);
    const { dispatch: globalDispatch } = React.useContext(GlobalContext);
  
    const [query, setQuery] = React.useState("");
    const [data, setCurrentTableData] = React.useState([]);
    const [pageSize, setPageSize] = React.useState(3);
    const [pageCount, setPageCount] = React.useState(0);
    const [dataTotal, setDataTotal] = React.useState(0);
    const [currentPage, setPage] = React.useState(0);
    const [canPreviousPage, setCanPreviousPage] = React.useState(false);
    const [canNextPage, setCanNextPage] = React.useState(false);
    const navigate = useNavigate();
  
    const schema = yup.object({
      country: yup.string(),
      description: yup.string(),
    });
  
    const [ShowConfirmationModal, setConfirmationModal] = useState(false);
  
  
    const {
      register,
      handleSubmit,
      setError,
      reset,
      formState: { errors },
    } = useForm({
      resolver: yupResolver(schema),
    });
  
    function updatePageSize(limit) {
      (async function () {
        setPageSize(limit);
        await getData(0, limit);
      })();
    }
  
    function previousPage() {
      (async function () {
        await getData(currentPage - 1 > 0 ? currentPage - 1 : 0, pageSize);
      })();
    }
  
    function nextPage() {
      (async function () {
        await getData(
          currentPage + 1 <= pageCount ? currentPage + 1 : 0,
          pageSize
        );
      })();
    }
  
    async function getData(pageNum, limitNum, data) {
      try {
  
        
  
        const result = await tdk.getPaginate('cms_partners',
        {
          filter: data,
          payload: { ...data },
          page: pageNum || 1,
          limit: limitNum,
          size: limitNum,
          order: "create_at",     
        }
      );
  
        const { list, total, limit, num_pages, page } = result;
  
        setCurrentTableData(list);
        setPageSize(limit);
        setPageCount(num_pages);
        setPage(page);
        setDataTotal(total);
        setCanPreviousPage(page > 1);
        setCanNextPage(page + 1 <= num_pages);
      } catch (error) {
        console.log("ERROR", error);
        tokenExpireError(dispatch, error.message);
      }
    }
  
    const onSubmit = (data) => {
   
      let country = getNonNullValue(data.country);
      let description = getNonNullValue(data.description);
      
      let filter = [];
      if (data.country) {
        filter.push(`country,cs,${country}`);
      }
      if (data.description) {
        filter.push(`description,cs,${description}`);
      }
      {
      getData(1, 200, filter)
      }
    };
    const deleteItem = async (id) => {
      let sdk2 = new MkdSDK();
      try {
        const result = await sdk2.callRawAPI(
          "/v4/api/records/cms_partners/"+id,{},
          "DELETE"
        );
        setCurrentTableData((list) =>
          list.filter((x) => Number(x.id) !== Number(id))
        );
      } catch (err) {
        throw new Error(err);
      }
    };
  
    const resetForm = async () => {
      reset();
      await getData(0, 10);
    };
  
    React.useEffect(() => {
      globalDispatch({
        type: "SETPATH",
        payload: {
          path: "partner",
        },
      });
  
      (async function () {
        await getData(0, 10);
      })();
    }, []);
  
    return (
      <>
    
        <form
          className="p-2 bg-white shadow rounded mb-5"
          onSubmit={handleSubmit(onSubmit)}
        >
          <h4 className="text-2xl font-medium">Partner Logos Search</h4>
          <div className="filter-form-holder mt-10 flex flex-wrap">
           
  
            <div className="mb-4 w-full md:w-1/3 pr-2 pl-2">
              <label className="block text-gray-700 text-sm font-bold mb-2">
              Country
              </label>
              <input
                type="text"
                placeholder="country"
                {...register("country")}
                className="shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
              />
              <p className="text-red-500 text-xs italic">
                {errors.country?.message}
              </p>
            </div>
            
            <div className="mb-4 w-full md:w-1/3 pr-2 pl-2">
              <label className="block text-gray-700 text-sm font-bold mb-2">
              description
              </label>
              <input
                type="text"
                placeholder="description"
                {...register("description")}
                className="shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
              />
              <p className="text-red-500 text-xs italic">
                {errors.description?.message}
              </p>
            </div>
           
          </div>
          
  
          <button type="submit" className=" inline ml-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
            Search
          </button>
          <button
            onClick={() => {
              resetForm();
            }}
            type="button"
            className=" inline ml-2 bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
          >
            Clear
          </button>
        </form>
  
        <div className="overflow-x-auto  p-2 pt-0 bg-white shadow rounded">
          <div className="mb-3 text-center justify-between w-full flex  ">
            <h4 className="text-2xl font-medium">Partner Logos</h4>
            <AddButton link={"/admin/add-partner"} />
          </div>
          <div className="shadow overflow-x-auto border-b border-gray-200 ">
            <table className="min-w-full divide-y divide-gray-200">
              <thead className="bg-gray-50">
                <tr>
                  {columns.map((column, i) => (
                    <th
                      key={i}
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      {column.header}
                      <span>
                        {column.isSorted
                          ? column.isSortedDesc
                            ? " ▼"
                            : " ▲"
                          : ""}
                      </span>
                    </th>
                  ))}
                </tr>
              </thead>
              <tbody className="bg-white divide-y divide-gray-200">
                {data.map((row, i) => {
                  return (
                    <tr key={i}>
                      {columns.map((cell, index) => {
                      
                        if (cell.accessor == "logo") {
                          return (
                            <td key={index} className="px-6 py-4 whitespace-nowrap">
                              {cell.accessor == "logo" ? <img width={'50px'} height={'50px'} src={`${row[cell.accessor]}`} /> : row[cell.accessor]}
                            </td>
                          );
                          }
                          if (cell.accessor == "description") {
                            return (
                              <td>
                              <p className="text-sm">    {ReactHtmlParser(row[cell.accessor])}</p>
                              </td>
                            );
                          }
                        if (cell.accessor == "") {
                          return (
                            <td
                              key={index}
                              className="px-6 py-4 whitespace-nowrap"
                            >
                              <button
                                className="text-xs"
                                onClick={() => {
                                  navigate("/admin/edit-partner/" + row.id, {
                                    state: row,
                                  });
                                }}
                              >
                                {" "}
                                Edit
                              </button>
                              <button
                                className="text-xs px-1 text-red-500"
                                onClick={() => deleteItem(row.id)}
                                
                              >
                                {" "}
                                Delete
                              </button>
                            </td>
                          );
                        }
                        if (cell.mapping) {
                          return (
                            <td
                              key={index}
                              className="px-6 py-4 whitespace-nowrap"
                            >
                              {cell.mapping[row[cell.accessor]]}
                            </td>
                          );
                        }
                        return (
                          <td key={index} className="px-6 py-4 whitespace-nowrap">
                            {row[cell.accessor]}
                          </td>
                        );
                      })}
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
        <PaginationBar
          currentPage={currentPage}
          pageCount={pageCount}
          pageSize={pageSize}
          canPreviousPage={canPreviousPage}
          canNextPage={canNextPage}
          updatePageSize={updatePageSize}
          previousPage={previousPage}
          nextPage={nextPage}
        />
      </>
    );
}

export default OurPartnersLogos