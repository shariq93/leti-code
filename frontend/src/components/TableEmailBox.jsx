import React from "react";

const TableEmailBox = ({row, sendEmail}) => {

    const [toEmail, setToEmail] = React.useState('');

    const hasGmailDomain = toEmail.includes("@");


const emailClass = hasGmailDomain ? '' : 'opacity-5 pointer-events-none';
  return (
    <div className="table-input-holder flex gap-3">
        
      <input
        onChange={(e) => {
          setToEmail(e.target.value);
        }}
        type="email"
        className="w-[200px]"
        placeholder="type user Email"
      />
      <button
        onClick={() => {
          sendEmail(row,toEmail);
        }}
        type="button"
        className={`py-1 px-5 font-bold bg-black text-white ${emailClass}`}
      >
        Send
      </button>
    </div>
  );
};

export default TableEmailBox;
