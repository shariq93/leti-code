
import React from 'react'
import ReactQuill from 'react-quill';
import EditorToolbar, { modules, formats } from "./EditorToolbars";
const Editor = ( { content, handleChange } ) => {
  const editorStyle = {
    // maxheight: '500px',
    // minheight: '500px',
    height: '500px',
    // overFlow: 'auto',

    // set the height to 500 pixels
  };

  return (
    <>
      <EditorToolbar />
      <ReactQuill
        theme="snow"
        value={ content }
        onChange={ handleChange }
        placeholder={ "Write something awesome..." }
        modules={ modules }
        formats={ formats }
        style={ editorStyle }
      />
    </>
  )
}

export default Editor
