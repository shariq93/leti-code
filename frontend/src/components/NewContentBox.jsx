import React from "react";
import { AuthContext } from "../authContext";
import MkdSDK from "../utils/MkdSDK";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { GlobalContext } from "../globalContext";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { getNonNullValue } from "../utils/utils";
import PaginationBar from "../components/PaginationBar";
import AddButton from "../components/AddButton";
import ModalDelete from "Components/Modal/ModalDelete";
import { useCallback, useState } from "react";
import TreeSDK from "Utils/TreeSDK";

let sdk = new MkdSDK();
let tdk = new TreeSDK();

const columns = [
  {
    header: "Action",
    accessor: "",
  },
  // {
  //   header: "Page",
  //   accessor: "page",
  // },
  // {
  //   header: "Sequence",
  //   accessor: "seq",
  // },
  {
    header: "Identifier",
    accessor: "content_key",
  },
  {
    header: "Value",
    accessor: "content_value",
  },
  {
    header: "Content Type",
    accessor: "content_type",
  },
];


const NewContentBox = () => {
    const { dispatch } = React.useContext(AuthContext);
    const { dispatch: globalDispatch } = React.useContext(GlobalContext);
  
    const [query, setQuery] = React.useState("");
    const [data, setCurrentTableData] = React.useState([]);
    const [pageSize, setPageSize] = React.useState(3);
    const [pageCount, setPageCount] = React.useState(0);
    const [dataTotal, setDataTotal] = React.useState(0);
    const [currentPage, setPage] = React.useState(0);
    const [canPreviousPage, setCanPreviousPage] = React.useState(false);
    const [canNextPage, setCanNextPage] = React.useState(false);
    const navigate = useNavigate();
  
    const schema = yup.object({

      content_value: yup.string(),
      // key: yup.string(),
      // type: yup.string(),
    });
  
    const selectType = [
      { key: "", value: "All" },
      { key: "text", value: "Text" },
      { key: "image", value: "Image" },
      { key: "number", value: "Number" },
    ];
    const [itemToDelete, setItemToDelete] = React.useState(null);
    const [ShowConfirmationModal, setConfirmationModal] = useState(false);
    const [id, setID] = useState("");
    const onCloseConfirmationModal = useCallback(() => {
      setConfirmationModal(false);
    }, [ShowConfirmationModal]);
  
    function handleDeleteClick(itemId) {
      setItemToDelete(itemId);
      setConfirmationModal(true);
    }
  
    function handleConfirmDelete() {
      // console.log(itemToDelete);
      deleteItem(itemToDelete);
      setConfirmationModal(false);
    }
  
    function handleCancelDelete() {
      console.log(itemToDelete);
      setItemToDelete(null);
      setConfirmationModal(false);
    }
  
    const {
      register,
      handleSubmit,
      setError,
      reset,
      formState: { errors },
    } = useForm({
      resolver: yupResolver(schema),
    });
  
    function updatePageSize(limit) {
      (async function () {
        setPageSize(limit);
        await getData(0, limit);
      })();
    }
  
    function previousPage() {
      (async function () {
        await getData(currentPage - 1 > 0 ? currentPage - 1 : 0, pageSize);
      })();
    }
  
    function nextPage() {
      (async function () {
        await getData(
          currentPage + 1 <= pageCount ? currentPage + 1 : 0,
          pageSize
        );
      })();
    }
  
    async function getData(pageNum, limitNum, data) {
      try {
        sdk.setTable("cms");
  

        const filter = [];
      {
      filter.push(`page,cs,news-and-events`);
      }
      {
        filter.push(`${data}`);
      }
        // const filter = [`page,cs,news-and-events`];
        const result = await tdk.getPaginate("cms", {
          filter: filter,
          payload: { ...data },
          page: pageNum || 1,
          size: limitNum,
          limit: limitNum,
          order: "create_at",
        });
  
        // const result = await sdk.callRestAPI(
        //   {
        //     payload: { ...data },
        //     page: pageNum,
        //     limit: limitNum,
        //   },
        //   "PAGINATE"
        // );
  
        const { list, total, limit, num_pages, page } = result;
  
        setCurrentTableData(list);
        setPageSize(limit);
        setPageCount(num_pages);
        setPage(page);
        setDataTotal(total);
        setCanPreviousPage(page > 1);
        setCanNextPage(page + 1 <= num_pages);
      } catch (error) {
        console.log("ERROR", error);
        tokenExpireError(dispatch, error.message);
      }
    }
  
    const onSubmit = (data) => {
  
      let content_value = getNonNullValue(data.content_value);
      let filter = [];

      if (data.content_value) {
        filter.push(`content_value,cs,${content_value}`);
      }
      {
       getData(1, 200, filter) ;
      }
  
      // getData(0, pageSize, filter);
    };
  
    const deleteItem = async (id) => {
      try {
        sdk.setTable("cms");
        const result = await sdk.callRestAPI({ id }, "DELETE");
        setCurrentTableData((list) =>
          list.filter((x) => Number(x.id) !== Number(id))
        );
      } catch (err) {
        throw new Error(err);
      }
    };
  
    const resetForm = async () => {
      reset();
      await getData(0, 10);
    };
  
    React.useEffect(() => {
      globalDispatch({
        type: "SETPATH",
        payload: {
          path: "news",
        },
      });
  
      (async function () {
        await getData(0, 10);
      })();
    }, []);
  
    return (
      <>
        {ShowConfirmationModal ? (
          <ModalDelete
            title="Are you sure"
            id={id}
            deleteItem={deleteItem}
            onConfirmDelete={handleConfirmDelete}
            onCloseConfirmationModal={handleCancelDelete}
          />
        ) : null}
        <form
          className="p-2 bg-white shadow rounded mb-5"
          onSubmit={handleSubmit(onSubmit)}
        >
          <h4 className="text-2xl font-medium">News and Events Content Search</h4>
          <div className="filter-form-holder mt-10 flex flex-wrap">
            <div className="mb-4 w-full md:w-1/3 pr-2 pl-2">
              <label className="block text-gray-700 text-sm font-bold mb-2">
                Value
              </label>
              <input
                type="text"
                placeholder="content_value"
                {...register("content_value")}
                className="shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
              />
              <p className="text-red-500 text-xs italic">
                {errors.content_value?.message}
              </p>
            </div>
          </div>
          <button
            type="submit"
            className=" inline ml-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
          >
            Search
          </button>
  
          <button
            onClick={() => {
              resetForm();
            }}
            type="button"
            className=" inline ml-2 bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
          >
            Clear
          </button>
        </form>
  
        <div className="overflow-x-auto  p-2 pt-0 bg-white shadow rounded">
          <div className="mb-3 text-center justify-between w-full flex  ">
            <h4 className="text-2xl font-medium">News and Events Content </h4>
            <AddButton link={"/admin/add_news_content"} />
          </div>
          <div className="shadow overflow-x-auto border-b border-gray-200 ">
            <table className="min-w-full divide-y divide-gray-200">
              <thead className="bg-gray-50">
                <tr>
                  {columns.map((column, i) => (
                    <th
                      key={i}
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      {column.header}
                      <span>
                        {column.isSorted
                          ? column.isSortedDesc
                            ? " ▼"
                            : " ▲"
                          : ""}
                      </span>
                    </th>
                  ))}
                </tr>
              </thead>
              <tbody className="bg-white divide-y divide-gray-200">
                {data.map((row, i) => {
                  return (
                    <tr key={i}>
                      {columns.map((cell, index) => {
                        if (cell.accessor == "content_value") {
                          return (
                            <td>
                              <p className="text-sm"> {row[cell.accessor]}</p>
                            </td>
                          );
                        }
                        if (cell.accessor == "") {
                          return (
                            <td
                              key={index}
                              className="px-6 py-4 whitespace-nowrap"
                            >
                              <button
                                className="text-xs"
                                onClick={() => {
                                  navigate("/admin/edit_news_content/" + row.id, {
                                    state: row,
                                  });
                                }}
                              >
                                {" "}
                                Edit
                              </button>
                              {/* <button
                                className="text-xs px-1 text-red-500"
                                
                                onClick={() => handleDeleteClick(row.id)}
                              >
                                {" "}
                                Delete
                              </button> */}
                            </td>
                          );
                        }
                        if (cell.mapping) {
                          return (
                            <td
                              key={index}
                              className="px-6 py-4 whitespace-nowrap"
                            >
                              {cell.mapping[row[cell.accessor]]}
                            </td>
                          );
                        }
                        return (
                          <td key={index} className="px-6 py-4 whitespace-nowrap">
                            {row[cell.accessor]}
                          </td>
                        );
                      })}
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
        <PaginationBar
          currentPage={currentPage}
          pageCount={pageCount}
          pageSize={pageSize}
          canPreviousPage={canPreviousPage}
          canNextPage={canNextPage}
          updatePageSize={updatePageSize}
          previousPage={previousPage}
          nextPage={nextPage}
        />
      </>
    );
}

export default NewContentBox