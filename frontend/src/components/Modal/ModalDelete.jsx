import React from 'react'

const ModalDelete = ({title,onCloseConfirmationModal, onConfirmDelete,message}) => {
  return (
    <aside
    className='fixed top-0 right-0 bottom-0 left-0 flex justify-center items-center '
    style={ {
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
      zIndex: '1000',
    } }
  >
    <section className='bg-white min-w-[25rem] w-[25rem]  rounded-[.5rem] py-6 px-6 flex flex-col gap-6'>
      {
        title ?
          <h1 className='text-center font-bold text-2xl text-red-600' >
            { title }
          </h1> : null
      }
      {
        message ?
          <div className={ `text-[1.5rem] leading-[1.5rem] text-[#667085] font-normal`}>
            { message }
      
          </div> : null
      }

      <div className='w-full flex justify-center gap-2 items-center font-medium text-[base] leading-[1.5rem]'>
        <button
          className='border  border-[#DC5A5D] bg-[#DC5A5D] rounded-[.5rem] w-[10.375rem] h-[2.75rem] flex items-center justify-center text-white'
          style={ {
            width: '10.375rem',
            height: '2.75rem',
          } }
          onClick={ onCloseConfirmationModal }
        >
          Close
        </button>
        <button
          className='border  border-[#2ec742] bg-[#2ec742] rounded-[.5rem] w-[10.375rem] h-[2.75rem] flex items-center justify-center text-white'
          style={ {
            width: '10.375rem',
            height: '2.75rem',
          } }
          onClick={onConfirmDelete}
        >
         Delete
        </button>
      </div>
    </section>
  </aside>
  )
}

export default ModalDelete