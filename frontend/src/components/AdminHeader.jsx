import React from "react";
import { AuthContext } from "../authContext";
import { NavLink } from "react-router-dom";
import { GlobalContext } from "../globalContext";
export const AdminHeader = () => {
  const { dispatch } = React.useContext(AuthContext);
  const { state } = React.useContext(GlobalContext);

  const { dispatch: gDispatch } = React.useContext(GlobalContext);

  return (
    <>
      <div className={`sidebar-holder ${!state.isOpen ? "open-nav" : ""}`}>
        <div className="sticky top-0 h-fit">
          <div className="w-full p-4 dash-title-header">
            <div className="text-white font-bold text-center text-2xl">
              IBTE Admin
            </div>
          </div>
          <div className="w-full sidebar-list">
            <ul className="flex flex-wrap">
              {/* <li className="list-none block w-full">
                <NavLink
                  to="/admin/dashboard"
                  className={`${
                    state.path == "admin" ? "text-black bg-gray-200" : ""
                  }`}
                >
                  Dashboard
                </NavLink>
              </li> */}

              <li className="list-none block w-full">
                <NavLink
                 onClick={()=>{
                  gDispatch({
                    type: "EDIT_PAGE",
                    payload: { pageName: '' },
                  });
                
                }}
                  to="/admin/page"
                  className={`${
                    state.path == "page" ? "text-black bg-gray-200" : ""
                  }`}
                >
                  Pages
                </NavLink>
              </li>

              <li className="list-none block w-full">
                <NavLink
                  to="/admin/header"
                  className={`${
                    state.path == "header" ? "text-black bg-gray-200" : ""
                  }`}
                >
                  Header
                </NavLink>
              </li>

              <li className="list-none block w-full">
                <NavLink
                  to="/admin/home"
                  className={`${
                    state.path == "home" ? "text-black bg-gray-200" : ""
                  }`}
                >
                  Home
                </NavLink>
              </li>

              <li className="list-none block w-full">
                <NavLink
                  to="/admin/about"
                  className={`${
                    state.path == "about" ? "text-black bg-white" : ""
                  }`}
                >
                  About
                </NavLink>

                <ul className="flex flex-wrap">
                  <li className="list-none block w-full">
                    <NavLink
                      to="/admin/about"
                      className={`${
                        state.path == "about" ? "text-black bg-gray-200" : ""
                      }`}
                    >
                      About us
                    </NavLink>
                  </li>
                  <li className="list-none block w-full">
                    <NavLink
                      to="/admin/why"
                      className={`${
                        state.path == "why" ? "text-black bg-gray-200" : ""
                      }`}
                    >
                      Why IBTE
                    </NavLink>
                  </li>
                  <li className="list-none block w-full">
                    <NavLink
                      to="/admin/faculty_content"
                      className={`${
                        state.path == "team" ? "text-black bg-gray-200" : ""
                      }`}
                    >
                      Faculty/IBTE Team
                    </NavLink>
                  </li>
                  <li className="list-none block w-full">
                    <NavLink
                      to="/admin/accreditation"
                      className={`${
                        state.path == "accreditation"
                          ? "text-black bg-gray-200"
                          : ""
                      }`}
                    >
                      Accreditation And Recognition
                    </NavLink>
                  </li>

                  <li className="list-none block w-full">
                    <NavLink
                      to="/admin/partner"
                      className={`${
                        state.path == "partner" ? "text-black bg-gray-200" : ""
                      }`}
                    >
                      Our Partners
                    </NavLink>
                  </li>
                  <li className="list-none block w-full">
                    <NavLink
                      to="/admin/dubai"
                      className={`${
                        state.path == "dubai" ? "text-black bg-gray-200" : ""
                      }`}
                    >
                      About Dubai
                    </NavLink>
                  </li>
                </ul>
              </li>
              <li className="list-none block w-full">
                <NavLink
                  to="/admin/study"
                  className={`${
                    state.path == "study" ? "text-black bg-white" : ""
                  }`}
                >
                  Study with IBTE
                </NavLink>
                <ul className="flex flex-wrap">
                  <li className="list-none block w-full">
                    <NavLink
                      to="/admin/study"
                      className={`${
                        state.path == "study" ? "text-black bg-gray-200" : ""
                      }`}
                    >
                      Study with IBTE
                    </NavLink>
                  </li>
                  <li className="list-none block w-full">
                    <NavLink
                      to="/admin/programs"
                      className={`${
                        state.path == "program" ? "text-black bg-gray-200" : ""
                      }`}
                    >
                      Programs
                    </NavLink>
                    <ul className="flex flex-wrap">
                      <li className="list-none block w-full">
                        <NavLink
                          to="/admin/program-details"
                          className={`${
                            state.path == "program-details"
                              ? "text-black bg-gray-200"
                              : ""
                          }`}
                        >
                          Programs Details Content
                        </NavLink>
                      </li>
                    </ul>
                  </li>
                  <li className="list-none block w-full">
                    <NavLink
                      to="/admin/fees"
                      className={`${
                        state.path == "fees" ? "text-black bg-gray-200" : ""
                      }`}
                    >
                      Tution Fees
                    </NavLink>
                  </li>
                  <li className="list-none block w-full">
                    <NavLink
                      to="/admin/admission"
                      className={`${
                        state.path == "admission"
                          ? "text-black bg-gray-200"
                          : ""
                      }`}
                    >
                      Admission
                    </NavLink>
                  </li>
                  <li className="list-none block w-full">
                    <NavLink
                      to="/admin/visa"
                      className={`${
                        state.path == "visa" ? "text-black bg-gray-200" : ""
                      }`}
                    >
                      Visas and Insurance
                    </NavLink>
                  </li>
                  <li className="list-none block w-full">
                    <NavLink
                      to="/admin/path"
                      className={`${
                        state.path == "path" ? "text-black bg-gray-200" : ""
                      }`}
                    >
                      Student Path
                    </NavLink>
                  </li>
                  <li className="list-none block w-full">
                    <NavLink
                      to="/admin/academic"
                      className={`${
                        state.path == "academic" ? "text-black bg-gray-200" : ""
                      }`}
                    >
                      Academic Calender
                    </NavLink>
                  </li>
                  <li className="list-none block w-full">
                    <NavLink
                      to="/admin/life"
                      className={`${
                        state.path == "life" ? "text-black bg-gray-200" : ""
                      }`}
                    >
                      Student Life
                    </NavLink>
                    <ul className="flex flex-wrap">
                      <li className="list-none block w-full">
                        <NavLink
                          to="/admin/support"
                          className={`${
                            state.path == "support"
                              ? "text-black bg-gray-200"
                              : ""
                          }`}
                        >
                          Support During Studies
                        </NavLink>
                      </li>
                      <li className="list-none block w-full">
                        <NavLink
                          to="/admin/graduation"
                          className={`${
                            state.path == "graduation"
                              ? "text-black bg-gray-200"
                              : ""
                          }`}
                        >
                          Post-Graduation Support
                        </NavLink>
                      </li>
                    </ul>
                  </li>
                  <li className="list-none block w-full">
                    <NavLink
                      to="/admin/campus"
                      className={`${
                        state.path == "campus" ? "text-black bg-gray-200" : ""
                      }`}
                    >
                      IBTE Campus
                    </NavLink>
                  </li>
                </ul>
              </li>

              <li className="list-none block w-full">
                <NavLink
                  to="/admin/faq"
                  className={`${
                    state.path == "faq" ? "text-black bg-gray-200" : ""
                  }`}
                >
                  FAQ
                </NavLink>
              </li>

              <li className="list-none block w-full">
                <NavLink
                  to="/admin/news"
                  className={`${
                    state.path == "news" ? "text-black bg-gray-200" : ""
                  }`}
                >
                  News & Events
                </NavLink>
              </li>

              <li className="list-none block w-full">
                <NavLink
                  to="/admin/contact_info"
                  className={`${
                    state.path == "contact_info" ? "text-black bg-gray-200" : ""
                  }`}
                >
                  Contact
                </NavLink>
              </li>
              <li className="list-none block w-full">
                <NavLink
                  to="/admin/contact_email"
                  className={`${
                    state.path == "contact_email"
                      ? "text-black bg-gray-200"
                      : ""
                  }`}
                >
                  Contact Emails
                </NavLink>
              </li>

              <li className="list-none block w-full">
                <NavLink
                  to="/admin/application"
                  className={`${
                    state.path == "application" ? "text-black bg-gray-200" : ""
                  }`}
                >
                  Application
                </NavLink>
              </li>

              {/* <li className="list-none block w-full">
                <NavLink
                  to="/admin/cms"
                  className={`${
                    state.path == "cms" ? "text-black bg-gray-200" : ""
                  }`}
                >
                  Cms
                </NavLink>
              </li> */}
              <li className="list-none block w-full">
                <NavLink
                  to="/admin/features"
                  className={`${
                    state.path == "features" ? "text-black bg-gray-200" : ""
                  }`}
                >
                  Features
                </NavLink>
              </li>

              {/* <li className="list-none block w-full">
                <NavLink
                  to="/admin/faculty"
                  className={`${
                    state.path == "faculty" ? "text-black bg-gray-200" : ""
                  }`}
                >
                  Faculty
                </NavLink>
              </li> */}

              <li className="list-none block w-full">
                <NavLink
                  to="/admin/testimonials"
                  className={`${
                    state.path == "testimonial" ? "text-black bg-gray-200" : ""
                  }`}
                >
                  Testimonials
                </NavLink>
              </li>

              <li className="list-none block w-full">
                <NavLink
                  to="/admin/section"
                  className={`${
                    state.path == "section" ? "text-black bg-gray-200" : ""
                  }`}
                >
                   Sections (Hide/Show)
                </NavLink>
              </li>

              <li className="list-none block w-full">
                <NavLink
                  to="/admin/privacy"
                  className={`${
                    state.path == "privacy" ? "text-black bg-gray-200" : ""
                  }`}
                >
                  Privacy policy
                </NavLink>
              </li>

              <li className="list-none block w-full">
                <NavLink
                  to="/admin/terms/"
                  className={`${
                    state.path == "terms" ? "text-black bg-gray-200" : ""
                  }`}
                >
                  Terms and Conditions
                </NavLink>
              </li>

              <li className="list-none block w-full">
                <NavLink
                  to="/admin/footer"
                  className={`${
                    state.path == "footer" ? "text-black bg-gray-200" : ""
                  }`}
                >
                  Footer
                </NavLink>
              </li>

              {/*     
    <li className="list-none block w-full">
    <NavLink
    to="/admin/email"
    className={`${state.path == "emails" ? "text-black bg-gray-200" : ""}`}
    >
    Emails
    </NavLink>
    </li> */}

              {/* 
    
    <li className="list-none block w-full">
    <NavLink
    to="/admin/photo"
    className={`${state.path == "photos" ? "text-black bg-gray-200" : ""}`}
    >
    Photos
    </NavLink>
    </li> */}

              {/*     
    <li className="list-none block w-full">
    <NavLink
    to="/admin/posts"
    className={`${state.path == "postss" ? "text-black bg-gray-200" : ""}`}
    >
    Posts
    </NavLink>
    </li> */}

              {/* 
    
    <li className="list-none block w-full">
    <NavLink
    to="/admin/setting"
    className={`${state.path == "settings" ? "text-black bg-gray-200" : ""}`}
    >
    Settings
    </NavLink>
    </li> */}

              {/* 
    
    <li className="list-none block w-full">
    <NavLink
    to="/admin/users"
    className={`${state.path == "userss" ? "text-black bg-gray-200" : ""}`}
    >
    Users
    </NavLink>
    </li>
     */}

              {/*               
              <li className="list-none block w-full">
                <NavLink
                  to="/admin/profile"
                  className={`${state.path == "profile" ? "text-black bg-gray-200" : ""}`}
                >
                  Profile
                </NavLink>
              </li> */}
              <li className="list-none block w-full">
                <NavLink
                  to="/admin/login"
                  onClick={() =>
                    dispatch({
                      type: "LOGOUT",
                    })
                  }
                >
                  Logout
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </>
  );
};

export default AdminHeader;
