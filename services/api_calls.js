
const axios = require('axios')
module.exports = class {
    

    constructor(){
          this.project_header = btoa(process.env.PROJECT_ID + ":" + process.env.PROJECT_SECRET )
    }

    async getRequest(url, query={}){
          console.log(this.project_header);
          let r = await axios.get(process.env.API_BASE_URL + url, {
              headers:{
                    'x-project': this.project_header
              },
              params: {
                 ...query
              }
          })
          console.log("****", r.data);
          return r.data;
    }

    async postRequest(url, body, query={}){
          let r = await axios.post(process.env.API_BASE_URL + url, {
              Headers:{
                    'x-project': this.project_header
              },
              params: {
                 ...query
              },
              body: JSON.stringify(body)
          })

          return r.data;
    }
}