
const api_calls = require('./api_calls');

let blogInBlogList = `
<article data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000" class="blog-card-item">
<a class="blog-card-img" href="/pages/blog-details/{{{id}}}"><img src="{{{thumbnail}}}" alt="baby's hand" /></a>
<h4><a href="/pages/blog-details" >
{{{title}}}
</a></h4>
<p>
  {{{description}}}
</p>
<a href="/pages/blog-details/{{{id}}}">Read full article</a>
</article>
`

module.exports = {
  async getBlogList(query) {
    let request = new api_calls();
    let result = await request.getRequest(`/v2/api/lambda/blog/filter`, query);

    let html = ``;

    for (let x of result.data) {
      let tmp = blogInBlogList.substring(0);
      tmp = tmp.replace('{{{thumbnail}}}', x.thumbnail)
      tmp = tmp.replace('{{{description}}}', `${x.description.substring(0, 100)}...`)
      tmp = tmp.replaceAll('{{{id}}}', x.id)
      tmp = tmp.replace('{{{title}}}', x.title)
      html += tmp;
    }

    return html;
  },

  async getSingleBlogAttributes(id) {
    let request = new api_calls();
    let result = await request.getRequest(`/v2/api/lambda/blog/single/${id}`);

    let tag = ``;

    for (let x of result.data.tags) {
      tag += `<li> ${x.name} </li>`
    }

    let categories = ``;

    for (let x of result.data.categories) {
      categories += `<li> ${x.name} </li>`
    }

    return {
      title: result.data.title,
      content: result.data.content,
      thumbnail: result.data.thumbnail,
      tag,
      categories,
      create_at: result.data.create_at,
      author: result.data.author
    }

  },
  async getSimilarBlogs(id) {
    let request = new api_calls();
    let result = await request.getRequest(`/v2/api/lambda/blog/similar/${id}?top=3`);

    let html = ``;

    for (let x of result.data) {
      let tmp = blogInBlogList.substring(0);
      tmp = tmp.replace('{{{thumbnail}}}', x.thumbnail)
      tmp = tmp.replace('{{{description}}}', `${x.description.substring(0, 100)}...`)
      tmp = tmp.replaceAll('{{{id}}}', x.id)
      tmp = tmp.replace('{{{title}}}', x.title)
      html += tmp;
    }
    return html;

  }

}
