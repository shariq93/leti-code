
"use strict";
const app = require("./app");

const PORT = 3260;

const onListen = async () => {
  console.log("Server running at ", `http://localhost:${PORT}`);
  console.log("");
};

const server = app.listen(PORT, onListen);

module.exports = { server, onListen, PORT };
