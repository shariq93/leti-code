
const rp = require("request-promise");
const fs = require("fs");

async function queryCms(req, pageType, clearCache = false) {
  let condition = req.session && req.session.cms && req.session.cms.length > 0;
  let payload = [];

  if (!condition || clearCache) {
    const url = "https://api.ibte.me/v1/api/public/cms/GETALL";
    const headers = {
      Accept: "application/json",
      "Content-Type": "application/json",
      "x-project": "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
    };
    const options = {
      url,
      headers,
      method: "POST",
      json: true
    };

    try {
      const result = await rp(options); 
      const list = result.list;
      const buckets = {};
      list.forEach((value) => {
        if (buckets.hasOwnProperty(value.page)) {
          buckets[value.page].push({
            key: value.content_key,
            type: value.content_type,
            value: value.content_value
          });
        } else {
          buckets[value.page] = [];
          buckets[value.page].push({
            key: value.content_key,
            type: value.content_type,
            value: value.content_value
          });
        }
      });
      req.session.cms = buckets;
      payload = buckets;
    } catch (error) {
      console.error(error);
    }
  } else {
    payload = req.session.cms;
  }

  if (payload[pageType]) {
    return payload[pageType];
  }
  return payload;
}
module.exports = {
  renderPage: async function (session, pageType, clearCache, filePath) {
    try {
      const data = await queryCms(session, pageType, clearCache);
      let text = await fs.readFileSync(filePath, "utf8");
      if (Array.isArray(data)) {
        data.forEach((value) => {
          text = text.replace(new RegExp(`{{{${value.key}}}}`, "g"), value.value);
        });
      }
      return text;
    } catch (error) {
      console.log("error", error);
      return "ERROR";
    }
  }
};
