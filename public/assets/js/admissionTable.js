function getAdmission() {
    var myHeaders = new Headers();
    myHeaders.append(
      "x-project",
      "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
    );

    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };


    const token = localStorage.getItem("anonymous_token");

    var secondApiHeaders = new Headers();
    secondApiHeaders.append(
      "x-project",
      "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
    );
    secondApiHeaders.append("Authorization", `Bearer ${token}`);

    var secondApiOptions = {
      method: "GET",
      headers: secondApiHeaders,
      redirect: "follow",
    };
    fetch(
      "https://api.ibte.me/v4/api/records/cms_admissions",
      secondApiOptions
    )
      .then((response) => response.json())
      .then((result) => {
        const contentMap = {};
        result.list.forEach((item) => {
          contentMap[item.content_key] = item.content_value;
        });

        const tableBody = document.getElementById("tableBody");
        tableBody.innerHTML = "";
        const rows = [
            ["tr1_td1", "tr1_td2"],
            ["tr2_td1", "tr2_td2"],
            ["tr3_td1", "tr3_td2"],
            ["tr4_td1", "tr4_td2"],
            ["tr5_td1", "tr5_td2"],
            ["tr6_td1", "tr6_td2"],
            ["tr7_td1", "tr7_td2"],
            ["tr8_td1", "tr8_td2"],
            ["tr9_td1", "tr9_td2"],
            ["tr10_td1", "tr10_td2"],
            ["tr11_td1", "tr11_td2"],
            ["tr12_td1", "tr12_td2"],
            ["tr13_td1", "tr13_td2"],
            ["tr14_td1", "tr14_td2"],
            ["tr15_td1", "tr15_td2"],
            ["tr16_td1", "tr16_td2"]
        ];
        rows.forEach((row) => {
          const tr = document.createElement("tr");
          row.forEach((contentKey) => {
            const td = document.createElement("td");
            if (contentMap[contentKey].includes("<br/>")) {
              td.innerHTML = contentMap[contentKey];
            } else {
              td.textContent = contentMap[contentKey];
            }
            tr.appendChild(td);
          });
          tableBody.appendChild(tr);
        });

        const th1 = document.getElementById("th1");
        th1.innerHTML = `<img class="mr-2" src="assets/images/calendar-icon.svg" alt="" />${contentMap["tr1th1"]}`;
  
        const th2 = document.getElementById("th2");
        th2.innerHTML = `<img class="mr-2" src="assets/images/file-icon.svg" alt="" />${contentMap["tr1th2"]}`;
      })
      .catch((error) => console.log("error", error));


  }

  $(window).on("load", function () {
    getAdmission();
});







//  function getAdmission() {
// var myHeaders = new Headers();
// const token = localStorage.getItem("anonymous_token");

// myHeaders.append("authorization", `Bearer ${token}`);
// myHeaders.append("x-project", "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg==");

// var requestOptions = {
//   method: 'GET',
//   headers: myHeaders,
//   redirect: 'follow'
// };

// fetch("https://api.ibte.me/v4/api/records/cms_admissions", requestOptions)
//   .then(response => response.text())
//   .then(result => console.log(result))
//   .catch(error => console.log('error', error));
// }

//   $(window).on("load", function () {
//     getAdmission();
// });

