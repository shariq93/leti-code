const getCampusImages = async function () {
  var myHeaders = new Headers();
  myHeaders.append("x-project", "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg==");

  var raw = "";

  var requestOptions = {
    method: "POST",
    headers: myHeaders,
    body: raw,
    redirect: "follow",
  };

  try {
    const response = await fetch(
      "https://api.ibte.me/v1/api/public/cms/GETALL",
      requestOptions
    );
    const data = await response.json();

    // Find campus data with content_key "image_slider"
    const campusData = data.list.find(
      (item) => item.page === "campus" && item.content_key === "image_slider"
    );

    if (campusData && campusData.content_type === "image-list") {
      const imageSliderArray = JSON.parse(campusData.content_value);

      // Create Swiper slides dynamically
      const swiperWrapper = document.querySelector(
        ".swiper-container.campus-img-slide .swiper-wrapper"
      );

      let campusImageHtml = "";

      imageSliderArray.forEach((imageObj, index) => {
        if (imageObj.value) {
          // Check if the image URL is not null
          const campusImageTemplate = `
            <div class="swiper-slide">
              <img src="${imageObj.value}" alt="${index + 1}" />
            </div>
          `;
          campusImageHtml += campusImageTemplate;
        }
      });

      if (campusImageHtml) {
        swiperWrapper.innerHTML = campusImageHtml;
        renderSlider();
      } else {
    
      }
    } else {
  
    }
  } catch (error) {
    console.error("Error:", error);

  }
};

// Call the function to load campus images
document.addEventListener("DOMContentLoaded", function () {
  getCampusImages();
});

function renderSlider() {
  var swiper = new Swiper(".campus-img-slide", {
    slidesPerView: 3,
    loop: false,
    spaceBetween: 10,
    pagination: {
      el: ".swiper-pagination",
      dynamicBullets: true,
      clickable: true,
    },
    navigation: {
      prevEl: ".campus-slide-left",
      nextEl: ".campus-slide-right",
    },

    breakpoints: {
      500: {
        slidesPerView: 2,
      },
      1000: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 3,
      },
      2000: {
        slidesPerView: 3,
      },
    },
  });
}
