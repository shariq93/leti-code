function getProgramsInner() {
  var myHeaders = new Headers();
  myHeaders.append(
    "x-project",
    "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
  );

  var requestOptions = {
    method: "GET",
    headers: myHeaders,
    redirect: "follow",
  };

  const token = localStorage.getItem("anonymous_token");
  var secondApiHeaders = new Headers();
  secondApiHeaders.append(
    "x-project",
    "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
  );
  secondApiHeaders.append("Authorization", `Bearer ${token}`);

  var secondApiOptions = {
    method: "GET",
    headers: secondApiHeaders,
    redirect: "follow",
  };

  fetch(
    "https://api.ibte.me/v4/api/records/cms_programs",
    secondApiOptions
  )
    .then((response) => response.json())
    .then((result) => {
      const prgramList = result.list;


       const customIndices = {
        "Cyber Security and Networking": 1,
        "Artificial Intelligence": 2,
        "Business and Management": 3,
        "Business and Computing": 4,
        "Software Engineering": 5,
        "Computer Science": 6,
      };
      prgramList.sort((a, b) => {
        const indexA = customIndices[a.program_name] || Infinity;
        const indexB = customIndices[b.program_name] || Infinity;
        return indexA - indexB;
      });

      const categories = {
        "current programs": document.getElementById("currentProgramsContainer"),
        german: document.getElementById("germanContainer"),
        french: document.getElementById("frenchContainer"),
        other: document.getElementById("otherContainer"),
        upcoming: document.getElementById("upcomingContainer"),
      };

      prgramList.forEach((program) => {
        if (program.status === "active") { 
        let programHtml = "";
        const categoryContainer =
          categories[program.program_category.toLowerCase()];
        if (program.program_category.toLowerCase() === "current programs") {
          programHtml = `
              <a href="program-details?id=${program.id}" class="program-box">
                <img class="p-arrow" src="assets/images/arrow.svg" />
                <div class="program-icon">
                  <img src="assets/images/program-icon.svg" />
                </div>
                <h5>${program.program_name}</h5>
                <div class="programs-descs">
                <p>${program.description}</p>
                </div>
              </a>
            `;
        } else if (program.program_category.toLowerCase() === "other") {
          programHtml = `
              <a href="program-details?id=${program.id}" class="program-box">
                <img class="p-arrow" src="assets/images/arrow.svg" />
                <div class="program-icon">
                  <img src="assets/images/program-icon.svg" />
                </div>
                <h5>${program.program_name}</h5>
                <div class="programs-descs">
                <p>${program.description}</p>
                </div>
              </a>
            `;
        } else if (
          program.program_category.toLowerCase() === "german"
        ) {
          programHtml = `
          <form class="journey-form" id="german_form">
          <h5>${program.description}</h5>
          <fieldset>
            <label>Email</label>
            <input name="email" placeholder="you@domain.com" type="email">
          </fieldset>
        
          <button class="mt-4" type="submit">Submit Email</button>
        </form>
            `;
        }else if (
          
          program.program_category.toLowerCase() === "french"
        ) {
          programHtml = `
          <form class="journey-form" id="french_form">
          <h5>${program.description}</h5>
          <fieldset>
            <label>Email</label>
            <input name="email" placeholder="you@domain.com" type="email" >
          </fieldset>
        
          <button class="mt-4" type="submit">Submit Email</button>
        </form>
            `;
        }  else if (program.program_category.toLowerCase() === "upcoming") {
          programHtml = `
          <div class="upcomming-program-box">
          <div class="summary_data" data-summary="${program.program_summary}"></div>
          <div class="pro_name" data-summary="${program.program_name}"></div>
              <div class="program-icon">
                  <img src="assets/images/program-icon.svg" />
              </div>
              <div class="upcomming-program-text">
                  <h5>${program.program_name}</h5>
                  
                  <div class="short-desc">${program.description}</div>
                  <button
                      data-toggle="modal"
                      data-target="#upcoming-program"
                      type="button"
                  >
                      Learn more ->
                  </button>
              </div>
          </div>
            `;
        }

        const programElement = document.createElement("div");
        programElement.classList.add("swiper-slide");
        programElement.innerHTML = programHtml;

        categoryContainer.appendChild(programElement);

        const learnMoreButton = programElement.querySelector(
          '[data-target="#upcoming-program"]'
        );


       
        


        document.addEventListener("click", function (event) {
          if (event.target.matches('[data-toggle="modal"]')) {
            const modal = document.getElementById("upcoming-program");
            const modalBody = modal.querySelector(".modal-body");
            const modalHeader = modal.querySelector(".modal-header h4");
            
            const programSummary = event.target.closest(".upcomming-program-box").querySelector(".summary_data").getAttribute("data-summary");;
            const programName = event.target.closest(".upcomming-program-box").querySelector(".pro_name").getAttribute("data-summary");
            const cleanedSummary = programSummary.replace(/&nbsp;/g, "");
            
            modalBody.innerHTML = cleanedSummary;
            modalHeader.innerHTML = programName;
            

            pauseSwiper(swiper1);
            pauseSwiper(swiper2);
            pauseSwiper(swiper3);

          }
        });
        
      }
      });
      loadSLider();




  const form2 = document.getElementById("german_form");

  form2.addEventListener("submit", async function (event) {
    event.preventDefault(); // Prevent the default form submission behavior

    const token = localStorage.getItem("anonymous_token");
    const projectId =
      "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="; // Your project ID

    const formData = new FormData(event.target);
    const formObject = {};
    formData.forEach((value, key) => {
      formObject[key] = value;
    });

    const headers = new Headers();
    headers.append("x-project", projectId);
    headers.append("Authorization", `Bearer ${token}`);
    headers.append("Content-Type", "application/json");

    const options = {
      method: "POST",
      headers: headers,
      body: JSON.stringify(formObject),
      redirect: "follow",
    };

    try {
      const response = await fetch(
        "https://api.ibte.me/v4/api/records/news_letter",
        options
      );
      const data = await response.json();

      toastr.success("Email Submitted", "Success");
      console.log(data);

      event.target.reset();
    } catch (error) {
      console.error("Error submitting form:", error);
    }
  });





  const form = document.getElementById("french_form");

  form.addEventListener("submit", async function (event) {

    event.preventDefault(); // Prevent the default form submission behavior

    const token = localStorage.getItem("anonymous_token");
    const projectId =
      "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="; // Your project ID

    const formData = new FormData(event.target);
    const formObject = {};
    formData.forEach((value, key) => {
      formObject[key] = value;
    });

    const headers = new Headers();
    headers.append("x-project", projectId);
    headers.append("Authorization", `Bearer ${token}`);
    headers.append("Content-Type", "application/json");

    const options = {
      method: "POST",
      headers: headers,
      body: JSON.stringify(formObject),
      redirect: "follow",
    };

    try {
      const response = await fetch(
        "https://api.ibte.me/v4/api/records/news_letter",
        options
      );
      const data = await response.json();

      toastr.success("Email Submitted", "Success");
      console.log(data);

      event.target.reset();
    } catch (error) {
      console.error("Error submitting form:", error);
    }
  });




    })
    .catch((error) => console.log("error", error));
}

$(window).on("load", function () {
  getProgramsInner();
});

$(".tablinks").on("click", function () {
  loadSLider();
});




$("#upcoming-program").on("hidden.bs.modal", function () {
  resumeSwiper(swiper1);
  resumeSwiper(swiper2);
  resumeSwiper(swiper3);
});


function pauseSwiper(swiper) {
  swiper.autoplay.stop();

}


function resumeSwiper(swiper) {

  swiper.autoplay.start();
}


var swiper1, swiper2, swiper3;

function loadSLider() {
  var swiper1 = new Swiper(".program-box-slide1", {
    slidesPerView: 4,
    // centeredSlides: true,

    grid: {
      rows: 2,
      fill: "row",
    },
    slidesOffsetAfter: 1500,
    loop: false,
    spaceBetween: 10,
    pagination: {
      el: ".swiper-pagination",
      dynamicBullets: true,
      clickable: true,
    },
    navigation: {
      nextEl: ".program-s-r1",
      prevEl: ".program-s-l1",
    },

    breakpoints: {
      500: {
        slidesPerView: 3,
      },
      100: {
        slidesPerView: 1,
      },
      767: {
        slidesPerView: 3,
      },
      1200: {
        slidesPerView: 3,
      },
      2000: {
        slidesPerView: 3,
      },
    },
  });

  var swiper2 = new Swiper(".program-box-slide2", {
    slidesPerView: 4,
    loop: false,
    slidesOffsetAfter: 500,
    grid: {
        rows: 2,
        fill: "row",
      },
    spaceBetween: 10,
    pagination: {
      el: ".swiper-pagination",
      dynamicBullets: true,
      clickable: true,
    },
    navigation: {
      nextEl: ".program-s-r2",
      prevEl: ".program-s-l2",
    },

    breakpoints: {
      500: {
        slidesPerView: 3,
      },
      100: {
        slidesPerView: 1,
      },
      767: {
        slidesPerView: 3,
      },
      1200: {
        slidesPerView: 3,
      },
      2000: {
        slidesPerView: 3,
      },
    },
  });
  var swiper3 = new Swiper(".program-box-slide3", {
    slidesPerView: 4,
    slidesOffsetAfter: 500,
    loop: false,
    grid: {
        rows: 3,
        fill: "row",
      },
    spaceBetween: 10,
    pagination: {
      el: ".swiper-pagination",
      dynamicBullets: true,
      clickable: true,
    },
    navigation: {
      nextEl: ".program-s-r3",
      prevEl: ".program-s-l3",
    },

    breakpoints: {
      500: {
        slidesPerView: 3,
      },
      100: {
        slidesPerView: 1,
      },
      767: {
        slidesPerView: 3,
      },
      1200: {
        slidesPerView: 3,
      },
      2000: {
        slidesPerView: 3,
      },
    },
  });
}
