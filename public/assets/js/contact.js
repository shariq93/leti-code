const ContactInfo = async function () {
    var myHeaders = new Headers();
    myHeaders.append("x-project", "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg==");
  
    var raw = "";
  
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
  
    try {
      const response = await fetch("https://api.ibte.me/v1/api/public/cms/GETALL", requestOptions);
      const data = await response.json();
  
      const contactPageData = data.list.filter(item => item.page === 'contact');
  
      const contactData = {};
      contactPageData.forEach(item => {
        contactData[item.content_key] = item.content_value;
      });
  
      document.getElementById("section14_header1").innerText = contactData.section14_header1;
      document.getElementById("section14_header1_desc").innerText = contactData.section14_header1_desc;
      document.getElementById("section14_tagline").innerText = contactData.section14_tagline;
      document.getElementById("section14_header2").innerText = contactData.section14_header2;
      document.getElementById("section14_header2_desc").innerHTML = contactData.section14_header2_desc;
      document.getElementById("section14_card1_header1").innerText = contactData.section14_card1_header1;
      document.getElementById("section14_card1_header2").innerText = contactData.section14_card1_header2;
      document.getElementById("section14_card1_header2").setAttribute("href", `tel:${contactData.section14_card1_header2}`);
      document.getElementById("section14_card1_desc").innerText = contactData.section14_card1_desc;
      document.getElementById("section14_card2_header1").innerText = contactData.section14_card2_header1;
      document.getElementById("section14_card2_header2").innerText = contactData.section14_card2_header2;
      document.getElementById("section14_card2_header2").setAttribute("href", `mailto:${contactData.section14_card2_header2}`);
      document.getElementById("section14_card2_desc").innerText = contactData.section14_card2_desc;
      document.getElementById("section14_card3_header").innerText = contactData.section14_card3_header;
      document.getElementById("section14_card3_desc").innerText = contactData.section14_card3_desc;
      document.getElementById("section14_card4_header").innerText = contactData.section14_card4_header;
      document.getElementById("section14_card4_desc").innerText = contactData.section14_card4_desc;

      document.getElementById("twiter").setAttribute("href", `${contactData.twiter_url}`);
      document.getElementById("facebook").setAttribute("href", `${contactData.facebook_url}`);
      document.getElementById("linkedin").setAttribute("href", `${contactData.linkedin_url}`);
      document.getElementById("insta").setAttribute("href", `${contactData.insta_url}`);

      document.getElementById("foot_twiter").setAttribute("href", `${contactData.twiter_url}`);
      document.getElementById("foot_facebook").setAttribute("href", `${contactData.facebook_url}`);
      document.getElementById("foot_linkedin").setAttribute("href", `${contactData.linkedin_url}`);
      document.getElementById("foot_insta").setAttribute("href", `${contactData.insta_url}`);
      
      document.getElementById("getInfoImg").setAttribute("src", `${contactData.section14_img}`);

    //   footer
    document.getElementById("foot_email").innerText = contactData.section14_card2_header2;
    document.getElementById("foot_email").setAttribute("href", `mailto:${contactData.section14_card2_header2}`);
    document.getElementById("foot_phone").innerText = contactData.section14_card1_header2;
    document.getElementById("foot_phone").setAttribute("href", `tel:${contactData.section14_card1_header2}`);

    // header
    document.getElementById("head_email").innerText = contactData.section14_card2_header2;
    document.getElementById("head_email").setAttribute("href", `mailto:${contactData.section14_card2_header2}`);
    document.getElementById("head_phone").innerText = contactData.section14_card1_header2;
    document.getElementById("head_phone").setAttribute("href", `tel:${contactData.section14_card1_header2}`);

    document.getElementById("op_whatsapp").setAttribute("href", `whatsapp://send?phone=${contactData.section14_card1_header2}`);
    document.getElementById("op_call").setAttribute("href", `tel:${contactData.section14_card1_header2}`);
    document.getElementById("op_email").setAttribute("href", `mailto:${contactData.section14_card2_header2}`);
    document.getElementById("op_location").setAttribute("href", `https://www.google.com/maps/place/IETI/@25.1246804,55.407242,17z/data=!3m1!4b1!4m6!3m5!1s0x3e5f637ab0586513:0xdc97643d69a775c8!8m2!3d25.1246756!4d55.4098169!16s%2Fg%2F11rw_qqv3f?entry=tts&shorturl=1`);

   
    setTimeout(function () {
      replaceTimezoneText(document.body);
       }, 500);


    } catch (error) {
      console.log('error', error);
    }
  };
  

  ContactInfo()

  function replaceTimezoneText(node) {
    const dubaiOffset = 4; // Dubai is GMT+4
    const gmtTime = new Date(new Date().getTime() - (dubaiOffset * 60 * 60 * 1000)).toUTCString();

    if (node.nodeType === Node.TEXT_NODE) {
      const originalText = node.textContent;
      const replacedText = originalText.replace(/GMT \+ 04:00/g, gmtTime);
      node.textContent = replacedText;
    } else {
      for (const childNode of node.childNodes) {
        replaceTimezoneText(childNode);
      }
    }
  }





  