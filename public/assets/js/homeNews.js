function getHomeNews() {
  var myHeaders = new Headers();
  myHeaders.append(
    "x-project",
    "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
  );

  var requestOptions = {
    method: "GET",
    headers: myHeaders,
    redirect: "follow",
  };

  const token = localStorage.getItem("anonymous_token");
  var secondApiHeaders = new Headers();
  secondApiHeaders.append(
    "x-project",
    "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
  );
  secondApiHeaders.append("Authorization", `Bearer ${token}`);

  var secondApiOptions = {
    method: "GET",
    headers: secondApiHeaders,
    redirect: "follow",
  };

  const newsContainer = document.getElementById("news-container"); // Assuming you have a container with id "news-container"

  fetch("https://api.ibte.me/v4/api/records/cms_events_news", secondApiOptions)
    .then((response) => response.json())
    .then((result) => {
      const newsList = result.list;
    
      // Shuffle the newsList array to get random news items
      const shuffledNewsList = shuffleArray(newsList);

      // Display the first three items from the shuffled list
      const randomNewsList = shuffledNewsList.slice(0, 3);

      randomNewsList.forEach((newsItem) => {
        const newsBox = document.createElement("div");
        newsBox.className = "col-md-4 mt-1 mb-1";

        const newsLink = document.createElement("a");
        newsLink.href = "javascript:void(0)";
        newsLink.className = "news-archive-box";

        newsLink.addEventListener("click", function () {
          const newsId = newsItem.id;
          window.location.href = `/post-view?id=${newsId}`;
      });

        const newsImage = document.createElement("img");
        newsImage.src = newsItem.cover;

        const newsTags = document.createElement("div");
        newsTags.className = "news-tags";
        
        const tags = newsItem.tags.split(","); // Split tags by comma
        
        tags.forEach((tag) => {
          const tagElement = document.createElement("span");
          tagElement.textContent = tag.trim(); // Remove spaces around the tag
          newsTags.appendChild(tagElement);
        });

        const newsTitle = document.createElement("h5");
        newsTitle.textContent = newsItem.title;

        const newsSummary = document.createElement("p");
        newsSummary.innerHTML = newsItem.summary;

        const newsDate = document.createElement("h6");
        const formattedDate = moment(newsItem.published_at).format("DD MMM YYYY");
        newsDate.textContent = formattedDate;

        newsLink.appendChild(newsImage);
        newsLink.appendChild(newsTags);
        newsLink.appendChild(newsTitle);
        newsLink.appendChild(newsSummary);
        newsLink.appendChild(newsDate);

        newsBox.appendChild(newsLink);

        newsContainer.appendChild(newsBox);
      });
    })
    .catch((error) => console.log("error", error));
}

function shuffleArray(array) {
  // Fisher-Yates shuffle algorithm
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}

$(window).on("load", function () {
  getHomeNews();
});
