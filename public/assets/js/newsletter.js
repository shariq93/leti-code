document.addEventListener("DOMContentLoaded", function () {
    const form = document.getElementById("newsletterForm");
  
    form.addEventListener("submit", async function (event) {
      event.preventDefault(); // Prevent the default form submission behavior
  
      const token = localStorage.getItem("anonymous_token");
      const projectId =
        "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="; // Your project ID
  
      const formData = new FormData(event.target);
      const formObject = {};
      formData.forEach((value, key) => {
        formObject[key] = value;
      });
  
      const headers = new Headers();
      headers.append("x-project", projectId);
      headers.append("Authorization", `Bearer ${token}`);
      headers.append("Content-Type", "application/json");
  
      const options = {
        method: "POST",
        headers: headers,
        body: JSON.stringify(formObject),
        redirect: "follow",
      };
  
      try {
        const response = await fetch(
          "https://api.ibte.me/v4/api/records/news_letter",
          options
        );
        const data = await response.json();
  
        toastr.success("Email Submitted", "Success");
        console.log(data);
  
        event.target.reset();
      } catch (error) {
        console.error("Error submitting form:", error);
      }
    });
  });