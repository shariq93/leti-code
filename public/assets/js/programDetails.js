document.addEventListener("DOMContentLoaded", async function () {

    const urlParams = new URLSearchParams(window.location.search);
    const programId = urlParams.get("id");

    const projectId = "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg==";
    const token = localStorage.getItem("anonymous_token");

    const secondApiHeaders = new Headers();
    secondApiHeaders.append("x-project", projectId);
    secondApiHeaders.append("Authorization", `Bearer ${token}`);

    try {
        // Fetch program details
        const programResponse = await fetch(`https://api.ibte.me/v4/api/records/cms_programs/${programId}`, {
            method: 'GET',
            headers: secondApiHeaders,
            redirect: 'follow'
        });
        if (!programResponse.ok) {
            throw new Error(`Error fetching program details. Status: ${programResponse.status}`);
        }
        const programData = await programResponse.json();

        // Fetch levels for the program
        const levelsResponse = await fetch(`https://api.ibte.me/v4/api/records/cms_program_level?filter=program_id,eq,${programId}`, {
            method: 'GET',
            headers: secondApiHeaders,
            redirect: 'follow'
        });
        if (!levelsResponse.ok) {
            throw new Error(`Error fetching program levels. Status: ${levelsResponse.status}`);
        }
        const levelsData = await levelsResponse.json();

        // Fetch and populate semesters for each level
        const programLevels = await Promise.all(levelsData.list.map(async (level) => {
            const semestersResponse = await fetch(`https://api.ibte.me/v4/api/records/cms_program_semester?filter=level_id,eq,${level.id}`, {
                method: 'GET',
                headers: secondApiHeaders,
                redirect: 'follow'
            });
            if (!semestersResponse.ok) {
                throw new Error(`Error fetching semesters for level ${level.id}. Status: ${semestersResponse.status}`);
            }
            const semestersData = await semestersResponse.json();
            level.semesters = semestersData.list.map(semester => semester.description);
            return level;
        }));

        // Populate left side navigation and display program details on the right
        renderLeftNavigation(programLevels);
        renderProgramDetails(programData);
        renderLevelDetails(programLevels);
        renderBanner(programData)


        setTimeout(function () {
            // removeEmptyTags();
            // hideEmptyElements();
            hideElementsWithNbsp()
            $('.programs-loader').addClass('loaded')

            const links = $(".programs-navigator a");

            links.on("click", function (event) {
                event.preventDefault();
                const targetId = $(this).attr("href");
                $(targetId).get(0).scrollIntoView({
                    behavior: "smooth"
                });
        
                // Remove the 'active' class from other links
                links.removeClass("active");
        
                // Add the 'active' class to the clicked link
                $(this).addClass("active");
            });
        },10)

    } catch (error) {
        console.error("Error fetching program data:", error);
    }


function renderLeftNavigation(programLevels) {
    const programNavigator = document.getElementById("programNavigator");
    
    // Reverse the order of program levels
    // const reversedLevels = [...programLevels].reverse();
    const sortedLevels = [...programLevels].sort((a, b) => a.label.localeCompare(b.label));
    
    const navigationHTML = `
        <h5>Description</h5>
        <ul>
            <li>
                Program structure
                <ul>
                    ${sortedLevels.map(level => `<li><a href="#level-${level.id}">${level.label}</a></li>`).join("")}
                </ul>
            </li>
        </ul>
    `;
    programNavigator.innerHTML = navigationHTML;
}

function renderBanner(programData) {
    const renderBanner = document.getElementById("renderBanner");
    
   
    
    const renderBannerHTML = `
    <div
    class="banner"
    style="background-image: url(${programData.model.program_cover})"
  >
    <div class="banner-content wow fadeInUp" data-wow-delay="0.2s">
      <h4>Program</h4>
      <h1 id="programName">${programData.model.program_name}</h1>
    </div>
  </div>
    `;
    renderBanner.innerHTML = renderBannerHTML;
}


function renderProgramDetails(programData) {
   
    // const programNameElement = document.getElementById("programName"); 
    // programNameElement.textContent = programData.model.program_name;

    const programNameElement2 = document.getElementById("programName2"); 
    programNameElement2.textContent = programData.model.program_name;
    const programDetails = document.getElementById("programDetails");
    
    const programOutcomeHTML = programData.model.program_outcome;

    programDetails.innerHTML = `
        <h2>${programData.model.program_name}</h2>
        <div>${programData.model.description}</div>
        <div class="program-outcome">
          <h4>Program outcomes</h4>
          ${programOutcomeHTML}
        </div>
    `;
}

function renderLevelDetails(programLevels) {
    const levelDetails = document.getElementById("levelDetails");
    // const reversedLevels = [...programLevels].reverse();

    const sortedLevels = [...programLevels].sort((a, b) => a.label.localeCompare(b.label));


    
    sortedLevels.forEach(level => {
        // Reverse the order of semesters
        const reversedSemesters = [...level.semesters].reverse();
        
        const levelHTML = `
            <div id="level-${level.id}">
                <h4>${level.label}</h4>
                ${reversedSemesters.map(semester => `<p>${semester}</p>`).join("")}
            </div>
        `;
        levelDetails.innerHTML += levelHTML;
    });
    
}






function hideElementsWithNbsp() {
    const nbspElements = document.querySelectorAll('p, h3, h5, h4, strong');
    nbspElements.forEach(element => {
        if (element.innerHTML.trim() === '&nbsp;') {
            element.style.display = 'none';
        }
    });
}





});