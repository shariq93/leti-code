const getPathway = async function () {
    var myHeaders = new Headers();
    myHeaders.append("x-project", "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg==");
  
    var raw = "";
  
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
  
    try {
      const response = await fetch("https://api.ibte.me/v1/api/public/cms/GETALL", requestOptions);
      const data = await response.json();
  
      const contactPageData = data.list.filter(item => item.page === 'study');
  
      const pathData = {};
      contactPageData.forEach(item => {
        pathData[item.content_key] = item.content_value;
      });
  
      document.getElementById("path_summary").innerText = pathData.path_summary;
      document.getElementById("path_higher_education_desc").innerText = pathData.path_higher_education_desc;
      document.getElementById("path_university_name_desc").innerText = pathData.path_university_name_desc; 
      document.getElementById("pathway_bottom_desc").innerHTML = pathData.pathway_bottom_desc;  
      
    } catch (error) {
      console.log('error', error);
    }
  };
  
  $(window ).on("load", function() {
  getPathway()
  })