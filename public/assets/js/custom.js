
async function fetchAndSaveToken() {
  // Create loader element

  var myHeaders = new Headers();
  myHeaders.append(
    "x-project",
    "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
  );

  var requestOptions = {
    method: "GET",
    headers: myHeaders,
    redirect: "follow",
  };
  try {
    const tokenResponse = await fetch(
      "https://api.ibte.me/v3/api/lambda/anonymous",
      requestOptions
    );
    const tokenData = await tokenResponse.json();
    const token = tokenData.token;

    localStorage.setItem("anonymous_token", "");
    localStorage.setItem("anonymous_token", token);

    sectionSettings()

    console.log("Token successfully fetched and saved to local storage.");
  } catch (error) {
    console.log("Error fetching or saving the token:", error);

    document.body.removeChild(loader);
  }
}

// Call the function
fetchAndSaveToken();

const getMenus = async function () {
  var myHeaders = new Headers();
  myHeaders.append("x-project", "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg==");

  var raw = "";

  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
  };

  try {
    const response = await fetch("https://api.ibte.me/v1/api/public/cms/GETALL", requestOptions);
    const data = await response.json();

    const menuItem = data.list.filter(item => item.page === 'header');

    const contactData = {};
    menuItem.forEach(item => {
      menuItem[item.content_key] = item.content_value;
    });

    document.getElementById("nav_menu1").innerText = menuItem.nav_menu1;
    document.getElementById("nav_menu1_sub1_head").innerText = menuItem.nav_menu1_sub1_head;
    document.getElementById("nav_menu1_sub1_desc").innerText = menuItem.nav_menu1_sub1_desc;
    document.getElementById("nav_menu1_sub2_head").innerText = menuItem.nav_menu1_sub2_head;
    document.getElementById("nav_menu1_sub2_desc").innerText = menuItem.nav_menu1_sub2_desc;
    document.getElementById("nav_menu1_sub3_head").innerText = menuItem.nav_menu1_sub3_head;
    document.getElementById("nav_menu1_sub3_desc").innerText = menuItem.nav_menu1_sub3_desc;
    document.getElementById("nav_menu1_sub4_head").innerText = menuItem.nav_menu1_sub4_head;
    document.getElementById("nav_menu1_sub4_desc").innerText = menuItem.nav_menu1_sub4_desc;
    document.getElementById("nav_menu1_sub5_head").innerText = menuItem.nav_menu1_sub5_head;
    document.getElementById("nav_menu1_sub5_desc").innerText = menuItem.nav_menu1_sub5_desc;
    document.getElementById("nav_menu1_sub6_head").innerText = menuItem.nav_menu1_sub6_head;
    document.getElementById("nav_menu1_sub6_desc").innerText = menuItem.nav_menu1_sub6_desc;
    document.getElementById("nav_menu2").innerText = menuItem.nav_menu2;
    document.getElementById("nav_menu2_sub1_head").innerText = menuItem.nav_menu2_sub1_head;
    document.getElementById("nav_menu2_sub1_desc").innerText = menuItem.nav_menu2_sub1_desc;

    document.getElementById("nav_menu2_sub2_head").innerText = menuItem.nav_menu2_sub2_head;
    document.getElementById("nav_menu2_sub2_desc").innerText = menuItem.nav_menu2_sub2_desc;
    document.getElementById("nav_menu2_sub3_head").innerText = menuItem.nav_menu2_sub3_head;
    document.getElementById("nav_menu2_sub3_desc").innerText = menuItem.nav_menu2_sub3_desc;
    document.getElementById("nav_menu2_sub4_head").innerText = menuItem.nav_menu2_sub4_head;
    document.getElementById("nav_menu2_sub4_desc").innerText = menuItem.nav_menu2_sub4_desc;
     document.getElementById("nav_menu2_sub5_head").innerText = menuItem.nav_menu2_sub5_head;
    document.getElementById("nav_menu2_sub5_desc").innerText = menuItem.nav_menu2_sub5_desc;
    document.getElementById("nav_menu2_sub6_head").innerText = menuItem.nav_menu2_sub6_head;
    document.getElementById("nav_menu2_sub6_desc").innerText = menuItem.nav_menu2_sub6_desc;
    document.getElementById("nav_menu2_sub7_head").innerText = menuItem.nav_menu2_sub7_head;
    document.getElementById("nav_menu2_sub7_desc").innerText = menuItem.nav_menu2_sub7_desc;

    document.getElementById("nav_menu2_sub8_head").innerText = menuItem.nav_menu2_sub8_head;
    document.getElementById("nav_menu2_sub8_desc").innerText = menuItem.nav_menu2_sub8_desc;
    document.getElementById("nav_menu2_sub9_head").innerText = menuItem.nav_menu2_sub9_head;
    document.getElementById("nav_menu2_sub9_desc").innerText = menuItem.nav_menu2_sub9_desc;
    document.getElementById("nav_menu3").innerText = menuItem.nav_menu3;
    document.getElementById("nav_menu4").innerText = menuItem.nav_menu4;
    document.getElementById("nav_menu5").innerText = menuItem.nav_menu5;
    document.getElementById("nav_apply_btn").innerText = menuItem.nav_apply_btn;
    document.getElementById("nav_student_login").innerText = menuItem.nav_student_login;


  // footer



    document.getElementById("nav_menu1_2").innerText = menuItem.nav_menu1;
    document.getElementById("nav_menu1_sub1_head_2").innerText = menuItem.nav_menu1_sub1_head;
    document.getElementById("nav_menu1_sub2_head_2").innerText = menuItem.nav_menu1_sub2_head;
    document.getElementById("nav_menu1_sub3_head_2").innerText = menuItem.nav_menu1_sub3_head;
    document.getElementById("nav_menu1_sub4_head_2").innerText = menuItem.nav_menu1_sub4_head;
    document.getElementById("nav_menu1_sub5_head_2").innerText = menuItem.nav_menu1_sub5_head;
    document.getElementById("nav_menu1_sub6_head_2").innerText = menuItem.nav_menu1_sub6_head;
    document.getElementById("nav_menu2_2").innerText = menuItem.nav_menu2;
    document.getElementById("nav_menu2_sub1_head_2").innerText = menuItem.nav_menu2_sub1_head;
    document.getElementById("nav_menu2_sub2_head_2").innerText = menuItem.nav_menu2_sub2_head;
    document.getElementById("nav_menu2_sub3_head_2").innerText = menuItem.nav_menu2_sub3_head;
    document.getElementById("nav_menu2_sub4_head_2").innerText = menuItem.nav_menu2_sub4_head;
    document.getElementById("nav_menu2_sub5_head_2").innerText = menuItem.nav_menu2_sub5_head;
    document.getElementById("nav_menu2_sub6_head_2").innerText = menuItem.nav_menu2_sub6_head;
    document.getElementById("nav_menu2_sub7_head_2").innerText = menuItem.nav_menu2_sub7_head;
    document.getElementById("nav_menu2_sub8_head_2").innerText = menuItem.nav_menu2_sub8_head;
    document.getElementById("nav_menu2_sub9_head_2").innerText = menuItem.nav_menu2_sub9_head;
    document.getElementById("nav_menu3_2").innerText = menuItem.nav_menu3;
    document.getElementById("nav_menu4_2").innerText = menuItem.nav_menu4;
    document.getElementById("nav_menu5_2").innerText = menuItem.nav_menu5;



  } catch (error) {
    console.log('error', error);
  }
};




const getFooterMenus = async function () {
  var myHeaders = new Headers();
  myHeaders.append("x-project", "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg==");

  var raw = "";

  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
  };

  try {
    const response = await fetch("https://api.ibte.me/v1/api/public/cms/GETALL", requestOptions);
    const data = await response.json();

    const menuItem = data.list.filter(item => item.page === 'footer');

    const contactData = {};
    menuItem.forEach(item => {
      menuItem[item.content_key] = item.content_value;
    });


 
    document.getElementById("foot-logo").innerHTML =`
    <img src='${menuItem.foot_logo}' />
    `
      document.getElementById("footer_tag").innerText = menuItem.footer_tag;
    document.getElementById("foot_title1").innerText = menuItem.foot_title1;
    document.getElementById("foot_title2").innerText = menuItem.foot_title2;
    document.getElementById("foot_menu1").innerText = menuItem.foot_menu1;
    document.getElementById("foot_menu2").innerText = menuItem.foot_menu2;
    document.getElementById("foot_menu3").innerText = menuItem.foot_menu3;
    document.getElementById("foot_menu4").innerText = menuItem.foot_menu4;
    document.getElementById("foot_menu5").innerText = menuItem.foot_menu5;
    document.getElementById("foot_menu6").innerText = menuItem.foot_menu6;
    document.getElementById("foot_menu7").innerText = menuItem.foot_menu7;
    document.getElementById("foot_menu8").innerText = menuItem.foot_menu8;
    document.getElementById("foot_menu9").innerText = menuItem.foot_menu9;
    document.getElementById("foot_menu10").innerText = menuItem.foot_menu10;
    document.getElementById("foot_menu11").innerText = menuItem.foot_menu11;
    document.getElementById("foot_menu12").innerText = menuItem.foot_menu12;
    document.getElementById("foot_menu13").innerText = menuItem.foot_menu13;
    document.getElementById("foot_menu14").innerText = menuItem.foot_menu14;
    document.getElementById("foot_menu15").innerText = menuItem.foot_menu15;
    document.getElementById("foot_menu16").innerText = menuItem.foot_menu16;




  } catch (error) {
    console.log('error', error);
  }
};







// $(window ).on("load", function() {
//   getMenus()
//   getFooterMenus()
// });




const more_option = document.getElementById("menu-btn");
const mega_option = document.querySelector("nav");

more_option.addEventListener("click", (e) => {
  e.preventDefault();
  mega_option.classList.toggle("open");
  more_option.classList.toggle("open");
});

document.addEventListener("click", (e) => {
  if (e.defaultPrevented) return;
  mega_option.classList.remove("open");
  more_option.classList.remove("open");
});

// \\\\\\\\\\

const page_btn = document.getElementById("option_btn");
const page_box = document.querySelector(".option_box"); // Use ".option_box" to select by class name

page_btn.addEventListener("click", (e) => {
  e.preventDefault();
  page_btn.classList.toggle("open");
  page_box.classList.toggle("open");
});

document.addEventListener("click", (e) => {
  if (e.defaultPrevented) return;
  page_btn.classList.remove("open");
  page_box.classList.remove("open");
});







$(window).scroll(function () {
  var scroll = $(window).scrollTop();
  if (scroll >= 200) {
    $(".header").addClass("white-header");
  } else {
    $(".header").removeClass("white-header");
  }
});

var object1 = $("#object1");
var layer = $("#layer");

function handleMouseMove(e) {
  var valueX = (e.pageX * -1) / 120;
  var valueY = (e.pageY * -1) / 120;

  object1.css({
    transform: "translate3d(" + valueX + "px," + valueY + "px,0)",
  });
}

if ($(window).width() > 990) {
  layer.mousemove(handleMouseMove);
}





function sectionSettings() {
  var myHeaders = new Headers();
  myHeaders.append(
    "x-project",
    "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
  );

  var requestOptions = {
    method: "GET",
    headers: myHeaders,
    redirect: "follow",
  };


  const token = localStorage.getItem("anonymous_token");
  var secondApiHeaders = new Headers();
  secondApiHeaders.append(
    "x-project",
    "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
  );
  secondApiHeaders.append("Authorization", `Bearer ${token}`);

  var secondApiOptions = {
    method: "GET",
    headers: secondApiHeaders,
    redirect: "follow",
  };
  fetch(
    "https://api.ibte.me/v4/api/records/render_settings",
    secondApiOptions
  )
    .then((response) => response.json())
    .then((result) => {
      result.list.forEach((setting) => {
        const section = document.getElementById(setting.setting_key);
        if (setting.setting_value === "0") {
          if (section) {
            section.style.display = "none";
          }
        }
      });
      result.list.forEach((setting) => {
        const section = document.getElementById(setting.setting_key);
        if (setting.animate === "1") {
          if (section) {
            section.classList.add('wow', 'fadeInUp');
          }
        }
      });
      getMenus()
      getFooterMenus()
      setTimeout(function () {
        
        $('.page-loader').addClass('loaded')
           wow = new WOW({
             animateClass: "animated",
             offset: 100,
             callback: function (box) {
               //  console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
             },
           });
           wow.init();
         }, 1500);
    })
    .catch((error) => console.log("error", error));
}
