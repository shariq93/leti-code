function createAccordionCard(id, question, answer) {
  const accordion = document.querySelector(".accordion");
  const card = document.createElement("div");
  card.classList.add("acc-card");

  const header = document.createElement("div");
  header.classList.add("acc-card-header");
  header.id = `faqhead${id}`;
  const headerLink = document.createElement("a");
  headerLink.href = "#";
  headerLink.dataset.toggle = "collapse";
  headerLink.dataset.target = `#faq${id}`;
   headerLink.classList.add("collapsed");
  headerLink.setAttribute("aria-expanded", "true");
  headerLink.setAttribute("aria-controls", `faq${id}`);
  headerLink.innerHTML = `${question}
   <img class="acc-minus" src="assets/images/minus.svg" />
    <img class="acc-plus" src="assets/images/plus.svg" />`;
  header.appendChild(headerLink);

  const collapse = document.createElement("div");
  collapse.id = `faq${id}`;
  collapse.classList.add("collapse");
  collapse.setAttribute("aria-labelledby", `faqhead${id}`);
  collapse.dataset.parent = "#faq";
  const body = document.createElement("div");
  body.classList.add("acc-card-body");
  body.innerHTML = answer;
  collapse.appendChild(body);

  card.appendChild(header);
  card.appendChild(collapse);

  accordion.appendChild(card);
}

window.onload = async function () {
  try {
    const token = localStorage.getItem("anonymous_token");
    const cmsHeaders = new Headers();
    cmsHeaders.append("x-project", "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg==");
    cmsHeaders.append("Authorization", `Bearer ${token}`);

    const cmsOptions = {
      method: "GET",
      headers: cmsHeaders,
      redirect: "follow",
    };

    const cmsResponse = await fetch("https://api.ibte.me/v4/api/records/cms_faq", cmsOptions);
    const data = await cmsResponse.json();
    const invertedList = data.list.reverse().slice(0, 4);

    const modalBody = document.querySelector(".accordion");
    modalBody.innerHTML = "";

    invertedList.forEach((item, index) => { 
      createAccordionCard(item.id, item.question, item.answer);
    });
  } catch (error) {
    console.log("error", error);
  }
};

