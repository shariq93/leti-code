document.addEventListener("DOMContentLoaded", function () {
  function renderFAQs(category, faqs) {
    const accordion = document.createElement("div");
    accordion.classList.add("accordion");

    faqs.forEach((faq) => {
      if (faq.category === category) {
        const id = faq.id;
        const question = faq.question;
        const answer = faq.answer;

        const card = document.createElement("div");
        card.classList.add("acc-card");

        const header = document.createElement("div");
        header.classList.add("acc-card-header");
        header.id = `faqhead${id}`;
        const headerLink = document.createElement("a");
        headerLink.href = "javascript:void(0)";
        headerLink.addEventListener("click", () => toggleAccordion(id));
        headerLink.innerHTML = `${question}
            <img class="acc-minus" src="assets/images/minus.svg" />
            <img class="acc-plus" src="assets/images/plus.svg" />`;
        header.appendChild(headerLink);

        const collapse = document.createElement("div");
        collapse.id = `faq${id}`;
        collapse.classList.add("collapse");
        const body = document.createElement("div");
        body.classList.add("acc-card-body");
        body.innerHTML = answer;
        collapse.appendChild(body);

        card.appendChild(header);
        card.appendChild(collapse);

        accordion.appendChild(card);
      }
    });

    const faqHolder = document.getElementById(category);
    faqHolder.appendChild(accordion);
  }

  function setModalHeader(sectionName) {
    const modalHeader = document.querySelector("#faq-modal .modal-header h4");
    modalHeader.textContent = sectionName;
  }

  function handleLoadMore(category, sectionName) {
    setModalHeader(sectionName);
    const categoryContainer = document.getElementById(category);
    const faqs = categoryContainer.querySelectorAll(".acc-card");
    const modalBody = document.querySelector("#faq-modal .modal-body");
    modalBody.innerHTML = "";

    faqs.forEach((faq) => {
      modalBody.appendChild(faq.cloneNode(true));
    });

    $("#faq-modal").modal("show");
  }

  const loadMoreButtons = document.querySelectorAll(".load-more-btn");
  loadMoreButtons.forEach((button) => {
    const category = button.getAttribute("data-category");
    const sectionName = button.getAttribute("data-sec"); 
    button.addEventListener("click", () => handleLoadMore(category, sectionName));
  });
  

  function toggleAccordion(id) {
    const collapse = document.getElementById(`faq${id}`);
    const isCollapsed = collapse.classList.contains("show");
    if (collapse.style.display === "block") {
      collapse.style.display = "none";
      collapse.classList.remove("show");
    } else {
      collapse.style.display = "block";
      collapse.classList.add("show");
    }
    const header = document.getElementById(`faqhead${id}`);
    const headerLink = header.querySelector(".acc-card-header a");
  }

  function updateLoadMoreButtonCount(category, count) {
    const loadMoreButton = document.querySelector(`.load-more-btn[data-category="${category}"]`);
    if (loadMoreButton) {
      loadMoreButton.textContent = `View all ${count} questions`;
    }
  }

  function getFaq() {
    const myHeaders = new Headers();
    myHeaders.append(
      "x-project",
      "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
    );

    const requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };

    const token = localStorage.getItem("anonymous_token");
    const secondApiHeaders = new Headers();
    secondApiHeaders.append(
      "x-project",
      "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
    );
    secondApiHeaders.append("Authorization", `Bearer ${token}`);

    const secondApiOptions = {
      method: "GET",
      headers: secondApiHeaders,
      redirect: "follow",
    };

    fetch("https://api.ibte.me/v4/api/records/cms_faq", secondApiOptions)
      .then((response) => response.json())
      .then((result) => {
        const faqs = result.list;

        const categories = [
          "General Info", "Students Info", "IBTI Portal", "Programs",
          "Admissions", "Fees", "Career and Internship", "Transfer",
          "Online", "Student Life", "International campuses", "partnership"
        ];

        categories.forEach((category) => {
          const filteredFaqs = faqs.filter((faq) => faq.category === category);
          renderFAQs(category, filteredFaqs);
          updateLoadMoreButtonCount(category, filteredFaqs.length);
        });

        renderFAQs("General Info", faqs);
        renderFAQs("Students Info", faqs);
        renderFAQs("IBTI Portal	", faqs);
        renderFAQs("Programs", faqs);
        renderFAQs("Admissions", faqs);
        renderFAQs("Fees", faqs);
        renderFAQs("Career and Internship", faqs);
        renderFAQs("Transfer", faqs);
        renderFAQs("Online", faqs);
        renderFAQs("Student Life", faqs);
        renderFAQs("International campuses", faqs);
        renderFAQs("partnership", faqs);
      })
      .catch((error) => console.log("error", error));
  }

  $(window).on("load", function () {
    getFaq();
  });
});
