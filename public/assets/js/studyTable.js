function studyTableInfo() {
  var myHeaders = new Headers();
  myHeaders.append(
    "x-project",
    "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
  );

  var requestOptions = {
    method: "GET",
    headers: myHeaders,
    redirect: "follow",
  };

  const token = localStorage.getItem("anonymous_token");

  var secondApiHeaders = new Headers();
  secondApiHeaders.append(
    "x-project",
    "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
  );
  secondApiHeaders.append("Authorization", `Bearer ${token}`);

  var secondApiOptions = {
    method: "GET",
    headers: secondApiHeaders,
    redirect: "follow",
  };
  fetch(
    "https://api.ibte.me/v4/api/records/cms_features",
    secondApiOptions
  )
    .then((response) => response.json())
    .then((result) => {
      const contentMap = {};
      result.list.forEach((item) => {
        contentMap[item.content_key] = item.content_value;
      });

      const tableBody = document.getElementById("tableBody");
      tableBody.innerHTML = "";
      const rows = [
        ["tr1td1", "tr1td2", "tr1td3"],
        ["tr2td1", "tr2td2", "tr2td3"],
        ["tr3td1", "tr3td2", "tr3td3"],
        ["tr4td1", "tr4td2", "tr4td3"],
        ["tr5td1", "tr5td2", "tr5td3"],
        ["tr6td1", "tr6td2", "tr6td3"],
        ["tr7td1", "tr7td2", "tr7td3"],
        ["tr8td1", "tr8td2", "tr8td3"],
        ["tr9td1", "tr9td2", "tr9td3"],
        ["tr10td1", "tr10td2", "tr10td3"],
        ["tr11td1", "tr11td2", "tr11td3"],
        ["tr12td1", "tr12td2", "tr12td3"],
        ["tr13td1", "tr13td2", "tr13td3"],
        ["tr14td1", "tr14td2", "tr14td3"],
        ["tr15td1", "tr15td2", "tr15td3"],
        ["tr16td1", "tr16td2", "tr16td3"],
        ["tr17td1", "tr17td2", "tr17td3"],
      ];
      rows.forEach((row) => {
        const tr = document.createElement("tr");
        row.forEach((contentKey) => {
          const td = document.createElement("td");
          if (contentMap[contentKey].includes("span")) {
            td.innerHTML = contentMap[contentKey];
          } else {
            td.textContent = contentMap[contentKey];
          }
          tr.appendChild(td);
        });
        tableBody.appendChild(tr);
      });

      const th1 = document.getElementById("globally_tr_th1");
      th1.innerHTML = `${contentMap["tr1th1"]}`;

      const th2 = document.getElementById("globally_tr_th2");
      th2.innerHTML = `${contentMap["tr1th2"]}`;
    })
    .catch((error) => console.log("error", error));
}

$(window).on("load", function () {
  studyTableInfo();
});

studyTableInfo();
