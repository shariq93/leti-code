const ColabLogo = async function () {
    var myHeaders = new Headers();
    myHeaders.append("x-project", "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg==");
  
    var raw = "";
  
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
  
    try {
      const response = await fetch("https://api.ibte.me/v1/api/public/cms/GETALL", requestOptions);
      const data = await response.json();
  
      const indexPageData = data.list.filter(item => item.page === 'index');
  
      const colabData = {};
      indexPageData.forEach(item => {
        colabData[item.content_key] = item.content_value;
      });
  
      document.getElementById("colab_title").innerText = colabData.colab_title;
      document.getElementById("colab_logo1").setAttribute("src", `${colabData.colab_logo1}`);
      document.getElementById("colab_logo2").setAttribute("src", `${colabData.colab_logo2}`);
      document.getElementById("colab_logo3").setAttribute("src", `${colabData.colab_logo3}`);
      document.getElementById("colab_logo4").setAttribute("src", `${colabData.colab_logo4}`);
      document.getElementById("colab_logo5").setAttribute("src", `${colabData.colab_logo5}`);
      document.getElementById("colab_logo6").setAttribute("src", `${colabData.colab_logo6}`);
      document.getElementById("colab_logo7").setAttribute("src", `${colabData.colab_logo7}`);
      document.getElementById("colab_logo8").setAttribute("src", `${colabData.colab_logo8}`);

    } catch (error) {
      console.log('error', error);
    }
  };
  

  ColabLogo()