function allFaculty() {
  var myHeaders = new Headers();
  myHeaders.append(
    "x-project",
    "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
  );

  var requestOptions = {
    method: "GET",
    headers: myHeaders,
    redirect: "follow",
  };

  const token = localStorage.getItem("anonymous_token");
  const projectId = "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg==";

  var secondApiHeaders = new Headers();
  secondApiHeaders.append("x-project", projectId);
  secondApiHeaders.append("Authorization", `Bearer ${token}`);

  var secondApiOptions = {
    method: "GET",
    headers: secondApiHeaders,
    redirect: "follow",
  };

  fetch("https://api.ibte.me/v4/api/records/cms_team", secondApiOptions)
    .then((response) => response.json())
    .then((result) => {
      // Render the team members
      renderTeamMembers(result.list, token);
    })
    .catch((error) => console.log("error", error));
}

function renderTeamMembers(teamMembers, token) {
  // Separate team members based on designation
  const chairmanMembers = teamMembers.filter(
    (member) => member.designation === "chairman"
  );
  const leadershipMembers = teamMembers.filter(
    (member) => member.designation === "leadership"
  );
  const facultyMembers = teamMembers.filter(
    (member) => member.designation === "faculty"
  );

  // Sort each array based on the order_ parameter
  chairmanMembers.sort((a, b) => a.order_ - b.order_);
  leadershipMembers.sort((a, b) => a.order_ - b.order_);
  facultyMembers.sort((a, b) => a.order_ - b.order_);

  // Render chairman section
  renderSection(chairmanMembers, "chairman");

  // Render leadership section
  renderSection(leadershipMembers, "leadership");

  // Render faculty section
  renderFacultySection(facultyMembers, token);
}

function renderSection(sectionMembers, sectionName) {
  const teamCardHolder = document.querySelector(
    `.${sectionName}-card-holder .row`
  );
  const memberCardTemplate = `
      <div class="row faculty-card align-items-center mb-4">
              <div class="col-md-3 ">
              <img src="{photo}" alt="" />
              </div>
              <div class="col-md-9">
              <h4>{first_name}</h4>
              <h6>{specialty}</h6>
                <h6>{summary}</h6>
                <div class="faculty-parahs">
                {description}
                </div>
                <a class="read-more-btn" data-toggle="modal" data-target="#faculty-modal" data-member-id="{id}" href="#">Read More</a> 
              </div>
            </div>

  `;

  let membersHTML = "";

  sectionMembers.forEach((member) => {
    const memberHTML = memberCardTemplate
      .replace("{photo}", member.photo || "assets/images/faculty1.jpg")
      .replace("{first_name}", member.first_name)
      .replace("{specialty}", member.specialty)
      .replace("{summary}", member.summary)
      .replace("{description}", member.description)
      .replace("{id}", member.id);

    membersHTML += memberHTML;
  });

  teamCardHolder.innerHTML = membersHTML;

  const readMoreButtons = document.querySelectorAll(".read-more-btn");
  readMoreButtons.forEach((button) => {
    // Remove any existing click event listeners
    button.removeEventListener("click", handleReadMoreClick);

    // Add a new click event listener
    button.addEventListener("click", handleReadMoreClick);
  });
}

function renderFacultySection(facultyMembers, token) {
  const teamCardHolder = document.querySelector(".faculty-card-holder .row");

  let membersHTML = "";

  facultyMembers.forEach((member) => {
    const memberHTML = `
          <div class="col-md-3 faculty-card mt-2 mb-2">
              <img src="${
                member.photo || "assets/images/faculty1.jpg"
              }" alt="" />
              <h4>${member.first_name}</h4>
              <h6>${member.specialty}</h6>
              ${member.description}
              <a class="read-more-btn" data-toggle="modal" data-target="#faculty-modal" data-member-id="${
                member.id
              }" href="#">Read More</a>
          </div>
      `;

    membersHTML += memberHTML;
  });

  teamCardHolder.innerHTML = membersHTML;

  const readMoreButtons = document.querySelectorAll(".read-more-btn");
  readMoreButtons.forEach((button) => {
    // Remove any existing click event listeners
    button.removeEventListener("click", handleReadMoreClick);

    // Add a new click event listener
    button.addEventListener("click", handleReadMoreClick);
  });
}

function handleReadMoreClick(event, token) {
  const memberId = event.currentTarget.getAttribute("data-member-id");
  loadMemberDataAndShowModal(memberId, token);
}

function loadMemberDataAndShowModal(memberId, token) {
  let token2 = localStorage.getItem("anonymous_token");

  const modalContent = document.querySelector(".faculty-modal");
  const modalHeader = document.querySelector(".modal-header h4");

  // Create new headers for the individual member API call, including the token and project ID
  var memberApiHeaders = new Headers();
  memberApiHeaders.append(
    "x-project",
    "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
  ); // Replace with your actual project ID
  memberApiHeaders.append("Authorization", `Bearer ${token2}`);

  var memberApiOptions = {
    method: "GET",
    headers: memberApiHeaders,
    redirect: "follow",
  };

  const apiEndpoint = `https://api.ibte.me/v4/api/records/cms_team/${memberId}`;
  console.log(apiEndpoint);
  fetch(apiEndpoint, memberApiOptions)
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      modalHeader.innerHTML = data.model.first_name;
      modalContent.innerHTML = `
              <div class="row faculty-card  mb-4">
                  <div class="col-md-4 col-sm-4 mb-2 mt-2">
                      <img src="${
                        data.model.photo || "assets/images/facalty1.jpg"
                      }" alt="" />
                  </div>
                  <div class="col-md-8 col-sm-8 mb-2 mt-2">
                      <h4>${data.model.first_name}</h4>
                      <h6>${data.model.specialty}</h6>
                      <h6>${data.model.summary ? data.model.summary : ""}</h6>
                      <ul class="flex items-center">
                          ${
                            data.model.twitter
                              ? `
                                  <li>
                                      <a href="${data.model.twitter}">
                                          <img src="assets/images/foot-icon14.svg" />
                                      </a>
                                  </li>`
                              : ""
                          }
                          ${
                            data.model.linkedin
                              ? `
                                  <li>
                                      <a href="${data.model.linkedin}">
                                          <img src="assets/images/foot-icon2.svg" />
                                      </a>
                                  </li>`
                              : ""
                          }
                          ${
                            data.model.facebook
                              ? `
                                  <li>
                                      <a href="${data.model.facebook}">
                                          <img src="assets/images/foot-icon1.svg" />
                                      </a>
                                  </li>`
                              : ""
                          }
                          ${
                            data.model.instagram
                              ? `
                                  <li>
                                      <a href="${data.model.instagram}">
                                          <img src="assets/images/foot-icon3.svg" />
                                      </a>
                                  </li>`
                              : ""
                          }
                      </ul>
                  </div>
                  <div class="col-12 mt-4">
                      ${data.model.description}
                  </div>
              </div>
          `;

      // Display the modal after updating the content
      $("#faculty-modal").modal("show");
    })
    .catch((error) => console.error("Error fetching member data:", error));
}

// Initialize the faculty data retrieval on window load
window.addEventListener("load", function () {
  allFaculty();
  $("#faculty-modal").on("hidden.bs.modal", function () {
    const modalContent = document.querySelector(".faculty-modal");
    const modalHeader = document.querySelector(".modal-header h4");
    modalHeader.innerHTML = "";
    modalContent.innerHTML = `
          <div class="faculty-loader">
              <div class="dot-holder">
                  <div class="dot"></div>
                  <div class="dot"></div>
                  <div class="dot"></div>
              </div>
          </div>
      `;
  });
});

// old code

// function allFaculty() {
//   var myHeaders = new Headers();
//   myHeaders.append(
//     "x-project",
//     "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
//   );

//   var requestOptions = {
//     method: "GET",
//     headers: myHeaders,
//     redirect: "follow",
//   };

//   const token = localStorage.getItem("anonymous_token");
//   const projectId = "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg==";

//   var secondApiHeaders = new Headers();
//   secondApiHeaders.append("x-project", projectId);
//   secondApiHeaders.append("Authorization", `Bearer ${token}`);

//   var secondApiOptions = {
//     method: "GET",
//     headers: secondApiHeaders,
//     redirect: "follow",
//   };

//   fetch("https://api.ibte.me/v4/api/records/cms_team", secondApiOptions)
//     .then((response) => response.json())
//     .then((result) => {
//       // Render the team members
//       renderTeamMembers(result.list, token);
//     })
//     .catch((error) => console.log("error", error));
// }

// function renderTeamMembers(teamMembers, token) {
//   const teamCardHolder = document.querySelector(".team-card-holder .row");

//   const memberCardTemplate = `
//     <div class="col-md-3 faculty-card mt-2 mb-2">
//       <img src="{photo}" alt="" />
//       <h4>{first_name}</h4>
//       <h6>{specialty}</h6>
//       {description}
//       <a class="read-more-btn" data-toggle="modal" data-target="#faculty-modal" data-member-id="{id}" href="#">Read More</a>
//     </div>
//   `;

//   let membersHTML = "";

//   const yazanIndex = teamMembers.findIndex(
//     (member) => member.first_name === "Fatima Al-Joker"
//   );

//   if (yazanIndex !== -1) {
//     const yazanData = teamMembers.splice(yazanIndex, 1)[0];
//     const yazanHTML = `
//       <div class="row faculty-card align-items-center mb-4">
//         <div class="col-md-3 ">
//           <img src="${
//             yazanData.photo || "assets/images/faculty1.jpg"
//           }" alt="" />
//         </div>
//         <div class="col-md-9">
//           <h4>${yazanData.first_name}</h4>
//           <h6>${yazanData.specialty}</h6>
//           <h6>${yazanData.summary ? yazanData.summary : ""}</h6>
//           <div class="faculty-parahs">
//           ${yazanData.description}
//           </div>
//           <a class="read-more-btn" data-toggle="modal" data-target="#faculty-modal" data-member-id="${
//             yazanData.id
//           }" href="#">Read More</a>
//         </div>
//       </div>
//     `;
//     membersHTML += yazanHTML;
//   }

//   // Now add the rest of the team members
//   teamMembers.forEach((member) => {
//     const memberHTML = memberCardTemplate
//       .replace("{photo}", member.photo || "assets/images/faculty1.jpg")
//       .replace("{first_name}", member.first_name)
//       .replace("{specialty}", member.specialty)
//       .replace("{description}", member.description)
//       .replace("{id}", member.id);

//     membersHTML += memberHTML;
//   });

//   teamCardHolder.innerHTML = membersHTML;

//   const readMoreButtons = document.querySelectorAll(".read-more-btn");
//   readMoreButtons.forEach((button) => {
//     button.addEventListener("click", (event) => {
//       const memberId = event.currentTarget.getAttribute("data-member-id");
//       loadMemberDataAndShowModal(memberId, token);
//     });
//   });
// }

// function loadMemberDataAndShowModal(memberId, token) {
//   const modalContent = document.querySelector(".faculty-modal");
//   const modalHeader = document.querySelector(".modal-header h4");

//   // Create new headers for the individual member API call, including the token and project ID
//   var memberApiHeaders = new Headers();
//   memberApiHeaders.append(
//     "x-project",
//     "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
//   ); // Replace with your actual project ID
//   memberApiHeaders.append("Authorization", `Bearer ${token}`);

//   var memberApiOptions = {
//     method: "GET",
//     headers: memberApiHeaders,
//     redirect: "follow",
//   };

//   const apiEndpoint = `https://api.ibte.me/v4/api/records/cms_team/${memberId}`;
//   fetch(apiEndpoint, memberApiOptions)
//     .then((response) => response.json())
//     .then((data) => {
//       modalHeader.innerHTML = data.model.first_name
//       modalContent.innerHTML = `
//         <div class="row faculty-card  mb-4">
//           <div class="col-md-4 col-sm-4 mb-2 mt-2">
//             <img src="${
//               data.model.photo || "assets/images/facalty1.jpg"
//             }" alt="" />
//           </div>
//           <div class="col-md-8 col-sm-8 mb-2 mt-2">
//             <h4>${data.model.first_name}</h4>
//             <h6>${data.model.specialty}</h6>
//             <h6>${data.model.summary ? data.model.summary : ""}</h6>
//             <ul class="flex items-center">
//   ${
//     data.model.twitter
//       ? `
//     <li>
//       <a href="${data.model.twitter}">
//         <img src="assets/images/foot-icon14.svg" />
//       </a>
//     </li>`
//       : ""
//   }
//   ${
//     data.model.linkedin
//       ? `
//     <li>
//       <a href="${data.model.linkedin}">
//         <img src="assets/images/foot-icon2.svg" />
//       </a>
//     </li>`
//       : ""
//   }
//   ${
//     data.model.facebook
//       ? `
//     <li>
//       <a href="${data.model.facebook}">
//         <img src="assets/images/foot-icon1.svg" />
//       </a>
//     </li>`
//       : ""
//   }
//   ${
//     data.model.instagram
//       ? `
//     <li>
//       <a href="${data.model.instagram}">
//         <img src="assets/images/foot-icon3.svg" />
//       </a>
//     </li>`
//       : ""
//   }
// </ul>
//           </div>
//           <div class="col-12 mt-4">
//             ${data.model.description}
//           </div>
//         </div>
//       `;

//       // Display the modal after updating the content
//       $("#faculty-modal").modal("show");
//     })
//     .catch((error) => console.error("Error fetching member data:", error));
// }

// $(window).on("load", function () {
//   allFaculty();
//   $("#faculty-modal").on("hidden.bs.modal", function () {
//     const modalContent = document.querySelector(".faculty-modal");
//     const modalHeader = document.querySelector(".modal-header h4");
//     modalHeader.innerHTML = ""
//     modalContent.innerHTML = `
//             <div class="faculty-loader">
//               <div class="dot-holder">
//                 <div class="dot"></div>
//                 <div class="dot"></div>
//                 <div class="dot"></div>
//               </div>
//             </div>
//     `;
//   });
// });
