const fetchAndDisplayImages = async (contentKey, containerSelector) => {
    var myHeaders = new Headers();
    myHeaders.append(
      "x-project",
      "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
    );
  
    var raw = "";
  
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
  
    try {
      const response = await fetch(
        "https://api.ibte.me/v1/api/public/cms/GETALL",
        requestOptions
      );
      const data = await response.json();
  
      // Find campus data with the specified content key
      const campusData = data.list.find(
        (item) =>
          item.page === "student-path" && item.content_key === contentKey
      );
  
      if (campusData && campusData.content_type === "image-list") {
        const imageSliderArray = JSON.parse(campusData.content_value);
        const pathLogoBox = document.querySelector(containerSelector);
        let campusImageHtml = "";
  
        imageSliderArray.forEach((imageObj, index) => {
          if (imageObj.value) {
            // Check if the image URL is not null
            const campusImageTemplate = `
              <img class="path-logo" src="${imageObj.value}" alt="${index + 1}" />
            `;
            campusImageHtml += campusImageTemplate;
          }
        });
  
        if (campusImageHtml) {
          pathLogoBox.innerHTML = campusImageHtml;
        } else {
         
        }
      } else {
    
      }
    } catch (error) {
      console.error("Error:", error);

    }
  };
  
  // Call the function to load campus images for different content keys
  document.addEventListener("DOMContentLoaded", function () {
    fetchAndDisplayImages("card1_top_logos", ".card1_top_logos");
    fetchAndDisplayImages("card1_bottom_logos", ".card1_bottom_logos");
    fetchAndDisplayImages("card2_top_logos", ".card2_top_logos");
    fetchAndDisplayImages("card2_bottom_logos", ".card2_bottom_logos");
    fetchAndDisplayImages("card3_top_logos", ".card3_top_logos");
    fetchAndDisplayImages("card3_bottom_logos", ".card3_bottom_logos");
  });
  