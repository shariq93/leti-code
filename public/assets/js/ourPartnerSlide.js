function allPartnerSlide() {
  var myHeaders = new Headers();
  myHeaders.append(
    "x-project",
    "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
  );

  var requestOptions = {
    method: "GET",
    headers: myHeaders,
    redirect: "follow",
  };

  const token = localStorage.getItem("anonymous_token");
  const projectId = "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg==";

  var secondApiHeaders = new Headers();
  secondApiHeaders.append("x-project", projectId);
  secondApiHeaders.append("Authorization", `Bearer ${token}`);

  var secondApiOptions = {
    method: "GET",
    headers: secondApiHeaders,
    redirect: "follow",
  };

  fetch(
    "https://api.ibte.me/v4/api/records/cms_campus_detail",
    secondApiOptions
  )
    .then((response) => response.json())
    .then((result) => {
      // Render the team members
      renderTeamMembers(result.list, token);
      renderSLider();
    })
    .catch((error) => console.log("error", error));
}

function renderTeamMembers(teamMembers, token) {
  const teamCardHolder = document.querySelector(
    ".qualification-partner-slide .swiper-wrapper"
  );

  const memberCardTemplate = `
  <div class="swiper-slide">
  <div class="qualification-info-box pb-0">
    <div class="row">
      <div class="col-md-9 col-sm-12 col-12 mt-2 mb-2">
        <div class="sec-title text-left m-0" data-wow-delay="0.2s">
          <h5>{tagline}</h5>
          <h2>{title}</h2>
        </div>
      </div>
      <div class="col-md-3 col-sm-12 col-12 mt-2 mb-2">
        <img
          class="partner-logo"
          src="{logo}"
          alt=""
        />
      </div>
      <div class="col-md-8 mt-2 mb-2">
        <p>
         {description1}
        </p>
      </div>
      <div class="col-md-6 mt-2 mb-2">
        <p>
        {description2}
        </p>
      </div>
      <div class="col-md-6 mt-2 mb-2">
        <p>
        {description3}
        </p>
      </div>
    </div>
  </div>
</div>
    `;

  let membersHTML = "";

  teamMembers.forEach((member) => {
    const memberHTML = memberCardTemplate
      .replace("{tagline}", member.tagline)
      .replace("{title}", member.title)
      .replace("{logo}", member.logo)
      .replace("{description1}", member.description1)
      .replace("{description2}", member.description2)
      .replace("{description3}", member.description3);

    membersHTML += memberHTML;
  });

  teamCardHolder.innerHTML = membersHTML;
}

$(window).on("load", function () {
  allPartnerSlide();
});

function renderSLider() {
  var swiper = new Swiper(".qualification-partner-slide", {
    slidesPerView: 1,
    loop: false,
    spaceBetween: 10,

    navigation: {
      nextEl: ".partner-slide-left",
      prevEl: ".partner-slide-right",
    },
  });

  hideElementsWithNbsp()
}



function hideElementsWithNbsp() {
  const nbspElements = document.querySelectorAll('p');

  nbspElements.forEach(element => {
      if (element.innerHTML.trim() === '') {
          element.style.display = 'none';
      }
  });
}