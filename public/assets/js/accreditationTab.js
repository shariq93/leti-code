function accreditaionTab() {
  var myHeaders = new Headers();
  myHeaders.append(
    "x-project",
    "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
  );

  var requestOptions = {
    method: "GET",
    headers: myHeaders,
    redirect: "follow",
  };

  const token = localStorage.getItem("anonymous_token");
  const projectId = "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg==";

  var secondApiHeaders = new Headers();
  secondApiHeaders.append("x-project", projectId);
  secondApiHeaders.append("Authorization", `Bearer ${token}`);

  var secondApiOptions = {
    method: "GET",
    headers: secondApiHeaders,
    redirect: "follow",
  };

  fetch(
    "https://api.ibte.me/v4/api/records/cms_campus_detail2",
    secondApiOptions
  )
    .then((response) => response.json())
    .then((result) => {
      // Create dynamic tabs and content
      createTabsAndContent(result.list);
      const tabButtons = document.querySelectorAll(".tablinks");
      tabButtons.forEach((button) => {
        button.addEventListener("click", () => {
          // Remove the "active" class from all tab buttons
          tabButtons.forEach((btn) => btn.classList.remove("active"));

          // Add the "active" class to the clicked tab button
          button.classList.add("active");
        });
      });

      // Trigger a click event on the first tab button to show it by default
      const firstTabButton = document.querySelector(".tablinks");
      firstTabButton.click();
    })
    .catch((error) => console.log("error", error));
}

function createTabsAndContent(data) {
  const tabButtonsContainer = document.getElementById("tabButtons");
  const tabContentContainer = document.getElementById("tabContent");


  data = data.reverse();

  data.forEach((item, index) => {


    if (item.status === 0) {
      // If status is 0, don't create the tab button and content
      return;
    }

    const tabButton = document.createElement("button");
    tabButton.className = "tablinks";
    tabButton.textContent = item.title;
    tabButton.addEventListener("click", () => openTab(event, `tab_${index}`));
    tabButtonsContainer.appendChild(tabButton);

    const tabContent = document.createElement("div");
    tabContent.id = `tab_${index}`;
    tabContent.className = "accreditation_tabcontent";
    tabContent.innerHTML = `
  <div class="accreditation-tab-content-holder">
    <div class="row">
      ${item.description1 ? `<div class="${item.logo == '[object FileList]' ? "col-md-12": "col-md-6"} col-md-6 mt-4 mb-4"><p>${item.description1}</p></div>` : ''}
      ${item.logo == '[object FileList]' ? '' : `<div class="col-md-6 mt-4 mb-4"><img src="${item.logo}" /></div>`}
      ${item.description2 ? `<div class="col-md-6 mt-4 mb-4"><p>${item.description2}</p></div>` : ''}
      ${item.description3 ? `<div class="col-md-6 mt-4 mb-4"><p>${item.description3}</p></div>` : ''}
    </div>
  </div>
`;
    tabContentContainer.appendChild(tabContent);
  });
}

// Call accreditaionTab function on window load
window.addEventListener("load", function () {
  accreditaionTab();
});

function openTab(event, tabId) {
  const tabContent = document.getElementById(tabId);

  if (tabContent.classList.contains("active")) {
    // If the tab is already active, remove the active class to hide it
    tabContent.classList.remove("active");
  } else {
    // Remove the active class from all tab content elements
    const tabContents = document.querySelectorAll(".accreditation_tabcontent");
    tabContents.forEach((content) => {
      content.classList.remove("active");
    });

    // Add the active class to the selected tab content to show it
    tabContent.classList.add("active");
  }
}

