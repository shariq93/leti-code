function fetchTestimonials() {
    var myHeaders = new Headers();
    myHeaders.append(
      "x-project",
      "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
    );

    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };

    const token = localStorage.getItem("anonymous_token");;

    var secondApiHeaders = new Headers();
    secondApiHeaders.append(
      "x-project",
      "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
    );
    secondApiHeaders.append("Authorization", `Bearer ${token}`);

    var secondApiOptions = {
      method: "GET",
      headers: secondApiHeaders,
      redirect: "follow",
    };

    fetch(
      "https://api.ibte.me/v4/api/records/cms_testimonials",
      secondApiOptions
    )
      .then((response) => response.json())
      .then((result) => {
        const testimonialsData1 = result.list.map((testimonial) => ({
          review: testimonial.review,
          user_first_name: testimonial.user_first_name,
          user_last_name: testimonial.user_last_name,
          photo: testimonial.photo,
          user_name_label: testimonial.user_name_label,
          rate: testimonial.rate,
        }));


        displayTestimonials(testimonialsData1, "testimonialsContainer");
      })
      .catch((error) => console.log("error", error));

    fetch(
      "https://api.ibte.me/v4/api/records/cms_testimonials",
      secondApiOptions
    )
      .then((response) => response.json())
      .then((result) => {
        const testimonialsData2 = result.list.map((testimonial) => ({
          review: testimonial.review,
          user_first_name: testimonial.user_first_name,
          user_last_name: testimonial.user_last_name,
          photo: testimonial.photo,
          user_name_label: testimonial.user_name_label,
          rate: testimonial.rate,
        }));


        displayTestimonials(testimonialsData2, "testimonialsContainer2");
      })
      .catch((error) => console.log("error", error));

   
  }

 
  function displayTestimonials(testimonials, containerId) {
    const testimonialsContainer = document.getElementById(containerId);
  

    const khalidTestimonial = testimonials.find(
      (testimonial) => testimonial.user_first_name === "Khalid Al-Mansouri, United Arab Emirates" 
    );
  
    if (khalidTestimonial) {
      // Render the Khalid testimonial first
      renderTestimonial(khalidTestimonial, testimonialsContainer, "col-md-4", "mt-3", "mb-3");
  
      // Remove the Khalid testimonial from the list
      testimonials = testimonials.filter(
        (testimonial) => testimonial !== khalidTestimonial
      );
    }
  
    // Render the rest of the testimonials
    testimonials.forEach((testimonial) => {
      renderTestimonial(testimonial, testimonialsContainer, "col-md-4", "mt-3", "mb-3");
    });
  }
  
  // Function to render a single testimonial
  function renderTestimonial(testimonial, container, ...classNames) {
    const { review, user_first_name, user_last_name, photo, user_name_label, rate } = testimonial;
  
    // Create a new div to wrap the testimonial box with appropriate classes
    const testimonialWrapper = document.createElement("div");
    testimonialWrapper.classList.add(...classNames);
  
    const testimonialBox = document.createElement("div");
    testimonialBox.classList.add("testi-box");
  
    const ratingImg = document.createElement("img");
    ratingImg.classList.add("rating-img");
    ratingImg.src = "assets/images/rating.svg";
    ratingImg.alt = "Rating";
  
    const reviewParagraph = document.createElement("p");
    reviewParagraph.innerHTML = review;
  
    const userImage = document.createElement("img");
    userImage.src = user_name_label;
  
    const userNameParagraph = document.createElement("p");
    userNameParagraph.textContent = `${user_first_name}`;
  
    // const userRoleParagraph = document.createElement("p");
    // userRoleParagraph.textContent = "Students (Cybersecurity)";
  
    const userImageContainer = document.createElement("li");
    userImageContainer.appendChild(userImage);

   const userImageLabel = document.createElement("img");
    userImageLabel.src = user_name_label; // Assuming user_name_label is a URL

    const userImageLabelContainer = document.createElement("span");
    userImageLabelContainer.appendChild(userImageLabel);
  
    const userNameContainer = document.createElement("li");
    userNameContainer.appendChild(userNameParagraph);
    userNameContainer.appendChild(userImageLabelContainer);
    // userNameContainer.appendChild(userRoleParagraph);
  
    const userList = document.createElement("ul");
    userList.appendChild(userImageContainer);
    userList.appendChild(userNameContainer);
  
    testimonialBox.appendChild(ratingImg);
    testimonialBox.appendChild(reviewParagraph);
    testimonialBox.appendChild(userList);
  
    // Append the testimonial box to the wrapper and then append the wrapper to the container
    testimonialWrapper.appendChild(testimonialBox);
    container.appendChild(testimonialWrapper);
  }
  fetchTestimonials();