function getNewsItems() {
    var myHeaders = new Headers();
    myHeaders.append(
      "x-project",
      "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
    );
  
    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };
  
    const token = localStorage.getItem("anonymous_token");
    var secondApiHeaders = new Headers();
    secondApiHeaders.append(
      "x-project",
      "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
    );
    secondApiHeaders.append("Authorization", `Bearer ${token}`);
  
    var secondApiOptions = {
      method: "GET",
      headers: secondApiHeaders,
      redirect: "follow",
    };
  
    const newsContainer = document.getElementById("news-container");
  
    fetch(
      "https://api.ibte.me/v4/api/records/cms_events_news",
      secondApiOptions
    )
      .then((response) => response.json())
      .then((result) => {
        const newsList = result.list;
  
        newsList.forEach((newsItem) => {
          const newsBox = document.createElement("a");
          newsBox.href = `/post-view?id=${newsItem.id}`;
          newsBox.className = "news-box d-block swiper-slide";
  
          const newsImage = document.createElement("img");
          newsImage.src = newsItem.cover;
          newsImage.alt = "";
  
          const newsMeta = document.createElement("div");
          newsMeta.className = "d-flex align-items-center justify-content-between";
  
          const newsDate = document.createElement("h6");

    
          const formattedDate = moment(newsItem.published_at).format(
            "DD MMM YYYY"
          );
          newsDate.textContent = newsItem.author +  " " + '•' + " " + formattedDate;
  
          const newsArrowIcon = document.createElement("svg");
          newsArrowIcon.setAttribute("width", "24");
          newsArrowIcon.setAttribute("height", "24");
          newsArrowIcon.setAttribute("viewBox", "0 0 24 24");
          newsArrowIcon.setAttribute("fill", "none");
          newsArrowIcon.setAttribute("xmlns", "http://www.w3.org/2000/svg");
  
          const arrowPath = document.createElement("path");
          arrowPath.setAttribute("d", "M7 17L17 7M17 7H7M17 7V17");
          arrowPath.setAttribute("stroke", "black");
          arrowPath.setAttribute("stroke-width", "2");
          arrowPath.setAttribute("stroke-linecap", "round");
          arrowPath.setAttribute("stroke-linejoin", "round");
  
          newsArrowIcon.appendChild(arrowPath);
          newsMeta.appendChild(newsDate);
          newsMeta.appendChild(newsArrowIcon);
  
          const newsTitle = document.createElement("h4");
          newsTitle.textContent = newsItem.title;
  
          const newsSummary = document.createElement("p");
          newsSummary.innerHTML = newsItem.summary;
  
          const newsTagHolder = document.createElement("div");
          newsTagHolder.className = "news-tag-holder";
  
          const tags = newsItem.tags.split(",");
          tags.forEach((tag) => {
            const tagElement = document.createElement("span");
            tagElement.textContent = tag.trim();
            newsTagHolder.appendChild(tagElement);
          });
  
          newsBox.appendChild(newsImage);
          newsBox.appendChild(newsMeta);
          newsBox.appendChild(newsTitle);
          newsBox.appendChild(newsSummary);
          newsBox.appendChild(newsTagHolder);
  
          newsContainer.appendChild(newsBox);
        });
      })
      .catch((error) => console.log("error", error));
  }
  
  $(window).on("load", function () {
    getNewsItems();
  });
  