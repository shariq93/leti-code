function fetchPartnerCountry() {
  var myHeaders = new Headers();
  myHeaders.append(
    "x-project",
    "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
  );

  var requestOptions = {
    method: "GET",
    headers: myHeaders,
    redirect: "follow",
  };

  const token = localStorage.getItem("anonymous_token");
  var secondApiHeaders = new Headers();
  secondApiHeaders.append(
    "x-project",
    "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
  );
  secondApiHeaders.append("Authorization", `Bearer ${token}`);

  var secondApiOptions = {
    method: "GET",
    headers: secondApiHeaders,
    redirect: "follow",
  };

  fetch(
    "https://api.ibte.me/v4/api/records/cms_partners",
    secondApiOptions
  )
    .then((response) => response.json())
    .then((result) => {
      const invertedList = result.list.reverse();

      const modalBody = document.querySelector(".partner_country");
      modalBody.innerHTML = "";

      const flagList = document.createElement("ul");

      invertedList.forEach((countryData) => {
        const listItem = document.createElement("li");

        const flagImage = document.createElement("img");
        flagImage.src = countryData.logo;
        flagImage.alt = "";
        listItem.appendChild(flagImage);

        const countryName = document.createTextNode(
          ` ${countryData.country}`
        );
        listItem.appendChild(countryName);

        flagList.appendChild(listItem);
      });

  

      const viewAllLink = document.createElement("a");
      viewAllLink.className = "site-btn text-center";
      viewAllLink.dataset.toggle = "modal";
      viewAllLink.dataset.target = "#partner-modal";
      viewAllLink.href = "#";
      viewAllLink.textContent = `View all Partners (${invertedList.length})`;

      const textCenterDiv = document.createElement("div");
      textCenterDiv.className = "text-center";
      textCenterDiv.appendChild(viewAllLink);

   
      modalBody.appendChild(flagList);
      modalBody.appendChild(textCenterDiv);
    })
    .catch((error) => console.log("error", error));
}
  
  $(window).on("load", function () {
    fetchPartnerCountry();
  });
  