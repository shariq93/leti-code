function getFees() {
    var myHeaders = new Headers();
    myHeaders.append(
      "x-project",
      "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
    );

    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };


    const token = localStorage.getItem("anonymous_token");
    var secondApiHeaders = new Headers();
    secondApiHeaders.append(
      "x-project",
      "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
    );
    secondApiHeaders.append("Authorization", `Bearer ${token}`);

    var secondApiOptions = {
      method: "GET",
      headers: secondApiHeaders,
      redirect: "follow",
    };
    fetch(
      "https://api.ibte.me/v4/api/records/cms_fees",
      secondApiOptions
    )
      .then((response) => response.json())
      .then((result) => {
        
        const contentMap = {};
        result.list.forEach((item) => {
          contentMap[item.content_key] = item.content_value;
        });

        const tableBody = document.getElementById("tableBody");
        tableBody.innerHTML = "";
        const rows = [
          ["tr1_td1", "tr1_td2", "tr1_td3"],
          ["tr2_td1", "tr2_td2", "tr2_td3"],
          ["tr3_td1", "tr3_td2", "tr3_td3"],
          ["tr4_td1", "tr4_td2", "tr4_td3"],
        ];
        rows.forEach((row) => {
          const tr = document.createElement("tr");
          row.forEach((contentKey) => {
            const td = document.createElement("td");
            if (contentMap[contentKey].includes("<br/>")) {
              td.innerHTML = contentMap[contentKey];
            } else {
              td.textContent = contentMap[contentKey];
            }
            tr.appendChild(td);
          });
          tableBody.appendChild(tr);
        });

        const th1 = document.getElementById("trth1");
        th1.innerHTML = `<img class="mr-2" src="assets/images/level-bar.svg" alt="" />${contentMap["trth1"]}`;
  
        const th2 = document.getElementById("trth2");
        th2.innerHTML = `<img class="mr-2" src="assets/images/calendar-icon.svg" alt="" />${contentMap["trth2"]}`;

        const th3 = document.getElementById("trth3");
        th3.innerHTML = `<img class="mr-2" src="assets/images/divide-icon.svg" alt="" />${contentMap["trth3"]}`;
      })
      .catch((error) => console.log("error", error));


  }

  $(window).on("load", function () {
    getFees();
});