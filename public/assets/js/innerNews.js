let pageNumber = 1;
function getNewsItems(newPageNumber) {

  pageNumber = newPageNumber;
  loader.style.display = "flex";
 
  const itemsPerPage = 5; // Number of news items per page
  const startIndex = (pageNumber - 1) * itemsPerPage;
    var myHeaders = new Headers();
    myHeaders.append(
      "x-project",
      "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
    );
  
    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };
  
    const token = localStorage.getItem("anonymous_token");
    var secondApiHeaders = new Headers();
    secondApiHeaders.append(
      "x-project",
      "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
    );
    secondApiHeaders.append("Authorization", `Bearer ${token}`);
  
    var secondApiOptions = {
      method: "GET",
      headers: secondApiHeaders,
      redirect: "follow",
    };
  
    // const newsContainer = document.getElementById("news-container");
    const newsContainer = document.getElementById("news-container");
    newsContainer.innerHTML = ""; 
  
    
  
    fetch(
      "https://api.ibte.me/v4/api/records/cms_events_news",
      secondApiOptions
    )
      .then((response) => response.json())
      .then((result) => {
        const newsList = result.list.slice(startIndex, startIndex + itemsPerPage);
  
        newsList.forEach((newsItem) => {
          const newsBox = document.createElement("a");
          newsBox.href = `/post-view?id=${newsItem.id}`;
          newsBox.className = "news-box d-block col-md-6";
  
          const newsImage = document.createElement("img");
          newsImage.src = newsItem.cover;
          newsImage.alt = "";
  
          const newsMeta = document.createElement("div");
          newsMeta.className = "d-flex align-items-center justify-content-between";
  
          const newsDate = document.createElement("h6");

    
          const formattedDate = moment(newsItem.published_at).format(
            "DD MMM YYYY"
          );
          newsDate.textContent = newsItem.author +  " " + '•' + " " + formattedDate;
  
          const newsArrowIcon = document.createElement("svg");
          newsArrowIcon.setAttribute("width", "24");
          newsArrowIcon.setAttribute("height", "24");
          newsArrowIcon.setAttribute("viewBox", "0 0 24 24");
          newsArrowIcon.setAttribute("fill", "none");
          newsArrowIcon.setAttribute("xmlns", "http://www.w3.org/2000/svg");
  
          const arrowPath = document.createElement("path");
          arrowPath.setAttribute("d", "M7 17L17 7M17 7H7M17 7V17");
          arrowPath.setAttribute("stroke", "black");
          arrowPath.setAttribute("stroke-width", "2");
          arrowPath.setAttribute("stroke-linecap", "round");
          arrowPath.setAttribute("stroke-linejoin", "round");
  
          newsArrowIcon.appendChild(arrowPath);
          newsMeta.appendChild(newsDate);
          newsMeta.appendChild(newsArrowIcon);
  
          const newsTitle = document.createElement("h4");
          newsTitle.textContent = newsItem.title;
  
          const newsSummary = document.createElement("p");
          newsSummary.innerHTML = newsItem.summary;
  
          const newsTagHolder = document.createElement("div");
          newsTagHolder.className = "news-tag-holder";
  
          const tags = newsItem.tags.split(",");
          tags.forEach((tag) => {
            const tagElement = document.createElement("span");
            tagElement.textContent = tag.trim();
            newsTagHolder.appendChild(tagElement);
          });
  
          newsBox.appendChild(newsImage);
          newsBox.appendChild(newsMeta);
          newsBox.appendChild(newsTitle);
          newsBox.appendChild(newsSummary);
          newsBox.appendChild(newsTagHolder);
       
          newsContainer.appendChild(newsBox);
        });

        const totalPages = Math.ceil(result.list.length / itemsPerPage);
        const paginationContainer = document.createElement("div");
        paginationContainer.className = "pagination-container";
  
        // Previous button
        const prevButton = document.createElement("button");
  prevButton.textContent = "Previous";
  prevButton.className = "news_Prev";
  prevButton.disabled = pageNumber === 1;
  prevButton.addEventListener("click", () => {
    getNewsItems(pageNumber - 1);
    window.scrollTo({ top: 0, behavior: 'smooth' });
  });
  
        // Next button
        const nextButton = document.createElement("button");
  nextButton.textContent = "Next";
  nextButton.className = "news_next";
  nextButton.disabled = pageNumber === totalPages;
  nextButton.addEventListener("click", () => {
    getNewsItems(pageNumber + 1);
    window.scrollTo({ top: 0, });
  });
  
        // Page numbers
        const pageNumbers = Array.from({ length: totalPages }, (_, index) => index + 1);
  pageNumbers.forEach((pageNum) => {
    const pageButton = document.createElement("button");
    pageButton.textContent = pageNum;
    pageButton.className = "pageNumber";
    pageButton.disabled = pageNum === pageNumber;
    pageButton.addEventListener("click", () => {
      getNewsItems(pageNum);
      window.scrollTo({ top: 0,  });
    });
    paginationContainer.appendChild(pageButton);
  });

  paginationContainer.appendChild(prevButton);
  paginationContainer.appendChild(nextButton);
  
        paginationContainer.appendChild(prevButton);
        paginationContainer.appendChild(nextButton);
  
        // newsContainer.appendChild(paginationContainer);
        
        const existingPagination = document.querySelector(".pagination-container");
        if (existingPagination) {
          existingPagination.remove();
        }
        newsContainer.insertAdjacentElement("afterend", paginationContainer);
        loader.style.display = "none";
      
      })
      .catch((error) => console.log("error", error));
  }
  
  $(window).on("load", function () {
    getNewsItems(1);
  });
  