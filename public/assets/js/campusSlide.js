function campusSLide() {
    var myHeaders = new Headers();
    myHeaders.append(
      "x-project",
      "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
    );
  
    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };
  
    const token = localStorage.getItem("anonymous_token");
    const projectId = "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg==";
  
    var secondApiHeaders = new Headers();
    secondApiHeaders.append("x-project", projectId);
    secondApiHeaders.append("Authorization", `Bearer ${token}`);
  
    var secondApiOptions = {
      method: "GET",
      headers: secondApiHeaders,
      redirect: "follow",
    };
  
    fetch(
      "https://api.ibte.me/v4/api/records/cms_campus",
      secondApiOptions
    )
      .then((response) => response.json())
      .then((result) => {
        // Render the team members
        renderTeamMembers(result.list, token);
        renderSLider();
      })
      .catch((error) => console.log("error", error));
  }
  
  function renderTeamMembers(teamMembers, token) {
    teamMembers.sort((a, b) => a.order_ - b.order_);

    const teamCardHolder = document.querySelector(
      ".campus-slide .swiper-wrapper"
    );
  
    const memberCardTemplate = `
    <div class="swiper-slide">
              <div class="row">
                <div class="col-md-6 mt-1 mb-1">
                  <div
                    class="sec-title text-left wow fadeInUp"
                    data-wow-delay="0.2s"
                  >
                    <h5>{tagline}</h5>
                    <h2>{title}</h2>
                    <p>
                     {description}
                    </p>
                  </div>
                </div>
                <div class="col-md-6 mt-1 mb-1">
                  <img
                    class="square-img"
                    src="{image}"
                    alt=""
                  />
                </div>
              </div>
            </div>
      `;
  
    let membersHTML = "";
  
    teamMembers.forEach((member) => {
      const memberHTML = memberCardTemplate
        .replace("{tagline}", member.tagline)
        .replace("{title}", member.title)
        .replace("{image}", member.image)
        .replace("{description}", member.description)
  
      membersHTML += memberHTML;
    });
  
    teamCardHolder.innerHTML = membersHTML;
  }
  
  $(window).on("load", function () {
    campusSLide();
  });
  
  function renderSLider() {
    var swiper = new Swiper(".campus-slide", {
        slidesPerView: 1,
        loop: false,
        spaceBetween: 10,
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
      });
  }
  