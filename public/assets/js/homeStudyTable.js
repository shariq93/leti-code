function studyTableInfo() {
  var myHeaders = new Headers();
  myHeaders.append(
    "x-project",
    "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
  );

  var requestOptions = {
    method: "GET",
    headers: myHeaders,
    redirect: "follow",
  };

  const token = localStorage.getItem("anonymous_token");

  var secondApiHeaders = new Headers();
  secondApiHeaders.append(
    "x-project",
    "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
  );
  secondApiHeaders.append("Authorization", `Bearer ${token}`);

  var secondApiOptions = {
    method: "GET",
    headers: secondApiHeaders,
    redirect: "follow",
  };
  fetch(
    "https://api.ibte.me/v4/api/records/cms_features",
    secondApiOptions
  )
    .then((response) => response.json())
    .then((result) => {
      const contentMap = {};
      result.list.forEach((item) => {
        contentMap[item.content_key] = item.content_value;
      });

      const tableBody = document.getElementById("tableBody");
      tableBody.innerHTML = "";
      const rows = [
        ["tr2td1", "tr2td2", "tr2td3"],
        ["tr4td1", "tr4td2", "tr4td3"],
        ["tr7td1", "tr7td2", "tr7td3"],
        ["tr11td1", "tr11td2", "tr11td3"],
        ["tr17td1", "tr17td2", "tr17td3"],
      ];
      rows.forEach((row) => {
        const tr = document.createElement("tr");
        row.forEach((contentKey) => {
          const td = document.createElement("td");
          if (contentMap[contentKey].includes("span")) {
            td.innerHTML = contentMap[contentKey];
          } else {
            td.textContent = contentMap[contentKey];
          }
          tr.appendChild(td);
        });
        tableBody.appendChild(tr);
      });

      const th1 = document.getElementById("globally_tr_th1");
      th1.innerHTML = `${contentMap["tr1th1"]}`;

      const th2 = document.getElementById("globally_tr_th2");
      th2.innerHTML = `${contentMap["tr1th2"]}`;
    })
    .catch((error) => console.log("error", error));
}

$(window).on("load", function () {
  studyTableInfo();
});

studyTableInfo();
