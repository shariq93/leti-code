function fetchPartners() {
    var myHeaders = new Headers();
    myHeaders.append(
      "x-project",
      "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
    );
  
    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };
  
   
    const token = localStorage.getItem("anonymous_token");
    var secondApiHeaders = new Headers();
    secondApiHeaders.append(
      "x-project",
      "eWFzZW46cTBudmJxYTljdm9leG8xbHNqY3VxcGl3dGtjdmRkZg=="
    );
    secondApiHeaders.append("Authorization", `Bearer ${token}`);

    var secondApiOptions = {
      method: "GET",
      headers: secondApiHeaders,
      redirect: "follow",
    };

    fetch(
      "https://api.ibte.me/v4/api/records/cms_partners",
      secondApiOptions
    )
      .then((response) => response.json())
      .then((result) => {
        const invertedList = result.list.reverse();

        const modalBody = document.querySelector(".partners-modal");
        modalBody.innerHTML = "";

        invertedList.forEach((countryData) => {
          const countryBox = document.createElement("div");
          countryBox.classList.add("modal-partner-box");

          const heading = document.createElement("h4");
          const countryImage = document.createElement("img");
          countryImage.src = countryData.logo;
          heading.appendChild(countryImage);

          const countryName = document.createTextNode(
            ` ${countryData.country}`
          );
          heading.appendChild(countryName);
          countryBox.appendChild(heading);

          const descriptionList = document.createElement("ul");
          countryData.description.split("<li>").forEach((item, index) => {
            if (index > 0) {
              const listItem = item.split("</li>")[0];
              if (listItem) {
                const listItemElement = document.createElement("li");
                listItemElement.textContent = listItem;
                descriptionList.appendChild(listItemElement);
              }
            }
          });

          countryBox.appendChild(descriptionList);
          modalBody.appendChild(countryBox);
        });
      })
      .catch((error) => console.log("error", error));


  }
  
  $(window).on("load", function () {
      fetchPartners();
  });