
require('dotenv').config()
const express = require('express');
const path = require('path');
const cors = require('cors');
const session = require('express-session');
const { renderPage } = require('./CmsService');
const { withSSR } = require('./ReactSSRService');
const excludedRoutes = require('./excludedRoutes');
const { getBlogList, getSingleBlogAttributes, getSimilarBlogs } = require('./services/blog_rendering');
const fs = require('fs');
let app = express();

app.use(express.json());
app.use(
  express.urlencoded({
    extended: false,
  })
);
app.use(cors());

app.use(
  session({
    secret: 'G1VaSLfZfxdRlPN4qls8X6Kqnkdr5DSl',
    resave: false,
    saveUninitialized: true,
  })
);

if (process.env.IS_HTTPS === 'true') {
  app.set('trust proxy', 1);
  session.cookie.secure = true;
  session.cookie.sameSite = 'strict';
}




// app.get('/pages', async function (req, res, next) {
//   /**
//    * TODO:
//    * 1. Make folder of static html files
//    * 2. If CMS data in session, dont call API
//    * 3. Call API, save into session
//    * 4. replace sections in html template with {{{}}}
//    * 5. return template
//    * 6. If is_cache=false query parameter passed in, skip session
//    */
//   const pageType = req.query.hasOwnProperty('page') ? req.query.page : 'index';
//   const clearCache = req.query.clear_cache === 'true';
//   const filePath = `./pages/${pageType}.html`;
//   const text = await renderPage(req, pageType, clearCache, filePath);
//   //TODO: can cache page too if we wanted
//   return res.send(text);
// });

app.all('/', async function (req, res, next) {
  const page = 'index';
  const clearCache = req.query.clear_cache === 'true';
  const filePath = `./pages/${page}.html`;
  const text = await renderPage(req, page, clearCache, filePath);
  return res.send(text);
});



app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'frontend', 'dist')));

app.get('/pages/blog-details/:id', async function (req, res, next) {
  try {
    let text = await fs.readFileSync('./pages/blog-details.html', "utf8");
    let blog = await getSingleBlogAttributes(req.params.id);
    let similarBlogs = await getSimilarBlogs(req.params.id);
    console.log(blog);
    console.log(similarBlogs);
    text = text.replace(`{{{SIMILAR_BLOGS}}}`, similarBlogs)
    for (let key in blog) {
      let value = blog[key];
      const TAG = new RegExp('{{{' + key + '}}}', 'g')
      text = text.replace(TAG, value);
    }
    return res.send(text);
  }
  catch (err) {
    console.log(err);
    return res.send("Something Went Wrong.");
  }
});

app.get('/pages/blogs', async function (req, res, next) {
  try {
    let text = await fs.readFileSync('./pages/blog.html', "utf8");
    let blogs = await getBlogList(req.query);
    console.log(blogs);
    text = text.replace('{{{BLOG_LIST}}}', blogs);
    return res.send(text);
  }
  catch (err) {
    return res.send(text.replace('{{{BLOG_LIST}}}', "Something Went Wrong."));
  }

});





app.get('/:page?', async function (req, res, next) {
  const page = req.params.page ? req.params.page : 'index';
  const clearCache = req.query.clear_cache === 'true';
  const filePath = `./pages/${page}.html`;
  const text = await renderPage(req, page, clearCache, filePath);
  return res.send(text);
});

// All blogs
// Filtered blogs
// Single Blog

app.get('/favicon.ico', async function (req, res, next) {
  return res.sendStatus(200);
});



app.use(withSSR({ metadata: '', excludedRoutes: excludedRoutes }));

app.get('/*', (req, res) => {
  return res.status(404).send('Not Found');
});

app.use((err, req, res, next) => {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  return res.status(err.status || 500).json({
    message: err.message,
  });
});

module.exports = app;
