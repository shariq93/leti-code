
// Add routes to exclude from React SSR
module.exports = [
  '/',
  '*',
  '/favicon.ico',
  '/pages/index',
  '/pages/blogs'
]
